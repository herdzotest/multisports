<!DOCTYPE html>
<html lang="en">

<head>
    <title>Multisport | Boxing</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<style>
    main {
        padding-top: 100px;
    }

    .table-form {
        width: 70px;
        padding-right: 5px;
    }

    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
        padding: 15px 5px;
    }
</style>


<body>
    <header>

    </header>
    <main>
        @if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="container">
            <h3>
                Event Name : 2021 World Boxing Championship
            </h3>
            <h3>
                Game Name : Platyer Name 1 vs Player Name 2
            </h3>
            <br><br>

            {!! Form::open(['url' => 'admin/bo/scorecard/update/'.$gameId, 'class' => 'submit_form']) !!}
            <div class="row">
                <div class="col-md-10">

                </div>
                <div class="col-md-1">
                    <input class="form-control" type='button' value='+' id='addButton'>&nbsp;


                </div>
                <div class="col-md-1">
                    <input class="form-control" type='button' value='-' id='removeButton'>

                </div>
                <hr>
                <div class="col-md-12">

                    <table class="table playerData">
                        <thead>
                            <tr>
                                <th>Judges</th>
                                <th colspan="2" style="text-align: center;">Judge1</th>
                                <th colspan="2" style="text-align: center;">Judge2</th>
                                <th colspan="2" style="text-align: center;">Judge3</th>
                                <th colspan="2" style="text-align: center;">Judge4</th>
                                <th colspan="2" style="text-align: center;">Judge5</th>
                                <th colspan="2" style="text-align: center;">Knockout</th>
                                <td>

                                </td>
                            </tr>
                            <tr>
                                <td>Player Name</td>
                                <td style="text-align: center;">Point</td>
                                <td style="text-align: center;">Deductions</td>
                                <td style="text-align: center;">Point</td>
                                <td style="text-align: center;">Deductions</td>
                                <td style="text-align: center;">Point</td>
                                <td style="text-align: center;">Deductions</td>
                                <td style="text-align: center;">Point</td>
                                <td style="text-align: center;">Deductions</td>
                                <td style="text-align: center;">Point</td>
                                <td style="text-align: center;">Deductions</td>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>Player Name 1</td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input type="checkbox" class="knockout" name="" id="" data-round="1"></td>
                            </tr>
                            <tr>
                                <td>Player Name 2</td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input class="form-control table-form" min="0" name="" required="" type="number" value="&lt;?php echo $result['punch']; ?&gt;" /></td>
                                <td><input type="checkbox" class="knockout" name="" id="" data-round="1"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <br><br>
            </div>
            <br><br>
            <h3>Match Summary</h3>
            <!-- <form> -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Player 1 Total Score:</label>
                        <input type="text" value="<?php echo isset($gamesArray[0]) ?  $gamesArray[0]->score1 : 'Total Score of Player 1'; ?>" class="form-control" id="total_score" name="total_score" readonly>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Player 2 Total Score:</label>
                        <input type="text" value="<?php echo isset($gamesArray[0]) ?  $gamesArray[0]->score1 : 'Total Score of Player 2'; ?>" class="form-control" id="total_score2" name="total_score2" readonly>
                    </div>
                </div>
            </div>
            <?php
            $coachId = $coachName = $coach2Id = $coach2Name = '';
            if (count($gameteamArray) > 0) {
                $coachName = $gameteamArray['coach']['name'];
                $coachId = $gameteamArray['coach']['id'];
                $coach2Name = $gameteamArray['assistantcoach']['name'];
                $coach2Id = $gameteamArray['assistantcoach']['id'];
            }
            ?>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Player 1 Coach:</label>
                        <input type="text" placeholder="Enter the name of the player 1 Coach" class="form-control" id="" value="<?php echo $coachName; ?>" name="coach">
                        <input type="hidden" name="coachId" value="<?php echo $coachId; ?>">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Player 2 Coach:</label>
                        <input type="text" class="form-control" placeholder="Enter the name of the player 2 Coach" value="<?php echo $coach2Name; ?>" id="" name="coach2">
                        <input type="hidden" name="coach2Id" value="<?php echo $coach2Id; ?>">
                    </div>
                </div>
            </div>

            <?php
            $refereeId = $refereeName = $judge1Name = $judge1Id = $judge2Name = $judge2Id = '';
            if (count($gameArray) > 0) {
                $refereeName = $gameArray['referee']['name'];
                $refereeId = $gameArray['referee']['id'];
                $judge1Name = $gameArray['judge_1']['name'];
                $judge1Id = $gameArray['judge_1']['id'];
                $judge2Name = $gameArray['judge_2']['name'];
                $judge2Id = $gameArray['judge_2']['id'];
            }
            ?>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Match refree :</label>
                        <input type="text" placeholder="Enter the name of Refree for the bout" class="form-control" value="<?php echo $refereeName; ?>" id="" name="refree">
                        <input type="hidden" name="refreeId" value="<?php echo $refereeId; ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Judge 1:</label>
                        <input type="text" class="form-control" placeholder="Enter the name of Judge1" id="" value="<?php echo $judge1Name; ?>" name="judge1">
                        <input type="hidden" name="judge1Id" value="<?php echo $judge1Id; ?>">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Judge 2:</label>
                        <input type="text" placeholder="Enter the name of Judge2" class="form-control" id="" value="<?php echo $judge2Name; ?>" name="judge2">
                        <input type="hidden" name="judge2Id" value="<?php echo $judge2Id; ?>">
                    </div>
                </div>
            </div>
            <input type="hidden" id="playerDatas" name="playerDatas" class="playerDatas" value="">
            <input type="hidden" id="gameId" name="gameIdVal" value="<?php echo $gameId; ?>">
            <button type="submit" id="button_submit" class="btn btn-default btn-primary">Submit Scoreboard</button>
            <!-- </form> -->

            </form>

        </div>
        <input type="hidden" id="submitted" value="0">
        <?php

        ?>

    </main>
</body>
<script type="text/javascript">
    jQuery(function() {

        var round = $(".player_det").length / 2;
        if (round <= 1) {
            $("#removeButton").addClass('hide');
        }
        if ($(".knockout").is(":checked")) {
            $("#addButton").addClass("hide");
        }
        var currentLevel = $(".playerData").find(".knockout:checked").attr('data-round');
        // disable previous knockout checkbox
        var prevLevel = parseInt(currentLevel) - 1;
        var prevLevel2 = prevLevel - 1;
        $(".playerData").find("[data-round='" + prevLevel + "']").prop('disabled', true);
        $(".playerData").find("[data-round='" + prevLevel2 + "']").prop('disabled', true);
        $(".playerData").find("[data-round='" + currentLevel + "']:not(:checked)").prop('disabled', true);

        // add button
        jQuery('#addButton').click(function(event) {
            if (round <= 2) {

                var currentLevel = $("table.playerData tbody tr:last-child").find('input.knockout').attr('data-round');
                var nextLevel = parseInt(currentLevel) + 1;
                var html = ' <tr class="form-group player_det"> <td> <input type="text" class="form-control" name="" id="" readonly value="<?php echo $player[0]->full_name; ?>"> <input type="hidden" name="" class="playerId" value="<?php echo $player[0]->player_id; ?>"> </td> <td> <input type="number" min="0" class="form-control punch" name="" id="" value="" required> </td> <td> <input type="number" min="0" class="form-control k1" name="" id="" value="" required> </td> <td> <input type="number" min="0" class="form-control k2" name="" id="" value="" required> </td> <td> <input type="number" min="0" class="form-control k3" name="" id="" value="" required> </td> <td> <input type="number" min="0" class="form-control k4" name="" id="" value="" required> </td> <td> <input type="number" min="0" class="form-control deduction" name="" id="" value="" required> </td> <td> <input data-round="' + nextLevel + '" type="checkbox" class="knockout" name="" id=""> </td> </tr> <tr class="form-group player_det"> <td> <input type="text" class="form-control" name="" id="" readonly value="<?php echo $player[1]->full_name; ?>"> <input type="hidden" name="" class="playerId" value="<?php echo $player[1]->player_id; ?>"> </td> <td> <input type="number" min="0" class="form-control punch" name="" id="" value="" required> </td> <td> <input type="number" min="0" class="form-control k1" name="" id="" value="" required> </td> <td> <input type="number" min="0" class="form-control k2" name="" id="" value="" required> </td> <td> <input type="number" min="0" class="form-control k3" name="" id="" value="" required> </td> <td> <input type="number" min="0" class="form-control k4" name="" id="" value="" required> </td>  <td> <input type="number" min="0" class="form-control deduction" name="" id="" value="" required> </td> <td> <input type="checkbox" data-round="' + nextLevel + '" class="knockout" name="" id=""> </td> </tr>';
                jQuery('table.table tbody').append(html);
                if (round == 2) {
                    $("#addButton").addClass('hide');
                }
                $("#removeButton").removeClass('hide');
                round++;
            }
        });
        // knockout click
        $(".playerData").on("click", '.knockout', function() {
            if ($(this).is(':checked')) {
                $("#addButton").addClass("hide");
                var currentLevel = $(this).attr('data-round');
                round = currentLevel;
                var nextLevel = parseInt(currentLevel) + 1;
                var nextLevel2 = nextLevel + 1;
                $(".playerData").find("[data-round='" + nextLevel + "']").parent().parent().remove();
                $(".playerData").find("[data-round='" + nextLevel2 + "']").parent().parent().remove();

                // disable previous knockout checkbox
                var prevLevel = parseInt(currentLevel) - 1;
                var prevLevel2 = prevLevel - 1;
                $(".playerData").find("[data-round='" + prevLevel + "']").prop('disabled', true);
                $(".playerData").find("[data-round='" + prevLevel2 + "']").prop('disabled', true);
                // disable current level unchecked
                $(".playerData").find("[data-round='" + currentLevel + "']:not(:checked)").prop('disabled', true);

                if (currentLevel == '1') {
                    $("#removeButton").addClass("hide");
                }
            } else if (!$(".playerData .knockout").is(":checked")) {
                $("#addButton").removeClass("hide");
                $(".playerData .knockout").prop('disabled', false);
            }
        });
        //remove button
        $("#removeButton").click(function(event) {
            $("#addButton").removeClass('hide');
            $("table.playerData tbody tr:last-child").remove();
            $("table.playerData tbody tr:last-child").remove();
            round--;
            if (round == 1) {
                $("#removeButton").addClass('hide');
            }
            $(".playerData .knockout").prop('disabled', false);
        });
        // submit form
        $(".submit_form").submit(function(event) {

            if ($("#submitted").val() == '0') {
                $("#submitted").val(1);
                event.preventDefault();
                var gameId = $("#gameId").val();
                var playerData = {};
                var score1 = score2 = 0;
                var player1 = 0;
                var player2 = 'undefined';
                $(".player_det").each(function(i, val) {

                    var playerId = $(this).find('input.playerId').val();
                    if (player1 == '' && player2 != playerId) {
                        player1 = playerId;
                    } else if (player2 == 'undefined' && player1 != playerId) {
                        player2 = playerId;
                    }
                    var punch = $(this).find('input.punch').val();
                    var k1 = $(this).find('input.k1').val();
                    var k2 = $(this).find('input.k2').val();
                    var k3 = $(this).find('input.k3').val();
                    var k4 = $(this).find('input.k4').val();
                    var deduction = $(this).find('input.deduction').val();
                    var knockout = ($(this).find('input.knockout').is(':checked')) ? 1 : 0;

                    if (playerId == player1) {
                        score1 = (parseInt(score1) + parseInt(punch) * 1 + parseInt(k1) * 1 + parseInt(k2) * 2 + parseInt(k3) * 3 + parseInt(k4) * 4) - (parseInt(deduction) * 1);
                    } else if (playerId == player2) {
                        score2 = (parseInt(score2) + parseInt(punch) * 1 + parseInt(k1) * 1 + parseInt(k2) * 2 + parseInt(k3) * 3 + parseInt(k4) * 4) - (parseInt(deduction) * 1);
                    }
                    playerData[i] = {
                        'game_id': gameId,
                        'player_id': playerId,
                        'punch': punch,
                        'k1': k1,
                        'k2': k2,
                        'k3': k3,
                        'k4': k4,
                        'deduction': deduction,
                        'knockout': knockout
                    };
                });
                $("#playerDatas").val(JSON.stringify(playerData));
                $("#total_score").val(score1);
                $("#total_score2").val(score2);
                $(".submit_form").submit();
            } else {
                return true;
            }
        });
    })
</script>