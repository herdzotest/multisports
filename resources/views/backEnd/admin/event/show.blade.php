@extends('../backLayout.pages')
@section('title')
Event
@stop

@section('content')

    <h1>Event</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Season</th><th>Country</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $event->event_id }}</td> <td> {{ $event->name }} </td><td> {{ $event->season }} </td><td> {{ $event->country['name'] }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection