@extends('../backLayout.pages')
@section('breadcrumb')

   <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">View Event</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">Event</a>
                            </li>
                            <li class="breadcrumb-item active">View</li>
                        </ol>
                    </div>
              
                </div>

@endsection
@section('content')
@if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
 @if(Session::has('message'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
@endif
<?php $table_prefix= \Session::get('table_prefix') ?>




                <div class="card">
                            <div class="card-body">
                                <a href="{{ url('admin/'.$table_prefix.'/event/create') }}" class="btn btn-primary pull-right btn-sm">Add Events</a>
                                <!-- <h4 class="card-title">Data Table</h4> -->
                                <!-- <h6 class="card-subtitle">Data table example</h6> -->
                                <div class="table-responsive m-t-40">
                                    <table id="tbladmin" class="table table-bordered table-striped">
                                        <thead>
                                          
                    <th>ID</th><th>Name</th><th>Season</th><th>Country</th><th>Category</th><th width="100">Date</th><th>Actions</th>
                
                                        </thead>
                                        <tbody>
                                        

               @foreach($event as $item)
                <tr>
                    <td>{{ $item->event_id }}</td>
                    <td><a href="{{ url('admin/'.$table_prefix.'/event', $item->event_id) }}">{{ $item->name }}</a></td><td>{{ $item->season }}</td><td>{{ $item->country['name'] }}</td><td>{{ $item->category }}</td><td>{{ date('d-m-Y', strtotime($item->start_date)) }}</td>
                    <td class="jsgrid-cell jsgrid-control-field jsgrid-align-center">
                    <a href="{{ url('admin/'.$table_prefix.'/fixture/edit' .'/'. $item->event_id) }}"> <button class="jsgrid-button jsgrid-edit-button editFixture" type="button" title="Edit">Edit Fixture</button></a> 
                                        <a href="{{ url('admin/'.$table_prefix.'/event/' . $item->event_id. '/edit') }}"> <button class="jsgrid-button jsgrid-edit-button edit" type="button" title="Edit"><span class="fas fa-pencil-alt" aria-hidden="true"></span></button></a> 

                                        {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['admin/'.$table_prefix.'/event', $item->event_id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        <button class="jsgrid-button jsgrid-edit-button edit" type="submit" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></button>
                                        {!! Form::close() !!}

                                        </td>
                </tr>
            @endforeach
                                    
                                      
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
    $('#tbladmin').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
    $(".editFixture").click(function(){
            return confirm("Do you want to edit this ?");
        });
    $(".edit").click(function(){
            return confirm("Do you want to edit this ?");
        });
        $(".delete").click(function(){
            return confirm("Do you want to delete this ?");
        });
});
</script>
@endsection