@extends('../backLayout.pages')
@section('breadcrumb')

   <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Edit Event</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">Event</a>
                            </li>
                            <li class="breadcrumb-item active">Edit</li>
                        </ol>
                    </div>
              
                </div>

@endsection
@section('content')
@if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
 @if(Session::has('message'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
@endif
<?php $table_prefix= \Session::get('table_prefix') ?>
    {!! Form::model($event, [
        'method' => 'PATCH',
        'url' => ['admin/'.$table_prefix.'/event', $event->event_id],
        'class' => 'form-horizontal'
    ]) !!}

@if(isset($event))        

        <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Event Details</h4>
                            </div>
                            <div class="card-body">
                                <form action="#">
                                    <div class="form-body">
                                        
                                        <div class="row p-t-20">
                                           
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('full_name') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Name</label>
                                                     {!! Form::text('name', $event->name, ['class' => 'form-control']) !!}
                                                    <small class="form-control-feedback">Enter Name of Event </small> </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('full_name') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Season</label>
                                                     {!! Form::text('season', $event->season, ['class' => 'form-control']) !!}
                                                    <small class="form-control-feedback">Enter Event Season </small> </div>
                                            
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                           <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('country_id') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Country</label>
                                                     <select name= "country_id" id="country_id" class="form-control">
                                                        @foreach($country as $key => $value)
                                                        <?php if($event->country_id==$key){
                                                            $selected="selected";
                                                        }else{
                                                            $selected="";
                                                        }
                                                        ?>
                                                        <option value="<?=$key?>"  <?=$selected?> ><?=$value?></option>
                                                        @endforeach
                                                    </select>
                                                    <small class="form-control-feedback"> </small> </div>
                                            
                                            </div>
                                               <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('region') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Region</label>
                                                      <select class="form-control" id="region" name="region">
                                                        @foreach($region as $key => $value)
                                                        <?php if($event->region==$key){
                                                            $selected="selected";
                                                        }else{
                                                            $selected="";
                                                        }
                                                        ?>
                                                        <option value="<?=$key?>"  <?=$selected?> ><?=$value?></option>
                                                        @endforeach
                                                    </select>
                                                    <small class="form-control-feedback">Enter Region of Event </small> </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                           <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('sub_region') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Sub Region</label>
                                                     <select class="form-control" id="sub_region" name="sub_region">
                                                        @foreach($subregion as $key => $value)
                                                        <?php if($event->sub_region_id==$key){
                                                            $selected="selected";
                                                        }else{
                                                            $selected="";
                                                        }
                                                        ?>
                                                        <option value="<?=$key?>"  <?=$selected?> ><?=$value?></option>
                                                        @endforeach
                                                    </select>
                                                    <small class="form-control-feedback">Enter Sub Region of Event </small> </div>
                                            </div>
                                              <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('location') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Location</label>
                                                     <select class="form-control" id="location" name="location">
                                                        @foreach($location as $key => $value)
                                                        <?php if($event->location==$key){
                                                            $selected="selected";
                                                        }else{
                                                            $selected="";
                                                        }
                                                        ?>
                                                        <option value="<?=$key?>"  <?=$selected?> ><?=$value?></option>
                                                        @endforeach
                                                    </select>
                                                    <small class="form-control-feedback">Enter Sub Region of Event </small> </div>
                                            </div>
                                        </div>

                                        <!--/row-->
                                           <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('generic_name') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Generic Name</label>
                                                    {!! Form::text('generic_name', isset($event->generic_name) ? $event->generic_name : null, ['class' => 'form-control']) !!}
                                                    <small class="form-control-feedback">Enter Generic Name of Event </small> </div>
                                            
                                            </div>
											<div class="col-md-6">
                                                <div class="form-group {{ $errors->has('start_date') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Start Date</label>
                                                    <input name="start_date" id="start_date" type="text" class="form-control" placeholder="yyyy-mm-dd" value="<?=$event->start_date?>">
                                                    <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                    <small class="form-control-feedback">Enter Event Start Date </small> </div>
                                            
                                            </div>
                                        </div>
                                     <?php
                                            if(!(isset($event_picture->picture_id)))
                                            {
                                            $event_picture_disp="";
                                            }else
                                            {
                                                 $event_picture_disp=$event_picture->picture_id;
                                            }
                                            ?>     
                                    <div class="row">
                                         <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('picture_id') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Picture Id</label>
                                                     {!! Form::text('picture_id', $event_picture_disp, ['class' => 'form-control']) !!}
                                                    <small class="form-control-feedback">Enter Picture Id of Event </small> </div>
                                            
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('category') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Category</label>
                                                     <select name= "category" class="form-control">
                                                        
                                                        @foreach($category as $key => $value)
                                                        <?php if($event->category==$value){
                                                            $selected="selected";
                                                        }else{
                                                            $selected="";
                                                        }
                                                        ?>
                                                        <option value="<?=$value?>"  <?=$selected?> ><?=$value?></option>
                                                        @endforeach
                                                    </select>
                                                    <small class="form-control-feedback"> </small> </div>
                                            
                                            </div>
                                    </div>
                                   
                                         
                                 
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success btn-rounded"> <i class="fa fa-check"></i> Save</button>
                                        <input type="reset" value="Reset" class="btn btn-primary btn-rounded" value="Clear">
                                        <a href="{{ url('admin/'.$table_prefix.'/event') }}"><button type="button" class="btn btn-inverse btn-rounded">View Events</button>
                                        <!-- <button type="button" class="btn btn-inverse">Cancel</button> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
@endif
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection
@section('scripts')
<script type="text/javascript">
  $( function() {
    $( ".datepicker" ).datepicker();
  } );
</script>

<script type="text/javascript">
$(document).ready(function(){


var APP_URL = {!! json_encode(url('/')) !!}
    $('#country_id').change(function(){
        
        var country_id = $(this).val();
            $.ajax({
            type: "get",
            url: APP_URL+'/getregion',
            data: {'country_id': country_id},
            success: function( json ) {
             json = $.parseJSON(json); 
             $("#sub_region").empty();
             $("#region").empty();
             $("#location").empty();
             
$.each(json,function(key, value) 
{
    $('#region').prepend('<option value=' + key + '>' + value + '</option>');
});



            }
        });
    });



    $('#region').change(function(){
        
        var region_id = $(this).val();
            $.ajax({
            type: "get",
            url: APP_URL+'/getsubregion',
            data: {'region_id': region_id},
            success: function( json ) {
             json = $.parseJSON(json); 
             $("#sub_region").empty();
             $("#location").empty();
$.each(json,function(key, value) 
{
    $('#sub_region').prepend('<option value=' + key + '>' + value + '</option>');
});
            }
        });
    });
      $('#sub_region').change(function(){
        
        var sub_region_id = $(this).val();
            $.ajax({
            type: "get",
            url: APP_URL+'/getlocation',
            data: {'sub_region_id': sub_region_id},
            success: function( json ) {
             json = $.parseJSON(json); 
             $("#Location").empty();
$.each(json,function(key, value) 
{
    $('#Location').prepend('<option value=' + key + '>' + value + '</option>');
});
            }
        });
    });
    
});


</script>


@endsection