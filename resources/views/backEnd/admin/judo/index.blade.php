<!DOCTYPE html>
<html lang="en">

<head>
    <title>Multisport | Judo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<style>
    main {
        padding-top: 100px;
    }

    .table td {
        width: 50px;
        border: 0 !important;
    }

    .table th:first-child,
    .table td:first-child {
        width: 150px;
    }
</style>

<body>
    <header>

    </header>
    <main>
        @if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="container">
            <h3>
                Event Name : 2020 World Judo Championship
            </h3>
            <h3>
                Game Name : Sagar M Premanathan vs Shahas Siddeeq
            </h3>
            <h3>
                Game Time : 4:00 Minutes
            </h3>
            <br><br>

            {!! Form::open(['url' => 'admin/ju/events/update/'.$event->event_id, 'class' => 'submit_form']) !!}

            <table class="table playerData">
                <thead>
                    <tr>
                        <th>Player Name</th>
                        <th>Scoring Method</th>
                        <th>Technique / Penality</th>
                        <th>Time</th>
                        <th>Score</th>
                        <td>
                            <input type='button' value='+' id='addButton'>&nbsp;
                            <input type='button' value='-' id='removeButton'>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($gamePlayerArray) > 0) {
                        foreach ($gamePlayerArray as $result) {
                    ?>
                            <tr class="form-group player_det" id="TextBoxDiv1">
                                <td>
                                    <select class="form-control playerName" name="" id="sel1" required>
                                        <option required value="">Select</option>
                                        <?php
                                        foreach ($player as $result12) {
                                            echo '<option value="' . $result12->player_id . '" ' . (($result['player'] == $result12->player_id) ? 'selected' : '') . '>' . $result12->full_name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control scoringMethod" name="" id="textbox1">
                                        <option required value="Ippon" <?php ($result['scoring_method'] == 'Ippon') ? 'selected' : ''; ?>>Ippon</option>
                                        <option required value="Wazart" <?php ($result['scoring_method'] == 'Wazart') ? 'selected' : ''; ?>>Wazart</option>
                                        <option required value="Penality" <?php ($result['scoring_method'] == 'Penality') ? 'selected' : ''; ?>>Penality</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control technique" name="" id="textbox1">
                                        <option required value="wt1" <?php ($result['technique'] == 'wt1') ? 'selected' : ''; ?>>Wazart Technique 1</option>
                                        <option required value="wt2" <?php ($result['technique'] == 'wt2') ? 'selected' : ''; ?>>Wazart Technique 11</option>
                                        <option required value="wt3" <?php ($result['technique'] == 'wt3') ? 'selected' : ''; ?>>wazart Technique 1111</option>
                                    </select>
                                </td>
                                <td>
                                    <input required type="text" name="" class="form-control time" value="<?php echo $result['time']; ?>">
                                </td>
                                <td>
                                    <input required type="text" name="" class="form-control score" value="<?php echo $result['score']; ?>">
                                </td>
                            </tr>
                        <?php
                        }
                    } else {
                        ?>
                        <tr class="form-group player_det" id="TextBoxDiv1">
                            <td>
                                <select class="form-control playerName" name="" id="sel1" required>
                                    <option required value="">Select</option>
                                    <?php
                                    foreach ($player as $result) {
                                        echo '<option value="' . $result->player_id . '">' . $result->full_name . '</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                            <td>
                                <select class="form-control scoringMethod" name="" id="textbox1">
                                    <option required value="Ippon">Ippon</option>
                                    <option required value="Wazart">Wazart</option>
                                    <option required value="Penality">Penality</option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control technique" name="" id="textbox1">
                                    <option required value="wt1">Wazart Technique 1</option>
                                    <option required value="wt2">Wazart Technique 11</option>
                                    <option required value="wt3">wazart Technique 1111</option>
                                </select>
                            </td>
                            <td>
                                <input required type="text" name="" class="form-control time">
                            </td>
                            <td>
                                <input required type="text" name="" class="form-control score">
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
            <br><br>
            <h3>Match Summary</h3>
            <!-- <form> -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Player 1 Total Score:</label>
                        <input type="text" class="form-control" id="total_score" name="total_score" readonly>
                        <span>Enter the total score for player 1 for the bout</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Player 2 Total Score:</label>
                        <input type="text" class="form-control" id="total_score2" name="total_score2" readonly>
                        <span>Enter the total score for player 2 for the bout</span>
                    </div>
                </div>
            </div>
            <?php
            $coachId = $coachName = $coach2Id = $coach2Name = '';
            if (count($gameteamArray) > 0) {
                $coachName = $gameteamArray['coach']['name'];
                $coachId = $gameteamArray['coach']['id'];
                $coach2Name = $gameteamArray['assistantcoach']['name'];
                $coach2Id = $gameteamArray['assistantcoach']['id'];
            }
            ?>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Player 1 Coach:</label>
                        <input type="text" class="form-control" id="" value="<?php echo $coachName; ?>" name="coach">
                        <input type="hidden" name="coachId" value="<?php echo $coachId; ?>">
                        <span>Enter the name of the player 1 Coach</span>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Player 2 Coach:</label>
                        <input type="text" class="form-control" value="<?php echo $coach2Name; ?>" id="" name="coach2">
                        <input type="hidden" name="coach2Id" value="<?php echo $coach2Id; ?>">
                        <span>Enter the name of the player 2 Coach</span>
                    </div>
                </div>
            </div>

            <?php
            $refereeId = $refereeName = $timekeeperId = $timekeeperName = $contest_recorderName = $contest_recorderId = '';
            $senior_recorderId = $senior_recorderName = $competition_recorderName = $competition_recorderId = '';
            if (count($gameArray) > 0) {
                $refereeName = $gameArray['referee']['name'];
                $refereeId = $gameArray['referee']['id'];
                $timekeeperName = $gameArray['timekeeper']['name'];
                $timekeeperId = $gameArray['timekeeper']['id'];
                $contest_recorderName = $gameArray['contest_recorder']['name'];
                $contest_recorderId = $gameArray['contest_recorder']['id'];
                $senior_recorderName = $gameArray['senior_recorder']['name'];
                $senior_recorderId = $gameArray['senior_recorder']['id'];
                $competition_recorderName = $gameArray['competition_recorder']['name'];
                $competition_recorderId = $gameArray['competition_recorder']['id'];
            }
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Refree :</label>
                        <input type="text" placeholder="Enter the name of Refree for the bout" class="form-control" value="<?php echo $refereeName; ?>" id="" name="refree">
                        <input type="hidden" name="refreeId" value="<?php echo $refereeId; ?>">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Timekeeper:</label>
                        <input type="text" placeholder="Enter the name of Timekeeper for the bout" class="form-control" id="" value="<?php echo $timekeeperName; ?>" name="timekeeper">
                        <input type="hidden" name="timekeeperId" value="<?php echo $timekeeperId; ?>">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Contest Recorder </label>
<<<<<<< HEAD
                        <input type="text" placeholder="Enter the name of Contest Recorder for the bout" class="form-control" id="" value="<?php echo $contest_recorderName; ?>" name="contest_recorder">
                        <input type="hidden" name="contest_recorderId" value="<?php echo $contest_recorderId; ?>">
=======
                        <input type="text" class="form-control" id="" placeholder="Enter the name of Contest Recorder for the bout" value="<?php echo $contest_recorderName;?>" name="contest_recorder">
                        <input type="hidden" name="contest_recorderId" value="<?php echo $contest_recorderId;?>">
						<span>Enter the name of Contest Recorder for the bout</span>
>>>>>>> 2e5e58f866e6f25a071c051d9c76442304b929c2
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Senior Recorder :</label>
<<<<<<< HEAD
                        <input type="text" placeholder="Enter the name of Senior Recorder for the bout" class="form-control" value="<?php echo $senior_recorderName; ?>" id="" name="senior_recorder">
                        <input type="hidden" name="senior_recorderId" value="<?php echo $senior_recorderId; ?>">
=======
                        <input type="text" class="form-control" placeholder="Enter the name of Senior Recorder for the bout" value="<?php echo $senior_recorderName;?>" id="" name="senior_recorder">
                        <input type="hidden" name="senior_recorderId" value="<?php echo $senior_recorderId;?>">
						<span>Enter the name of Senior Recorder for the bout</span>
>>>>>>> 2e5e58f866e6f25a071c051d9c76442304b929c2
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Competition Controller:</label>
<<<<<<< HEAD
                        <input type="text" placeholder="Enter the name ofCompetition Controller for the bout" class="form-control" value="<?php echo $competition_recorderName; ?>" id="" name="competition_controller">
                        <input type="hidden" name="competition_recorderId" value="<?php echo $competition_recorderId; ?>">
=======
                        <input type="text" class="form-control" placeholder="Enter the name ofCompetition Controller for the bout" value="<?php echo $competition_recorderName;?>" id="" name="competition_controller">
                        <input type="hidden" name="competition_recorderId" value="<?php echo $competition_recorderId;?>">
						<span>Enter the name ofCompetition Controller for the bout</span>
>>>>>>> 2e5e58f866e6f25a071c051d9c76442304b929c2
                    </div>
                </div>
            </div>
            <input type="hidden" id="playerDatas" name="playerDatas" class="playerDatas" value="">
            <input type="hidden" id="gameId" name="gameIdVal" value="<?php echo $gameId; ?>">
            <button type="submit" id="button_submit" class="btn btn-default btn-primary">Submit Scoreboard</button>
            <!-- </form> -->

            </form>

        </div>
        <input type="hidden" id="submitted" value="0">
        <?php

        ?>

    </main>
</body>
<script type="text/javascript">
    jQuery(function() {
        var counter = 1;
        jQuery('#addButton').click(function(event) {
            event.preventDefault();

            var html = '<tr class="form-group player_det" id=""><td><select class="form-control playerName" name="" id="sel1" required><option required value="">Select</option><?php foreach ($player as $result) {
                                                                                                                                                                                    echo '<option value="' . $result->player_id . '">' . $result->full_name . '</option>';
                                                                                                                                                                                } ?></select></td><td><select required class="form-control scoringMethod" name="" id="textbox1"><option required value="Ippon">Ippon</option><option required value="Wazart">Wazart</option><option required value="Penality">Penality</option></select></td><td><select required class="form-control technique" name="" id="textbox1"><option required value="wt1">Wazart Technique 1</option><option required value="wt2">Wazart Technique 11</option><option required value="wt3">wazart Technique 1111</option></select></td><td><input required type="text" name="" class="form-control time"></td><td><input required type="text" name="" class="form-control score"></td></tr>';
            counter++;
            jQuery('table.table').append(html);

        });
        $("#removeButton").click(function(event) {
            $("table.playerData tbody tr:last-child").remove();
        });
        $(".submit_form").submit(function(event) {

            if ($("#submitted").val() == '0') {
                $("#submitted").val(1);
                event.preventDefault();
                var gameId = $("#gameId").val();
                var playerData = {};
                var score1 = score2 = 0;
                var player1 = 0;
                var player2 = 'undefined';
                $(".player_det").each(function(i, val) {

                    var playerId = $(this).find('select.playerName option:selected').val();
                    if (player1 == '' && player2 != playerId) {
                        player1 = playerId;
                    } else if (player2 == 'undefined' && player1 != playerId) {
                        player2 = playerId;
                    }
                    var scoringMethod = $(this).find('select.scoringMethod option:selected').val();
                    var technique = $(this).find('select.technique option:selected').val();
                    var time = $(this).find('input.time').val();
                    var score = $(this).find('input.score').val();

                    if (playerId == player1) {
                        score1 = parseInt(score1) + parseInt(score);
                    } else if (playerId == player2) {
                        score2 = parseInt(score2) + parseInt(score);
                    }
                    playerData[i] = {
                        'game_id': gameId,
                        'player_id': playerId,
                        'scoring_method': scoringMethod,
                        'technique': technique,
                        'time': time,
                        'score': score
                    };
                });
                $("#playerDatas").val(JSON.stringify(playerData));
                $("#total_score").val(score1);
                $("#total_score2").val(score2);
                $(".submit_form").submit();
            } else {
                return true;
            }
        });
    })
</script>