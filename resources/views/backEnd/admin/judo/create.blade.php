@extends('../backLayout.pages')
@section('breadcrumb')

   <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Add Event</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">Player</a>
                            </li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>
              
                </div>

@endsection
@section('content')
@if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
 @if(Session::has('message'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
@endif
<?php $table_prefix= \Session::get('table_prefix') ?>
    {!! Form::open(['url' => 'admin/'.$table_prefix.'/event', 'class' => 'form-horizontal']) !!}

        <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Event Details</h4>
                            </div>
                            <div class="card-body">
                                <form action="#">
                                    <div class="form-body">
                                        
                                        <div class="row p-t-20">
                                           
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('name') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Name</label>
                                                     {!! Form::text('name', null, ['class' => 'form-control','value' => old('name')]) !!}
                                                    <small class="form-control-feedback">Enter Name of Event </small> </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('season') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Season</label>
                                                     {!! Form::text('season', null, ['class' => 'form-control','value' => old('season')]) !!}
                                                    <small class="form-control-feedback">Enter Event Season </small> </div>
                                            
                                            </div>
                                            <!--/span-->
                                        </div>
                                         <!--/row-->
                                           <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('country_id') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Country</label>
                                                    {!! Form::select('country_id', $country, null, ['class' => 'form-control','id' =>'country_id']) !!}
                                                    <small class="form-control-feedback">Select Country</small> </div>
                                            
                                            </div>
                                               <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('region') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Region</label>
                                                    {!! Form::select('region', $region, null, ['class' => 'form-control','id' => 'region']) !!}
                                                    <small class="form-control-feedback">Enter Region Event </small> </div>
                                            </div>
                                        </div>
                                         <!--/row-->
                                           <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('sub_region_id') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Sub Region</label>
                                                     {!! Form::select('sub_region_id', $subregion, null, ['class' => 'form-control','id' => 'sub_region']) !!}
                                                    <small class="form-control-feedback">Enter Sub Region of Event </small> </div>
                                            </div>
                                               <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('location') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Location</label>
                                                     {!! Form::select('location', $location, null, ['class' => 'form-control','id' => 'location']) !!}
                                                    <small class="form-control-feedback">Enter Location of Event </small> </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                           <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('generic_name') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Generic Name</label>
                                                     {!! Form::text('generic_name', null, ['class' => 'form-control','value' => old('generic_name')]) !!}
                                                    <small class="form-control-feedback">Enter Generic Name </small></div>
                                            
                                            </div>
                                               <div class="col-md-6">
                                               <div class="form-group {{ $errors->has('start_date') ? 'has-error' : 'has-success'}}">
                                                   <label class="control-label">Start Date</label>
                                                   <input name="start_date" id="start_date" type="text" class="form-control" placeholder="yyyy-mm-dd">
                                                   <span class="input-group-addon">
                                                   <span class="glyphicon glyphicon-calendar"></span>
                                                   <small class="form-control-feedback">Enter Event Start Date </small> </div>
                                           
                                           </div>
                                            
                                        </div>

                                    <div class="row">
                                         <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('picture_id') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Picture Id</label>
                                                     {!! Form::text('picture_id', null, ['class' => 'form-control','value' => old('picture_id')]) !!}
                                                    <small class="form-control-feedback">Enter Picture Id of Event </small> </div>
                                            
                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group {{ $errors->has('category') ? 'has-error' : 'has-success'}}">
                                                    <label class="control-label">Category</label>
                                                    <select name= "category" class="form-control">

                                                        @foreach($category as $key => $value)
                                                        
                                                        <option value="<?=$value?>"   ><?=$value?></option>
                                                        @endforeach
                                                    </select>

                                                    <small class="form-control-feedback"> </small> </div>
                                               
                                            
                                            </div>
                                    </div>
                                   
                                         
                                 
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success btn-rounded"> <i class="fa fa-check"></i> Save</button>
                                        <input type="reset" value="Reset" class="btn btn-primary btn-rounded" value="Clear">
                                        <a href="{{ url('admin/'.$table_prefix.'/event') }}"><button type="button" class="btn btn-inverse btn-rounded">View Events</button>
                                        <!-- <button type="button" class="btn btn-inverse">Cancel</button> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>



    {!! Form::close() !!}


@endsection
@section('scripts')
<script type="text/javascript">
  $( function() {
    $( ".datepicker" ).datepicker();
  } );
</script>
<script type="text/javascript">
$(document).ready(function(){
var APP_URL = {!! json_encode(url('/')) !!}
$("#region").empty();
$("#sub_region").empty();
$("#location").empty();

    $('#country_id').change(function(){
        
        var country_id = $(this).val();
            $.ajax({
            type: "get",
            url: APP_URL+'/getregion',
            data: {'country_id': country_id},
            dataType:"json",
            success: function( json ) {
             //json = $.parseJSON(json); 
             $("#sub_region").empty();
             $("#region").empty();
             
$.each(json,function(key, value) 
{
    $('#region').prepend('<option value=' + value + '>' + key + '</option>');
});


            }
        });
    });



    $('#region').change(function(){
        
        var region_id = $(this).val();
            $.ajax({
            type: "get",
            url: APP_URL+'/getsubregion',
            data: {'region_id': region_id},
            dataType:"json",
            success: function( json ) {
             //json = $.parseJSON(json); 
             $("#sub_region").empty();
$.each(json,function(key, value) 
{
    $('#sub_region').prepend('<option value=' + value + '>' + key + '</option>');
});
            }
        });
    });

     $('#sub_region').change(function(){
        
        var sub_region_id = $(this).val();
            $.ajax({
            type: "get",
            url: APP_URL+'/getlocation',
            data: {'sub_region_id': sub_region_id},
            dataType:"json",
            success: function( json ) {
             //json = $.parseJSON(json); 
             $("#Location").empty();
$.each(json,function(key, value) 
{
    $('#Location').prepend('<option value=' + value + '>' + key + '</option>');
});
            }
        });
    });
    
});

      


</script>


@endsection