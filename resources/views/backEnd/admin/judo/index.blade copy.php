<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<style>
    main
    {
        padding: 100px 0;
    }
    .table td 
    {
        width: 80px;
        border :0 !important;
    }

    .table th:first-child,  .table td:first-child
    {
        width: 200px;
    }

</style>
      <main>
		@if($errors->any())
				<ul class="alert alert-danger">
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
		 @if(Session::has('message'))
		<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
		@endif
	  
        <div class="container">
            <h3> Event Name : {{ $event->name }}</h3>
			<h3>Game Name : Mohammed Iqbal Khan vs Samad</h3> <br><br>

	{!! Form::open(['url' => 'admin/ju/events/update/'.$event->event_id, 'class' => '']) !!}
	
    <div class="container sc2">
            
            <script>
              
                function addp1i() {
                  
                    document.getElementById('p1ippon').value++;
                }
                function subp1i() {
                  
                  document.getElementById('p1ippon').value--;
              }
              function addp2i() {
                  
                  document.getElementById('p2ippon').value++;
              }
              function subp2i() {
                
                document.getElementById('p2ippon').value--;
            }
            function addp1w() {
                  
                  document.getElementById('p1wazari').value++;
              }
              function subp1w() {
                  
                  document.getElementById('p1wazari').value--;
              }
              function addp2w() {
                  
                  document.getElementById('p2wazari').value++;
              }
              function subp2w() {
                  
                  document.getElementById('p2wazari').value--;
              }
              function addp1f() {
                  
                  document.getElementById('p1final').value++;
              }
              function subp1f() {
                  
                  document.getElementById('p1final').value--;
              }

              function addp2f() {
                  
                  document.getElementById('p2final').value++;
              }
              function subp2f() {
                  
                  document.getElementById('p2final').value--;
              }
            </script>

		<?php
		$p1ippon = $p2ippon = $p1wazari = $p2wazari = $p1final = $p2final = '';
		if(count($gamePlayerArray)) {
			$p1ippon = $gamePlayerArray[0]['ippon'];
			$p2ippon = $gamePlayerArray[1]['ippon'];
			$p1wazari = $gamePlayerArray[0]['wazari'];
			$p2wazari = $gamePlayerArray[1]['wazari'];
			$p1final = $gamePlayerArray[0]['penalty'];
			$p2final = $gamePlayerArray[1]['penalty'];
		}
		?>
			
        <div class="p1 clearfix">
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="player1"></label>
                    <select class="form-control" name="player1Id" id="sel1" required>
						<option value="">Select</option>
						<?php
						foreach($player as $result) {
							echo '<option value="'.$result->player_id.'" '.((count($gamePlayerArray) > 0 && $gamePlayerArray[0]['player'] == $result->player_id) ? 'selected' : '').'>'.$result->full_name.'</option>';
						}
						?>
                    </select>
                  </div> 
            </div>
            <div class="col-md-2">
                <div class="ippon">
                    <div class="df">
                        <a href="#" class="btn" onclick="subp1i()">-</a>
                        <h4>Ippon</h4>
                        <a href="#" class="btn" onclick="addp1i()">+</a>
                    </div>

                    <input type="number" class="form-control" min="0" value="<?php echo $p1ippon; ?>" name="p1ippon" id="p1ippon" required>
                </div>
            </div>
            <div class="col-md-2">
                <div class="ippon">
                    <div class="df">
                        <a href="#" class="btn"  onclick="subp1w()">-</a>
                        <h4>Wazari</h4>
                        <a href="#" class="btn"  onclick="addp1w()">+</a>
                    </div>

                    <input type="number" name="p1wazari" min="0" value="<?php echo $p1wazari; ?>" class="form-control" id="p1wazari" required>
                </div>
            </div>
            <div class="col-md-2">
                <div class="ippon final">
                    <div class="df">
                        <a href="#" class="btn"  onclick="subp1f()">-</a>
                        <h4>Penalty</h4>
                        <a href="#" class="btn" onclick="addp1f()">+</a>
                    </div>

                    <input type="number" class="form-control" min="0" value="<?php echo $p1final; ?>" name="p1final" id="p1final" number>
                </div>
            </div>
        </div>
        
        <div class="p1 p2 clearfix">
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="player1"></label>
                    <select class="form-control" name="player2Id" id="" required>
						<option value="">Select</option>
						<?php
						foreach($player as $result) {
							echo '<option value="'.$result->player_id.'" '.((count($gamePlayerArray) > 0 && $gamePlayerArray[1]['player'] == $result->player_id) ? 'selected' : '').'>'.$result->full_name.'</option>';
						}
						?>
                    </select>
                  </div> 
            </div>
            <div class="col-md-2">
                <div class="ippon">
                    <div class="df">
                        <a href="#" class="btn" onclick="subp2i()">-</a>
                        <h4>Ippon</h4>
                        <a href="#" class="btn"  onclick="addp2i()">+</a>
                    </div>

                    <input type="number" min="0" required name="p2ippon" value="<?php echo $p2ippon; ?>" class="form-control" id="p2ippon">
                </div>
            </div>
            <div class="col-md-2">
                <div class="ippon">
                    <div class="df">
                        <a href="#" class="btn" onclick="subp2w()">-</a>
                        <h4>Wazari</h4>
                        <a href="#" class="btn"  onclick="addp2w()">+</a>
                    </div>

                    <input type="number" min="0" required  class="form-control" name="p2wazari" id="p2wazari" value="<?php echo $p2wazari; ?>">
                </div>
            </div>
            <div class="col-md-2">
                <div class="ippon final">
                    <div class="df">
                        <a href="#" class="btn"  onclick="subp2f()">-</a>
                        <h4>Penalty</h4>
                        <a href="#" class="btn"  onclick="addp2f()">+</a>
                    </div>

                    <input type="number" min="0" required class="form-control" name="p2final" id="p2final" value="<?php echo $p2final; ?>">
                </div>
            </div>
        </div>
    </div>			
		
            <br><br>
                <h3>Match Summary</h3>
                <div id='TextBoxesGroup'>
                    <div id="TextBoxDiv1">
                        <label>Player Name </label>
                        <select name="player" id="textbox1">
                            <option value="8522">Sagar M Premanathan</option>
                            <option value="8523">Shahas Siddeeq</option>
                        </select>
                        <label>Time : </label><input type='textbox' id='textbox2' name="time">
                        <label>Comment : </label><input type='textbox' id='textbox3' name="comment">
                        <input type='button' value='Add Summary' id=' addButton '><br>
                    </div>
                </div>
                <br><br>	
				<?php
				$score1 = $score2 = '';
				if(count($gamesArray) > 0) {
					$score1 = $gamesArray[0]['score1'];
					$score2 = $gamesArray[0]['score2'];
				}
				?>				
               <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Player 1 Total Score:</label>
                            <input type="number" min="0" value="<?php echo $score1; ?>" required class="form-control" id="" name="total_score">
                            <span>Enter the total score for player 1 for the bout</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Player 2 Total Score:</label>
                            <input type="number" min="0" required value="<?php echo $score2; ?>" class="form-control" id="" name="total_score2">
                            <span>Enter the total score for player 2 for the bout</span>
                        </div>
                    </div>
                </div>
				<?php
				$coach = $coach2 = '';
				if(count($gameteamArray) > 0) {
					$coach = $gameteamArray['coach'];
					$coach2 = $gameteamArray['assistantcoach'];
				}
				?>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Player 1 Coach:</label>
                            <input type="text" value="<?php echo $coach; ?>" required class="form-control" id="" name="coach">
                            <span>Enter the name of the player 1 Coach</span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Player 2 Coach:</label>
                            <input type="text" value="<?php echo $coach2; ?>" required class="form-control" id="" name="coach2">
                            <span>Enter the name of the player 2 Coach</span>
                        </div>
                    </div>
                </div>
				<?php
				$refereeName = $timeKeeper = $contest_recorder = $senior_recorder = $competition_recorder = '';
				if(count($gameArray) > 0) {
					$refereeName = $gameArray['referee'];
					$timeKeeper = $gameArray['timekeeper'];
					$contest_recorder = $gameArray['contest_recorder'];
					$senior_recorder = $gameArray['senior_recorder'];
					$competition_recorder = $gameArray['competition_recorder'];
				}
				?>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Refree :</label>
                        <input type="text" required class="form-control" value="<?php echo $refereeName; ?>" id="" name="refree">
                        <span>Enter the name of Refree for the bout</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Timekeeper:</label>
                            <input type="text" required class="form-control" value="<?php echo $timeKeeper; ?>" id="" name="timekeeper">
                            <span>Enter the name of Timekeeper for the bout</span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Contest Recorder </label>
                            <input type="text" required class="form-control" value="<?php echo $contest_recorder; ?>" id="" name="contest_recorder">
                            <span>Enter the name of Contest Recorder for the bout</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Senior Recorder :</label>
                            <input type="text"  required class="form-control" id="" value="<?php echo $senior_recorder; ?>" name="senior_recorder">
                            <span>Enter the name of Senior Recorder for the bout</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Competition Controller:</label>
                            <input type="text"  required class="form-control" id="" value="<?php echo $competition_recorder; ?>" name="competition_controller">
                            <span>Enter the name ofCompetition Controller for the bout</span>
                        </div>
                    </div>
                </div>
    
                <button type="submit" class="btn btn-default btn-primary">Submit Scoreboard</button>
            {!! Form::close() !!}
        </div>
        <br><br>
        <style>
                .p1 
    {
        background: rgb(245, 240, 240);
        padding: 40px 0;
        padding-top: 60px;
    }
   .sc2 .form-control 
    {
        font-size: 40px;
        height: 100px;
        border: 0;
        box-shadow: none;
        text-align: center;
    }
    .sc2 h4 
    {
        text-align: center;
        margin-left: 10px;
        margin-right: 10px;
    }
    .ippon
    {
        margin-top: -30px;
    }
    .ippon input.form-control
    {
        height: 100px;
        width: 140px;
        display: block;
        margin: auto;
    }
    .df 
    {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-bottom: 10px;
    }
    .df .btn
    {
        background: #fff;
    }
    .final .form-control
    {
        border: 4px solid #caccd1;
    }
.p2
{
    background: #d7d7d8;
}
</style>

</main>