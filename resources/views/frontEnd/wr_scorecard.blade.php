@extends('frontEnd/layouts.web')
@section('content') 
  <?php $table_prefix= \Session::get('table_prefix') ?> 
<!--scorecard-->
     <?php 
              $gmt = ($game->gmt_start_time!='')?$game->gmt_start_time:'00-00-00';
              $local = ($game->local_start_time!='')?$game->local_start_time:'00-00-00';
              ?>  
            <div class="scorecard">
                <div class="container">
                    <h1>{{ $game->player1['full_name']}} v {{ $game->player2['full_name']}}</h1>
                    <p><span>League:</span> {{ $event_data->name }} @if($game->phase!='')({{ $game->phase }})@endif</p>
                    <p><span>Played at:</span> <?=$game->venue['name']?></p>
                    <p><span>Played on:</span> {{ date('d M Y',strtotime($game->game_date))}} &nbsp &nbsp GMT {{ $gmt }} &nbsp &nbsp     </p>
                    <p><span>Local Start Time:</span> {{ $local }}</p>


                </div>
            </div>
            <!--scorecard-->

            <div class="container">
                <ol class="breadcrumb bg-transparent">
                    <li><a href="{{ url('') }}">Home</a></li>
                    <li><a href="{{ url('/vb/events') }}">Events</a></li>
                    <li><a href="{{ url('/vb/events/'.$event_data->event_id) }}">{{ $event_data->name }}</a></li>
                    <li class="active">{{ $game->player1['full_name']}} v {{ $game->player2['full_name']}}</li>
                </ol>
            </div>
            <?php
                                               
                                                $team1_url = url($table_prefix.'/teams/t/')."/".$game->team1_id;
                                                $team2_url = url($table_prefix.'/teams/t/')."/".$game->team2_id;
                                                ?>
                                               
            <section class="scorecard-container">
                <div class="container">
                    <div class="title-bar">
                        <h2>SUMMARY</h2>
                    </div>
                    <div>
                        <table class="table summary-table table-borderbottom table-responsive-sm" id="tbladmin">
                            <thead>
                                <tr>
                                                    
                                                    <th>Player</th>
                                                    <!-- <th>Player Number</th> -->
                                                    <th class="text-right">Takedown(t2)</th>
                                                    <th class="text-right">Nearfall(n2)</th>
                                                    <th class="text-right">Nearfall(n3)</th>
                                                    <th class="text-right">Reversal(r2)</th>
                                                    <th data-hide="phone, tablet" class="text-right">Escape(e1)</th>
                                                    <th data-hide="phone, tablet" class="text-right">Penalty(p1)</th>
                                                    <th data-hide="phone, tablet" class="text-right">Penalty(p2)</th>
                                                    <th data-hide="phone, tablet" class="text-right">Stalling</th>
                                                    <th data-hide="phone, tablet" class="text-right">Stalling(w2)</th>
                                                    <th data-hide="phone, tablet" class="text-right">Caution</th>
                                                    <th data-hide="phone, tablet" class="text-right">Caution(c1)</th>
                                                    <th data-hide="phone, tablet" class="text-right">Game Time</th>
                                                </tr>
                            </thead>
                            <?php $note = ""; ?>
                @if (count($scorecard))

                <?php
                  $i='';
                  $n=0;
                  $team1 ='';
                  $team2 ='';

                  foreach ($scorecard as $score) {

                    $flag = 0;
                    $b = $score->team_id;

                    $name = $score->player['full_name'];
                      if($score->starter == 'y'){
                        $name = "*".$name;
                      }
                      if($score->captain == 'y'){
                        $name = $name."(c)";
                      }
                  
                     // $jersey_no = $score->player_num;
                      // $jersey_no = ($jersey_no==-1)? '' : $jersey_no;
                      $takedown_c2 = $score->takedown_c2==-1?'?':$score->takedown_c2;
                      $nearfall_n2 = $score->nearfall_n2==-1?'?':$score->nearfall_n2;
                      $nearfall_n3 = $score->nearfall_n3==-1?'?':$score->nearfall_n3;
                      $reversal_r2 = $score->reversal_r2==-1?'?':$score->reversal_r2;
                      $escape_e1 = $score->escape_e1==-1?'?':$score->escape_e1;
                      $penalty_p1 = $score->penalty_p1==-1?'?':$score->penalty_p1;
                      $penalty_p2 = $score->penalty_p2==-1?'?':$score->penalty_p2;
                      $stalling = $score->stalling==-1?'?':$score->stalling;
                      $stalling_w2 = $score->stalling_w2==-1?'?':$score->stalling_w2;
                      $caution = $score->caution==-1?'?':$score->caution;
                      $caution_c1 = $score->caution_c1==-1?'?':$score->caution_c1;
                      $gametime_seconds = $score->gametime_seconds==-1?'?':$score->gametime_seconds;
                      
                      $player_url = url('players').'/'.$score->player_id;
                      $link="#myModal";
                      $team ='<tr>';
            if($score->region == 1)
                       {
                          $team .= '<td><a href="'.$player_url.'"><b>'.$name.'</b></a></td>';
                       }
                       else
                       {
                        $team .= '<td>'.$name.'</td>';
                       }
                      
                      $team .= '<td class="text-right"><a href="#myModal" class="videoclick" data-game_id='.$score->game_id.' data-id='.$score->player_id.' data-type="3" data-type_sec="4" data-toggle="modal">'.$takedown_c2.'</a></td>
                      <td class="text-right"><a href="#myModal" class="videoclick" data-game_id='.$score->game_id.' data-id='.$score->player_id.' data-type="1" data-type_sec="2" data-toggle="modal">'.$nearfall_n2.'</a></td>
                      <td class="text-right"><a href="#myModal" class="videoclick" data-game_id='.$score->game_id.' data-id='.$score->player_id.' data-type="5" data-type_sec="6" data-toggle="modal">'.$nearfall_n3.'</a></td>
                      <td class="text-right "> <a href="#myModal" class="videoclick" data-game_id='.$score->game_id.' data-id='.$score->player_id.' data-type="7" data-toggle="modal">'.$reversal_r2.'</a></td>
                      <td class="text-right"><a href="#myModal" class="videoclick" data-game_id='.$score->game_id.' data-id='.$score->player_id.' data-type="8" data-toggle="modal">'.$escape_e1.'</a></td>
                      <td class="text-right"><a href="#myModal" class="videoclick" data-game_id='.$score->game_id.' data-id='.$score->player_id.' data-type="11" data-toggle="modal">'.$penalty_p1.'</a></td> 
                      <td class="text-right"><a href="#myModal" class="videoclick" data-game_id='.$score->game_id.' data-id='.$score->player_id.' data-type="12" data-toggle="modal">'.$penalty_p2.'</a></td> 
                      <td class="text-right"><a href="#myModal" class="videoclick" data-game_id='.$score->game_id.' data-id='.$score->player_id.' data-type="12" data-toggle="modal">'.$stalling.'</a></td> 
                      <td class="text-right"><a href="#myModal" class="videoclick" data-game_id='.$score->game_id.' data-id='.$score->player_id.' data-type="0" data-toggle="modal">'.$stalling_w2.'</a></td>
                      <td class="text-right"><a href="#myModal" class="videoclick" data-game_id='.$score->game_id.' data-id='.$score->player_id.' data-type="12" data-toggle="modal">'.$caution.'</a></td> 
                      <td class="text-right"><a href="#myModal" class="videoclick" data-game_id='.$score->game_id.' data-id='.$score->player_id.' data-type="13" data-toggle="modal">'.$caution_c1.'</a></td>
                      <td class="text-right"><a href="#myModal" class="videoclick" data-game_id='.$score->game_id.' data-id='.$score->player_id.' data-type="9" data-toggle="modal">'.$gametime_seconds.'</a></td>
                                         
                      </tr>';
                      ?>
                      <!-- Modal Display Body -->
  

<!-- End of Modal Display Body -->
                      <?php


                      // if($i!=$b){
                      //   $i=$b;
                      //     $flag=1;  
                      // }
                   

                      // if($score->player_id==$game->player2_id){
                        $team2.=$team;  
                      // }elseif($score->player_id==$game->player1_id){
                        $team1.=$team;  
                      // }


                        $n++;

                  }
                ?>

                @else
                 <?php  
                      $team2=$team1="<tr><td colspan='13'></td></tr>";
                      $note="No scorecard data available"; 
                      
                 ?>
                @endif
                            <tbody>
                                 <?php if(isset($team1)){ echo $team1; }?>        
                            </tbody>
                        </table>
                    </div>
                    
                    <br><br>
                    <div>
                                  <?php

           
            $referee ="";
            $linejudge ="";
            $umpire ="";
            $backjudge ="";
            $fieldjudge ="";
            $sidejudge ="";
            foreach($gameofficial as $g_off){                
            if($g_off->official_type == 'referee'){
            $c_url = url($table_prefix.'/players').'/'.$g_off->game_official_id;
            $referee = "<a href='".$c_url."'>".$g_off->official_name."</a>";
            }
            if($g_off->official_type == 'judge'){
            $c_url = url($table_prefix.'/players').'/'.$g_off->game_official_id;
            $judge = "<a href='".$c_url."'>".$g_off->official_name."</a>";
            }
            if($g_off->official_type == 'matchairman'){
            $c_url = url($table_prefix.'/players').'/'.$g_off->game_official_id;
            $matchairman = "<a href='".$c_url."'>".$g_off->official_name."</a>";
            }
          
            }
            
            ?>  
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <table class="table summary-table table-borderbottom table-responsive-xs" id="tbladmin">
                                    <thead>
                                        <tr>
                                            <th>Official</th>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <tr></tr>
                                         @if($referee != null)
                                            <tr>
                                                <td><b>Referee</b></td>
                                                <td><?=$referee?></td>
                                            </tr>
                                           @endif
                                          @if($referee != null)
                                            <tr>
                                                <td><b>Judge</b></td>
                                                <td><?=$judge?></td>
                                            </tr>
                                             @endif
                                            @if($referee != null)
                                            <tr>
                                                <td><b>Mat Chairman</b></td>
                                                <td><?=$matchairman?></td>
                                            </tr>
                                             @endif
                                      
                                            
                                        <?php

                                        $t1coach = "";
                                        $t1assistant = "";
                                        $t2coach = "";
                                        $t2assistant = "";

                                        foreach($gameteamofficial as $gt_off){ 
                                          // echo $gt_off->official_name;
                                          // echo $game->player2_id;
                                          // echo $gt_off->player_id ;

                                        if($gt_off->player_id == $game->player1_id){
                                        if($gt_off->official_type == "coach"){
                                        $t1c_url = url($table_prefix.'/players').'/'.$gt_off->game_team_official_id;
                                        $t1coach = "<a href='".$t1c_url."'>".$gt_off->official_name."</a>";
                                        }elseif($gt_off->official_type == "assistantcoach"){
                                        $t1a_url = url($table_prefix.'/players').'/'.$gt_off->game_team_official_id;
                                        $t1assistant = "<a href='".$t1a_url."'>".$gt_off->official_name."</a>";
                                        }
                                        }
                                        if($gt_off->player_id == $game->player2_id){
                                        if($gt_off->official_type == "coach"){
                                        $t2c_url = url($table_prefix.'/players').'/'.$gt_off->game_team_official_id;
                                        $t2coach = "<a href='".$t2c_url."'>".$gt_off->official_name."</a>";
                                        }elseif($gt_off->official_type == "assistantcoach"){
                                        $t2a_url = url($table_prefix.'/players').'/'.$gt_off->game_team_official_id;
                                        $t2assistant = "<a href='".$t2a_url."'>".$gt_off->official_name."</a>";
                                        }
                                        }
                                        }
                                        ?>
                                            @if($t1coach!='')
                                            <tr>
                                                <td><b>{{ $game->player1['full_name'] }} 's Coach</b></td>
                                                <td><?=$t1coach?></td>
                                            </tr>
                                            @endif
                                            @if($t2coach!='')
                                            <tr>
                                                <td><b>{{ $game->player2['full_name'] }} 's Coach</b></td>
                                                <td><?=$t2coach?></td>
                                            </tr>
                                            @endif
                                            @if($t1assistant!='')
                                            <tr>
                                                <td><b>{{ $game->player1['full_name'] }}s Assistant Coach</b></td>
                                                <td><?=$t1assistant?></td>
                                            </tr>
                                            @endif
                                            @if($t2assistant!='')
                                            <tr>
                                                <td><b>{{ $game->player2['full_name'] }} 's Assistant Coach</b></td>
                                                <td><?=$t2assistant?></td>
                                            </tr>
                                            @endif     
                                    </tbody>
                                </table>
                            </div>
                            <!-- <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="notes">
                                    <div class="title">Notes:</div>
                                    <div class="desc"><?=$note?></div>
                                </div>
                            </div> -->
                        </div>

                    </div>
                </div>
            </section>
@endsection

@section('scripts')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
    .bs-example{
      margin: 20px;
    }
    .modal-content iframe{
        margin: 0 auto;
        display: block;
    }
</style>


<script type="text/javascript">
 var video_index     = 0;
  var video_player    = null;
  var video_list=null;
  var APP_URL = {!! json_encode(url('/')) !!}
$(document).ready(function(){

    $('#myModal').on('hide.bs.modal', function(){
        $("#cartoonVideosrc").attr('src', '');
    });
    

    $('.videoclick').click(function(e){

 video_index     = 0;
video_player    = null;
 video_list=null;
 var player_id = $(this).data('id');
 var point_type = $(this).data('type');
var game_id = $(this).data('game_id');
var point_type_sec=$(this).data('type_sec');
// alert( APP_URL+'/scorecard/getvideos'); 
    
            $.ajax({
            type: "get",
            url: APP_URL+'/getvideos',
            data: {
              'player_id': player_id,
              'point_type': point_type,
              'game_id':game_id,
              'point_type_sec':point_type_sec
                  },
            success: function( json ) {

                video_list = $.parseJSON(json); 
                // alert(video_list[0]); 
                // $('#myModal').modal();
            video_player        = document.getElementById("cartoonVideo");
            video_player.setAttribute("src", video_list[video_index]);
            video_player.play();
                
              }
        });


   
        
 //      $('#myModal').on('show.bs.modal', function(){
 // alert("Jingallaalal"); 
 //         video_player        = document.getElementById("cartoonVideo");
 //            video_player.setAttribute("src", video_list[video_index]);
 //            video_player.play();
 //          });
        
    });

 

});
     function onVideoEnded(){
      
            console.log("video ended");
            if(video_index < video_list.length - 1){
                video_index++;
            }
            else{
                video_index = 0;
            }
            video_player.setAttribute("src", video_list[video_index]);
            video_player.play();
        }
</script>
@endsection 