@extends('frontEnd/layouts.web')
@section('content')
<?php
$url = URL::to('/');
$table_prefix = session()->get('table_prefix');
?>
<style type="text/css">
    .sportmargin {
        margin-top: 0px !important;
    }
</style>

<div class="inner-top-img">
    <div class="container">
        @if($table_prefix == 'wr')
        <img src="{{ url('website/images/wresting.jpg') }}" alt="" class="img-responsive" />
        @endif
    </div>
</div>

<div class="search-box-container">
    <div class="container">
        @if($table_prefix == 'wr')
        <div class="search-box">
            @else
            <div class="search-box sportmargin">
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <!-- <nav class="navbar navbar-expand-lg navbar-light">
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent"> -->
                        <nav>
                            <div id="navbarSupportedContent" class="searchcontent">
                                <form method="POST" action="<?= url($table_prefix . '/searchevent') ?>" id="eventsearchform" class="form-inline my-2 my-lg-0">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group has-search mr-2 select-eventlist">
                                        {!! Form::select('season_sel', $seasons, $defaultseason, ['class' => 'required form-control form-control-sm select searchcontent','id'=>'select-season']) !!}
                                    </div>

                                    <div class="form-group has-search mr-2 select-eventlist">
                                        {!! Form::select('age_group', $age_group, $age_group_sel, ['class' => 'form-control form-control-sm select searchcontent','id'=>'age_group']) !!}
                                    </div>

                                    <div class="form-group has-search mr-2 select-eventlist">
                                        {!! Form::select('category_sel', $category, $defaultcategory, ['class' => 'required form-control form-control-sm select searchcontent','id'=>'select-category']) !!}
                                    </div>

                                    <div class="form-group  has-search mr-2">
                                        <!-- <span class="fa fa-search form-control-feedback"></span> -->
                                        <input name="query_str" id="query_str" type="text" class="form-control searchcontent" value="{{ $query_str }}" placeholder="Search">
                                    </div>

                                    <div class="form-group  has-search">
                                        <input class="btn btn-primary btn-sm searchcontent" type="submit" name="submit" value="Submit">
                                    </div>
                                </form>
                            </div>
                        </nav>
                    </div>
                </div>

            </div>
        </div>
    </div>



    <div class="search-result-container">
        <div class="container">
            <div class="title-bar">

                <h2>Events :
                    @if($table_prefix == 'at')
                    Athletics
                    @elseif($table_prefix == 'bb')
                    Basketball
                    @elseif($table_prefix == 'bo')
                    Boxing
                    @elseif($table_prefix == 'fb')
                    Football
                    @elseif($table_prefix == 'ju')
                    Judo
                    @elseif($table_prefix == 'ta')
                    Taekwondo
                    @elseif($table_prefix == 'vb')
                    Volleyball
                    @elseif($table_prefix == 'wr')
                    Wrestling
                    @endif
                </h2>
                <div class="clearfix">&nbsp;</div>
                <div class="clearfix">
                    <ol class="breadcrumb">
                        <li><a href="http://multisports.in">Home</a></li>
                        <li class="active">Events</li>
                    </ol>
                </div>
            </div>
            <!--   <div class="clearfix">&nbsp;</div> -->


            <!--profile section-->
            <section id="player_profile_section" class="player_profile_section section_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-holder">
                                <table class="table table-striped table-condensed footable">
                                    <thead>
                                        <tr>
                                            <th width="100px">Start Date</th>
                                            <th>Name</th>
                                            <th>Winner</th>
                                            <th>Runner-up</th>
                                            <th>Third </th>
                                            <th>Fourth </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 0;   ?>
                                        @foreach($vsql as $item1)
                                        <?php $i++;   ?>
                                        <?php
                                        $url = url($table_prefix . '/events/' . $item1->event_id);

                                        ?>
                                        <tr>

                                            <td><?php
                                                if ($item1->start_date != '0000-00-00') {
                                                    $dateArray = explode('-', $item1->start_date);
                                                    if (($dateArray[1] != '00') && ($dateArray[2] == '00')) {
                                                        $yearmonth = $dateArray[0] . '-' . $dateArray[1];
                                                        echo date("M Y", strtotime($yearmonth));
                                                    } elseif ($dateArray[1] == '00') {
                                                        echo $dateArray[0];
                                                    } else {
                                                        echo date("d M Y", strtotime($item1->start_date));
                                                    }
                                                } else {
                                                    echo 'not known';
                                                }

                                                ?></td>
                                            <td><a href="<?= $url ?>">{{ $item1->name }}</a></td>
                                            <td>
                                                @if(isset($item1->winner ))
                                                <?php
                                                $values = 'India';
                                                $pattern = "/^" . $values . "/i"; ?>
                                                @if($item1->category == 'International' )
                                                @if(preg_match($pattern, $item1->winner))
                                                {{ $item1->winner }}
                                                @endif
                                                @elseif($item1->category == 'National' || $item1->category == 'State')
                                                @if($item1->winnerregion == 1)
                                                {{ $item1->winner }}
                                                @endif
                                                @endif
                                                @endif
                                            </td>
                                            <td>@if(isset($item1->runnerup ))
                                                <?php
                                                $values = 'India';
                                                $pattern = "/^" . $values . "/i"; ?>
                                                @if($item1->category == 'International' )
                                                @if(preg_match($pattern, $item1->runnerup))
                                                {{ $item1->runnerup }}
                                                @endif
                                                @elseif($item1->category == 'National' || $item1->category == 'State')
                                                @if($item1->runnerupregion == 1)
                                                {{ $item1->runnerup }}
                                                @endif
                                                @endif
                                                @endif
                                            </td>
                                            <td>@if(isset($item1->third ))
                                                <?php
                                                $values = 'India';
                                                $pattern = "/^" . $values . "/i"; ?>
                                                @if($item1->category == 'International' )
                                                @if(preg_match($pattern, $item1->third))
                                                {{ $item1->third }}
                                                @endif
                                                @elseif($item1->category == 'National' || $item1->category == 'State')
                                                @if($item1->thirdregion == 1)
                                                {{ $item1->third }}
                                                @endif
                                                @endif
                                                @endif
                                            </td>
                                            <td>
                                                @if(isset($item1->fourth ))
                                                <?php
                                                $values = 'India';
                                                $pattern = "/^" . $values . "/i"; ?>
                                                @if($item1->category == 'International' )
                                                @if(preg_match($pattern, $item1->fourth))
                                                {{ $item1->fourth }}
                                                @endif
                                                @elseif($item1->category == 'National' || $item1->category == 'State')
                                                @if($item1->fourthregion == 1)
                                                {{ $item1->fourth }}
                                                @endif
                                                @endif
                                                @endif
                                            </td>

                                        </tr>
                                        @endforeach
                                        <?php if ($i == 0) { ?>
                                            <tr>
                                                <td colspan="2"><b>No Event details found for the selection.</b></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>
            </section>
            <!--profile section-->
        </div>
    </div>



    @endsection
    @section('scripts')
    <script type="text/javascript">
        var APP_URL = {
            !!json_encode(url('/')) !!
        }
        // $('#country_id').on('select2:select', function (e) {
        // var data = e.params.data;
        // // console.log(data);
        // var country_id = data.id;
        // var xhttp = new XMLHttpRequest();
        // xhttp.onreadystatechange = function() {
        //     if (this.readyState == 4 && this.status == 200) {
        //         document.getElementById('team_id').innerText = null;
        //         var options = JSON.parse(this.responseText);
        //         Object.keys(options).forEach(function(key) {
        //             var el = document.createElement("option");
        //             el.textContent = options[key];
        //             el.value = key;
        //             document.getElementById("team_id").appendChild(el);
        //         })
        //     }
        // };
        // xhttp.open("GET", APP_URL+'/getteamsbycountry?country_id='+country_id, true);
        // xhttp.send();
        // });
        // $('#team_id').on('select2:select',function(e){
        //     var data=e.params.data;
        //     var team_id=data.id;
        //     document.location.href = APP_URL+'/teamsnew/t/'+team_id;
        // });

        $('#select-season').change(function() {
            var season = $(this).val();

            if (season != '') {
                season = season.replace("/", "-");
                // var data=e.params.data;
                // var team_id=data.id;
                document.location.href = APP_URL + '/<?= $table_prefix ?>/events/season/' + season;
            } else {
                season = 0;

                document.location.href = APP_URL + '/<?= $table_prefix ?>/events/season/' + season;
            }
        });


        /*$('#select-category').change(function(){
            var category = $(this).val();
            //alert(category);

            if(category!=''){
            
            // var data=e.params.data;
            // var team_id=data.id;
            document.location.href = APP_URL+'/<?= $table_prefix ?>/events/category/'+category;
        }
        });*/
    </script>
    @endsection