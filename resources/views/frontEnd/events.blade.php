@extends('frontEnd/layouts.web')
@section('content')

<?php
if (!empty($defaultevent)) {
  $season_sel = $defaultevent->season;
  $country_sel = $defaultevent->country_id;
  $event_sel = $defaultevent->event_id;
}
?>
<?php $table_prefix = \Session::get('table_prefix') ?>


<?php
$staturl = url($table_prefix . '/events/' . $event_sel . '/playerstats');
$offstaturl = url($table_prefix . '/events/' . $event_sel . '/officialstats');
$squadsurl = url($table_prefix . '/events/' . $event_sel . '/squads');
//Start Old Query//
/*if(isset($games[0])){
                                    $staturl=url($table_prefix.'/events/'.$games[0]->event_id.'/playerstats');
                                    $offstaturl=url($table_prefix.'/events/'.$games[0]->event_id.'/officialstats');
                                    $squadsurl=url($table_prefix.'/events/'.$games[0]->event_id.'/squads');
                                    }else
                                    {
                                      $staturl="";
                                      $offstaturl="";
                                      $squadsurl="";
                                    }*/
//End Old Query// 
?>








<div class="inner-top-img">
  <div class="container">
    @if($table_prefix == 'bb')
    <img src="{{ url('website/images/inside-top.jpg') }}" alt="" class="img-responsive" />
    @endif
  </div>
</div>

<!-- <div class="search-box-container">
                <div class="container">
                    <div class="search-box">
                        <div class="row">
                            <div class="col-md-12">
                                <nav class="navbar navbar-expand-lg navbar-light">
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">

                                        <form method="POST" action="<?= url($table_prefix . '/searchevent') ?>" id="eventsearchform"  class="form-inline my-2 my-lg-0">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-group has-search mr-2">
                                                {!! Form::select('season_sel', $seasons, $defaultevent->season, ['class' => 'required form-control form-control-sm select','id'=>'select-season']) !!}
                                            </div>
                                            <div class="form-group  has-search mr-2">
                                                <input  name="query_str" id="query_str" type="text" class="form-control" placeholder="Search">
                                            </div>
                                            <div class="form-group  has-search">
                                                <input class="btn btn-primary btn-sm" type="submit" name="submit" value="search">
                                            </div>
                                        </form>
                                    </div>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
            </div>-->

<div class="search-result-container">
  <div class="container">
    <div class="title-bar">
      <h2>{{ $event_name }}</h2>
      <!--                         <div class="btn-group" role="group" aria-label="ListType">
                            <a href="#" class="btn btn-link"><i class="fa fa-2x fa-th-list"></i></a>
                            <a href="#" class="btn btn-link active"><i class="fa fa-2x fa-th-large"></i></a>
                        </div> -->
      <div class="clearfix">&nbsp;</div>
      <div class="clearfix">Event Stats | Squads</div>
    </div>
    <div class="clearfix">&nbsp;</div>

    <div class="sj-toolbar">
      <a class="sj-tools" href="{{ $staturl }}">
        <div class="sj-ico">
          <i class="fa fa-bar-chart"></i>
        </div>
        <div class="sj-txt">
          Event Stats
        </div>
      </a>
      <a class="sj-tools" href="{{ $squadsurl }}">
        <div class="sj-ico">
          <i class="fa fa-users"></i>
        </div>
        <div class="sj-txt">
          Squads
        </div>
      </a>
      <!-- <a class="sj-tools" href="{{ $offstaturl }}">
                                            <div class="sj-ico">
                                              <i class="fa fa-area-chart"></i>
                                            </div>
                                            <div class="sj-txt">
                                              Official Stats
                                            </div>
                                          </a> -->

    </div>
    <div>
      @foreach($winner as $winners)
      <?php $winnerData = $winners->winner;
      if ($winnerData  == 1) {
        echo '<b>Winner :</b> ' . $winners->team1 . ' <i class="fa fa-trophy" aria-hidden="true" style="color:red;"></i>';
      } else {
        echo '';
      }
      if ($winnerData == 2) {
        echo '<b>Winner : </b>' . $winners->team2 . ' <i class="fa fa-trophy" aria-hidden="true" style="color:red;"></i>';
      } else {
        echo '';
      }
      ?>
      @endforeach
    </div>

    <div class="teams-container wide">

      @foreach($games as $game)

      <?php

      $venue_url = url($table_prefix . '/venues/' . $game->venue_id);

      if ($game->time != "") {
        $str2 = $game->time;
        //$str1=explode(":", $str);
        //$str2=$str1[0].":".$str1[1];
      } else {
        $str2 = "";
      }

      ?>
      <div class="matchbox">
        <div class="teams">
          <div class="team">
            <div class="badge badge-warning">Phase: {{ $game->phase }}</div>
            <div class="clearfix"></div>
            <!-- <div class="team-logo">
                                        <img src="images/slides/team-logo-1.png" alt=""/>
                                    </div> -->
            <?php
            $winner_icon_1 = '';
            $winner_icon_2 = '';

            if ($game->winner == 1) {
              $winner_icon_1 = '<i class="fa fa-trophy" aria-hidden="true"  style="color:red;"></i>';
            } elseif ($game->winner == 2) {
              $winner_icon_2 = '<i class="fa fa-trophy" aria-hidden="true"  style="color:red;"></i>';
            }

            if ($table_prefix == 'bb') {
              $url = url('/website/images/slides/basketball.jpg');
            } elseif ($table_prefix == 'fb') {
              $url = url('/website/images/slides/football.png');
            } elseif ($table_prefix == 'vb') {
              $url = url('/website/images/slides/volleyball.jpg');
            } elseif ($table_prefix == 'wr') {
              $url = url('/website/images/slides/wrestling.png');
            }
            $goals1 = '<img width="15px" src="' . $url . '">';
            $goals2 = '<img width="15px" src="' . $url . '">';
            ?>
            <div class="team-name">
              @if($game->status !='fixture')
              <h3>{!! $winner_icon_1 !!} {{ $game->team1 }} {!! $goals1.' '.$game->score1 !!} </h3>
              @else
              <h3> {{ $game->team1 }} </h3>
              @endif
            </div>
          </div>
          <div class="line-block" style="text-align:center; line-height:22px;">
            <span class="bullet">vs</span>
          </div>
          <div class="team">
            <div class="team-name">
              @if($game->status !='fixture')
              <h3>{!! $winner_icon_2 !!} {{ $game->team2 }} {!! $goals2.' '.$game->score2 !!} </h3>
              @else
              <h3> {{ $game->team2 }} </h3>
              @endif
            </div>
            <?php
            if ($game->status != 'fixture') {
              $url = url($table_prefix . '/scorecard/' . $game->game_id);

            ?>

              <div>
                <a class="btn btn-primary btn-sm" href="{{ $url }}">View Scorecard</a>
              </div>

            <?php } else {
            }
            ?>

            <!-- <div class="team-logo">
                                        <img src="images/slides/team-logo-2.png" alt=""/>
                                    </div> -->
          </div>
        </div>
        <div class="triangular">
          &nbsp;
        </div>
        <div class="match-details">
          <div class="bg"> </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="each-det">
                <div class="left-icon">
                  <i class="fa fa-calendar"></i>
                </div>
                <div class="right-value">
                  <div class="title">Date</div>
                  <div class="value">{{ date("d M Y", strtotime($game->date)) }}</div>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="each-det">
                <div class="left-icon">
                  <i class="fa fa-clock-o"></i>
                </div>
                <div class="right-value">
                  <div class="title">Time</div>
                  <div class="value">{{ $str2 }}</div>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="each-det">
                <div class="left-icon">
                  <i class="fa fa-map-marker"></i>
                </div>
                <div class="right-value">
                  <div class="title">Location</div>
                  <div class="value">
                    <a class="url_inside_orange" href="{{ $venue_url }}">{{ $game->venue }} -
                      @if($game->subname != null)

                      {{ $game->subname }} ,
                      @endif
                      @if($game->regionname != null)

                      {{ $game->regionname }} ,
                      @endif
                      {{ $game->countryname }}
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="each-det phase">
                <div class="left-icon">
                  <i class="fa fa-flag"></i>
                </div>
                <div class="right-value">
                  <div class="title">Phase</div>
                  <div class="value">Group B</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      @endforeach

    </div>

  </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
  var APP_URL = {
    !!json_encode(url('/')) !!
  }
  $('#season').on('select2:select', function(e) {
    var data = e.params.data;
    var season = data.text;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById('country_id').innerText = null;
        document.getElementById('selectedseason').value = season;
        var options = JSON.parse(this.responseText);
        Object.keys(options).forEach(function(key) {
          var el = document.createElement("option");
          el.textContent = options[key];
          el.value = key;
          document.getElementById("country_id").appendChild(el);
        })
      }
    };
    xhttp.open("GET", APP_URL + '/getcountries?season=' + season, true);
    xhttp.send();
  });
  $('#country_id').on('select2:select', function(e) {
    var data = e.params.data;
    var country_id = data.id;
    var season = document.getElementById('season').value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById('events').innerText = null;

        var options = JSON.parse(this.responseText);
        Object.keys(options).forEach(function(key) {
          var el = document.createElement("option");
          el.textContent = options[key];
          el.value = key;
          document.getElementById("events").appendChild(el);
        })
      }
    };
    xhttp.open("GET", APP_URL + '/getevents?season=' + season + '&country_id=' + country_id, true);
    xhttp.send();
  });

  // $('#events').on('select2:select', function (e) {
  //   var data = e.params.data;
  //   var event_id = data.id;

  //   document.location.href = APP_URL+'/events/'+event_id;
  // });
  $('#events').change(function() {
    var data = document.getElementById('events').value;
    alert(data);
  });

  $('#select-season').change(function() {
    var season = $(this).val();

    season = season.replace("/", "-");
    // var data=e.params.data;
    // var team_id=data.id;
    document.location.href = APP_URL + '/<?= $table_prefix ?>/events/season/' + season;
  });
</script>
@endsection