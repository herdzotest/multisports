<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>SPORTS COUNCIL</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <link href="{{ url('/website') }}/images/favicon.png" rel="icon">
        <link href="{{ url('/website') }}/images/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Fjalla+One|Russo+One&display=swap" rel="stylesheet">
        <link href="{{ url('/website') }}/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{ url('/website') }}/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{ url('/website') }}/lib/animate/animate.min.css" rel="stylesheet">
        <link href="{{ url('/website') }}/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="{{ url('/website') }}/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="{{ url('/website') }}/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
        <link href="{{ url('/website') }}/css/style.css" rel="stylesheet">
        <link href="{{ url('/website') }}/css/custom.css" rel="stylesheet">
    </head>

    <body>

               <?php 
       if(session('table_prefix')!=null){
        $table_prefix=session('table_prefix');
       }else{
        $table_prefix="bb";
        session(['table_prefix' =>  $table_prefix]);
       }

       /*$switcher_arr=array('at'=>'ATHLETICS','bb'=>'BASKETBALL','bo' => 'Boxing','fb'=>'FOOTBALL','ju' => 'Judo','ta' => 'Taekwondo','vb'=>'VOLLEYBALL','wr'=>'WRESTLING');*/
       $switcher_arr=array('bb'=>'BASKETBALL','fb'=>'FOOTBALL','vb'=>'VOLLEYBALL','wr'=>'WRESTLING','at'=>'ATHLETICS','ju'=>'JUDO','ta'=>'TAEKWONDO');
           ?>
 <div id="main-wrapper">
        <header id="header">
            <div class="container"> 
                <div class="logo float-left">
                    <a href="#header" class="scrollto"><img src="{{ url('/website') }}/images/sports-kerala.svg" alt="" class="img-fluid"></a> 
                </div>
                <nav class="main-nav float-right d-none d-lg-block">
                    <ul>
                        <li><a href="{{ url('/web') }}">HOME</a> </li>
                        <!-- <li><a href="{{ url('/news') }}">NEWS</a> </li> -->
                        <li><a href="{{ url($table_prefix.'/events') }}">EVENTS</a> </li>
                        <!-- <li><a href="{{ url($table_prefix.'/teams') }}">TEAMS</a> </li>-->
						<li><a href="{{ url($table_prefix.'/teams/n/a') }}">TEAMS</a>
                        <li><a href="{{ url($table_prefix.'/m/players') }}">PLAYERS</a> </li>
                        <li><a href="{{ url($table_prefix.'/venues') }}">VENUES</a> </li>
                        <?php  $awardnames = DB::table('award')->select('award_name','award_name_slug')->first(); 
                        $val = url('achievements/'.$awardnames->award_name_slug);?>
                        <li><a href="{{ $val }}">ACHIEVEMENTS</a> </li>
                        <li><a href="{{ url('/docsearch') }}">DOCUMENT SEARCH</a> </li>
                        <li><a href="{{ url('/annualreport') }}">ANNUAL REPORT</a> </li>
                        <li><a href="{{ url('/calendar') }}">CALENDAR</a> </li>
                        <li>
                            <div class="dropdown show">
                                <a class="btn-link   dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    SPORTS
                                </a>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    @foreach($switcher_arr as $key => $value)
                                    <a class="dropdown-item switcher" data-sport="{{ $key }}" href="#">{{ $value }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>

        <form id="sport_switcher_form" action="{{ url('/setsessionnew') }}" method="POST">
  {{ csrf_field() }}
  <input id="sport_switcher" type="hidden" name="sport_switcher" value="">
  
</form>
        <div id="carouselIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselIndicators" data-slide-to="1"></li>
                <li data-target="#carouselIndicators" data-slide-to="2"></li>
                <li data-target="#carouselIndicators" data-slide-to="3"></li>
                <li data-target="#carouselIndicators" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="img-fluid" src="{{ url('/website') }}/images/slides/basketballnew.jpg" alt=""/>

                </div>
                <div class="carousel-item">
                    <!--<img class="img-fluid" src="{{ url('/website') }}/images/slides/football.jpg" alt=""/>-->
                    <img class="img-fluid" src="{{ url('/website') }}/images/slides/ptusha.png" alt=""/>
                </div>
                <div class="carousel-item">
                    <img class="img-fluid" src="{{ url('/website') }}/images/slides/volleyball1.jpg" alt=""/>
                </div>
                <div class="carousel-item">
                    <!--<img class="img-fluid" src="{{ url('/website') }}/images/slides/sport.jpg" alt=""/>-->
                        <img class="img-fluid" src="{{ url('/website') }}/images/slides/banner2.png" alt=""/>


                </div>
                <div class="carousel-item">
                    <img class="img-fluid" src="{{ url('/website') }}/images/slides/racing.jpg" alt=""/>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <main id="main">
          @yield('content')
        </main>

        <footer id="footer" class="section-bg">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h2><span>SPORTS</span>FOLLOW US</h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="social-links">
                                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bot">
                <div class="container text-center">
                    &copy; 2020 Copyright <strong>Directorate of Sports and Youth Affairs
</strong>. All Rights Reserved 
                    <br>
                    <small>Designed and Developed by <a href="#">CricketArchive Pvt Ltd</a></small>
                </div>
            </div>
        </footer>

        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        <div id="preloader"></div> 

        <script src="{{ url('/website') }}/lib/jquery/jquery.min.js"></script>
        <script src="{{ url('/website') }}/lib/jquery/jquery-migrate.min.js"></script>
        <script src="{{ url('/website') }}/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="{{ url('/website') }}/lib/easing/easing.min.js"></script>
        <script src="{{ url('/website') }}/lib/mobile-nav/mobile-nav.js"></script>
        <script src="{{ url('/website') }}/lib/wow/wow.min.js"></script>
        <script src="{{ url('/website') }}/lib/waypoints/waypoints.min.js"></script>
        <script src="{{ url('/website') }}/lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="{{ url('/website') }}/lib/isotope/isotope.pkgd.min.js"></script>
        <script src="{{ url('/website') }}/lib/lightbox/js/lightbox.min.js"></script>
        <script src="{{ url('/website') }}/lib/jquery.matchHeight.min.js" type="text/javascript"></script>
        <script src="{{ url('/website') }}/js/main.js"></script>


<script>
$(document).on('click', '.switcher', function (e) {
    e.preventDefault();
    var sport =$(this).data('sport');
    $("#sport_switcher").val(sport);
    $("#sport_switcher_form").submit();
});
</script>
<script type="text/javascript">
$(document).ready(function(){
     $("#carouselIndicators").carousel({
         interval : 2000,
         pause: false
     });
});
</script>
</div>
    </body>
</html>
