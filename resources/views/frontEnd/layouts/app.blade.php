
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 
  <link rel="shortcut icon" href="https://www.sportskerala.org/images/favicon-sports.ico" />
  <title>Welcome to Sports Kerala - Official Website of Directorate of Sports and Youth Affairs, Government of Kerala | Sports Kerala</title>
<meta name="keywords" content="Sports and Youth Affairs, Kerala Government, Sports Kerala, Sports, Sports and games" />
<meta name="description" content="Sports Kerala official website provides information related to sports and games in Kerala.  Also provides details of sports award winners, photos, videos, rules and rates for various sporting activities." />
<meta name="Title" content="Welcome to Sports Kerala - Official Website of Directorate of Sports and Youth Affairs, Government of Kerala | Sports Kerala| Sports Kerala" />
  <link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'/>
  <link href="https://www.sportskerala.org/css/sports-css.css"  rel="stylesheet" />
  <link href="https://www.sportskerala.org/css/demo.css"  rel="stylesheet" />
  <link rel="stylesheet" href="<?=url('/frontendnew/resources/styles.min.css')?>">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald:400,700" rel="stylesheet">
        <link rel="shortcut icon" href="<?=url('/frontendnew/resources/images/favicon/favicon.ico')?>" type="image/x-icon">
        <link rel="icon" href="<?=url('/frontendnew/resources/images/favicon/favicon.ico')?>" type="image/x-icon">
  <!--<link href="css/common.css"  rel="stylesheet" />
-->
  <script src="https://www.sportskerala.org/js/jquery-1.10.2.js"></script>
  <script src="https://www.sportskerala.org/js/jquery.js"></script>
<script src="https://www.sportskerala.org/js/bootstrap.js"></script>
  <link rel="stylesheet" href="https://www.sportskerala.org/css/bootstrap.min.css"/>
  <script src="https://www.sportskerala.org/js/bootstrap-dropdown.js"></script>

  
  <script src="https://www.sportskerala.org/js/jquery.easing.1.3.js"></script>
  <script src="https://www.sportskerala.org/js/wow.js"></script>
  <link href="https://www.sportskerala.org/css/animate.css" rel="stylesheet" type="text/css" />
  <link href="https://www.sportskerala.org/css/global.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://www.sportskerala.org/css/normalize_1.css" />
  <link rel="stylesheet" type="text/css" href="https://www.sportskerala.org/css/demo_1.css" />
  <link rel="stylesheet" type="text/css" href="https://www.sportskerala.org/css/component_1.css" />
  <!-- <script src="https://www.sportskerala.org/js/modernizr.custom_1.js"></script> -->
  <!-- <script src="https://www.sportskerala.org/js/common/modernizr.js"></script> -->
  <link rel="stylesheet" href="https://www.sportskerala.org/css/demo_slider.css"/>
  <link rel="stylesheet" href="https://www.sportskerala.org/css/responsiveslides.css"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <script src="https://www.sportskerala.org/js/responsiveslides.min.js"></script>


  <!-- 	<script type="text/javascript" src="js/jquery-1.4.min.js"></script>
--><script type="text/javascript" src="https://www.sportskerala.org/js/common.js"></script>
  <link rel="stylesheet" type="text/css" href="https://www.sportskerala.org/css/component.css" />
  <!-- <script src="https://www.sportskerala.org/js/modernizr.custom.js"></script> -->
  <link rel="stylesheet" href="https://www.sportskerala.org/css/screen.css" type="text/css" media="all" />
  
 
        <style>
       
        .switcher {
        color: #000000;
        font-family: 'Oswald', sans-serif;
        font-weight: normal;
        font-size: 17px;
        padding-top: 15px;
        padding-bottom: 15px;
        line-height: 20px;
        position: relative;
        display: block;
        padding: 10px 12px;

        }
         .list {
        color: #000000;
        font-family: 'Oswald', sans-serif;
        font-weight: normal;
        font-size: 17px;
        padding-top: 15px;
        padding-bottom: 15px;
        line-height: 20px;
        position: relative;
        display: block;
        padding: 10px 12px;

        }

      body a {
        color: #428bca;
      }

        </style>
  </head>

  <body>
   
<div class="row" style="padding:0px">
    <div class="row" style="position:absolute">
    <div class="container" >
        <ul id="gn-menu" class="gn-menu-main" > 
       
       
      </ul>
      </div>
  </div>
 
    

    <div class="row" style="background-color:#000; margin-top:-2px">
    <div class="container" style="padding:0px">
        <nav class="navbar navbar-inverse navbar-custom" role="navigation">
        <div class="navbar-header">
        <button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
      </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
       <?php 
       if(session('table_prefix')!=null){
        $table_prefix=session('table_prefix');
       }else{
        $table_prefix="bb";
        session(['table_prefix' =>  $table_prefix]);
       }

       //$switcher_arr=array('bb'=>'BASKETBALL','fb'=>'FOOTBALL','vb'=>'VOLLEYBALL','wr'=>'WRESTLING');
       
       $switcher_arr=array('at'=>'ATHLETICS','bb'=>'BASKETBALL','bo' => 'Boxing','fb'=>'FOOTBALL','ju' => 'Judo','ta' => 'Taekwondo','vb'=>'VOLLEYBALL','wr'=>'WRESTLING');
           ?>
            <li><a href="{{ url('/web') }}">HOME</a> </li>
            <!-- <li><a href="{{ url('/news') }}">NEWS</a> </li> -->
            <li><a href="{{ url($table_prefix.'/events') }}">EVENTS</a> </li>
           <!--  @if($table_prefix!='wr' && $table_prefix!='ju' && $table_prefix!='ta' && $table_prefix!='bo') --> 
            <li><a href="{{ url($table_prefix.'/teams') }}">TEAMS</a> </li>
            <!-- @endif -->
            <li><a href="{{ url($table_prefix.'/players') }}">PLAYERS</a> </li>
            <li><a href="{{ url($table_prefix.'/venues') }}">VENUES</a> </li>
            <li><a href="{{ url('/docsearch') }}">DOCUMENT SEARCH</a> </li>
            <!-- <li><a href="{{ url($table_prefix.'/player_district_search') }}">PLAYER SEARCH</a> </li> -->
            <li><a href="{{ url('flipbook') }}">ANNUAL REPORT</a> </li>
            <!-- <li><a href="{{ url($table_prefix.'/archive/events/seasons') }}">ARCHIVE</a> </li> -->
            <li>
              <select class="switcher" id="switcher">
                @foreach($switcher_arr as $key => $value)

                <option {{ $key==$table_prefix ? 'selected' :' ' }} value="{{ $key }}">{{ $value }}</option>
                @endforeach
              </select>
            
            </li>
          </ul>
      </div>
      </div>
  </div>
  </div>


<form id="sport_switcher_form" action="{{ url('/setsessionnew') }}" method="POST">
  {{ csrf_field() }}
  <input id="sport_switcher" type="hidden" name="sport_switcher" value="">
  
</form>




  

 
<script>
$("#switcher").change(function(){
var e =$(this).val();
$("#sport_switcher").val(e);
$("#sport_switcher_form").submit();
});


</script>

@yield('content')




<div class="row bg" style="padding:2% 0">
    <div class="container">
    <div class="col-sm-12" style="padding:0">
        <div class="cl20 " style="float:left; text-align:left">
        <ul class="bottom_menu">
            <!-- <li><a href="#">Downloads</a></li> -->
            <!-- <li><a href="#">Rules</a></li>-->
            <li><a href="https://www.sportskerala.org/rti">RTI</a></li>
            <li><a href="https://www.sportskerala.org/pdfs/sportsact.pdf" target="_blank"  >Sports Act</a></li> 
            <li><a href="https://www.sportskerala.org/pdfs/sports-bill-2010.pdf" target="_blank"  >Sports Bill</a></li>
            <li><a href="https://www.sportskerala.org/pdfs/kerala-sports-policy2012-englishversion.pdf" target="_blank"  >Sports Policy 2012</a></li>
            
                        <li><a href="https://www.sportskerala.org/tenders">Tenders</a></li>            

            <!-- <li><a href="#">Sports Council</a></li> -->
          </ul>
      </div>
        <div class="cl20" style="float:left; text-align:left">
        <ul class="bottom_menu">
        	<li><a href="https://www.sportskerala.org/news">News And Updates</a></li>
        	<li><a href="https://www.sportskerala.org/award">Awards</a></li>
            <li><a href="https://www.sportskerala.org/video/view">Videos</a></li>
            <!-- <li><a href="#">FAQs</a></li> -->
            
            <li><a href="https://www.sportskerala.org/picture">Photos</a></li>
            			<li><a href="https://www.sportskerala.org/contact">Contact Us</a></li>

        </ul>
      </div>
        <!--<div class="col-sm-4 pad_R" style="float:left; text-align:left">
        <ul class="bottom_menu">
            			
          </ul>
      </div>-->
      

      <div class="cl20 " style=" float:left">
            <a href="http://www.sportscouncil.kerala.gov.in" target="_blank" ><img src="https://www.sportskerala.org/images/Sports-council-logo.png" width="114" height="95" style="margin-left:25px"/></a><br />
			<p">  Kerala State Sports Council</p>
 		</div>
      		 <div class="cl20" style="center; float:left">
            <a href="http://www.kerala2015.com" target="_blank" ><img src="https://www.sportskerala.org/images/National-games-logo.png" width="114" height="95" /></a><br />
 				35th National Games
                  </div>
      </div>
      
    <div class="clear"></div>
    <div class="col-sm-12 wow fadeIn " style="padding:0px; margin-top:5%; background-color:#FFF; margin-bottom:10px">
        <div class="col-sm-7 " style="padding:0px">
      <!--  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1972.9563915645942!2d76.95897827588603!3d8.507849251157817!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b05bbcbb820aaa5%3A0x5ffc0270988d396d!2sJimmy+George+Indoor+Stadium!5e0!3m2!1sen!2sin!4v1458819900142" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>--> 
      
       <link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700|Open+Sans+Condensed:700,300,300italic|Open+Sans:400,300italic,400italic,600,600italic,700,700italic,800,800italic|PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
  	<link href='https://fonts.googleapis.com/css?family=Rock+Salt' rel='stylesheet' type='text/css'/>
    <link rel="stylesheet" type="text/css" href="https://www.sportskerala.org/css/style.css">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.20&sensor=false"></script>
   <script type="text/javascript" language="javascript">

/*
 * 5 ways to customize the infowindow
 * 2015 - en.marnoto.com
*/

// map center
var center = new google.maps.LatLng(8.508133, 76.960107);

// marker position
var factory = new google.maps.LatLng(8.508133, 76.960107);

function initialize() {
  var mapOptions = {
    center: center,
    zoom: 18,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  var map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);

  // InfoWindow content
  var content = '<div id="iw-container">' +
                    '<div class="iw-title">Jimmy George Indoor Stadium</div>' +
                    '<div class="iw-content">' +
                       '<img src="'+'https://www.sportskerala.org/'+'images/map/jimmy.jpg" alt="Jimmy George Indoor Stadium" height="120" width="100">' +
                      '<div class="iw-subTitle">Contacts</div>' +
                      '<p>Directorate of Sports and Youth Affairs Jimmy George Indoor Stadium Vellayambalam, Thiruvananthapuram,<br> Kerala - 695033<br>'+
                       '<br>Tel: +91 471 2327271<br>Fax: +91 471 2327271<br>Email: dsyagok@gmail.com</p>'+

                      '</p>'+
                    '</div>' +
                    '<div class="iw-bottom-gradient"></div>' +
                  '</div>';
                  
                  
               

  // A new Info Window is created and set content
  var infowindow = new google.maps.InfoWindow({
    content: content,

    // Assign a maximum value for the width of the infowindow allows
    // greater control over the various content elements
    maxWidth: 350
  });
   
  // marker options
  var marker = new google.maps.Marker({
    position: factory,
    map: map,
    title:"Jimmy George Indoor Stadium"
  });

  // This event expects a click on a marker
  // When this event is fired the Info Window is opened.
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });

  // Event that closes the Info Window with a click on the map
  google.maps.event.addListener(map, 'click', function() {
    infowindow.close();
  });

  // *
  // START INFOWINDOW CUSTOMIZE.
  // The google.maps.event.addListener() event expects
  // the creation of the infowindow HTML structure 'domready'
  // and before the opening of the infowindow, defined styles are applied.
  // *
  google.maps.event.addListener(infowindow, 'domready', function() {

    // Reference to the DIV that wraps the bottom of infowindow
    var iwOuter = $('.gm-style-iw');

    /* Since this div is in a position prior to .gm-div style-iw.
     * We use jQuery and create a iwBackground variable,
     * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
    */
    var iwBackground = iwOuter.prev();

    // Removes background shadow DIV
    iwBackground.children(':nth-child(2)').css({'display' : 'none'});

    // Removes white background DIV
    iwBackground.children(':nth-child(4)').css({'display' : 'none'});

    // Moves the infowindow 115px to the right.
    iwOuter.parent().parent().css({left: '115px'});

    // Moves the shadow of the arrow 76px to the left margin.
    iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

    // Moves the arrow 76px to the left margin.
    iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

    // Changes the desired tail shadow color.
    iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

    // Reference to the div that groups the close button elements.
    var iwCloseBtn = iwOuter.next();

    // Apply the desired effect to the close button
    iwCloseBtn.css({   width: '30px', height: '30px', overflow: 'hidden',position: 'absolute',opacity: '1', right: '38px', top: '3px', border: '8px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});

    // If th

  </script>


  @yield('scripts')