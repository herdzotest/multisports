<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>data4 basketball | Events</title>
        <link rel="shortcut icon" href="resources/images/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="resources/images/favicon/favicon.ico" type="image/x-icon">


        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald:400,700" rel="stylesheet">

        <!--build:css resources/styles.min.css-->
        <link href="resources/lib/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="resources/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="resources/lib/owl.carousel.css" rel="stylesheet" type="text/css"/>
        <link href="resources/lib/owl.theme.default.min.css" rel="stylesheet" type="text/css"/>
        <link href="resources/lib/offcanvas.min.css" rel="stylesheet" type="text/css"/>
        <!--<link href="resources/lib/footable.standalone.css" rel="stylesheet" type="text/css"/>-->
        <!--<link href="resources/lib/footable.core.css" rel="stylesheet" type="text/css"/>-->
        <link href="resources/lib/footable.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="resources/lib/select2.min.css" rel="stylesheet" type="text/css"/>
        <link href="resources/styles.css" rel="stylesheet" type="text/css"/>
        <!--endbuild-->

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <div id="main-wrapper">
            <div class="header">
                <div class="container">
                    <div class="header-section1">
                        <div class="row">
                            <div class="col-md-3">
                                <a  href="/"><img class="img-responsive" src="resources/images/logo-ks-01.png" alt=""></a>
                            </div>
                            <div class="col-md-7">
                                <nav id="nav" class="clearfix">
                                    <ul class="clearfix">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">Events</a></li>
                                        <li><a href="#">Players</a></li>
                                        <li><a href="#">Venues</a></li>
                                        <li><a href="#">Document Search</a></li>
                                        <li><a href="#">Player Search</a></li>  
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-md-2">
                                <div class="social_icon">
                                    <a class="icons-sm fb-ic"><i class="fa fa-facebook"></i></a>
                                    <a class="icons-sm tw-ic"><i class="fa fa-twitter"></i></a>
                                    <a class="icons-sm gplus-ic"><i class="fa fa-google-plus"> </i></a>
                                    <a class="icons-sm pin-ic"><i class="fa fa-pinterest"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@yield('content')
           

            <!-- Footer Section -->
            <footer class="footer_section" >
                <div class="footer_top_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="text_widget footer_widget">
                                    <div class="footer_widget_title">
                                        <h2>About Data4Basketball</h2>
                                    </div>
                                    <div class="footer_widget_content">
                                        There are many variations of passages of Lorem Ipsum 
                                        available, but the majority have suffered alteration 
                                        in some form, by injected humour, or randomised words 
                                        which don't look even slightly believable. If you are 
                                        going to use a passage of Lorem Ipsum, you need to be 
                                        sure there isn't anything embarrassing hidden in the 
                                        middle of text.making this the first true generator 
                                        on the Internet.
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="footer_widget">
                                    <div class="footer_widget_title"><h2>QUICK LINKS</h2></div>
                                    <div class="footer_menu_item ">
                                        <div class="row">
                                            <div class="col-sm-4"> 
                                                <ul class="nav navbar-nav ">
                                                    <li><a href="#">HOME</a></li>
                                                    <li><a href="#">NEWS</a></li>
                                                    <li><a href="#">TOURNAMENTS</a></li>
                                                    <li><a href="#">TEAMS</a></li>
                                                    <li><a href="#">PLAYERS</a></li>
                                                    <li><a href="#">VENUE</a></li>
                                                    <li><a href="#">LIVE NOW</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="text_widget footer_widget">
                                    <div class="footer_widget_title">
                                        <h2>Follow Us</h2>
                                    </div>
                                    <div class="footer_widget_content">
                                        <div class="social_icon">
                                            <a class="icons-sm fb-ic"><i class="fa fa-facebook"></i></a>
                                            <a class="icons-sm tw-ic"><i class="fa fa-twitter"></i></a>
                                            <a class="icons-sm gplus-ic"><i class="fa fa-google-plus"> </i></a>
                                            <a class="icons-sm pin-ic"><i class="fa fa-pinterest"></i></a>
                                        </div>
                                    </div>

                                    <div class="footer_widget_follow_up">
                                        <div class="footer_widget_title">
                                            <h2>Newsletter</h2>
                                        </div>
                                        <div class="footer_widget_content">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Email Address">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button">Subscribe</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="footer_widget">
                                    <div class="footer-logo">
                                        <img src="resources/images/data4b-logo-footer.svg" alt=""/>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="address">
                                        <div class="name">Data4Sports</div>
                                        G5, Lane 1, Tagore Nagar, Vazhuthacaud,<br>
                                        Thiruvananthapuram, Kerala, India<br>
                                        PIN - 695035<br><br>

                                        <strong>Phone </strong> : +91 - 123 456 7890<br>
                                        <strong>Email </strong> : info@dataforsports.com
                                    </div>
                                </div>
                            </div>

                            <!--##############################################-->
                        </div>
                    </div>
                </div>

                <div class="copyright-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="copyright">
                                    2017 © Copyright data4basketball, All rights reserved.
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="design-by">
                                    <div class="text-right">Design By&nbsp; <a href="http://www.ergate.in/" target="_blank"><img src="resources/images/ergate-logo-footer.svg" alt="Ergate - We Digitize"></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

        </div>




        <!--go-top-->
        <a href="#" class="go-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <script src="resources/scripts.min.js"></script>
    </body>
</html>
