@extends('frontEnd/layouts.web')
@section('content')
<?php  $url=URL::to('/'); ?>
<?php $table_prefix= \Session::get('table_prefix') ?>
<!--breadcrumb-->
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ url('') }}">Home</a></li>
            <li><a href="{{ url($table_prefix.'/events') }}">Events</a></li> 
            <li><a href="{{ url($table_prefix.'/events'.'/'.$event_id) }}">{{ $event_name }}</a></li> 
            <li class="active">Event Stats</li>
        </ol>
         <h2 style="margin-top: -42px;text-align: right;">
                      
                          @if($table_prefix == 'at')
                            Athletics
                            @elseif($table_prefix == 'bb')
                            Basketball
                            @elseif($table_prefix == 'bo')
                            Boxing
                            @elseif($table_prefix == 'fb')
                            Football
                            @elseif($table_prefix == 'ju')
                            Judo
                            @elseif($table_prefix == 'ta')
                            Taekwondo
                            @elseif($table_prefix == 'vb')  
                            Volleyball
                            @elseif($table_prefix == 'wr')  
                            Wrestling                        
                            @endif
                    </h2>
    </div>
</div>
<!--breadcrumb-->
<!--profile section-->
<section id="team_profile_section" class="team_profile_section section_wrapper">
  <div class="container">                    
    <div class="row">
      <div class="col-md-12">
      <h3>{{ $event_name }} : Player Statistics</h3>
      <div class="table-holder">
        <table class="table table-striped table-sm datatable">
          <thead>
            <tr>
              <th data-type="number">Jersey</th>
              <th>Player</th>
              <th>Team</th>
              <th data-type="number">Games</th>
            </tr>
          </thead>
          <tbody>
              @if($playerstats=="")
              <tr>
                <td colspan="4">No data found</td>
              </tr>
              @endif
          </tbody>
        </table>
      </div>
    </div>
    </div>
  </div>
</section>
<!--profile section-->
@endsection


