@extends('frontEnd/layouts.web')
@section('content')
<!--breadcrumb-->
<?php $table_prefix= \Session::get('table_prefix') ?>
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ url('') }}">Home</a></li>
            <li><a href="{{ url($table_prefix.'/events') }}">Events</a></li> 
            <li><a href="{{ url($table_prefix.'/events'.'/'.$event_id) }}">{{ $event_name }}</a></li> 
            <li class="active">Event Squad</li>
        </ol>
        </ol>
         <h2 style="margin-top: -42px;text-align: right;">
                      
                          @if($table_prefix == 'at')
                            Athletics
                            @elseif($table_prefix == 'bb')
                            Basketball
                            @elseif($table_prefix == 'bo')
                            Boxing
                            @elseif($table_prefix == 'fb')
                            Football
                            @elseif($table_prefix == 'ju')
                            Judo
                            @elseif($table_prefix == 'ta')
                            Taekwondo
                            @elseif($table_prefix == 'vb')  
                            Volleyball
                            @elseif($table_prefix == 'wr')  
                            Wrestling                        
                            @endif
                    </h2>
    </div>
</div>
<!--breadcrumb-->
<!--profile section-->
<section id="player_profile_section" class="player_profile_section section_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
              <h3>Squad List</h3>
               <div class="table-holder">
                  <table class="table table-striped table-sm datatable">
                    <thead>
                      <tr>
                        <th>Team</th>
                        <th>Player</th>
                        <th>Style</th>
                        <th>Weight Category</th>
                        <th>Weight</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i=0;   ?>
                      @foreach($eventsquads as $item1)
                      <?php $i++;   ?>
                      <tr>                                               
                        <td><a href="{{ url($table_prefix.'/teams/t/'.$item1->team_id) }}">{{ $item1->team['name'] }}</a></td>
                        <td><a href="{{ url($table_prefix.'/players/'.$item1->player_id) }}">{{ $item1->player['full_name'] }}</a></td>
                        <td>{{ $item1->wrstyle['style_name'] }}</td>
                        <td>{{ $item1->wtcat['category_desc'] }}</td>
                        <td>{{ $item1->weight }}</td>
                      </tr>
                      @endforeach
                      <?php if($i==0){?>
                      <tr>
                        <td colspan="2"><b>No Event details found for the selection.</b></td>
                      </tr>
                      <?php } ?> 
                    </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
</section>
<!--profile section-->
@endsection
