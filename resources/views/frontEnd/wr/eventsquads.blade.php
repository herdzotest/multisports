@extends('frontEnd/layouts.web')
@section('content')
<!--breadcrumb-->
<?php $table_prefix = \Session::get('table_prefix') ?>
<div class="inner-top-img">
  <div class="container">
    @if($table_prefix == 'bb')
    <img src="{{ url('website/images/inside-top.jpg') }}" alt="" class="img-responsive" />
    @endif
  </div>
</div>
<div class="container">
  <div class="event-stats">
    <h3>{{ $event_name }}</h3>
    <!-- <div class="clearfix">Home     |     Events</div> -->
    <table class="table table-borderbottom table-responsive-md datatable" id="tbladmin">
      <thead>
        <tr>
          <th colspan="15">Event Squad</th>
        </tr>
        <tr>
          <th>Team</th>
          <th>Player</th>
          <th>Style</th>
          <th>Weight Category</th>
          <th>Weight</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 0;   ?>
        @foreach($eventsquads as $item1)
        <?php $i++;   ?>
        <tr>
          <td><a href="{{ url($table_prefix.'/teams/t/'.$item1->team_id) }}">{{ $item1->team['name'] }}</a></td>
          <td><a href="{{ url($table_prefix.'/players/'.$item1->player_id) }}">{{ $item1->player['full_name'] }}</a></td>
          <td>{{ $item1->wrstyle['style_name'] }}</td>
          <td>{{ $item1->wtcat['category_desc'] }}</td>
          <td>{{ $item1->weight }}</td>
        </tr>
        @endforeach
        <?php if ($i == 0) { ?>
          <tr>
            <td colspan="5"><b>No Event details found for the selection.</b></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
@endsection