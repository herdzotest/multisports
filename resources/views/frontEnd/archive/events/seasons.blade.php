@extends('frontEnd/layouts.app')

@section('content')
<?php  
$table_prefix= \Session::get('table_prefix');
$url=URL::to('/'); ?>
 <!--blog details-->
                      <!--breadcrumb-->
            <div class="breadcrumb-container">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li class="">Archive</li>
                        <li class="active">Seasons</li>
                    </ol>
        <h2 style="margin-top: -42px;text-align: right;">
                      
                          @if($table_prefix == 'at')
                            Athletics
                            @elseif($table_prefix == 'bb')
                            Basketball
                            @elseif($table_prefix == 'bo')
                            Boxing
                            @elseif($table_prefix == 'fb')
                            Football
                            @elseif($table_prefix == 'ju')
                            Judo
                            @elseif($table_prefix == 'ta')
                            Taekwondo
                            @elseif($table_prefix == 'vb')  
                            Volleyball
                            @elseif($table_prefix == 'wr')  
                            Wrestling                        
                            @endif
                    </h2>
                </div>
            </div>
            <!--breadcrumb-->
            <section id="feature_news_section" class="feature_news_section section_wrapper">
                <div class="container">  
                    <div class="row">
                        <div class="col-md-9">
                            <div class="single_content_layout">
                                <div class="category_list">
                                    <?php $season_list=""; ?> 
                                @foreach($seasons as $item)
                                <?php 
                                    $season_hyphened = str_replace('/', '-', $item->season)
                                ?> 

                                        <a href="{{ url('/archive/events/seasons/'.$season_hyphened) }}"><?=$item->season?></a>

                               @endforeach    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                                        <div class="ad">
                                            <img class="img-responsive" src="frontendnew/resources/images/advt-250-250.jpg" alt="img" />
                                        </div>
                                        <div class="ad">
                                            <div class="video-thumbnail">
                                               <!--  <img class="img-responsive video-thumb" src="frontendnew/resources/images/video.jpg" alt="img" /> -->
                                               <video width="250" height="200" controls poster="frontendnew/resources/images/thumb.jpg">
                                                <source src="frontendnew/resources/images/video.mp4" type="video/mp4">
                                                </video>
                                            </div>
                                        </div>
                        </div>
                    </div>
                </div>
            </section>
           
            <!--blog details-->

        <!--go-top-->
        <a href="#" class="go-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

@endsection