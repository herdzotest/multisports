@extends('frontEnd/layouts.app')

@section('content')
<?php  
$table_prefix= \Session::get('table_prefix');
$url=URL::to('/'); ?>
 <!--blog details-->
                        <!--breadcrumb-->
            <div class="breadcrumb-container">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li class="">Archive</li>
                        <li class=""><a href="{{ url('/archive/events/seasons') }}">Seasons</a></li>
                        <li class="active">Events</li>
                    </ol>
        <h2 style="margin-top: -42px;text-align: right;">
                      
                          @if($table_prefix == 'at')
                            Athletics
                            @elseif($table_prefix == 'bb')
                            Basketball
                            @elseif($table_prefix == 'bo')
                            Boxing
                            @elseif($table_prefix == 'fb')
                            Football
                            @elseif($table_prefix == 'ju')
                            Judo
                            @elseif($table_prefix == 'ta')
                            Taekwondo
                            @elseif($table_prefix == 'vb')  
                            Volleyball
                            @elseif($table_prefix == 'wr')  
                            Wrestling                        
                            @endif
                    </h2>
                </div>
            </div>
            <!--breadcrumb-->        
            <section id="feature_news_section" class="feature_news_section section_wrapper">
                <div class="container">  
                    <div class="row">
                        <div class="col-md-9">
                            <div class="single_content_layout">
                                <div class="table-holder">
                                        <table class="table table-striped table-condensed footable footable-filtering"  data-filterable="true">
                                            <thead><tr>
                                                    <th class="text-left">Event</th>
                                                    <th class="text-left">Country</th>
                                                </tr></thead>
                                            <tbody>
                                    <?php $season_list=""; ?> 
                                @foreach($events as $item)
<tr>
                                                    <td class="text-left"><a href="{{ url('/events/'.$item->event_id) }}"><?=$item->name?></a></td>
                                                    <td class="text-left"><?=$item->country['name']?></td>
                                                </tr>

                               @endforeach    

                           </tbody>
                       </table>
                   </div>
                               
                            </div>
                        </div>
                        <div class="col-md-3">
                                        <div class="ad">
                                            <img class="img-responsive" src="{{ url('frontendnew/resources/images/advt-250-250.jpg') }}" alt="img" />
                                        </div>
                                        <div class="ad">
                                            <div class="video-thumbnail">
                                               <!--  <img class="img-responsive video-thumb" src="frontendnew/resources/images/video.jpg" alt="img" /> -->
                                               <video width="250" height="200" controls poster="{{ url('frontendnew/resources/images/thumb.jpg') }}">
                                                <source src="{{ url('frontendnew/resources/images/video.mp4') }}" type="video/mp4">
                                                </video>
                                            </div>
                                        </div>
                        </div>
                    </div>
                </div>
            </section>
           
            <!--blog details-->

        <!--go-top-->
        <a href="#" class="go-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

@endsection