 @extends('frontEnd/layouts.app')
@section('content')
<?php $table_prefix= \Session::get('table_prefix'); ?>
<!-- <div class="container" id="events-container"> -->
  <!-- <div class="main-container"> -->
<!--   <div class="row page-title">
  <h4 class="col-lg-2">PLAYER STATS</h4> -->

   <!--breadcrumb-->
            <div class="breadcrumb-container">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/home') }}">Home</a></li>
                        <li><a href="{{ url('/events') }}">Events</a></li>
                        <li class="active"><a href="{{ url('/events/'.$event_id) }}">{{ $event_name }}</a></li>
                    </ol>
         <h2 style="margin-top: -42px;text-align: right;">
                      
                          @if($table_prefix == 'at')
                            Athletics
                            @elseif($table_prefix == 'bb')
                            Basketball
                            @elseif($table_prefix == 'bo')
                            Boxing
                            @elseif($table_prefix == 'fb')
                            Football
                            @elseif($table_prefix == 'ju')
                            Judo
                            @elseif($table_prefix == 'ta')
                            Taekwondo
                            @elseif($table_prefix == 'vb')  
                            Volleyball
                            @elseif($table_prefix == 'wr')  
                            Wrestling                        
                            @endif
                    </h2>
                </div>
            </div>
            <!--breadcrumb-->

  <!-- </div> -->



  <div id="demo1" class="collapse in">

<div class="tab-content match-list col-lg-12">
    <div id="home" class="tab-pane fade in active">       
        <div class="match-table col-lg-12">


<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#moff"><b>Match Officials</b></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#toff"><b>Match Team Officials</b></a>
  </li>
 
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane container active" id="moff">
    <table class="table table-striped table-condensed footable">
                                            <thead><tr>
                                              <th>Official Name</th>
                                              <th>As Referee</th>
                                              <th>As Commissioner</th>
                                              <th >As Time Keeper</th>
                                              <th >As Short Clock Operator</th>
                                              <th >As Scorer</th>
                                              <th> As Asst. Scorer</th>
                                                                                                  
                                                </tr></thead>
                                            <tbody>
                                                <?php $i=0;   ?>
                                                  @foreach($officialstats as $stats)
                                                  <?php  $i++; ?>
                                                  <tr>
                                                  <td><a href=<?=url('playersnew/'.$stats->player_id)?>>{{ stripslashes($stats->full_name) }}</a></td>
                                                  <td>{{ $stats->ref }}</td>
                                                  <td>{{ $stats->comm }}</td>
                                                  <td>{{ $stats->tk }}</td>
                                                  <td>{{ $stats->sh_clk_op }}</td>
                                                  <td>{{ $stats->sco }}</td>
                                                  <td>{{ $stats->as_sco }}</td>
                                               
                                                  </tr>
                                                  @endforeach
                                                <?php if($i==0){?>
                                            <tr>
                                             <td><b>No details found for the selection.</b></td>
                                             </tr>
                                             <?php } ?>
                                            </tbody>
    </table>
  </div>
  <div class="tab-pane container fade" id="toff">
    <table class="table table-striped table-condensed footable">
                                           <thead>
                                              <tr>
                                              <th>Official Name</th>
                                              <th>As Coach</th>
                                              <th>As Assistant Coach</th>
                                              
                                              </tr>
                                          </thead>
                                            <tbody>
                                                <?php $i=0;   ?>
                                                  @foreach($teamofficialstats as $stats)
                                                  <?php  $i++; ?>
                                                  <tr>
                                                  <td><a href=<?=url('playersnew/'.$stats->player_id)?>>{{ stripslashes($stats->full_name) }}</a></td>
                                                  <td>{{ $stats->coach }}</td>
                                                  <td>{{ $stats->as_coach }}</td>
                                                                                           
                                                  </tr>
                                                  @endforeach
                                                <?php if($i==0){?>
                                            <tr>
                                             <td><b>No details found for the selection.</b></td>
                                             </tr>
                                             <?php } ?>
                                            </tbody>
                 </table> 
  </div>
 
</div>
   
        </div>
    </div>    
  </div>
  </div>
</div>
<!-- </div> -->
<!-- </div> -->
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection