<?php $__env->startSection('content'); ?>
<?php  $url=URL::to('/'); ?>
<?php $table_prefix= \Session::get('table_prefix') ?>
<div class="inner-top-img">
    <div class="container">
      <!-- <img src="<?php echo e(url('website/images/inside-top.jpg')); ?>" alt="" class="img-responsive"/> -->
    </div>
  </div>
<div class="container"> 
  <div class="event-stats">      
    <h3><?php echo e($event_name); ?></h3>
      <table class="table table-borderbottom table-responsive-md datatable" id="tbladmin">
        <thead>
          <tr>
            <th colspan="15">Event Stats</th>
          </tr>
            <tr>
              <th data-type="number">Jersey</th>
              <th>Player</th>
              <th>Team</th>
              <th data-type="number">Games</th>
            </tr>
          </thead>
          <tbody>
              <?php if($playerstats==""): ?>
              <tr>
                <td colspan="4">No data found</td>
              </tr>
              <?php endif; ?>
          </tbody>
        </table>
      </div>
      </div>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('frontEnd/layouts.web', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/multisports/resources/views/frontEnd/wr/eventplayerstats.blade.php ENDPATH**/ ?>