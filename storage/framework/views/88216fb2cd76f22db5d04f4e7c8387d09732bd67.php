<?php $__env->startSection('content'); ?>
<?php
$url = URL::to('/');
$table_prefix = session()->get('table_prefix');
?>
<style type="text/css">
    .sportmargin {
        margin-top: 0px !important;
    }
</style>

<div class="inner-top-img">
    <div class="container">
        <?php if($table_prefix == 'wr'): ?>
        <img src="<?php echo e(url('website/images/wresting.jpg')); ?>" alt="" class="img-responsive" />
        <?php endif; ?>
    </div>
</div>

<div class="search-box-container">
    <div class="container">
        <?php if($table_prefix == 'wr'): ?>
        <div class="search-box">
            <?php else: ?>
            <div class="search-box sportmargin">
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">
                        <!-- <nav class="navbar navbar-expand-lg navbar-light">
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent"> -->
                        <nav>
                            <div id="navbarSupportedContent" class="searchcontent">
                                <form method="POST" action="<?= url($table_prefix . '/searchevent') ?>" id="eventsearchform" class="form-inline my-2 my-lg-0">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <div class="form-group has-search mr-2 select-eventlist">
                                        <?php echo Form::select('season_sel', $seasons, $defaultseason, ['class' => 'required form-control form-control-sm select searchcontent','id'=>'select-season']); ?>

                                    </div>

                                    <div class="form-group has-search mr-2 select-eventlist">
                                        <?php echo Form::select('age_group', $age_group, $age_group_sel, ['class' => 'form-control form-control-sm select searchcontent','id'=>'age_group']); ?>

                                    </div>

                                    <div class="form-group has-search mr-2 select-eventlist">
                                        <?php echo Form::select('category_sel', $category, $defaultcategory, ['class' => 'required form-control form-control-sm select searchcontent','id'=>'select-category']); ?>

                                    </div>

                                    <div class="form-group  has-search mr-2">
                                        <!-- <span class="fa fa-search form-control-feedback"></span> -->
                                        <input name="query_str" id="query_str" type="text" class="form-control searchcontent" value="<?php echo e($query_str); ?>" placeholder="Search">
                                    </div>

                                    <div class="form-group  has-search">
                                        <input class="btn btn-primary btn-sm searchcontent" type="submit" name="submit" value="Submit">
                                    </div>
                                </form>
                            </div>
                        </nav>
                    </div>
                </div>

            </div>
        </div>
    </div>



    <div class="search-result-container">
        <div class="container">
            <div class="title-bar">

                <h2>Events :
                    <?php if($table_prefix == 'at'): ?>
                    Athletics
                    <?php elseif($table_prefix == 'bb'): ?>
                    Basketball
                    <?php elseif($table_prefix == 'bo'): ?>
                    Boxing
                    <?php elseif($table_prefix == 'fb'): ?>
                    Football
                    <?php elseif($table_prefix == 'ju'): ?>
                    Judo
                    <?php elseif($table_prefix == 'ta'): ?>
                    Taekwondo
                    <?php elseif($table_prefix == 'vb'): ?>
                    Volleyball
                    <?php elseif($table_prefix == 'wr'): ?>
                    Wrestling
                    <?php endif; ?>
                </h2>
                <div class="clearfix">&nbsp;</div>
                <div class="clearfix">
                    <ol class="breadcrumb">
                        <li><a href="http://multisports.in">Home</a></li>
                        <li class="active">Events</li>
                    </ol>
                </div>
            </div>
            <!--   <div class="clearfix">&nbsp;</div> -->


            <!--profile section-->
            <section id="player_profile_section" class="player_profile_section section_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-holder">
                                <table class="table table-striped table-condensed footable">
                                    <thead>
                                        <tr>
                                            <th width="100px">Start Date</th>
                                            <th>Name</th>
                                            <th>Winner</th>
                                            <th>Runner-up</th>
                                            <th>Third </th>
                                            <th>Fourth </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 0;   ?>
                                        <?php $__currentLoopData = $vsql; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php $i++;   ?>
                                        <?php
                                        $url = url($table_prefix . '/events/' . $item1->event_id);

                                        ?>
                                        <tr>

                                            <td><?php
                                                if ($item1->start_date != '0000-00-00') {
                                                    $dateArray = explode('-', $item1->start_date);
                                                    if (($dateArray[1] != '00') && ($dateArray[2] == '00')) {
                                                        $yearmonth = $dateArray[0] . '-' . $dateArray[1];
                                                        echo date("M Y", strtotime($yearmonth));
                                                    } elseif ($dateArray[1] == '00') {
                                                        echo $dateArray[0];
                                                    } else {
                                                        echo date("d M Y", strtotime($item1->start_date));
                                                    }
                                                } else {
                                                    echo 'not known';
                                                }

                                                ?></td>
                                            <td><a href="<?= $url ?>"><?php echo e($item1->name); ?></a></td>
                                            <td>
                                                <?php if(isset($item1->winner )): ?>
                                                <?php
                                                $values = 'India';
                                                $pattern = "/^" . $values . "/i"; ?>
                                                <?php if($item1->category == 'International' ): ?>
                                                <?php if(preg_match($pattern, $item1->winner)): ?>
                                                <?php echo e($item1->winner); ?>

                                                <?php endif; ?>
                                                <?php elseif($item1->category == 'National' || $item1->category == 'State'): ?>
                                                <?php if($item1->winnerregion == 1): ?>
                                                <?php echo e($item1->winner); ?>

                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td><?php if(isset($item1->runnerup )): ?>
                                                <?php
                                                $values = 'India';
                                                $pattern = "/^" . $values . "/i"; ?>
                                                <?php if($item1->category == 'International' ): ?>
                                                <?php if(preg_match($pattern, $item1->runnerup)): ?>
                                                <?php echo e($item1->runnerup); ?>

                                                <?php endif; ?>
                                                <?php elseif($item1->category == 'National' || $item1->category == 'State'): ?>
                                                <?php if($item1->runnerupregion == 1): ?>
                                                <?php echo e($item1->runnerup); ?>

                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td><?php if(isset($item1->third )): ?>
                                                <?php
                                                $values = 'India';
                                                $pattern = "/^" . $values . "/i"; ?>
                                                <?php if($item1->category == 'International' ): ?>
                                                <?php if(preg_match($pattern, $item1->third)): ?>
                                                <?php echo e($item1->third); ?>

                                                <?php endif; ?>
                                                <?php elseif($item1->category == 'National' || $item1->category == 'State'): ?>
                                                <?php if($item1->thirdregion == 1): ?>
                                                <?php echo e($item1->third); ?>

                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if(isset($item1->fourth )): ?>
                                                <?php
                                                $values = 'India';
                                                $pattern = "/^" . $values . "/i"; ?>
                                                <?php if($item1->category == 'International' ): ?>
                                                <?php if(preg_match($pattern, $item1->fourth)): ?>
                                                <?php echo e($item1->fourth); ?>

                                                <?php endif; ?>
                                                <?php elseif($item1->category == 'National' || $item1->category == 'State'): ?>
                                                <?php if($item1->fourthregion == 1): ?>
                                                <?php echo e($item1->fourth); ?>

                                                <?php endif; ?>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </td>

                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php if ($i == 0) { ?>
                                            <tr>
                                                <td colspan="2"><b>No Event details found for the selection.</b></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>
            </section>
            <!--profile section-->
        </div>
    </div>



    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        var APP_URL = {
            !!json_encode(url('/')) !!
        }
        // $('#country_id').on('select2:select', function (e) {
        // var data = e.params.data;
        // // console.log(data);
        // var country_id = data.id;
        // var xhttp = new XMLHttpRequest();
        // xhttp.onreadystatechange = function() {
        //     if (this.readyState == 4 && this.status == 200) {
        //         document.getElementById('team_id').innerText = null;
        //         var options = JSON.parse(this.responseText);
        //         Object.keys(options).forEach(function(key) {
        //             var el = document.createElement("option");
        //             el.textContent = options[key];
        //             el.value = key;
        //             document.getElementById("team_id").appendChild(el);
        //         })
        //     }
        // };
        // xhttp.open("GET", APP_URL+'/getteamsbycountry?country_id='+country_id, true);
        // xhttp.send();
        // });
        // $('#team_id').on('select2:select',function(e){
        //     var data=e.params.data;
        //     var team_id=data.id;
        //     document.location.href = APP_URL+'/teamsnew/t/'+team_id;
        // });

        $('#select-season').change(function() {
            var season = $(this).val();

            if (season != '') {
                season = season.replace("/", "-");
                // var data=e.params.data;
                // var team_id=data.id;
                document.location.href = APP_URL + '/<?= $table_prefix ?>/events/season/' + season;
            } else {
                season = 0;

                document.location.href = APP_URL + '/<?= $table_prefix ?>/events/season/' + season;
            }
        });


        /*$('#select-category').change(function(){
            var category = $(this).val();
            //alert(category);

            if(category!=''){
            
            // var data=e.params.data;
            // var team_id=data.id;
            document.location.href = APP_URL+'/<?= $table_prefix ?>/events/category/'+category;
        }
        });*/
    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd/layouts.web', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/multisports/resources/views/frontEnd/eventlist.blade.php ENDPATH**/ ?>