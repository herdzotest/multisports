<?php $__env->startSection('content'); ?>
<!--breadcrumb-->
<?php $table_prefix = \Session::get('table_prefix') ?>
<div class="inner-top-img">
  <div class="container">
    <?php if($table_prefix == 'bb'): ?>
    <img src="<?php echo e(url('website/images/inside-top.jpg')); ?>" alt="" class="img-responsive" />
    <?php endif; ?>
  </div>
</div>
<div class="container">
  <div class="event-stats">
    <h3><?php echo e($event_name); ?></h3>
    <!-- <div class="clearfix">Home     |     Events</div> -->
    <table class="table table-borderbottom table-responsive-md datatable" id="tbladmin">
      <thead>
        <tr>
          <th colspan="15">Event Squad</th>
        </tr>
        <tr>
          <th>Team</th>
          <th>Player</th>
          <th>Style</th>
          <th>Weight Category</th>
          <th>Weight</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 0;   ?>
        <?php $__currentLoopData = $eventsquads; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php $i++;   ?>
        <tr>
          <td><a href="<?php echo e(url($table_prefix.'/teams/t/'.$item1->team_id)); ?>"><?php echo e($item1->team['name']); ?></a></td>
          <td><a href="<?php echo e(url($table_prefix.'/players/'.$item1->player_id)); ?>"><?php echo e($item1->player['full_name']); ?></a></td>
          <td><?php echo e($item1->wrstyle['style_name']); ?></td>
          <td><?php echo e($item1->wtcat['category_desc']); ?></td>
          <td><?php echo e($item1->weight); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php if ($i == 0) { ?>
          <tr>
            <td colspan="5"><b>No Event details found for the selection.</b></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd/layouts.web', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/multisports/resources/views/frontEnd/wr/eventsquads.blade.php ENDPATH**/ ?>