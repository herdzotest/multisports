<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Wrestling frontend
Route::get('wr/events', 'App\Http\Controllers\wr\EventController@eventsnew');
Route::post('wr/searchevent', 'App\Http\Controllers\wr\EventController@searcheventnew');
Route::get('wr/events/{id}', 'App\Http\Controllers\wr\EventController@eventsnewbyid');
Route::get('wr/events/n/{alph}', 'App\Http\Controllers\wr\EventController@eventsnewbyalph');
Route::get('wr/events/season/{id}', 'App\Http\Controllers\wr\EventController@eventsnewbyseason');
Route::get('wr/venues', 'App\Http\Controllers\wr\VenueController@venuesnew');
Route::get('wr/venues/subregion/{id}', 'App\Http\Controllers\wr\VenueController@venuebysubregion');
Route::post('wr/searchvenue', 'App\Http\Controllers\wr\VenueController@searchvenuenew');
Route::get('wr/venues/{id}', 'App\Http\Controllers\wr\VenueController@venuenewbyid');
Route::get('wr/scorecard/{game_id}', 'App\Http\Controllers\wr\ScorecardController@scorecardnew');
Route::get('wr/teams', 'App\Http\Controllers\wr\TeamController@teamsnew');
Route::post('wr/searchteam', 'App\Http\Controllers\wr\TeamController@searchteamnew');
Route::get('wr/teams/t/{team_id}', 'App\Http\Controllers\wr\TeamController@teambyidnew');
Route::get('wr/teams/n/{alph}', 'App\Http\Controllers\wr\TeamController@teamsnewbyalph');
Route::get('wr/events/category/{id}', 'App\Http\Controllers\wr\EventController@eventsnewbycategory');

Route::get('wr/{alph}/players', 'App\Http\Controllers\wr\PlayerController@players');
/*Route::get('wr/{gender}/players/n/{alph}', 'App\Http\Controllers\wr\PlayerController@playersbyalph');*/
Route::get('wr/m/players/n/{alph}', 'App\Http\Controllers\wr\PlayerController@playersbyalph');
Route::get('wr/f/players/n/{alph}', 'App\Http\Controllers\wr\PlayerController@playersbyalph');
Route::get('wr/players/{id}', 'App\Http\Controllers\wr\PlayerController@playersnewbyid');
Route::any('wr/{alph}/searchplayer', 'App\Http\Controllers\wr\PlayerController@searchplayer');

Route::get('wr/player/season', 'App\Http\Controllers\wr\PlayerController@playernewbyseason');
Route::get('wr/player/event', 'App\Http\Controllers\wr\PlayerController@playernewbyevent');
/*Route::get('wr/players', 'App\Http\Controllers\wr\PlayerController@players');
Route::get('wr/players/n/{alph}', 'App\Http\Controllers\wr\PlayerController@playersbyalph');
Route::get('wr/players/{id}', 'App\Http\Controllers\wr\PlayerController@playersnewbyid');*/


Route::get('wr/venues/{id}', 'App\Http\Controllers\wr\VenueController@venuenewbyid');
Route::get('wr/events/{id}/playerstats', 'App\Http\Controllers\wr\EventController@playerstats');
Route::get('wr/events/{id}/squads', 'App\Http\Controllers\wr\EventSquadController@frontEndviewsquads');
Route::any('wr/searchplayer', 'App\Http\Controllers\wr\PlayerController@searchplayer');
Route::get('wr/venues/n/{alph}', 'App\Http\Controllers\wr\VenueController@venuesbyalph');

Route::get('wr/player_district_search','App\Http\Controllers\wr\PlayerController@player_district_search');
Route::get('wr/getdistplayers','App\Http\Controllers\wr\PlayerController@getdistplayers');
Route::get('wr/getteams','App\Http\Controllers\wr\PlayerController@getteams');

// Judo frontend
Route::get('ju/events', 'App\Http\Controllers\ju\EventController@eventsnew');
Route::post('ju/searchevent', 'App\Http\Controllers\ju\EventController@searcheventnew');
Route::get('ju/events/{id}', 'App\Http\Controllers\ju\EventController@eventsnewbyid');
Route::get('ju/events/n/{alph}', 'App\Http\Controllers\ju\EventController@eventsnewbyalph');
Route::get('ju/events/season/{id}', 'App\Http\Controllers\ju\EventController@eventsnewbyseason');
Route::get('ju/venues', 'App\Http\Controllers\ju\VenueController@venuesnew');
Route::get('ju/venues/subregion/{id}', 'App\Http\Controllers\ju\VenueController@venuebysubregion');
Route::post('ju/searchvenue', 'App\Http\Controllers\ju\VenueController@searchvenuenew');
Route::get('ju/venues/{id}', 'App\Http\Controllers\ju\VenueController@venuenewbyid');
Route::get('ju/scorecard/{game_id}', 'App\Http\Controllers\ju\ScorecardController@scorecardnew');
Route::get('ju/teams', 'App\Http\Controllers\ju\TeamController@teamsnew');
Route::post('ju/searchteam', 'App\Http\Controllers\ju\TeamController@searchteamnew');
Route::get('ju/teams/t/{team_id}', 'App\Http\Controllers\ju\TeamController@teambyidnew');
Route::get('ju/teams/n/{alph}', 'App\Http\Controllers\ju\TeamController@teamsnewbyalph');
Route::get('ju/events/category/{id}', 'App\Http\Controllers\ju\EventController@eventsnewbycategory');

Route::get('ju/{alph}/players', 'App\Http\Controllers\ju\PlayerController@players');
/*Route::get('ju/{gender}/players/n/{alph}', 'App\Http\Controllers\ju\PlayerController@playersbyalph');*/
Route::get('ju/m/players/n/{alph}', 'App\Http\Controllers\ju\PlayerController@playersbyalph');
Route::get('ju/f/players/n/{alph}', 'App\Http\Controllers\ju\PlayerController@playersbyalph');
Route::get('ju/players/{id}', 'App\Http\Controllers\ju\PlayerController@playersnewbyid');
Route::any('ju/{alph}/searchplayer', 'App\Http\Controllers\ju\PlayerController@searchplayer');

Route::get('ju/player/season', 'App\Http\Controllers\ju\PlayerController@playernewbyseason');
Route::get('ju/player/event', 'App\Http\Controllers\ju\PlayerController@playernewbyevent');
/*Route::get('ju/players', 'App\Http\Controllers\ju\PlayerController@players');
Route::get('ju/players/n/{alph}', 'App\Http\Controllers\ju\PlayerController@playersbyalph');
Route::get('ju/players/{id}', 'App\Http\Controllers\ju\PlayerController@playersnewbyid');*/


Route::get('ju/venues/{id}', 'App\Http\Controllers\ju\VenueController@venuenewbyid');
Route::get('ju/events/{id}/playerstats', 'App\Http\Controllers\ju\EventController@playerstats');
Route::get('ju/events/{id}/squads', 'App\Http\Controllers\ju\EventSquadController@frontEndviewsquads');
Route::any('ju/searchplayer', 'App\Http\Controllers\ju\PlayerController@searchplayer');
Route::get('ju/venues/n/{alph}', 'App\Http\Controllers\ju\VenueController@venuesbyalph');

Route::get('ju/player_district_search','App\Http\Controllers\ju\PlayerController@player_district_search');
Route::get('ju/getdistplayers','App\Http\Controllers\ju\PlayerController@getdistplayers');
Route::get('ju/getteams','App\Http\Controllers\ju\PlayerController@getteams');



// Admin Routes for Wrestling
Route::resource('admin/wr/governingbody', 'GoverningBodyController');
Route::resource('admin/wr/documents', 'DocumentController');
Route::resource('admin/wr/documentcategory', 'DocumentCategoryController');
Route::resource('admin/wr/documentsubcategory', 'DocumentSubCategoryController');
Route::resource('admin/wr/player', 'wr\PlayerController');
Route::resource('admin/wr/team', 'wr\TeamController');
Route::resource('admin/wr/event', 'wr\EventController');
Route::resource('admin/wr/eventsquad', 'wr\EventSquadController');
Route::resource('admin/wr/venue', 'wr\VenueController');
Route::resource('admin/wr/game', 'wr\GameController');
Route::resource('admin/wr/scorecard', 'ScorecardController');
Route::resource('admin/wr/playerteam', 'wr\PlayerTeamController');
Route::resource('admin/wr/official', 'wr\OfficialController');
Route::resource('admin/wr/eventofficial', 'wr\EventOfficialController');
Route::resource('admin/wr/officialteam', 'wr\OfficialTeamController');


Route::get('admin/wr/eventupload', 'wr\EventController@upload');
Route::post('admin/wr/importevents', 'wr\EventController@import');

Route::post('admin/wr/massupdatefixture', 'wr\FixtureController@massupdatefixture');
Route::post('admin/wr/updatefixture', 'wr\FixtureController@updatefixture');
Route::post('admin/wr/deletefixture', 'wr\FixtureController@deletefixture');
Route::get('admin/wr/getevents', 'wr\EventSquadController@getevents');
Route::get('admin/wr/delplayer', 'wr\EventSquadController@delplayer');
Route::get('admin/wr/fixture', 'wr\FixtureController@upload');
Route::post('admin/wr/fixture', 'wr\FixtureController@import');
Route::get('admin/wr/fixture/edit/{id}', 'wr\FixtureController@editfixture');
Route::get('admin/wr/uploadscorecardsel', 'wr\ScorecardController@uploadscorecardsel');
Route::post('admin/wr/uploadscorecardsel', 'wr\ScorecardController@uploadscorecardselsubmit');
Route::post('admin/wr/submitscorecard', 'wr\ScorecardController@submitscorecard');
Route::get('admin/wr/getgames', 'wr\ScorecardController@getgames');

/*edit scorecard*/
Route::get('admin/wr/editscorecardsel', 'wr\ScorecardController@editscorecardsel');
Route::get('admin/wr/editscorecardbyid/{id}', 'wr\ScorecardController@editscorecardbyid');
Route::post('admin/wr/editscorecardsel', 'wr\ScorecardController@editscorecardselsubmit');
Route::post('admin/wr/editsubmitscorecard', 'wr\ScorecardController@editsubmitscorecard');
Route::get('admin/wr/editgetgames', 'wr\ScorecardController@editgetgames');
/*end edit scorecard*/


Route::get('admin/wr/intervalgameselect', 'ScorecardController@intervalgameselect');
Route::post('admin/wr/intervalgameselect', 'ScorecardController@intervaldisp');
Route::post('admin/wr/intervalscore', 'ScorecardController@intervalscore');
Route::get('admin/wr/addfsgameselect', 'ScorecardController@addfsgameselect');
Route::post('admin/wr/addfsgameselect', 'ScorecardController@addfullscore');
Route::post('admin/wr/addfssave', 'ScorecardController@addfssave');
Route::get('admin/wr/addssgameselect', 'ScorecardController@addssgameselect');
Route::post('admin/wr/addssgameselect', 'ScorecardController@addsummaryscore');
Route::post('admin/wr/addsssave', 'ScorecardController@addsssave');
Route::get('admin/wr/overtimegameselect', 'ScorecardController@overtimegameselect');
Route::get('/wr/getgamesovertime', 'ScorecardController@getgamesovertime')->name('getgamesovertime');
Route::post('admin/wr/overtimegameselect', 'ScorecardController@overtimedisp');
Route::post('admin/wr/overtimescore', 'ScorecardController@overtimescore');
Route::get('admin/wr/pdfgameselect', 'ScorecardController@pdfgameselect');
Route::get('/wr/getgamespdf', 'ScorecardController@getgamespdf')->name('getgamespdf');
Route::post('admin/wr/pdfgameselect', 'ScorecardController@pdfgameselectscrap');
Route::post('admin/wr/pdfsave', 'ScorecardController@pdfsave');
Route::get('/wr/getsubregion', 'VenueController@getsubregion')->name('getsubregion');
Route::get('/wr/getregion', 'VenueController@getregion')->name('getregion');
Route::get('/get_subcategory', 'AwardController@getsubcategory')->name('getsubcategory');
Route::resource('admin/wr/relateddocumentreport', 'RelatedDocumentReportController');

// Admin Routes for Judo
Route::get('admin/ju/events/{id}/{gameId}', 'App\Http\Controllers\admin\ju\JudoController@edit');
Route::post('admin/ju/events/update/{id}', 'App\Http\Controllers\admin\ju\JudoController@update');

// Admin Routes for taekwondo
Route::get('admin/ta/scorecard/{game_id}', 'App\Http\Controllers\admin\ta\TaekwondoController@scorecard');
Route::post('admin/ta/scorecard/update/{game_id}', 'App\Http\Controllers\admin\ta\TaekwondoController@update');

// Admin Routes for boxing
Route::get('admin/bo/scorecard/{game_id}', 'App\Http\Controllers\admin\bo\BoxingController@scorecard');
Route::post('admin/bo/scorecard/update/{game_id}', 'App\Http\Controllers\admin\bo\BoxingController@update');