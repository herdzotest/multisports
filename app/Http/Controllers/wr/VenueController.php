<?php

namespace App\Http\Controllers\wr;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Sport;
use App\Venue;
use App\Country;
use App\Region;
use App\SubRegion;
use App\Location;
use App\Model\wr\PictureVenue;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;
use App\Picture;
use App\VenueSport;

class VenueController extends Controller
{

    /**
     * Display a listing of the resource.eee
     *
     * @return Response
     */
   protected $sport_id,$table_prefix;
   public function __construct()
    {   
         $sport_id="";   
         // $this->table_prefix= \Session::get('table_prefix');
        $this->middleware(function ($request, $next){
        //$this->table_prefix = session('table_prefix');
        session()->put('table_prefix', 'wr');
        $this->table_prefix =   session()->get('table_prefix');
        $sport_id_arr=Sport::where('identifier',$this->table_prefix)->first();
        $this->sport_id=$sport_id_arr->sport_id;
        return $next($request);
        });
         
    }
    public function index()
    {

        $venue = Venue::
        leftJoin('venue_sport as vs', function($join) {
        $join->on('vs.venue_id', '=', 'venue.venue_id');
        })
        ->where('vs.sport_id',$this->sport_id)
        ->get();

        return view('backEnd.admin.venue.index', compact('venue'));
    }

    public function create()
    {
        $venuetype = array('indoor' => 'indoor', 'outdoor' => 'outdoor' );
        $floodlight = array('y' => 'Yes', 'n' => 'No' );
        $surface = array('2' => 'Asphalt','4' => 'Grass','1' => 'Hardwood', '3' => 'Multipurpose' );
        $country = Country::orderby('name','asc')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        $region = Region::orderby('name','asc')->pluck('name', 'region_id');
        $region->prepend('Select Region', '');
        $subregion = SubRegion::orderby('name','asc')->pluck('name', 'sub_region_id');
        $subregion->prepend('Select Subregion', '');
        $sports=Sport::orderby('sport_name','asc')->pluck('sport_name','sport_id');
        return view('backEnd.admin.venue.create', compact('country','venuetype','region','subregion','floodlight','surface','sports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            //'name' => 'required|unique:venue|',
            'name' => 'required',
            'country_id' => 'required',
            'region' => 'required',
            'sub_region' => 'required',
            'address' => 'required',
            'sport_id' => 'required',
            'floodlight' => 'required',
            'capacity' => 'max:6'
        ],
        [
            'name.required' => 'Name is required',
            'country_id.required' => 'Country is required',
            'region.required' => 'Region is required',
            'sub_region.required' => 'Subregion is required',
            'address.required' => 'Address is required',
            'floodlight.required' => 'Flood Light is required',
            'capacity.max' => 'Maximum 6 characters required',
            'sport_id.required' => 'Sport is required'
        ]
    );
        
        $venue = Venue::create($request->all());
        if($request->picture!=''){
            $picture = new PictureVenue;
            $picture->picture_id = $request->picture;
            $picture->venue_id = $venue->venue_id;
            $picture->save();

        }

        $venue_sport="";
        if($request->sport_id!=""){
            for ($i=0; $i < sizeof($request->sport_id) ; $i++) { 
                $venue_sport=new VenueSport;
                $venue_sport->venue_id= $venue->venue_id;
                $venue_sport->sport_id=$request->sport_id[$i];
                $venue_sport->save();
            }
        }
        Session::flash('message', 'Venue added!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/venue');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $venue = Venue::findOrFail($id);

        return view('backEnd.admin.venue.show', compact('venue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $venue = Venue::findOrFail($id);

        $venuetype = array('indoor' => 'indoor', 'outdoor' => 'outdoor' );
        $floodlight = array('y' => 'Yes', 'n' => 'No' );
        $surface = array('2' => 'Asphalt','1' => 'Hardwood', '3' => 'Multipurpose','4' => 'Grass' );
        $country = Country::orderBy('name','asc')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        $region = Region::orderBy('name','asc')->pluck('name', 'region_id');
        $region->prepend('Select Region', '');
        $subregion = SubRegion::orderBy('name','asc')->pluck('name', 'sub_region_id');
        $subregion->prepend('Select Subregion', '');
        $venue_picture=PictureVenue::where('venue_id',$id)->first();
        if(isset($venue_picture)){$venue_picture_pic=$venue_picture->picture_id;}else{$venue_picture_pic="";}
        $sports=Sport::orderby('sport_name','asc')->pluck('sport_name','sport_id');
        $sport_venue=VenueSport::select('sport_id')->where('venue_id',$id)->get()->toArray();
        // print_r($sport_venue);
        $i=0;
        $sport_venues=array();
        foreach ($sport_venue as $key) {
            $sport_venues[$i]= $key['sport_id'];
            $i++;
        }
        // print_r($sport_venues);
        // return;
        return view('backEnd.admin.venue.edit', compact('venue','country','venuetype','region','subregion','floodlight','surface','venue_picture_pic','sports','sport_venues'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $venue = Venue::findOrFail($id);
        $venue->update($request->all());
        if($request['picture']!=''){
            DB::table('wr_picture_venue')->where('venue_id', '=', $id)->delete();

            DB::table('wr_picture_venue')->insert(['picture_id' => $request['picture'], 'venue_id' => $id]);
        }
        $venue_sport_del=VenueSport::where('venue_id',$id)->delete();
        $venue_sport="";
        if($request->sport_id!=""){
            for ($i=0; $i < sizeof($request->sport_id) ; $i++) { 
                $venue_sport=new VenueSport;
                $venue_sport->venue_id= $venue->venue_id;
                $venue_sport->sport_id=$request->sport_id[$i];
                $venue_sport->save();
            }
        }
        Session::flash('message', 'Venue updated!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/venue');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $venue = Venue::findOrFail($id);

        $venue->delete();

        DB::table('wr_picture_venue')->where('venue_id', '=', $id)->delete();

        Session::flash('message', 'Venue deleted!');
        Session::flash('status', 'success');

       return redirect('admin/'.$this->table_prefix.'/venue');
    }


    public function getregion(Request $request)
    {
        $region = Region::where('country_id','=',$request->country_id)->orderby('name','desc')->pluck('region_id', 'name');
      /*  $region = Region::where('country_id','=',$request->country_id)->orderby('name','asc')->select('name as region', 'region_id')->distinct()->get();*/
        $region->prepend(0,'Select Region');
        return json_encode($region);
    }

    public function getsubregion(Request $request)
    {
        $subregion = SubRegion::where('region_id','=',$request->region_id)->orderby('name','desc')->pluck( 'sub_region_id','name');
        $subregion->prepend(0,'Select Sub Region');
        return json_encode($subregion);
    }
    public function getlocation(Request $request)
    {
        $location = location::where('sub_region_id','=',$request->sub_region_id)->orderby('name','desc')->pluck( 'location_id','name');
        $location->prepend(0,'Select location');
        return json_encode($location);
    }


    // public function getregionbycountry(Request $request)
    // {
    //     $region = Region::where('country_id','=',$request->country_id)->get(); 
    //     $inner = "";
    //     foreach($region as $Obj){
    //         $inner.='<li><a class="region_opt" rel="'.$Obj->name.'" id="'.$Obj->region_id.'">'.$Obj->name.'</a></li>';
    //     }

    //         echo $inner;    
       
    // }
    // public function getsubregionbyregion(Request $request)
    // {
    //     $subregion = SubRegion::where('region_id','=',$request->region_id)->get(); 
    //     $inner = "";
    //     foreach($subregion as $Obj){
    //         $inner.='<li><a class="subregion_opt" rel="'.$Obj->name.'" id="'.$Obj->sub_region_id.'">'.$Obj->name.'</a></li>';
    //     }

    //         echo $inner;    
       
    // }


public function venuebyregionnew(Request $request){
     $venueby = Venue::where('sub_region','=',$request->subregion_id)->orderby('name')->pluck('name', 'venue_id');
     $venueby->prepend(' ', 0);
        return json_encode($venueby);
}
        public function getvenuebyregion(Request $request)
    {
        $venues = Venue::where('region','=',$request->region_id)->get(); 

        $i=0;
        $inner1="";
        foreach($venues as $item1){
           
                if($i==0){
                    $active = "active";
                    $name = $item1->name;
                  }else{
                    $active = "";
                } $i++;
           
            $inner1.='<li class="menuli '.$active.'"><a class="venue_id" id="'.$item1->venue_id.'">'.$item1->name.'</a></li>';           
        }

                

            $inner2='';

            $venue = Venue::where('sub_region','=',$request->subregion_id)->first();
            $image = $venue->picture['picture_id'];
            $game_url = url("/venues")."/".$item1->venue_id."/games";

                    if($image){
                    $img = url('/uploads').'/'.$image.'.jpg';
                    }else{
                    $img = url('/images')."/basketball-venue-dummy.jpg";
                    }


                $inner2 = '
            <div class="venue-img col-lg-12">
                <h4>'.$venue->name.'</h4>
                <img src="'.$img.'" width="400" align="middle">
            </div>
            
                <div class="venue-info col-lg-12">
                    <div class="venueDetails-content-wrapper">
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Location</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p>'.$venue->address.'</p></div>
                        </div>
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Capacity</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p></p></div>
                        </div>
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Surface Type</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p></p></div>
                        </div>
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Facilities</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p></p></div>
                        </div>
                    </div>
                    <a href="'.$game_url.'"><button class="btn btn-default src_btn" type="button">View Games Played</button></a>
                </div>';

    echo json_encode(array($inner1, $inner2));
       
    }

    public function getvenuebysubregion(Request $request)
    {
        $venues = Venue::where('sub_region','=',$request->subregion_id)->get(); 

        $i=0;
        $inner1="";
        foreach($venues as $item1){
           
                if($i==0){
                    $active = "active";
                    $name = $item1->name;
                  }else{
                    $active = "";
                } $i++;
           
            $inner1.='<li class="menuli '.$active.'"><a class="venue_id" id="'.$item1->venue_id.'">'.$item1->name.'</a></li>';           
        }

                

            $inner2='';

            $venue = Venue::where('sub_region','=',$request->subregion_id)->first();
            $image = $venue->picture['picture_id'];
            $capacity = $venue->capacity;
            $game_url = url("/venues")."/".$item1->venue_id."/games";

                    if($image){
                    $img = url('/uploads').'/'.$image.'.jpg';
                    }else{
                    $img = url('/images')."/basketball-venue-dummy.jpg";
                    }


                $inner2 = '
            <div class="venue-img col-lg-12">
                <h4>'.$venue->name.'</h4>
                <img src="'.$img.'" width="400" align="middle">
            </div>
            
                <div class="venue-info col-lg-12">
                    <div class="venueDetails-content-wrapper">
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Location</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p>'.$venue->address.'</p></div>
                        </div>
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Capacity</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p>'.$capacity.'</p></div>
                        </div>
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Surface Type</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p></p></div>
                        </div>
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Facilities</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p></p></div>
                        </div>
                    </div>
                    <a href="'.$game_url.'"><button class="btn btn-default src_btn" type="button">View Games Played</button></a>
                </div>';

    echo json_encode(array($inner1, $inner2));
       
    }

    public function venues()
    {

        $country = Country::orderby('name')->pluck('name','country_id');
        $country ->prepend('Select Country','');
        $region = array('Select Region','');
        $defaultregionvenues = Venue::where('country_id', '=', '1')->orderby('name')->limit(30)->get();
        $surface = array('1' => 'Hardwood', '2' => 'Asphalt','3' => 'Multipurpose' );
        return view('frontEnd.venues', compact('defaultregionvenues','country','surface','region'));
    }
    public function venuesnew()
    {
        $d_alph = 'A';
        $venueby = '';
        $defaultregionvenues = DB::table('venue as v')
        ->leftJoin ('wr_picture_venue as pv', 'pv.venue_id', '=', 'v.venue_id')
        ->leftJoin ('picture as p', 'p.picture_id', '=', 'pv.picture_id')
        ->leftJoin ('venue_sport as vs', 'vs.venue_id', '=', 'v.venue_id')
        ->select('v.name as name','v.venue_id as venue_id','v.country_id as country_id','v.address as address','v.region as region','v.venue_type as venue_type','v.sub_region as sub_region','v.floodlight','v.surface','v.capacity','p.picture_id as picture_id','v.location_url')
        ->where('sport_id', '=', $this->sport_id)        
        ->where('v.region','=',1)
        ->first();

        $subregion = SubRegion::orderby('name')->where('region_id','1')->pluck('name', 'sub_region_id');
        $subregion ->prepend('Select Region','');

        if(!empty($defaultregionvenues)){
            $venue_id=$defaultregionvenues->venue_id;
            $games = DB::table('wr_game as g')
            ->join('player as p1', 'g.player1_id', '=', 'p1.player_id')
            ->join('player as p2', 'g.player2_id', '=', 'p2.player_id')
            ->join('wr_event_game as eg', 'g.game_id', '=', 'eg.game_id')
            ->join('wr_event as e', 'eg.event_id', '=', 'e.event_id')
            ->select('p1.full_name as team1','p2.full_name as team2','g.game_date as date','g.game_id as game_id','g.status','g.local_start_time as time','e.name as eventname','e.event_id')
            ->where('g.venue_id','=',$venue_id)
            ->get();
            $picture_id=$defaultregionvenues->picture_id;
        }else{
            $games=array();
            $defaultregionvenues=array();
            $picture_id="";
        }   
        $surface = array('1' => 'Hardwood', '2' => 'Asphalt','3' => 'Multipurpose' );
        return view('frontEnd.venues', compact('picture_id','d_alph','defaultregionvenues','surface','games','venueby','picture_id','subregion'));
    }
        public function venuesindex()
    {
        $d_alph = 'A';
         $defaultregionvenues = DB::table('venue as v')
        ->leftJoin ('picture_venue as pv', 'pv.venue_id', '=', 'v.venue_id')
        ->leftJoin ('picture as p', 'p.picture_id', '=', 'pv.picture_id')
        ->select('v.name as name','v.venue_id as venue_id','v.country_id as country_id','v.address as address','v.region as region','v.venue_type as venue_type','v.sub_region as sub_region','v.floodlight','v.surface','v.capacity','p.picture_id as picture_id','v.location_url')
        ->where('country_id', '=', '1')
      
        ->orderby('name')
        ->limit(30)
        ->get();
        $country = Country::orderby('name')->pluck('name','country_id');
        $country ->prepend('Select Country','');
        $region = Region::orderby('name')->where('country_id',$defaultregionvenues[0]->country_id)->pluck('name','region_id');
        $region ->prepend('Select Region','');
        $subregion = SubRegion::orderby('name')->where('region_id',$defaultregionvenues[0]->region)->pluck('name', 'sub_region_id');
        $subregion ->prepend('Select Sub Region','');
        $venueby = Venue::orderby('name')->where('sub_region',$defaultregionvenues[0]->sub_region)->pluck('name', 'venue_id');
        $venueby ->prepend('Select Venue','');
       
        $venue_id=$defaultregionvenues[0]->venue_id;
        $surface = array('1' => 'Hardwood', '2' => 'Asphalt','3' => 'Multipurpose' );

        $games = DB::table('game as g')
        ->join('team as t1', 'g.team1_id', '=', 't1.team_id')
        ->join('team as t2', 'g.team2_id', '=', 't2.team_id')
        ->select('t1.name as team1','t2.name as team2','g.game_date as date','g.game_id as game_id','g.status','g.local_start_time as time')
        ->where('g.venue_id','=',$venue_id)
        ->get();
        $picture_id=$defaultregionvenues[0]->picture_id;
        $venues=Venue::all();
        return view('frontEnd.venuesindex', compact('venues','picture_id','d_alph','defaultregionvenues','country','surface','region','games','subregion','venueby'));
    }

    public function venuebysubregion(Request $request,$id)
    {
        $d_alph = '';
        $regionid = $id;
        $query_str = $request->query_str;
         $vsql = DB::table('venue as v')
         ->leftJoin ('venue_sport as vs', 'vs.venue_id', '=', 'v.venue_id')
        ->leftJoin ('vb_picture_venue as pv', 'pv.venue_id', '=', 'v.venue_id')
        ->leftJoin ('picture as p', 'p.picture_id', '=', 'pv.picture_id')
        ->select('v.name as name','v.venue_id as venue_id','v.country_id as country_id','v.address as address','v.region as region','v.venue_type as venue_type','v.sub_region as sub_region','v.floodlight','v.surface','v.capacity','p.picture_id as picture_id','v.location_url')
        ->where('v.sub_region', '=', $id)  
        ->where('vs.sport_id','=',$this->sport_id)    
        ->orderby('name')
        ->get();
        // $country = Country::orderby('name')->pluck('name','country_id');
        // $country ->prepend('Select Country','');
        // $region = Region::orderby('name')->where('country_id',$vsql[0]->country_id)->pluck('name','region_id');
        // $region ->prepend('Select Region','');
        $subregion = SubRegion::orderby('name')->where('region_id',1)->pluck('name', 'sub_region_id');
        $subregion ->prepend('Select Sub Region','');
        // $venueby = Venue::orderby('name')->where('sub_region',$vsql[0]->sub_region)->pluck('name', 'venue_id');
        // $venueby ->prepend('Select Venue','');
       

        $venues=Venue::all();
        return view('frontEnd.venueslist', compact('venues','d_alph','vsql','subregion','query_str','regionid'));
    }

    public function searchvenuenew(Request $request)
    {   
     
        $query_str = $request->query_str;
        $regionid = null;
        $d_alph = strtoupper(substr($query_str, 0, 1));
       // return 'kshdhsgdkhsdlkgh';
        $vsql=DB::Select("SELECT * FROM venue v 
        left outer join venue_sport vs on vs.venue_id=v.venue_id
        where v.name like '%".$query_str."%'
        and v.region = 1
        and vs.sport_id=$this->sport_id
        order by v.name asc" );
        $country = Country::orderby('name')->pluck('name','country_id');
        $country ->prepend('Select Country','');
        $region = array('Select Region','');

        $subregion = SubRegion::orderby('name')->where('region_id',1)->pluck('name', 'sub_region_id');
        $subregion ->prepend('Select Sub Region','');
        
        return view('frontEnd.venueslist',compact('vsql','country','region','subregion','d_alph','query_str','regionid'));


  
    }

    public function venuebyid($id)
    {
        $country = Country::orderby('name')->pluck('name','country_id');
        $country ->prepend('Select Country','');
        $region = array('Select Region','');
        $defaultregionvenues = Venue::where('venue_id', '=', $id)->get();
        $surface = array('1' => 'Hardwood', '2' => 'Asphalt','3' => 'Multipurpose' );
        if(count($defaultregionvenues)){
            return view('frontEnd.venues', compact('defaultregionvenues','country','surface','region'));
        }else{
            return view('frontEnd.error.404');
        }
    }
    public function venuenewbyid($id)
    {  

       $d_alph="A";
       $picture_id="";

        $defaultregionvenues = DB::table('venue as v')
        ->leftJoin ('wr_picture_venue as pv', 'pv.venue_id', '=', 'v.venue_id')
        ->leftJoin ('picture as p', 'p.picture_id', '=', 'pv.picture_id')
        ->leftJoin ('venue_sport as vs', 'vs.venue_id', '=', 'v.venue_id')
        ->select('v.name as name','v.venue_id as venue_id','v.country_id as country_id','v.address as address','v.region as region','v.venue_type as venue_type','v.sub_region as sub_region','v.floodlight','v.surface','v.capacity','p.picture_id as picture_id','v.location_url')
        ->where('v.venue_id', '=', $id) 
        ->first();


        $country = Country::orderby('name')->pluck('name','country_id');
        $country ->prepend('Select Country','');
        $region = Region::orderby('name')->where('country_id',$defaultregionvenues->country_id)->pluck('name','region_id');
        $region ->prepend('Select Region','');
        $subregion = SubRegion::orderby('name')->where('region_id',$defaultregionvenues->region)->pluck('name', 'sub_region_id');
        $subregion ->prepend('Select Sub Region','');
        $venueby = Venue::orderby('name')->where('sub_region',$defaultregionvenues->sub_region)->pluck('name', 'venue_id');
        $venueby ->prepend('Select Venue','');
        $picture_id=$defaultregionvenues->picture_id;
        $surface = array('1' => 'Hardwood', '2' => 'Asphalt','3' => 'Multipurpose' );
        $games = DB::table('wr_game as g')
        ->join('player as p1', 'g.player1_id', '=', 'p1.player_id')
        ->join('player as p2', 'g.player2_id', '=', 'p2.player_id')
        ->join('wr_event_game as eg', 'g.game_id', '=', 'eg.game_id')
        ->join('wr_event as e', 'eg.event_id', '=', 'e.event_id')
        ->select('p1.full_name as team1','p2.full_name as team2','g.game_date as date','g.game_id as game_id','g.status','g.local_start_time as time','e.name as eventname','e.event_id')
        ->where('g.venue_id','=',$id)
        ->get();

        // print_r($games);
        // return;
        if($defaultregionvenues->venue_id){
            return view('frontEnd.venues', compact('picture_id','games','d_alph','defaultregionvenues','country','surface','region','subregion','venueby'));
        }else{
            return view('frontEnd.error.404');
        }
    }
    public function venuesbyalph(Request $request,$alph)
    {
        $title = "Venues";
        $d_alph = ucwords($alph);        
        $query_str = $request->query_str;
        $regionid = null;

        $vsql = DB::table('venue as v')
        ->leftJoin ('venue_sport as vs', 'vs.venue_id', '=', 'v.venue_id')
        ->select('v.name as name','v.venue_id as venue_id','v.country_id as country_id','v.region as region','v.venue_type as venue_type','v.sub_region as sub_region')
        ->where('vs.sport_id','=',$this->sport_id)
        ->where('v.region','=',1)
        ->where('name','LIKE',$d_alph.'%')    
        ->orderby('name')
        ->limit(30)
        ->get();
        $subregion = SubRegion::orderby('name')->where('region_id',1)->pluck('name', 'sub_region_id');
        $subregion ->prepend('Select Sub Region','');
        return view('frontEnd.venueslist', compact('vsql','title','d_alph','subregion','query_str','regionid'));
    }
    public function getvenuenew(Request $request){
        $venues = Venue::where('sub_region','=',$request->subregion_id)->orderby('name')->pluck('name', 'venue_id');
        $venues->prepend('Select Venue', 0);
        return json_encode($venues);
    }
    
    public function getvenuebyreg(Request $request){
        $venues = Venue::where('region','=',$request->region_id)->orderby('name')->pluck('name', 'venue_id');
        $venues->prepend('Select Venue', 0);
        return json_encode($venues);
    }
    public function getvenue(Request $request){

        $venue = Venue::where('venue_id','=',$request->venue_id)->first();

                    $image = $venue->picture['picture_id'];
                    $game_url = url("/venues")."/".$request->venue_id."/games";

                    if($image){
                    $img = url('/uploads').'/'.$image.'.jpg';
                    }else{
                    $img = url('/images')."/basketball-venue-dummy.jpg";
                    }


                $inner = '
            <div class="venue-img col-lg-12">
                <h4>'.$venue->name.'</h4>
                <img src="'.$img.'" width="400" align="middle">
            </div>
            
                <div class="venue-info col-lg-12"> 
                    <div class="venueDetails-content-wrapper">
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Location</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p>'.$venue->address.'</p></div>
                        </div>
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Capacity</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p></p></div>
                        </div>
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Surface Type</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p></p></div>
                        </div>
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Facilities</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p></p></div>
                        </div>
                    </div>
                    <a href="'.$game_url.'"><button class="btn btn-default src_btn" type="button">View Games Played</button></a>
                </div>';

    echo $inner;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    
}
