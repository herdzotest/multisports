<?php

namespace App\Http\Controllers\wr;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Official;
use App\PlayerMetaData;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;
use App\Sport;
use App\Country;
use App\Model\wr\GamePlayer as GamePlayer;
use App\Model\wr\PicturePlayer as PicturePlayer;
use App\Model\wr\Team as Team;
use App\Model\wr\PlayerTeam as PlayerTeam;
use App\SubRegion;

class OfficialController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
     protected $sport_id,$table_prefix;
     public function __construct()
    {   
         $sport_id="";   
         // $this->table_prefix= \Session::get('table_prefix');
        $this->middleware(function ($request, $next){
        //$this->table_prefix = session('table_prefix');
        session()->put('table_prefix', 'wr');
        $this->table_prefix =   session()->get('table_prefix');
        $sport_id_arr=Sport::where('identifier',$this->table_prefix)->first();
        $this->sport_id=$sport_id_arr->sport_id;
        return $next($request);
        });
         
    }

    
    public function index()
    {  
        $official = Official::where('sport_id',$this->sport_id)->get();
        /*echo "<pre>";
        print_r($official);
        exit();*/
        return view('backEnd.admin.official.index', compact('official'));
    }
    public function create()
    {
        $country = Country::orderby('name','asc')->pluck('name', 'country_id');
        $country->prepend('Select', '');
        $team=array();
        return view('backEnd.admin.official.create',compact('country'));
    }
    public function store(Request $request)
    {
     
        $this->validate($request, [
            'full_name' => 'required',
            'game_name' => 'required',
            'sort_name' => 'required',           
            'gender' => 'required',
            'country_id' => 'required'          
        ]);


        
        if($request['date_of_birth']!=""){
        $request['date_of_birth'] = date("Y-m-d", strtotime($request['date_of_birth']));
        }else{
         $request['date_of_birth'] ='0000-00-00'   ;
        }
        if($request['date_of_death']!=""){
        $request['date_of_death'] = date("Y-m-d", strtotime($request['date_of_death']));
        }else{
         $request['date_of_death'] ='0000-00-00'   ;
        }
        $request['sport_id']=$this->sport_id;
        $player_id = Official::create($request->all());
 
        Session::flash('message', 'Player added!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/official');
    }
      public function show($id)
    {
        $player = Official::findOrFail($id);

        return view('backEnd.admin.official.show', compact('player'));
    }

     public function edit($id)
    {  
     // return 'dumbo';
        $player = Official::findOrFail($id);
        $country = Country::orderby('name','asc')->pluck('name', 'country_id');
        $country->prepend('Select', '');
        return view('backEnd.admin.official.edit', compact('player','country'));
    }
     public function update($id, Request $request)
    {
        $this->validate($request, [
            'full_name' => 'required',
            'game_name' => 'required',
            'sort_name' => 'required',           
            'gender' => 'required',
            'country_id' => 'required'           
        ]);
        
        $player = Official::findOrFail($id);
        $request['date_of_birth'] = $request['date_of_birth']!=""?date("Y-m-d", strtotime($request['date_of_birth'])):"0000-00-00";
        $request['date_of_death'] = $request['date_of_death']!=""?date("Y-m-d", strtotime($request['date_of_death'])):"0000-00-00";

		
        $player->update($request->all());	

        Session::flash('message', 'Player updated!');
        Session::flash('status', 'success');

       return redirect('admin/'.$this->table_prefix.'/official');
    }
     public function destroy($id)
    {
        $player = Official::findOrFail($id);

        $player->delete();

        Session::flash('message', 'Player deleted!');
        Session::flash('status', 'success');
   

        return redirect('admin/'.$this->table_prefix.'/official');
    }
    
}
