<?php

namespace App\Http\Controllers\wr;

use App\Http\Requests;
use App\Http\Controllers\wr\Controller;

use App\Event;
use DB;
use App\Country;
use App\Team;
use App\Sport;
use App\Region;
use App\SubRegion;
use App\Location;
use App\Model\wr\PictureEvent;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Imports\wr\EventImport;
use Maatwebsite\Excel\Facades\Excel;

class EventController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  protected $sport_id, $table_prefix;
  public function __construct()
  {
    $sport_id = "";
    // $this->table_prefix= \Session::get('table_prefix');
    $this->middleware(function ($request, $next) {
      //$this->table_prefix = session('table_prefix');
      session()->put('table_prefix', 'wr');
      $this->table_prefix =   session()->get('table_prefix');
      $sport_id_arr = Sport::where('identifier', $this->table_prefix)->first();
      $this->sport_id = $sport_id_arr->sport_id;
      return $next($request);
    });
    // session()->put('table_prefix', 'wr');
    // $this->table_prefix = session()->get('table_prefix');
    // $sport_id_arr = Sport::where('identifier', $this->table_prefix)->first();
    // $this->sport_id = $sport_id_arr->sport_id;
    // return $next($request);
  }

  public function index()
  {
    $event = Event::orderBy('event_id', 'desc')->get();

    return view('backEnd.admin.event.index', compact('event'));
  }
  public function getregion(Request $request)
  {
    $region = Region::where('country_id', '=', $request->country_id)->orderby('name', 'asc')->pluck('name', 'region_id');
    $region->prepend('Select Region', 0);
    return json_encode($region);
  }

  public function getsubregion(Request $request)
  {
    $subregion = SubRegion::where('region_id', '=', $request->region_id)->orderby('name')->pluck('name', 'sub_region_id');
    $subregion->prepend('Select Sub Region', 0);
    return json_encode($subregion);
  }
  public function getlocation(Request $request)
  {
    $location = Location::where('sub_region_id', '=', $request->sub_region_id)->orderby('name')->pluck('name', 'location_id');
    $location->prepend('Select Location', 0);
    return json_encode($location);
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $country = Country::orderBy('name', 'asc')->pluck('name', 'country_id');
    $region = Region::orderBy('name', 'asc')->pluck('name', 'region_id');
    $region->prepend('Select Region', ' ');
    $subregion = SubRegion::orderBy('name', 'asc')->pluck('name', 'sub_region_id');
    $subregion->prepend('Select Sub Region', ' ');
    $location = Location::orderBy('name', 'asc')->pluck('name', 'location_id');
    $location->prepend('Select Location', ' ');
    $category = array('International', 'National', 'State');
    return view('backEnd.admin.event.create', compact('country', 'region', 'subregion', 'category', 'location'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required',
      'season' => 'required',
      'country_id' => 'required',
      'generic_name' => 'required',
      'start_date' => 'required',
      'category' => 'required'
    ]);
    // return 'here';
    if ($request['start_date'] != null) {
      $dateArray = explode('-', $request['start_date']);
      $count = count($dateArray);
      if ($count == 1) {
        $request['start_date'] = $request['start_date'] . '-00-00';
      } else {
        $request['start_date'] = $request['start_date'] . '-00';
      }
    }
    $event = Event::create($request->all());
    if ($request->picture_id != '') {
      $picture = new PictureEvent;
      $picture->picture_id = $request->picture_id;
      $picture->event_id = $event->event_id;
      $picture->save();
    }
    Session::flash('message', 'Event added!');
    Session::flash('status', 'success');

    return redirect('admin/' . $this->table_prefix . '/event');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   *
   * @return Response
   */
  public function show($id)
  {
    $event = Event::findOrFail($id);

    return view('backEnd.admin.event.show', compact('event'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   *
   * @return Response
   */
  public function edit($id)
  {
    $event = Event::findOrFail($id);
    $country = Country::orderBy('name', 'asc')->pluck('name', 'country_id');
    $region = Region::orderby('name', 'asc')->pluck('name', 'region_id');
    $region->prepend('Select', '');
    $subregion = SubRegion::orderby('name', 'asc')->pluck('name', 'sub_region_id');
    $subregion->prepend('Select', '');
    $location = Location::orderBy('name', 'asc')->pluck('name', 'location_id');
    $location->prepend('Select Location', ' ');
    $event_picture = PictureEvent::where('event_id', $id)->first();
    $category = array('International', 'National', 'State');

    return view('backEnd.admin.event.edit', compact('event', 'country', 'region', 'subregion', 'event_picture', 'category', 'location'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   *
   * @return Response
   */
  public function update($id, Request $request)
  {

    $this->validate($request, [
      'name' => 'required',
      'season' => 'required',
      'country_id' => 'required',
      'generic_name' => 'required',
      'start_date' => 'required',
      'category' => 'required'
    ]);
    if ($request['start_date'] != null) {
      $dateArray = explode('-', $request['start_date']);
      $count = count($dateArray);
      if ($count == 1) {
        $request['start_date'] = $request['start_date'] . '-00-00';
      } else {
        $request['start_date'] = $request['start_date'] . '-00';
      }
    }
    $event = Event::findOrFail($id);
    $event->update($request->all());
    if ($request['picture_id'] != '') {
      DB::table('wr_picture_event')->where('event_id', '=', $id)->delete();

      DB::table('wr_picture_event')->insert(['picture_id' => $request['picture_id'], 'event_id' => $id]);
    }
    Session::flash('message', 'Event updated!');
    Session::flash('status', 'success');

    return redirect('admin/' . $this->table_prefix . '/event');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    $event = Event::findOrFail($id);

    $event->delete();

    DB::table('wr_picture_event')->where('event_id', '=', $id)->delete();

    Session::flash('message', 'Event deleted!');
    Session::flash('status', 'success');

    return redirect('admin/' . $this->table_prefix . '/event');
  }

  public function events()
  {
    $seasons = Event::groupBy('season')->orderBy('season', 'desc')->select('season')->get();
    $defaultseason = $seasons->first()->season;

    $events = Event::orderBy('start_date', 'desc')->where('season', 'LIKE', $defaultseason . '%')->get();
    if (count($events)) {

      $event_id = $events->first()->event_id;

      $games = DB::table('wr_event_game as p')
        ->join('wr_game as g', 'p.game_id', '=', 'g.game_id')
        ->join('wr_team as t1', 'g.team1_id', '=', 't1.team_id')
        ->join('wr_team as t2', 'g.team2_id', '=', 't2.team_id')
        ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')
        ->select('t1.name as team1', 't2.name as team2', 'g.status as status', 'g.game_date as date', 'g.game_id as game_id', 'v.venue_id as venue_id', 'v.name as venue', 'p.event_id as event_id', 'g.gmt_start_time as time', 'g.phase')
        ->where('p.event_id', '=', $event_id)
        ->get();
    } else {

      $events = array();
      $games = array();
    }
    return view('frontEnd.events', compact('events', 'games', 'seasons', 'defaultseason', 'event_id'));
  }
  public function archiveeventsbyseason($id)
  {
    $defaultseason = str_replace('-', '/', $id);
    $events = Event::orderBy('start_date', 'desc')->where('season', 'LIKE', $defaultseason . '%')->get();
    return view('frontEnd.archive.events.events', compact('events'));
  }
  public function archiveeventseasons()
  {

    $seasons = Event::groupBy('season')->orderBy('season', 'desc')->select('season')->get();
    return view('frontEnd.archive.events.seasons', compact('seasons'));
  }


  public function eventsnew()
  {
    $d_alph = '';
    $query_str = '';
    $seasons_query = Event::groupBy('season')->orderBy('season', 'desc')->select('season');
    $seasons = $seasons_query->pluck('season', 'season');
    $seasons = $seasons->prepend('All seasons', '');
    $defaultseason = $seasons_query->first()->season;
    // $defaultseason = '2019/20';
    $defaultcategory = '';
    $countries = array();
    $countries[0] = 'Select Country';
    $age_group_sel = '';
    $age_group = array('' => 'All Age Group', 'Senior' => 'Senior', 'Youth' => 'Youth', 'Sub-Junior' => 'Sub Junior', 'Junior' => 'Junior');
    $category = array('' => 'All Category', 'International' => 'International', 'National' => 'National', 'State' => 'State');
    //Start Old Query
    /*$vsql=Event::orderBy('start_date', 'desc')->where('season','LIKE',$defaultseason)->get();*/
    //End Old Query
    /*$vsql = DB::table('wr_event as e')
                    ->orderBy('e.start_date', 'desc')
                    ->where('e.season','LIKE',$defaultseason.'%')
                    ->join('wr_event_game as eg','e.event_id','=','eg.event_id')
                    ->join('wr_game as g','eg.game_id','=','g.game_id')
                    ->join('wr_team as t1', 'g.team1_id', '=', 't1.team_id')
                    ->join('wr_team as t2', 'g.team2_id', '=', 't2.team_id') 
                    ->select('t1.name as team1','t2.name as team2','g.game_id as game_id','e.event_id','e.name','e.start_date','g.phase','g.winner')
                    ->groupBy('e.name')
                    ->get();*/
    $vsql = DB::select("SELECT DISTINCT fbe.event_id,fbe.start_date,fbe.name,fbe.category,fbe.season,fc.winner,fc.runnerup,third_place.third,third_place.fourth,fc.winnerregion,fc.runnerupregion,third_place.thirdregion,third_place.fourthregion
FROM wr_event fbe
LEFT JOIN(  
    SELECT if(winner = '1',fbt1.name, if(winner='2',fbt2.name,NUll)) as winner,
    if(winner = '1',fbt2.name, if(winner='2',fbt1.name,NUll)) as runnerup,
       if(winner = '1',fbt1.region, if(winner='2',fbt2.region,NUll)) as winnerregion,
       if(winner = '1',fbt2.region, if(winner='2',fbt1.region,NUll)) as runnerupregion,
       xxfbe.event_id
  FROM wr_event xxfbe
  LEFT JOIN wr_event_game xxfbeg ON xxfbe.event_id = xxfbeg.event_id
  LEFT JOIN wr_game xxfbg ON xxfbeg.game_id = xxfbg.game_id    
    LEFT JOIN wr_team fbt1 ON xxfbg.team1_id = fbt1.team_id
  LEFT JOIN wr_team fbt2 ON xxfbg.team2_id = fbt2.team_id
  WHERE xxfbg.phase = 'Final'
)fc ON fc.event_id = fbe.event_id

LEFT JOIN(  
    SELECT if(winner = '1',fbt1.name, if(winner='2',fbt2.name,NUll)) as third,
    if(winner = '1',fbt2.name, if(winner='2',fbt1.name,NUll)) as fourth,
    if(winner = '1',fbt1.region, if(winner='2',fbt2.region,NUll)) as thirdregion,
    if(winner = '1',fbt2.region, if(winner='2',fbt1.region,NUll)) as fourthregion,
    xxfbe.event_id
  FROM wr_event xxfbe
  LEFT JOIN wr_event_game xxfbeg ON xxfbe.event_id = xxfbeg.event_id
  LEFT JOIN wr_game xxfbg ON xxfbeg.game_id = xxfbg.game_id    
    LEFT JOIN wr_team fbt1 ON xxfbg.team1_id = fbt1.team_id
  LEFT JOIN wr_team fbt2 ON xxfbg.team2_id = fbt2.team_id
  WHERE xxfbg.phase = 'Third Place Play-Off' OR  xxfbg.phase = 'Loosers Final'
)third_place ON third_place.event_id = fbe.event_id

LEFT JOIN wr_event_game fbeg ON fbe.event_id = fbeg.event_id
LEFT JOIN wr_game fbg ON fbeg.game_id = fbg.game_id where fbe.season like '%" . $defaultseason . "' and fbe.category like '%" . $defaultcategory . "' order by fbe.start_date desc , fbe.season desc");
    $country = DB::table('wr_event as e')
      ->join('country as c', 'c.country_id', '=', 'e.country_id')
      ->distinct()
      ->pluck('c.name', 'e.country_id');
    return view('frontEnd.eventlist', compact('vsql', 'seasons', 'countries', 'd_alph', 'defaultseason', 'age_group', 'age_group_sel', 'category', 'defaultcategory', 'query_str'));
  }

  public function searcheventnew(Request $request)
  {
    $d_alph = 'A';
    $seasons_query = Event::groupBy('season')->orderBy('season', 'desc')->select('season');
    $seasons = $seasons_query->pluck('season', 'season');
    $seasons = $seasons->prepend('All seasons', '');
    /*$defaultseason=$request->season_sel;
  $defaultcategory=$request->category_sel;*/
    if ($request->season_sel != '') {

      $defaultseason = $request->season_sel;
    } else {

      $defaultseason = $request->season_sel;
    }
    if ($request->category_sel != '') {

      $defaultcategory = $request->category_sel;
    } else {

      $defaultcategory = '';
    }
    $countries = array();
    $countries[0] = 'Select Country';
    $age_group = array('' => 'All Age Group', 'Senior' => 'Senior', 'Youth' => 'Youth', 'Sub-Junior' => 'Sub Junior', 'Junior' => 'Junior');
    $category = array('' => 'All Category', 'International' => 'International', 'National' => 'National', 'State' => 'State');
    $age_group_sel = $request->age_group;
    $query_str = $request->query_str;

    $sub_query = '';

    if ($request->age_group == 'Senior') {
      $sub_query = "and fbe.name not like '%Junior%' and fbe.name not like '%Youth%'";
    } elseif ($request->age_group == 'Junior') {
      $sub_query = "and fbe.name like '%" . $request->age_group . "%' and fbe.name not like '%Sub-Junior%'";
    } else {
      $sub_query = "and fbe.name like '%" . $request->age_group . "%'";
    }
    // return $sub_query;
    //Start Old Query
    /*$vsql=DB::Select("SELECT * 
                    FROM wr_event e 
                    where e.name like '%".$query_str."%' 
                    ".$sub_query."
                    and e.season like '%".$request->season_sel."' 
                    order by e.name asc");*/
    //End Old Query       
    /*$vsql = DB::table('wr_event as e')
                    ->orderBy('e.name', 'asc')
                    ->where('e.name','LIKE',$query_str.'%')
                    ->where('e.season','LIKE',$request->season_sel.'%')
                    ->join('wr_event_game as eg','e.event_id','=','eg.event_id')
                    ->join('wr_game as g','eg.game_id','=','g.game_id')
                    ->join('wr_team as t1', 'g.team1_id', '=', 't1.team_id')
                    ->join('wr_team as t2', 'g.team2_id', '=', 't2.team_id')
                    ->select('t1.name as team1','t2.name as team2','g.game_id as game_id','e.event_id','e.name','e.start_date','g.phase','g.winner')
                    ->groupBy('e.name')
                    ->get();*/
    $vsql = DB::select("SELECT DISTINCT fbe.event_id,fbe.start_date,fbe.name,fbe.category,fbe.season,fc.winner,fc.runnerup,third_place.third,third_place.fourth,fc.winnerregion,fc.runnerupregion,third_place.thirdregion,third_place.fourthregion
FROM wr_event fbe
LEFT JOIN(  
    SELECT if(winner = '1',fbt1.name, if(winner='2',fbt2.name,NUll)) as winner,
    if(winner = '1',fbt2.name, if(winner='2',fbt1.name,NUll)) as runnerup,
       if(winner = '1',fbt1.region, if(winner='2',fbt2.region,NUll)) as winnerregion,
       if(winner = '1',fbt2.region, if(winner='2',fbt1.region,NUll)) as runnerupregion,
       xxfbe.event_id
  FROM wr_event xxfbe
  LEFT JOIN wr_event_game xxfbeg ON xxfbe.event_id = xxfbeg.event_id
  LEFT JOIN wr_game xxfbg ON xxfbeg.game_id = xxfbg.game_id    
    LEFT JOIN wr_team fbt1 ON xxfbg.team1_id = fbt1.team_id
  LEFT JOIN wr_team fbt2 ON xxfbg.team2_id = fbt2.team_id
  WHERE xxfbg.phase = 'Final'
)fc ON fc.event_id = fbe.event_id

LEFT JOIN(  
    SELECT if(winner = '1',fbt1.name, if(winner='2',fbt2.name,NUll)) as third,
    if(winner = '1',fbt2.name, if(winner='2',fbt1.name,NUll)) as fourth,
    if(winner = '1',fbt1.region, if(winner='2',fbt2.region,NUll)) as thirdregion,
    if(winner = '1',fbt2.region, if(winner='2',fbt1.region,NUll)) as fourthregion,
    xxfbe.event_id
  FROM wr_event xxfbe
  LEFT JOIN wr_event_game xxfbeg ON xxfbe.event_id = xxfbeg.event_id
  LEFT JOIN wr_game xxfbg ON xxfbeg.game_id = xxfbg.game_id    
    LEFT JOIN wr_team fbt1 ON xxfbg.team1_id = fbt1.team_id
  LEFT JOIN wr_team fbt2 ON xxfbg.team2_id = fbt2.team_id
  WHERE xxfbg.phase = 'Third Place Play-Off' OR  xxfbg.phase = 'Loosers Final'
)third_place ON third_place.event_id = fbe.event_id

LEFT JOIN wr_event_game fbeg ON fbe.event_id = fbeg.event_id
LEFT JOIN wr_game fbg ON fbeg.game_id = fbg.game_id where fbe.name like '%" . $query_str . "%'" . $sub_query . " and fbe.season like '%" . $defaultseason . "' and fbe.category like '" . $defaultcategory . "' order by fbe.start_date desc , fbe.season desc");

    $country = DB::table('wr_event as e')
      ->join('country as c', 'c.country_id', '=', 'e.country_id')
      ->distinct()
      ->pluck('c.name', 'e.country_id');
    return view('frontEnd.eventlist', compact('vsql', 'seasons', 'countries', 'd_alph', 'defaultseason', 'age_group', 'age_group_sel', 'category', 'defaultcategory', 'query_str'));
  }

  public function eventsnewbyseason(Request $request, $season)
  {
    if ($season  != 0) {

      $defaultseason = str_replace('-', '/', $season);
    } else {
      $defaultseason = '';
    }
    $d_alph = '';
    $query_str = '';
    $age_group_sel = '';
    $seasons_query = Event::groupBy('season')->orderBy('season', 'desc')->select('season');
    $seasons = $seasons_query->pluck('season', 'season');
    $seasons = $seasons->prepend('All seasons', '');
    $defaultcategory = $request->category_sel;
    $countries = array();
    $countries[0] = 'Select Country';
    $age_group = array('' => 'All Age Group', 'Senior' => 'Senior', 'Youth' => 'Youth', 'Sub-Junior' => 'Sub Junior', 'Junior' => 'Junior');
    $category = array('' => 'All Category', 'International' => 'International', 'National' => 'National', 'State' => 'State');
    /*$vsql=Event::orderBy('start_date', 'desc')->where('season','LIKE',$defaultseason)->get();*/
    $vsql = DB::select("SELECT DISTINCT fbe.event_id,fbe.start_date,fbe.name,fbe.category,fbe.season,fc.winner,fc.runnerup,third_place.third,third_place.fourth,fc.winnerregion,fc.runnerupregion,third_place.thirdregion,third_place.fourthregion
FROM wr_event fbe
LEFT JOIN(  
    SELECT if(winner = '1',fbt1.name, if(winner='2',fbt2.name,NUll)) as winner,
    if(winner = '1',fbt2.name, if(winner='2',fbt1.name,NUll)) as runnerup,
       if(winner = '1',fbt1.region, if(winner='2',fbt2.region,NUll)) as winnerregion,
       if(winner = '1',fbt2.region, if(winner='2',fbt1.region,NUll)) as runnerupregion,
       xxfbe.event_id
  FROM wr_event xxfbe
  LEFT JOIN wr_event_game xxfbeg ON xxfbe.event_id = xxfbeg.event_id
  LEFT JOIN wr_game xxfbg ON xxfbeg.game_id = xxfbg.game_id    
    LEFT JOIN wr_team fbt1 ON xxfbg.team1_id = fbt1.team_id
  LEFT JOIN wr_team fbt2 ON xxfbg.team2_id = fbt2.team_id
  WHERE xxfbg.phase = 'Final'
)fc ON fc.event_id = fbe.event_id

LEFT JOIN(  
    SELECT if(winner = '1',fbt1.name, if(winner='2',fbt2.name,NUll)) as third,
    if(winner = '1',fbt2.name, if(winner='2',fbt1.name,NUll)) as fourth,
    if(winner = '1',fbt1.region, if(winner='2',fbt2.region,NUll)) as thirdregion,
    if(winner = '1',fbt2.region, if(winner='2',fbt1.region,NUll)) as fourthregion,
    xxfbe.event_id
  FROM wr_event xxfbe
  LEFT JOIN wr_event_game xxfbeg ON xxfbe.event_id = xxfbeg.event_id
  LEFT JOIN wr_game xxfbg ON xxfbeg.game_id = xxfbg.game_id    
    LEFT JOIN wr_team fbt1 ON xxfbg.team1_id = fbt1.team_id
  LEFT JOIN wr_team fbt2 ON xxfbg.team2_id = fbt2.team_id
  WHERE xxfbg.phase = 'Third Place Play-Off' OR  xxfbg.phase = 'Loosers Final'
)third_place ON third_place.event_id = fbe.event_id

LEFT JOIN wr_event_game fbeg ON fbe.event_id = fbeg.event_id
LEFT JOIN wr_game fbg ON fbeg.game_id = fbg.game_id where fbe.season like '%" . $defaultseason . "' and fbe.category like '%" . $defaultcategory . "' order by fbe.start_date desc , fbe.season desc");

    $country = DB::table('wr_event as e')
      ->join('country as c', 'c.country_id', '=', 'e.country_id')
      ->distinct()
      ->pluck('c.name', 'e.country_id');
    return view('frontEnd.eventlist', compact('vsql', 'seasons', 'countries', 'd_alph', 'defaultseason', 'age_group', 'age_group_sel', 'category', 'defaultcategory', 'query_str'));
  }

  public function eventsnewbycategory(Request $request, $category)
  {
    $defaultcategory = $category;
    $d_alph = '';
    $query_str = '';
    $age_group_sel = '';
    $seasons_query = Event::groupBy('season')->orderBy('season', 'desc')->select('season');
    $seasons = $seasons_query->pluck('season', 'season');
    $seasons = $seasons->prepend('All seasons', '');
    $defaultseason = $seasons_query->first()->season;
    $countries = array();
    $countries[0] = 'Select Country';
    $age_group = array('' => 'All Age Group', 'Senior' => 'Senior', 'Youth' => 'Youth', 'Sub-Junior' => 'Sub Junior', 'Junior' => 'Junior');
    $category = array('' => 'All Category', 'International' => 'International', 'National' => 'National', 'State' => 'State');
    /*$vsql=Event::orderBy('start_date', 'desc')->where('season','LIKE',$defaultseason)->get();*/
    $vsql = DB::select("SELECT DISTINCT fbe.event_id,fbe.start_date,fbe.name,fbe.category,fbe.season,fc.winner,fc.runnerup,third_place.third,third_place.fourth,fc.winnerregion,fc.runnerupregion,third_place.thirdregion,third_place.fourthregion
FROM wr_event fbe
LEFT JOIN(  
    SELECT if(winner = '1',fbt1.name, if(winner='2',fbt2.name,NUll)) as winner,
    if(winner = '1',fbt2.name, if(winner='2',fbt1.name,NUll)) as runnerup,
       if(winner = '1',fbt1.region, if(winner='2',fbt2.region,NUll)) as winnerregion,
       if(winner = '1',fbt2.region, if(winner='2',fbt1.region,NUll)) as runnerupregion,
       xxfbe.event_id
  FROM wr_event xxfbe
  LEFT JOIN wr_event_game xxfbeg ON xxfbe.event_id = xxfbeg.event_id
  LEFT JOIN wr_game xxfbg ON xxfbeg.game_id = xxfbg.game_id    
    LEFT JOIN wr_team fbt1 ON xxfbg.team1_id = fbt1.team_id
  LEFT JOIN wr_team fbt2 ON xxfbg.team2_id = fbt2.team_id
  WHERE xxfbg.phase = 'Final'
)fc ON fc.event_id = fbe.event_id

LEFT JOIN(  
    SELECT if(winner = '1',fbt1.name, if(winner='2',fbt2.name,NUll)) as third,
    if(winner = '1',fbt2.name, if(winner='2',fbt1.name,NUll)) as fourth,
    if(winner = '1',fbt1.region, if(winner='2',fbt2.region,NUll)) as thirdregion,
    if(winner = '1',fbt2.region, if(winner='2',fbt1.region,NUll)) as fourthregion,
    xxfbe.event_id
  FROM wr_event xxfbe
  LEFT JOIN wr_event_game xxfbeg ON xxfbe.event_id = xxfbeg.event_id
  LEFT JOIN wr_game xxfbg ON xxfbeg.game_id = xxfbg.game_id    
    LEFT JOIN wr_team fbt1 ON xxfbg.team1_id = fbt1.team_id
  LEFT JOIN wr_team fbt2 ON xxfbg.team2_id = fbt2.team_id
  WHERE xxfbg.phase = 'Third Place Play-Off' OR  xxfbg.phase = 'Loosers Final'
)third_place ON third_place.event_id = fbe.event_id

LEFT JOIN wr_event_game fbeg ON fbe.event_id = fbeg.event_id
LEFT JOIN wr_game fbg ON fbeg.game_id = fbg.game_id where fbe.season like '%" . $defaultseason . "' and fbe.category like '%" . $defaultcategory . "' order by fbe.start_date desc , fbe.season desc");

    $country = DB::table('wr_event as e')
      ->join('country as c', 'c.country_id', '=', 'e.country_id')
      ->distinct()
      ->pluck('c.name', 'e.country_id');
    return view('frontEnd.eventlist', compact('vsql', 'seasons', 'countries', 'd_alph', 'defaultseason', 'age_group', 'age_group_sel', 'category', 'defaultcategory', 'query_str'));
  }
  public function eventsnewbyalph($alph)
  {
    $d_alph = '';
    $query_str = '';
    $seasons_query = Event::groupBy('season')->orderBy('season', 'desc')->select('season');
    $seasons = $seasons_query->pluck('season', 'season');
    $seasons = $seasons->prepend('All seasons', '');
    $defaultseason = $seasons_query->first()->season;
    $countries = array();
    $countries[0] = 'Select Country';
    $vsql = Event::orderBy('start_date', 'desc')->where('name', 'LIKE', $alph . '%')->get();
    $country = DB::table('wr_event as e')
      ->join('country as c', 'c.country_id', '=', 'e.country_id')
      ->distinct()
      ->pluck('c.name', 'e.country_id');
    return view('frontEnd.eventlist', compact('vsql', 'seasons', 'countries', 'd_alph', 'defaultseason', 'query_str'));
  }
  public function eventsnewbyid($id)
  {
    $d_alph = 'A';
    $event_id = $id;
    $winner = DB::table('wr_event_game as p')

      ->join('wr_game as g', 'p.game_id', '=', 'g.game_id')
      ->join('wr_team as t1', 'g.team1_id', '=', 't1.team_id')
      ->join('wr_team as t2', 'g.team2_id', '=', 't2.team_id')
      ->join('wr_event as e', 'p.event_id', '=', 'e.event_id')
      ->select('t1.name as team1', 't2.name as team2', 'g.status as status', 'g.game_date as date', 'g.game_id as game_id', 'e.name as event_name', 'e.event_id as event_id', 'p.event_id as event_id', 'g.phase', 'g.local_start_time as time', 'g.winner')
      ->where('p.event_id', '=', $event_id)
      ->where('g.phase', 'final')
      ->get()->toArray();
    // return $event_id;
    $seasons_query = Event::groupBy('season')->orderBy('season', 'desc')->select('season');
    $seasons = $seasons_query->pluck('season', 'season');
    $seasons = $seasons->prepend('All seasons', '');
    $defaultseason = '2019/20';
    // $defaultseason = $seasons_query->first()->season;

    $countries = Country::orderBy('name')->pluck('name', 'country_id');
    $countries->prepend('Select Country', '');

    // $events = Event::orderBy('start_date', 'desc')->where('season','LIKE',$defaultseason.'%')->get();
    $defaultevent = Event::where('event_id', $event_id)->first();
    $events = Event::pluck('name', 'event_id');
    $event_name = Event::where('event_id', $event_id)->first()->name;
    $games = DB::table('wr_event_game as p')

      ->join('wr_game as g', 'p.game_id', '=', 'g.game_id')
      ->join('player as t1', 'g.player1_id', '=', 't1.player_id')
      ->join('player as t2', 'g.player2_id', '=', 't2.player_id')
      ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')
      ->leftjoin('country as c', 'v.country_id', '=', 'c.country_id')
      ->leftjoin('regions as r', 'v.region', '=', 'r.region_id')
      ->leftjoin('sub_regions as sr', 'v.sub_region', '=', 'sr.sub_region_id')
      ->select('t1.full_name as team1', 't2.full_name as team2', 'g.status as status', 'g.game_date as date', 'g.game_id as game_id', 'v.name as venue', 'v.venue_id as venue_id', 'p.event_id as event_id', 'g.phase', 'g.local_start_time as time', 'c.name as countryname', 'r.name as regionname', 'sr.name as subname', 'g.winner', 'g.score1', 'g.score2')
      ->where('p.event_id', '=', $event_id)
      ->orderBy('g.game_date', 'desc')
      ->orderBy('g.local_start_time', 'desc')
      ->orderBy('g.game_id', 'desc')
      ->get();

    return view('frontEnd.events', compact('events', 'games', 'seasons', 'event_name', 'countries', 'd_alph', 'defaultevent', 'winner'));
  }

  public function getcountries(Request $request)

  {


    $countries = DB::table('event as e')
      ->join('country as c', 'c.country_id', '=', 'e.country_id')
      ->distinct()
      ->where('season', 'LIKE', $request->season)
      ->orderBy('c.name', 'asc')
      ->pluck('c.name', 'e.country_id');


    $countries->prepend('Select Country', 0);
    return json_encode($countries);
  }
  public function getevents(Request $request)

  {
    // $events=DB::table('event as e')
    //         ->where('season','=',$request->season)
    //         ->where('country_id','=',$request->country_id)
    //         ->pluck('e.name','e.event_id');

    $events = Event::orderBy('name')->where('season', '=', $request->season)->where('country_id', '=', $request->country_id)->pluck('name', 'event_id');

    $events->prepend('Select Event', 0);
    return json_encode($events);
  }
  public function getgamesbyevent(Request $request)

  {
    $venues = Venue::where('region', '=', $request->region_id)->get();

    $i = 0;
    $inner1 = "";
    foreach ($venues as $item1) {

      if ($i == 0) {
        $active = "active";
        $name = $item1->name;
      } else {
        $active = "";
      }
      $i++;

      $inner1 .= '<li class="menuli ' . $active . '"><a class="venue_id" id="' . $item1->venue_id . '">' . $item1->name . '</a></li>';
    }



    $inner2 = '';

    $venue = Venue::where('sub_region', '=', $request->subregion_id)->first();
    $image = $venue->picture['picture_id'];
    $game_url = url("/venues") . "/" . $item1->venue_id . "/games";

    if ($image) {
      $img = url('/uploads') . '/' . $image . '.jpg';
    } else {
      $img = url('/images') . "/basketball-venue-dummy.jpg";
    }


    $inner2 = '
            <div class="venue-img col-lg-12">
                <h4>' . $venue->name . '</h4>
                <img src="' . $img . '" width="400" align="middle">
            </div>
            
                <div class="venue-info col-lg-12">
                    <div class="venueDetails-content-wrapper">
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Location</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p>' . $venue->address . '</p></div>
                        </div>
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Capacity</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p></p></div>
                        </div>
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Surface Type</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p></p></div>
                        </div>
                        <div class="venueDetails-row">
                            <div class="venueDetails-col1"><p>Facilities</p></div>
                            <div class="venueDetails-col2"><p>:</p></div>
                            <div class="venueDetails-col3"><p></p></div>
                        </div>
                    </div>
                    <a href="' . $game_url . '"><button class="btn btn-default src_btn" type="button">View Games Played</button></a>
                </div>';

    echo json_encode(array($inner1, $inner2));
  }

  public function playerstats($id)
  {
    $event_id = $id;

    $event_name = Event::where('event_id', '=', $id)->first()->name;

    $playerstats = '';

    // return view('frontEnd.wr.eventplayerstats', compact('events', 'event_id', 'playerstats', 'event_name'));
    return view('frontEnd.wr.eventplayerstats', compact('event_id', 'playerstats', 'event_name'));
  }

  public function officialstats($id)
  {
    $event_id = $id;

    $event_name = Event::where('event_id', '=', $id)->first()->name;
    // $teams = DB::table('event_game as p')
    //                     ->join('game_player as gp', 'p.game_id', '=', 'gp.game_id')
    //                     ->where('p.event_id','=',$id)
    //                     ->get();
    $team_id_where = 'AND gp.team_id="$team_id"';
    $team_id_where = "";


    $officialstats = DB::select("
                            SELECT
                            p.player_id,
                            p.full_name,
                            sum(case when gaof.official_type='referee' then 1 else 0 end) as ref,
                            sum(case when gaof.official_type='commissioner' then 1 else 0 end) as comm,
                            sum(case when gaof.official_type='timekeeper' then 1 else 0 end) as tk,
                            sum(case when gaof.official_type='scorer' then 1 else 0 end) as sco,
                            sum(case when gaof.official_type='asst_scorer' then 1 else 0 end) as as_sco,
                            sum(case when gaof.official_type='sht_clk_op' then 1 else 0 end) as sh_clk_op
                            FROM 
                            game_official gaof 
                            left outer join player p on p.player_id=gaof.player_id
                            left outer join game g on g.game_id=gaof.game_id
                            left outer join  event_game eg on eg.game_id=g.game_id
                            where 
                            eg.event_id=$event_id
                            and g.status <> 'fixture'
                            group BY
                            p.player_id

        ");
    $teamofficialstats = DB::select("
                            SELECT
                            p.player_id,
                            p.full_name,
                            sum(case when gaof.official_type='coach' then 1 else 0 end) as coach,
                            sum(case when gaof.official_type='assistantcoach' then 1 else 0 end) as as_coach
                            FROM 
                            game_team_official gaof 
                            left outer join player p on p.player_id=gaof.player_id
                            left outer join game g on g.game_id=gaof.game_id
                            left outer join  event_game eg on eg.game_id=g.game_id
                            where 
                            eg.event_id=$event_id
                            and g.status <> 'fixture'
                            group BY
                            p.player_id

        ");
    return view('frontEnd.officialstats', compact('events', 'teams', 'event_id', 'officialstats', 'event_name', 'event_id', 'teamofficialstats'));
  }

  public function eventsbyid($id)
  {
    $seasons = Event::groupBy('season')->orderBy('season', 'desc')->select('season')->get();
    $defaultseason = '';
    $event_id = $id;
    $events = Event::where('event_id', '=', $id)->get();
    if (count($events)) {

      $games = DB::table('wr_event_game as p')
        ->join('wr_game as g', 'p.game_id', '=', 'g.game_id')
        ->join('player as t1', 'g.player1_id', '=', 't1.player_id')
        ->join('player as t2', 'g.player2_id', '=', 't2.player_id')
        ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')
        ->select('t1.full_name as team1', 't2.full_name as team2', 'g.status as status', 'g.game_date as date', 'g.game_id as game_id', 'v.venue_id as venue_id', 'v.name as venue', 'p.event_id as event_id', 'g.gmt_start_time', 'g.phase', 'g.local_start_time as time')
        ->where('p.event_id', '=', $id)
        ->get();
    } else {
      $events = array();
      $games = array();
    }
    return view('frontEnd.events', compact('events', 'games', 'seasons', 'defaultseason', 'event_id'));
  }


  public function eventsbyseason($season)
  {


    $seasons = Event::groupBy('season')->orderBy('season', 'desc')->select('season')->get();
    $defaultseason = $season;
    $title = $season;


    $events = Event::orderBy('start_date', 'desc')->where('season', 'LIKE', $season . '%')->get();
    if (count($events)) {

      $event_id = $events->first()->event_id;

      $games = DB::table('event_game as p')
        ->join('game as g', 'p.game_id', '=', 'g.game_id')
        ->join('team as t1', 'g.team1_id', '=', 't1.team_id')
        ->join('team as t2', 'g.team2_id', '=', 't2.team_id')
        ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')
        ->select('t1.name as team1', 't2.name as team2', 'g.status as status', 'g.game_date as date', 'g.game_id as game_id', 'v.venue_id as venue_id', 'v.name as venue', 'p.event_id as event_id', 'g.gmt_start_time', 'g.phase', 'g.local_start_time as time')
        ->where('p.event_id', '=', $event_id)
        ->get();
    } else {

      $events = array();
      $games = array();
    }
    return view('frontEnd.events', compact('events', 'games', 'seasons', 'defaultseason', 'event_id', 'title'));
  }
  public function latestEvents()
  {

    $events = Event::orderBy('start_date', 'desc')->limit(2)->get();
    return $events;
  }
  public function getslider()
  {

    $events = Event::orderBy('start_date', 'desc')->limit(5)->get();
    return $events;
  }
  public function showgames(Request $request)
  {

    $event_id = $request->event_id;

    $games = DB::table('event_game as p')
      ->join('game as g', 'p.game_id', '=', 'g.game_id')
      ->join('team as t1', 'g.team1_id', '=', 't1.team_id')
      ->join('team as t2', 'g.team2_id', '=', 't2.team_id')
      ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')
      ->select('t1.name as team1', 't2.name as team2', 'g.status as status', 'g.game_date as date', 'g.game_id as game_id', 'v.name as venue', 'v.venue_id as venue_id', 'p.event_id as event_id', 'g.gmt_start_time as time', 'g.phase')
      ->where('p.event_id', '=', $event_id)
      ->get();

    $inner = '<div class=" col-lg-12 stats-box">
            <a href="' . url('/events/' . $event_id . '/playerstats') . '">
            <button class="btn btn-default src_btn" type="button">Get Player Stats</button>
            </a>
          </div><table class="table table-responsive table-striped" id="tbladmin">
          <thead>
            <th>Date</th>
            <th>Event</th>
            <th>Venue</th>
          </thead>
        <tbody class="tablebody">';
    foreach ($games as $item2) {
      $url = url('scorecard/' . $item2->game_id);
      if ($item2->status != 'fixture') {
        $title = '<a href="' . $url . '">' . $item2->team1 . ' v ' . $item2->team2 . '</a>';
      } else {
        $title =  $item2->team1 . ' v ' . $item2->team2;
      }
      $venue_url = url('venues/' . $item2->venue_id);
      $inner .= '<tr>
                    <td>' . date("d M Y", strtotime($item2->date)) . '</td>
                    <td>' . $title . '</td>
                    <td><a href="' . $venue_url . '">' . $item2->venue . '</a></td>
                 </tr>';
    }

    echo $inner . '</tbody>
      </table> ';
  }



  /*import event*/
  public function upload()
  {
    return view('backEnd.admin.event.uploadevents');
  }
  public function import(Request $request)
  {
    // return $request->event_id;
    $fus = Excel::import(new EventImport, request()->file('file'));

    Session::flash('message', 'Event added!');
    Session::flash('status', 'success');

    return view('backEnd.admin.event.uploadevents');
  }
}
