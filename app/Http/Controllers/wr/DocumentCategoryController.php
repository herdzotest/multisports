<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Document;
use App\Sport;
use App\DocumentCategory;
use App\DocumentSubCategory;
use App\GoverningBody;
use App\UserHasAccess;
use Carbon\Carbon;
use Session;
use Auth;
use DB;

class DocumentCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $documentcategory=DocumentCategory::all();
        return view('backEnd.admin.documentcategory.index', compact('documentcategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        $sports = Sport::pluck('sport_name','sport_id');
        $sports->prepend('Select Associated Sport',' ');
        return view('backEnd.admin.documentcategory.create',compact('sports'));
    }

   
    public function store(Request $request)
    {
        $this->validate($request, [
            'document_category' => 'required'
        ]);
       $documentcategory=new DocumentCategory;
       $documentcategory->category_name=$request->document_category;
       $documentcategory->user_id=auth()->user()->id;
       $documentcategory->sport_id=$request->sport;
       $documentcategory->save();
       Session::flash('message', 'Document Category added!');
       Session::flash('status', 'success');
       $documentcategory=DocumentCategory::all();
       return view('backEnd.admin.documentcategory.index', compact('documentcategory'));   
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $documentcategory = DocumentCategory::findOrFail($id);
     $sports = Sport::pluck('sport_name','sport_id');
     $sports->prepend('Select Associated Sport',' ');
     return view('backEnd.admin.documentcategory.edit', compact('documentcategory','sports'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'document_category' => 'required'
        ]);
        $documentcategory = DocumentCategory::findOrFail($id);
        $documentcategory->category_name=$request->document_category;
        $documentcategory->sport_id=$request->sport;
        $documentcategory->save();
        Session::flash('message', 'Document Category updated!');
        Session::flash('status', 'success');

        return redirect('admin/documentcategory');
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Document::where('category_id','=',$id)->get();
        $category1 = DB::table('cat_subcat_link')->where('category_id','=',$id)->get();
      
        if((count($category) == 0) && (count($category1) == 0))
        {
            $documentcategory = DocumentCategory::findOrFail($id);

            $documentcategory->delete();

            Session::flash('message', 'Document Category deleted!');
            Session::flash('status', 'success');

            
        }
        else
        {
            Session::flash('message', 'cannot delete because this category id is used to document table!');
            Session::flash('status', 'success');
        }
        return redirect('admin/documentcategory');
    }
}
