<?php

namespace App\Http\Controllers\wr;

use App\Http\Requests;
use App\Http\Controllers\wr\Controller;
use App\Player;
use App\PlayerMetaData;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;
use App\Sport;
use App\Country;
use App\Model\wr\Event as Event;
use App\Model\wr\GamePlayer as GamePlayer;
use App\Model\wr\PicturePlayer as PicturePlayer;
use App\Model\wr\Team as Team;
use App\Model\wr\PlayerTeam as PlayerTeam;
use App\Model\wr\EventSquad as EventSquad;
use App\SubRegion;
class PlayerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
     protected $sport_id,$table_prefix;
     public function __construct()
    {   
         $sport_id="";   
         // $this->table_prefix= \Session::get('table_prefix');
        $this->middleware(function ($request, $next){
        //$this->table_prefix = session('table_prefix');
        session()->put('table_prefix', 'wr');
        $this->table_prefix =   session()->get('table_prefix');
        $sport_id_arr=Sport::where('identifier',$this->table_prefix)->first();
        $this->sport_id=$sport_id_arr->sport_id;
        return $next($request);
        });
         
    }

    
    public function index()
    {  
       // return $this->sport_id;
        $player = Player::where('sport_id',$this->sport_id)->get();
        // print_r($player);
        // return;
        return view('backEnd.admin.player.index', compact('player'));
    }
    public function create()
    {   $json_decoded=array();
        $info_json="";
        $country = Country::orderby('name','asc')->pluck('name', 'country_id');
        $country->prepend('Select', '');
        $addtnl_info=DB::table('player_adtnl_info')->where('sport_id', '=', $this->sport_id)->get(); 
        // print_r($addtnl_info);

        foreach ($addtnl_info as $key ) {
            if(isset($key->info_json)){
             $info_json=$key->info_json;
            }else{
              
            }
        }
       
        $json_decoded=json_decode($info_json,true);
         // dd($json_decoded);     
        $district=SubRegion::where('region_id','1')->orderby('name','asc')->pluck('name','sub_region_id');   
         $district->prepend('Select District', '');
         $team=array();
        return view('backEnd.admin.player.create',compact('country','json_decoded','district','team'));
    }
    public function store(Request $request)
    {
     
        $this->validate($request, [
            'full_name' => 'required',
            'sort_name' => 'required',    
            'game_name' => 'required',       
            'gender' => 'required',
            'country_id' => 'required',
            'regno' => 'nullable|unique:player'           
        ],
        [
          'full_name.required' => 'Full name is required',
          'sort_name.required' => 'Sort name is required',
          'game_name.required' => 'Game name is required',
          'gender.required' => 'Gender is required',
          'country_id.required' => 'Please select country',
          'regno.unique' => 'The regno has already been taken.'
        ]);
        // print_r($request->addtnl_info);
        // print_r($request->addtnl_info_type);
        // return;
        $picture_id = $request['picture_id'];
         $request['date_of_birth'] = $request['date_of_birth']!=""?date("Y-m-d", strtotime($request['date_of_birth'])):"0000-00-00";
        $request['date_of_death'] = $request['date_of_death']!=""?date("Y-m-d", strtotime($request['date_of_death'])):"0000-00-00";
        $request['sport_id']=$this->sport_id;
        $player_id = Player::create($request->all());
        if($picture_id!=""){
        $picture = new PicturePlayer;
        $picture->picture_id = $picture_id;
        $picture->player_id = $player_id->player_id;
        $picture->save();
        }
		if($request->addtnl_info_type != null)
		{
			for ($i=0; $i <sizeof($request->addtnl_info_type) ; $i++) { 
            if($request->addtnl_info[$i]!=""){
                $add_inf=new PlayerMetaData;
                $add_inf->player_id=$player_id->player_id;
                $add_inf->meta_key=$request->addtnl_info_type[$i];
                $add_inf->meta_data=$request->addtnl_info[$i];
                $add_inf->save();
            }
        }
		}
        
        Session::flash('message', 'Player added!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/player');
    }
      public function show($id)
    {
        $player = Player::findOrFail($id);

        $matches_count = GamePlayer::where('player_id','=',$id)->distinct("game_id")->count();

        return view('backEnd.admin.player.show', compact('player','matches_count'));
    }

     public function edit($id)
    {  
     $json_decoded=array();
        $info_json="";
        $player = Player::findOrFail($id);
        // $player->date_of_birth = $player->date_of_birth!=""?date("m/d/Y", strtotime($player->date_of_birth)):"";
        // $player->date_of_death = $player->date_of_death!=""?date("m/d/Y", strtotime($player->date_of_death)):"";
        $player_picture=PicturePlayer::where('player_id',$id)->first();
        
        // echo $player_picture['picture_id'];
        // return;
        $country = Country::orderby('name','asc')->pluck('name', 'country_id');
        $addtnl_info=DB::table('player_adtnl_info')->where('sport_id', '=', $this->sport_id)->get(); 
        // print_r($addtnl_info);

        foreach ($addtnl_info as $key ) {
            if(isset($key->info_json)){
             $info_json=$key->info_json;
            }else{
              
            }
        }
        $json_decoded=json_decode($info_json,true);
        $player_meta_data=PlayerMetaData::select('meta_key','meta_data')->where('player_id',$id)->get()->toArray();
     
        $meta_key=array();
        $meta_data_key=array();
        foreach ($player_meta_data as $key => $value) {
            $meta_key[]=$value['meta_key'];
            $meta_data_key[$value['meta_key']]=$value['meta_data'];
        }
      $district=SubRegion::where('region_id','1')->orderby('name','asc')->pluck('name','sub_region_id');   
        $district->prepend('Select District', '');
        $team =Team::where('sub_region_id',$player->district)->pluck('name','team_id');
        return view('backEnd.admin.player.edit', compact('player','country','player_picture','json_decoded','meta_key','meta_data_key','district','team'));
    }
     public function update($id, Request $request)
    {
        $date_of_birth = $request['date_of_birth'];
      $birth = str_replace('/', '-', $date_of_birth);
      $birthDate = date('Y-m-d',strtotime($birth));
      $date_of_death = $request['date_of_death'];
      $death = str_replace('/', '-', $date_of_death);
      $deathDate = date('Y-m-d',strtotime($death));
         $this->validate($request, [
            
            'full_name' => 'required',
            'game_name' => 'required',
            'sort_name' => 'required',           
            'gender' => 'required',
            'country_id' => 'required'

           
        ]);
        $player = Player::findOrFail($id);
         $request['date_of_birth'] = $request['date_of_birth']!=""?$birthDate:"0000-00-00";
        $request['date_of_death'] = $request['date_of_death']!=""?$deathDate:"0000-00-00";

        if($request['picture_id']!=''){
			DB::table('wr_picture_player')->where('player_id', '=', $id)->delete();    
			DB::table('wr_picture_player')->insert(['picture_id' => $request['picture_id'], 'player_id' => $id]);    
        }
		else{
			DB::table('wr_picture_player')->where('player_id', '=', $id)->delete();    
			DB::table('wr_picture_player')->insert(['picture_id' => '', 'player_id' => $id]);   
		}
		
	
		
        $player->update($request->all());
        $meta_data_del=PlayerMetaData::where('player_id',$id)->delete();
		
		$info = $request->addtnl_info_type;
		if($info != null)
		{
			for ($i=0; $i <sizeof($info) ; $i++) 
			{ 
				if($request->addtnl_info[$i]!="")
				{
					$add_inf=new PlayerMetaData;
					$add_inf->player_id=$id;
					$add_inf->meta_key=$request->addtnl_info_type[$i];
					$add_inf->meta_data=$request->addtnl_info[$i];
					$add_inf->save();
				}
			}  
		}
              

        Session::flash('message', 'Player updated!');
        Session::flash('status', 'success');

       return redirect('admin/'.$this->table_prefix.'/player');
    }
     public function destroy($id)
    {
        $player = Player::findOrFail($id);

        $player->delete();
        $player_meta_data=PlayerMetaData::where('player_id',$id)->delete();

        Session::flash('message', 'Player deleted!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/player');
    }
    public function players(Request $request)
    {  
	
	
		$request_url = request()->segment(2);
        $d_alph = 'A';
        $query_str = $request->query_str;
        //$vsql = Player::where('sort_name','LIKE',$d_alph.'%')->where('sport_id','=',$this->sport_id)->where('gender','=',$request_url)->orderBy('sort_name','asc')->paginate(20);
        // print_r($vsql);
		$vsql = DB::table('player as p')
                     ->leftjoin($this->table_prefix.'_playerteam as gp','gp.player_id','=','p.player_id')
                     ->leftjoin($this->table_prefix.'_team as t','t.team_id','=','gp.team_id')
                     ->leftjoin('regions as r','t.region','=','r.region_id')
                     //->select(DB::raw('p.full_name,group_concat(t.name) as name, group_concat(gp.start_season) as start_season, group_concat(gp.end_season) as end_season, p.player_id,p.gender'))

                     ->select(DB::raw('p.full_name,group_concat(t.name) as name, group_concat(IF(gp.start_season !="", gp.start_season, "")) as start_season, group_concat(IF(gp.end_season !="", gp.end_season, "" )) as end_season, p.player_id,p.gender'))
                     ->where('p.sort_name','LIKE',$d_alph.'%')
                     ->where('p.sport_id','=',$this->sport_id)
                     ->where('p.gender','=',$request_url)
                     ->where('r.region_id','=','1')
                     ->groupBy('p.player_id')
                     ->orderBy('p.sort_name', 'ASC')
                     ->get();
        $country=DB::table('wr_event as e')
          ->join('country as c', 'c.country_id', '=', 'e.country_id')
          ->distinct()
          ->pluck('c.name', 'e.country_id');
        $country->prepend('Select Country', '');
        $t_srch = Team::orderby('name')->pluck('name', 'team_id');
        $t_srch->prepend('Select Team','');
        return view('frontEnd.playerlist',compact('vsql','country','t_srch','d_alph','query_str'));
    }

    public function searchplayer(Request $request){
		$request_url = request()->segment(2);
        // return "herhe";
        $query_str = $request->query_str;
        $d_alph = strtoupper(substr($query_str, 0, 1));
        /*$vsql = Player::leftJoin('sub_regions as sub', function($join) {
              $join->on('sub.sub_region_id', '=', 'district');
              })->where('sub.region_id','=','1')
			  ->where('sort_name','LIKE','%'.$query_str.'%')
			  ->where('sport_id','=',$this->sport_id)
			  ->where('gender','=',$request_url)
			  ->orderBy('sort_name','asc')
			  ->paginate(20);*/
        
		$vsql = DB::table('player as p')
                     ->leftjoin($this->table_prefix.'_playerteam as gp','gp.player_id','=','p.player_id')
                     ->leftjoin($this->table_prefix.'_team as t','t.team_id','=','gp.team_id')
                     ->leftjoin('regions as r','t.region','=','r.region_id')
                     //->select(DB::raw('p.full_name,group_concat(t.name) as name, group_concat(gp.start_season) as start_season, group_concat(gp.end_season) as end_season, p.player_id,p.gender'))

                     ->select(DB::raw('p.full_name,group_concat(t.name) as name, group_concat(IF(gp.start_season !="", gp.start_season, "")) as start_season, group_concat(IF(gp.end_season !="", gp.end_season, "" )) as end_season, p.player_id,p.gender'))
                     ->where('p.sort_name','LIKE','%'.$query_str.'%')
                     ->where('p.sport_id','=',$this->sport_id)
                     ->where('p.gender','=',$request_url)
                     ->where('r.region_id','=','1')
                     ->groupBy('p.player_id')
					 
					->orderBy('p.sort_name','asc')
                     ->get();
		
        $country=DB::table('wr_event as e')
          ->join('country as c', 'c.country_id', '=', 'e.country_id')
          ->distinct()
          ->pluck('c.name', 'e.country_id');
        $country->prepend('Select Country', '');
        $t_srch = Team::orderby('name')->pluck('name', 'team_id');
        $t_srch->prepend('Select Team','');
        return view('frontEnd.playerlist',compact('vsql','country','t_srch','d_alph','query_str'));

    }


 public function playersbyalph(Request $request,$alph)
    {
		/*$request_url = request()->segment(2);
        $title = "Players";
        $d_alph = ucwords($alph);
        $country=DB::table('wr_event as e')
          ->join('country as c', 'c.country_id', '=', 'e.country_id')
          ->distinct()
          ->pluck('c.name', 'e.country_id');
        $country->prepend('Select Country', '');
    
        $vsql = DB::table('player as p')
            ->select('p.full_name','p.player_id')
            ->distinct()
            ->where('sort_name','LIKE',$alph.'%')
            ->where('sport_id','=',$this->sport_id)
			->where('gender','=',$request_url)
            ->orderBy('p.sort_name', 'asc')
            ->get();
        $t_srch = Team::orderby('name')->pluck('name', 'team_id');
        $t_srch->prepend('Select Team', '');
        return view('frontEnd.playerlist', compact('t_srch','vsql','country','title','d_alph'));*/
		$request_url = request()->segment(2);
        $title = "Players";
        $d_alph = ucwords($alph);
        $query_str = $request->query_str;
        $country=DB::table('wr_event as e')
          ->join('country as c', 'c.country_id', '=', 'e.country_id')
          ->distinct()
          ->pluck('c.name', 'e.country_id');
        $country->prepend('Select Country', '');
    


            /*$vsql = Player::leftJoin('sub_regions as sub', function($join) {
              $join->on('sub.sub_region_id', '=', 'district');
              })
			  ->where('sub.region_id','=','1')
			  ->where('sort_name','LIKE', $alph.'%')
			  ->where('sport_id','=',$this->sport_id)
			  ->where('gender','=',$request_url)
			  ->orderBy('sort_name','asc')
			  ->paginate(20);*/
			  $vsql = DB::table('player as p')
                     ->leftjoin($this->table_prefix.'_playerteam as gp','gp.player_id','=','p.player_id')
                     ->leftjoin($this->table_prefix.'_team as t','t.team_id','=','gp.team_id')
                     ->leftjoin('regions as r','t.region','=','r.region_id')
                     //->select(DB::raw('p.full_name,group_concat(t.name) as name, group_concat(gp.start_season) as start_season, group_concat(gp.end_season) as end_season, p.player_id,p.gender'))
                     
                     ->select(DB::raw('p.full_name,group_concat(t.name) as name, group_concat(IF(gp.start_season !="", gp.start_season, "")) as start_season, group_concat(IF(gp.end_season !="", gp.end_season, "" )) as end_season, p.player_id,p.gender'))
                     ->where('p.sort_name','LIKE',$alph.'%')
                     ->where('p.sport_id','=',$this->sport_id)
                     ->where('p.gender','=',$request_url)
                     ->where('r.region_id','=','1')
                     ->groupBy('p.player_id')
                     ->orderBy('p.sort_name', 'ASC')
                     ->get();
        $t_srch = Team::orderby('name')->pluck('name', 'team_id');
        $t_srch->prepend('Select Team', '');
        return view('frontEnd.playerlist', compact('t_srch','vsql','country','title','d_alph','query_str'));
    }


///-----------------------------------------------------------------//



    public function search(Request $request)
    {
        $query= $request['query'];
        // return $query;
        $player = Player::where('full_name', 'LIKE', "%".$query."%")->orwhere('game_name', 'LIKE', "%".$query."%")->paginate(100);
        return view('backEnd.admin.player.index', compact('player'));
    }
   public function mergeplayerssave(Request $request)
   {
    $keep=$request->keep;
    $delete=$request->delete;
    
    $update=GamePlayer::where('player_id', '=', $delete)
                      ->update(['player_id' => $keep]);
    $dlt=GamePlayer::where('player_id',$delete)->delete();  
    $update2=GameOfficial::where('player_id', '=', $delete)
                      ->update(['player_id' => $keep]);
    $dlt2=GameOfficial::where('player_id',$delete)->delete();  
    $update3=GameTeamOfficial::where('player_id', '=', $delete)
                      ->update(['player_id' => $keep]);
    $dlt3=GameTeamOfficial::where('player_id',$delete)->delete();
    $dlt5=PicturePlayer::where('player_id',$delete)->delete();
    $dlt4=Player::where('player_id',$delete)->delete();

    Session::flash('message', 'Players mereged successfully!  '.$request->game_id);  
    Session::flash('alert-class', 'alert-success');                  
    return view('backEnd.admin.player.mergeplayers');  
   }
   public function mergeplayersdisp(Request $request)
    {
    
        $keep=$request->player_to_keep;
        $delete=$request->player_to_delete;
        $keepdisp=
                DB::Select("select main.* from 
                (SELECT
                p.player_id,
                p.full_name,
                DATE_FORMAT(p.date_of_birth,'%d-%m-%Y')dob,
                p.place_of_birth,
                case when p.gender='m' then 'Male' else (case when p.gender='f' then 'Female' else '' end) end gender ,
                count(gp.game_id) number_of_matches,
                count(gto.game_id)+count(gao.game_id) matches_officiated
                from player p
                left outer join game_player gp on gp.player_id=p.player_id
                left outer join game_team_official gto on gto.player_id=p.player_id
                left outer join game_official gao on gao.player_id=p.player_id
                where 
                p.player_id=".$keep.")main
                where main.player_id<>''
                ");
          $deletedisp=
                DB::Select("select main.* from 
                (SELECT
                p.player_id,
                p.full_name,
                DATE_FORMAT(p.date_of_birth,'%d-%m-%Y')dob,
                p.place_of_birth,
                case when p.gender='m' then 'Male' else (case when p.gender='f' then 'Female' else '' end) end gender ,
                count(gp.game_id) number_of_matches,
                count(gto.game_id)+count(gao.game_id) matches_officiated
                from player p
                left outer join game_player gp on gp.player_id=p.player_id
                left outer join game_team_official gto on gto.player_id=p.player_id
                left outer join game_official gao on gao.player_id=p.player_id
                where 
                p.player_id=".$delete.")main
                where main.player_id<>''
                ");      
                // print_r($keepdisp);
        // return;
        // if(($keepdisp[0]->player_id=="") || ($deletedisp->player_id=="")){ 
        if(empty($deletedisp) || empty($keepdisp)) {  
        if(empty($deletedisp) && empty($keepdisp)) { 
        Session::flash('message', 'Players not found');  
        Session::flash('alert-class', 'alert-danger');  
        }elseif(empty($deletedisp)){    
        Session::flash('message', 'Player to be deleted not found');  
        Session::flash('alert-class', 'alert-danger'); 
        }elseif (empty($keepdisp)) {
         Session::flash('message', 'Player to be kept not found');  
         Session::flash('alert-class', 'alert-danger'); 
        }
        return view('backEnd.admin.player.mergeplayers');
        }else{         
        return view('backEnd.admin.player.mergeplayersdisp',compact('keep','delete','keepdisp','deletedisp'));
        }
    }
     public function mergeplayers()
    {
    

        return view('backEnd.admin.player.mergeplayers');
    }

   
    

        public function playersbycountry($country_id)
    {
        $title = "Players";
        $d_alph = '';
        $country_name = Country::where('country_id','=',$country_id)->first()->name;
        $nav_title = 'Showing teams in <b>'.$country_name.'</b>';
        $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        $players = Player::orderBy('full_name', 'asc')->where('country_id','=',$country_id)->where('full_name','!=','')->get();
        $player =$players->first();

        return view('frontEnd.players', compact('players','player','country','title','d_alph','nav_title'));
    }
    public function playersbycountrynew($country_id)
    {
        $title = "Players";
        $d_alph = '';
        $country_name = Country::where('country_id','=',$country_id)->first()->name;
        $nav_title = 'Showing players in <b>'.$country_name.'</b>';
        $country=DB::table('event as e')
          ->join('country as c', 'c.country_id', '=', 'e.country_id')
          ->distinct()
          ->pluck('c.name', 'e.country_id');
        $country->prepend('Select Country', '');
       

       $countryplayers = DB::table('player as p')
            ->select('p.full_name','p.player_id')
            ->distinct()
            ->where('full_name','!=','')
            ->orderBy('p.full_name', 'asc')
             ->get();

        // $player =$players->first();
        $t_srch = Team::where('country_id','=',$country_id)->orderby('name')->pluck('name', 'team_id');
        $t_srch->prepend('Select Team', '');
        return view('frontEnd.playerscountry', compact('t_srch','countryplayers','country','title','d_alph','nav_title'));
    }
    public function playersbyteam($team_id)
    {
        $title = "Players";
        $d_alph = '';
        $team_name = Team::where('team_id','=',$team_id)->first()->name;
        $nav_title = 'Showing players in <b>'.$team_name.'</b>';
        $t_srch = Team::orderby('name')->pluck('name', 'team_id');
        $t_srch->prepend('Select Team', '');
        
        $players = DB::table('player as p')
        ->join('game_player as gp','gp.player_id', '=', 'p.player_id')
       
        ->where('gp.team_id','=',$team_id)
        ->where('p.full_name','!=','')
        ->orderBy('p.full_name', 'asc')
        ->get();
        $player =$players->first();
        $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        return view('frontEnd.players', compact('players','player','country','t_srch','title','d_alph','nav_title'));
    }
    public function playersbyteamnew($team_id)
    {
        $title = "Players";
        $d_alph = '';
        $team_name = Team::where('team_id','=',$team_id)->first()->name;
        $nav_title = 'Showing players in <b>'.$team_name.'</b>';
        $t_srch = Team::orderby('name')->pluck('name', 'team_id');
        $t_srch->prepend('Select Team', '');
         $teamplayers="";
        $teamplayers = DB::table('player as p')
        ->join('game_player as gp','gp.player_id', '=', 'p.player_id')
        ->select('p.full_name','p.player_id')
        ->distinct()
        ->where('gp.team_id','=',$team_id)
        ->where('p.full_name','!=','')

        ->orderBy('p.full_name', 'asc')

        ->get();
        // $player =$players->first();
        $country=DB::table('event as e')
          ->join('country as c', 'c.country_id', '=', 'e.country_id')
          ->distinct()
          ->pluck('c.name', 'e.country_id');
        $country->prepend('Select Country', '');
        return view('frontEnd.playersteam', compact('teamplayers','country','t_srch','title','d_alph','nav_title'));
    }
    public function playersbyid($id)
    {
        $title = "Players";
        $d_alph = '';
        $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        $players = Player::orderBy('full_name', 'asc')->where('player_id','=',$id)->get();
        $player =$players->first();

        if(count($players)){
             return view('frontEnd.players', compact('players','player','country','title','d_alph'));
        }else{
            return view('frontEnd.error.404');
        }

       
    }
     public function playersnewbyid($id)
    {
        $title = "Players";
        $d_alph = '';
        $playerid = $id;
        $country=DB::table('wr_event as e')
          ->join('country as c', 'c.country_id', '=', 'e.country_id')
          ->distinct()
          ->pluck('c.name', 'e.country_id');
        $country->prepend('Select Country', '');
        // $players = Player::orderBy('full_name', 'asc')->where('player_id','=',$id)->get();
        // $player =$players->first();
        $players= DB::Select("SELECT p.*,IFNULL(pic.picture_id,'')picture_id,'' as teams 
            from player p 
            left outer join wr_picture_player pp on pp.player_id=p.player_id
            left outer join picture pic on pic.picture_id=pp.picture_id
            where
            p.player_id=".$id."
            order by p.full_name asc"
            );

        $player =$players[0];
     
        $games = DB::table('wr_game as g')
        ->join('player as p1', 'g.player1_id', '=', 'p1.player_id')
        ->join('player as p2', 'g.player2_id', '=', 'p2.player_id')
        ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')

        ->join('wr_event_game as eg', 'eg.game_id', '=', 'g.game_id')
        ->join('wr_event as e', 'e.event_id', '=', 'eg.event_id')
            
        ->select('p1.full_name as team1','p2.full_name as team2','g.game_date as date','g.game_id as game_id','v.venue_id as venue_id','v.name as venue','p1.game_name as player','g.status','g.local_start_time as time','eg.event_id as event_id','e.name as event_name')
        ->where('g.player1_id','=',$id)
        ->orWhere('g.player2_id','=',$id)
        ->orderby('date','desc')
        ->get();
        $events = DB::table('wr_game as g')
        ->join('player as p1', 'g.player1_id', '=', 'p1.player_id')
        ->join('player as p2', 'g.player2_id', '=', 'p2.player_id')
        ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')

        ->join('wr_event_game as eg', 'eg.game_id', '=', 'g.game_id')
        ->join('wr_event as e', 'e.event_id', '=', 'eg.event_id')
            
        ->select('p1.full_name as team1','p2.full_name as team2','g.game_date as date','g.game_id as game_id','v.venue_id as venue_id','v.name as venue','p1.game_name as player','g.status','g.local_start_time as time','eg.event_id as event_id','e.name as event_name')
        ->where('g.player1_id','=',$id)
        ->orWhere('g.player2_id','=',$id)
        ->orderby('date','desc')
        ->groupBy('eg.event_id')
        ->get();
           
        $addtnl_info=DB::table('player_adtnl_info')->where('sport_id', '=', $this->sport_id)->get(); 
        $info_json = '';
            foreach ($addtnl_info as $key ) {
                if(isset($key->info_json)){
                 $info_json .= $key->info_json;
                }else{
                  
                }
            }
        $info_json .= '';
        $json_decoded = json_decode($info_json,true);
        $player_info  = DB::table('player_adtnl_meta_data')->select('meta_key','meta_data')->where('player_id',$id)->get();
        $team_name="";
        $teamsSeason = PlayerTeam::join('wr_team as t','wr_playerteam.team_id','=','t.team_id')->where('wr_playerteam.player_id','=',$id)->select('t.name','wr_playerteam.start_season','wr_playerteam.end_season')->get();
        $weightcategorydetails = EventSquad::leftjoin('wr_weight_category as wc','wc.weight_category_id','=','wr_event_squad.weight_cat')->where('wr_event_squad.player_id','=',$id)->select('wc.category_desc','wr_event_squad.position')->get()->toarray();
        if(!empty($weightcategorydetails[0]['category_desc']))
        {

            $weightcategory = $weightcategorydetails[0]['category_desc'];
        }
        else
        {

            $weightcategory = '';
        }
        if(!empty($weightcategorydetails[0]['position']))
        {

            $position = $weightcategorydetails[0]['position'];
        }
        else
        {

            $position = '';
        } 
        //$position = $weightcategorydetails[0]['position'];
        $seasons_query = Event::groupBy('season')->orderBy('season','desc')->select('season');
        $seasons=$seasons_query->pluck('season','season');
        $seasons=$seasons->prepend('All seasons','');
        $defaultseason=$seasons_query->first()->season;
        $defaultcategory='';
        if(count($players)){
             return view('frontEnd.players', compact('players','player','country','title','d_alph','games','team_name','json_decoded','player_info','teamsSeason','category_desc','weightcategory','position','seasons','playerid','events'));
        }else{
            return view('frontEnd.error.404');
        }

       
    }
     
   
      public function getplayer(Request $request)
    {

        $player_id = $request->player_id;

        $player = Player::where('player_id','=',$player_id)->first();



// $d1 = new DateTime($player->date_of_birth);
// $d2 = new DateTime(Carbon::now());

// $diff = $d2->diff($d1);

// echo $diff->y;


                    $height = ($player->height!=0)?$player->height." cm":"";
                    $image = $player->picture_id['picture_id'];

                    if($image){
                    $img = url('/uploads').'/'.$image.'.jpg';
                    }else{
                    $img = url('/images')."/player-profile-img01.png";
                    }

                    $game_url = url("/players")."/".$player->player_id."/games";

        $inner = '<div class="player-img col-lg-4">
        <img src="'.$img.'" width="200">
        </div>
      
        <div class="player-info col-lg-8">

        <div class="profileDetails-content-wrapper">
        <div class="profile-title"><h3 class="profile-player-name">'.$player->game_name.'</h3></div>
          <div class="playerProfile-personal-row">
              <div class="playerProfile-personal-col1"><p>Full Name</p></div>
              <div class="playerProfile-personal-col2"><p>:</p></div>
              <div class="playerProfile-personal-col3"><p>'.$player->full_name.'</p></div>
          </div>';
          if(($player->date_of_birth!='0000-00-00')&&($player->date_of_birth!="")){
          $inner.='<div class="playerProfile-personal-row">
              <div class="playerProfile-personal-col1"><p>Date of Birth</p></div>
              <div class="playerProfile-personal-col2"><p>:</p></div>
              <div class="playerProfile-personal-col3"><p>'.$player->date_of_birth.'</p></div>
          </div>';
        }
        if($height!=""){
          $inner.='<div class="playerProfile-personal-row">
              <div class="playerProfile-personal-col1"><p>Height</p></div>
              <div class="playerProfile-personal-col2"><p>:</p></div>
              <div class="playerProfile-personal-col3"><p>'.$height.'</p></div>
          </div>
      </div>';
    }
      $inner.='<a href="'.$game_url.'"><button class="btn btn-default src_btn" type="button">View Games Played</button></a>

        </div>';

    echo $inner;
        
        
    }
     public function player_district_search()
    {
       $district=SubRegion::where('region_id','1')->pluck('name','sub_region_id');
       return view('frontEnd.player_district_search',compact('district'));
    }
    public function getdistplayers()
    {   
        $player_list="";
        $player=Player::where('district',$_GET['district'])->get();
         foreach ($player as $key ) {
          $player_list.="<a href='".url('wr/players/'.$key->player_id)."'><li>".$key->full_name."</li></a>";
                 }
         return $player_list;
    }
    public function getteams(){
        $teamlist="";
        $selected="";
        // $player=Player::where('player_id',$_GET['player_id'])->first();
        $teams=Team::where('sub_region_id',$_GET['district'])->pluck('name','team_id');
        foreach ($teams as $key => $value) {
          
           $teamlist.="<option value='".$key."'".$selected." >".$value."</option>";
        }
        return $teamlist;
    }
	public function olympics_awards()
        {
           
           return view('frontEnd.olympics_awards');
        }
          public function arjuna_awards()
        {
           
           return view('frontEnd.arjuna_awards');
        }
          public function kr_awards()
        {
           
           return view('frontEnd.kr_awards');
        }
          public function cw_awards()
        {
           
           return view('frontEnd.cw_awards');
        }
          public function ag_awards()
        {
           
           return view('frontEnd.ag_awards');
        }
        public function playernewbyseason(Request $request)
        {
          $id = $request->playerid;
          $season = $request->season;
        $games = DB::table('wr_game as g')
        ->join('player as p1', 'g.player1_id', '=', 'p1.player_id')
        ->join('player as p2', 'g.player2_id', '=', 'p2.player_id')
        ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')

        ->join('wr_event_game as eg', 'eg.game_id', '=', 'g.game_id')
        ->join('wr_event as e', 'e.event_id', '=', 'eg.event_id')
            
        ->select('p1.full_name as team1','p2.full_name as team2','g.game_date as date','g.game_id as game_id','v.venue_id as venue_id','v.name as venue','p1.game_name as player','g.status','g.local_start_time as time','eg.event_id as event_id','e.name as event_name')
        ->where('g.player1_id','=',$id)
        ->where('e.season','=',$season)
        ->orWhere('g.player2_id','=',$id)
        ->orderby('date','desc')
        ->get();
    $gametable = "";
    $i=0;
    foreach ($games as $key => $item1) {
     $i++; 
     $url = url('wr/scorecard/'.$item1->game_id);
 
                                                if($item1->status !='fixture'){

                                                $title = '<a href="'.$url.'">'.$item1->team1.' v '.$item1->team2.'</a>';
                                                }else{
                                                $title =  $item1->team1.' v '.$item1->team2;
                                                }


                                            $event_url = url('wr/events/'.$item1->event_id);
                                            $date = date("d M Y", strtotime($item1->date));
                                            if($item1->time!=""){
                                                  $str=$item1->time;
                                                  $str1=explode(":", $str);
                                                  $str2=$str1[0].":".$str1[1];
                                                }else
                                                {
                                                  $str2="";
                                                }
                                            $gametable .= '<tr><td>'.$date.'</td>';
                                            $gametable .= '<td class="hidden-md hidden-sm hidden-xs">'.$str2.'</td>';
                                            $gametable .= '<td>'.$title.'</td>';
                                            $gametable .= '<td><a href="'.$event_url.'">'.$item1->event_name.'</a></td></tr>';
                                            


     
    }
    if($i==0)
                                            {
                                            $gametable .= '<tr><td colspan="4"><b>No details found for the selection.</b></td></tr>';
                                       } 
    return $gametable;
  }


  public function playernewbyevent(Request $request)
        {
            $id = $request->playerid;
          $event = $request->event;
        $games = DB::table('wr_game as g')
        ->join('player as p1', 'g.player1_id', '=', 'p1.player_id')
        ->join('player as p2', 'g.player2_id', '=', 'p2.player_id')
        ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')

        ->join('wr_event_game as eg', 'eg.game_id', '=', 'g.game_id')
        ->join('wr_event as e', 'e.event_id', '=', 'eg.event_id')
            
        ->select('p1.full_name as team1','p2.full_name as team2','g.game_date as date','g.game_id as game_id','v.venue_id as venue_id','v.name as venue','p1.game_name as player','g.status','g.local_start_time as time','eg.event_id as event_id','e.name as event_name')
        ->where('g.player1_id','=',$id)
            ->where('e.event_id','=',$event)
        ->orWhere('g.player2_id','=',$id)
        ->orderby('date','desc')
        ->get();
    $gametable = "";
    $i=0;
    foreach ($games as $key => $item1) {
     $i++; 
     $url = url('wr/scorecard/'.$item1->game_id);
 
                                                if($item1->status !='fixture'){

                                                $title = '<a href="'.$url.'">'.$item1->team1.' v '.$item1->team2.'</a>';
                                                }else{
                                                $title =  $item1->team1.' v '.$item1->team2;
                                                }


                                            $event_url = url('wr/events/'.$item1->event_id);
                                            $date = date("d M Y", strtotime($item1->date));
                                            if($item1->time!=""){
                                                  $str=$item1->time;
                                                  $str1=explode(":", $str);
                                                  $str2=$str1[0].":".$str1[1];
                                                }else
                                                {
                                                  $str2="";
                                                }
                                            $gametable .= '<tr><td>'.$date.'</td>';
                                            $gametable .= '<td class="hidden-md hidden-sm hidden-xs">'.$str2.'</td>';
                                            $gametable .= '<td>'.$title.'</td>';
                                            $gametable .= '<td><a href="'.$event_url.'">'.$item1->event_name.'</a></td></tr>';
                                            


     
    }
    if($i==0)
                                            {
                                            $gametable .= '<tr><td colspan="4"><b>No details found for the selection.</b></td></tr>';
                                       } 
    return $gametable;
        }

}
