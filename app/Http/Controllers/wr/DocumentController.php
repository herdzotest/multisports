<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Document;
use App\Sport;
use App\DocumentCategory;
use App\DocumentSubCategory;
use App\GoverningBody;
use App\UserHasAccess;
use App\RelatedDocument;
use Carbon\Carbon;
use Session;
use Auth;
use DB;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // $document=array();
        // if(UserHasAccess::where('user_id','=',Auth::user()->id)->exists())
        // {
        // $useraccess=UserHasAccess::where('user_id','=',Auth::user()->id)->first()->governing_body_id;
        
    //select * from user_has_access where user_id=auth->user_id;
        
        // $document = Document::where('governing_body_id','=',$useraccess)->get();
        // $document = Document::all();//Model Name
        // }
        // return"Am here";
        $document=Document::all();
        foreach ($document as $item) {
          $sql=DB::Select("

            SELECT GROUP_CONCAT(dsc.subcategory_name) subcategory_name FROM document_subcategory dsc where FIND_IN_SET (dsc.subcategory_id,(select d.sub_category from documents d where d.document_id =$item->document_id ))    
            ");
          
             $document_subcat[$item->document_id]=$sql;
        
          
        }


        return view('backEnd.admin.document.index', compact('document','document_subcat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $governingbodyid = GoverningBody::pluck('name','governing_body_id');
        $document_category = DocumentCategory::pluck('category_name','category_id');
        $document_subcategory = DocumentSubCategory::pluck('subcategory_name','subcategory_id');
        $sports = Sport::pluck('sport_name','sport_id');
        $sports->prepend('Select Associated Sport',' ');
        return view('backEnd.admin.document.create',compact('governingbodyid','document_category','document_subcategory','sports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       
        $this->validate($request, [
            'document_category' => 'required',
            // 'document_subcategory' => 'required',
            'department' => 'required',
            'document_number' => 'required|unique:documents',
            'issued_by' => 'required',
            'issued_date' => 'required',
            'file_number' => 'required',
            'subject' => 'required',
            'file' => 'required'
        
        ]);

       // $request['document_date'] = Carbon::createFromFormat('Y-m-d', $request['document_date']);
      
        // $request['document_date']=date('Y-m-d',strtotime($request['document_date'])); 
        $issued_to="";
		if($request->issued_to != null)
		{
			for ($i=0; $i <sizeof($request->issued_to) ; $i++) { 
          $issued_to.=$request->issued_to[$i].",";
        }
        $issued_to=rtrim($issued_to,",");
		}
        
      


        $document_subcategory="";
		if($request->document_subcategory != null)
		{
        for ($i=0; $i <sizeof($request->document_subcategory) ; $i++) { 
          $document_subcategory.=$request->document_subcategory[$i].",";
        }
        $document_subcategory=rtrim($document_subcategory,",");
		}

        
        if( $request->hasFile('file')){ 
        $file = $request->file('file'); 
        $fileName = $file->getClientOriginalName();
        $fileExtension = $file->getClientOriginalExtension();
        // $last_id=Document::orderBy('document_id','desc')->first();
        // $curr_id=$last_id+1 ;
        $timestamp=time();
        $randnum = rand((int)1111111111, (int)9999999999);
       
        $exploded= explode('-',$request->issued_date);
      
       $year=$exploded[0];
       $month=$exploded[1];
        // $filenewName=$curr_id . '.' .$file->getClientOriginalExtension();
        $filenewName=$timestamp."_".$randnum . '.' .$file->getClientOriginalExtension();
        $filename_location='media/documents/'.$request->issued_by.'/'.$year.'/'.$month;

        $request['filename_location']=$filename_location.'/'.$filenewName ;
        $request->file->move(public_path($filename_location), $filenewName);
        // $request['filename_path']=$filenewName;
       

        } else {
        dd('No image was found');
                }


       // Document::create($request->all());
                $document=new Document;
                $document->category_id=$request->document_category;
                $document->sub_category=$document_subcategory;
                $document->gov_order_num=$request->gov_order_num;
                $document->department=$request->department;
                $document->document_number=$request->document_number;
                $document->issued_by=$request->issued_by;
                $document->issued_date=$request->issued_date;
                $document->subject=$request->subject;
                $document->abstract=$request->abstract;
                $document->file_number=$request->file_number;
                $document->file_location=$request->filename_location;
                $document->issued_to=$issued_to;
                $document->user_id=auth()->user()->id;
                $document->sport_id=$request->sport;
                $document->save();
        if($request->related_documents!=""){        
        $exploded=explode(',', $request->related_documents);        
       
        foreach ($exploded as $key) {
             $related_documents=new RelatedDocument;
             $related_documents->document_number=$request->document_number;
             $related_documents->related_document_num=$key;
             $related_documents->save();

          }
        }
                
       Session::flash('message', 'Document added!');
       Session::flash('status', 'success');
       $document=Document::all();

       foreach ($document as $item) {
          $sql=DB::Select("

            SELECT GROUP_CONCAT(dsc.subcategory_name) subcategory_name FROM document_subcategory dsc where FIND_IN_SET (dsc.subcategory_id,(select d.sub_category from documents d where d.document_id =$item->document_id ))    
            ");
          
             $document_subcat[$item->document_id]=$sql;
        
          
        }
       return view('backEnd.admin.document.index', compact('document','document_subcat'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $document = Document::findOrFail($id);

        return view('backEnd.admin.document.show', compact('document'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = Document::findOrFail($id);
        $governingbodyid = GoverningBody::pluck('name','governing_body_id');
        $document_category = DocumentCategory::pluck('category_name','category_id');
        $document_subcategory = DocumentSubCategory::pluck('subcategory_name','subcategory_id');
        $sports = Sport::pluck('sport_name','sport_id');
        $sports->prepend('Select Associated Sport',' ');
        $rel_val="";
        // $related_documents=RelatedDocument::select('related_document_num')->where('document_number',$document->document_number)->get();
        $related_documents=DB::Select("select group_concat(related_document_num) from related_documents where document_number='".$document->document_number."'");
        // print_r($related_documents);
        foreach ($related_documents[0] as $key ) {
          $rel_val= $key;
        }
        // return;
        return view('backEnd.admin.document.edit', compact('document','governingbodyid','document_category','document_subcategory','sports','rel_val'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request['document_date']=date('Y-m-d',strtotime($request['document_date'])); 
         $issued_to="";
		 if($request->issued_to != null)
		{
        for ($i=0; $i <sizeof($request->issued_to) ; $i++) { 
          $issued_to.=$request->issued_to[$i].",";
        }
        $issued_to=rtrim($issued_to,",");
		}


        $document_subcategory="";
		if($request->document_subcategory != null)
		{
        for ($i=0; $i <sizeof($request->document_subcategory) ; $i++) { 
          $document_subcategory.=$request->document_subcategory[$i].",";
        }
        $document_subcategory=rtrim($document_subcategory,",");
		}

        if( $request->hasFile('file')){ 
        $file = $request->file('file'); 
        $fileName = $file->getClientOriginalName();
        $fileExtension = $file->getClientOriginalExtension();
        // $last_id=Document::orderBy('document_id','desc')->first();
        // $curr_id=$last_id+1 ;
        $timestamp=time();
        $randnum = rand((int)1111111111, (int)9999999999);

        $exploded= explode('-',$request->issued_date);

        $year=$exploded[0];
        $month=$exploded[1];
        // $filenewName=$curr_id . '.' .$file->getClientOriginalExtension();
        $filenewName=$timestamp."_".$randnum . '.' .$file->getClientOriginalExtension();
        $filename_location='media/documents/'.$request->issued_by.'/'.$year.'/'.$month;

        $request['filename_location']=$filename_location.'/'.$filenewName ;
        $request->file->move(public_path($filename_location), $filenewName);
        // $request['filename_path']=$filenewName;


        }

        $document = Document::findOrFail($id);
        $oldDocumentNumber = $document->document_number;
        $rel=RelatedDocument::where('document_number',$oldDocumentNumber)->delete();
        $document->category_id=$request->document_category;
        $document->sub_category=$document_subcategory;
        $document->gov_order_num=$request->gov_order_num;
        $document->department=$request->department;
        $document->document_number=$request->document_number;
        $document->issued_by=$request->issued_by;
        $document->issued_date=$request->issued_date;
        $document->subject=$request->subject;
        $document->abstract=$request->abstract;
        $document->sport_id=$request->sport;
        $document->file_number=$request->file_number;
        if( $request->hasFile('file')){ 
        $document->file_location=$request->filename_location;
        }
        $document->issued_to=$issued_to;
        $document->save();

        /*$related_documents=DB::Select("select group_concat(related_document_num) from related_documents where document_number='".$document->document_number."'");
        // print_r($related_documents);
        foreach ($related_documents[0] as $key ) {
          $rel_val= $key;
        }

        // echo $rel_val;
        // return;
        if($rel_val!=$request->related_documents){


            $rel=RelatedDocument::where('document_number',$document->document_number)->delete();
            $exploded=explode(',', $request->related_documents);        
       
         foreach ($exploded as $key) {
             $related_documents=new RelatedDocument;
             $related_documents->document_number=$request->document_number;
             $related_documents->related_document_num=$key;
             $related_documents->save();

          }
        }*/
		if($request->related_documents != null)
        {
            $exploded=explode(',', $request->related_documents);        
       
         foreach ($exploded as $key) {
             $related_documents=new RelatedDocument;
             $related_documents->document_number=$request->document_number;
             $related_documents->related_document_num=$key;
             $related_documents->save();

          }
        }

        Session::flash('message', 'Document updated!');
        Session::flash('status', 'success');

        return redirect('admin/documents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document = Document::findOrFail($id);
        $oldDocumentNumber = $document->document_number;
        $rel=RelatedDocument::where('document_number',$oldDocumentNumber)->delete();

        $document->delete();

        Session::flash('message', 'Document deleted!');
        Session::flash('status', 'success');

        return redirect('admin/documents');
    }
    public function documentlink()
    {
        $document_number = Document::pluck('document_number','document_number');
      
        return view('backEnd.admin.document.documentlink',compact('document_number'));
    }
}
