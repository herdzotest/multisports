<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Article;
use App\Author;
use App\ArticleCategory;
use App\PictureArticle;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;

class ArticleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $articles = Article::paginate(30);

        return view('backEnd.admin.articles.index', compact('articles'));
    }
        public function search(Request $request)
    {
        $query= $request['query'];
        $articles = Article::where('title', 'LIKE', "%".$query."%")->orwhere('description', 'LIKE', "%".$query."%")->paginate(100);
        return view('backEnd.admin.articles.index', compact('articles'));
    }
        public function gethomeArticles()
    {
        $articles = Article::orderBy('article_id','desc')->limit(10)->get();

        return $articles;
    }

    public function news()
    {
        $newscat = ArticleCategory::all();
        $newscatfirst = ArticleCategory::first()->article_category_id;
        $news = Article::orderBy('article_id', 'desc')->paginate(10);
        return view('frontEnd.news', compact('newscat','news'));
    }
       public function newsnew()
    {
        $newscat = ArticleCategory::all();
        $newscatfirst = ArticleCategory::first()->article_category_id;

        $news = Article::orderBy('article_id', 'desc')->paginate(10);


        return view('frontEnd.news', compact('newscat','news'));
    }

    public function newsdetailnew(Request $request)
    {
        $news = Article::where('article_id','=',$request->newsid)->first();

        $latest = Article::orderBy('article_id','desc')->limit(5)->get();
        $tag1=DB::Select("SELECT p.player_id,
        p.full_name from player p where 
        FIND_IN_SET(p.player_id,(SELECT a.players from article  a where a.article_id=$request->newsid))");
        $tag2=DB::Select("SELECT sc.game_id ,
            concat(t1.name,' v ',t2.name) sc_disp
            from game sc 
            left outer join team t1 on t1.team_id=sc.team1_id
            left outer join team t2 on t2.team_id=sc.team2_id
            where 
        FIND_IN_SET(sc.game_id,(SELECT a.scorecards from article  a where a.article_id=$request->newsid))");
        return view('frontEnd.newsdetails', compact('news','latest','tag1','tag2'));
    }
    public function newsdetail(Request $request)
    {
        $news = Article::where('article_id','=',$request->newsid)->first();

        $latest = Article::orderBy('article_id','desc')->limit(5)->get();
        return view('frontEnd.newsdetails', compact('news','latest'));
    }

    public function newsbycat(Request $request)
    {
echo "asdd";
        $newscat = $request->newscat;

        $news = Article::orderBy('article_id', 'desc')->where('category','=',$newscat)->get();

           $inner = '';
foreach($news as $item2){
 
        $inner.= '<tr>
            <td>'.date("d M Y", strtotime($item2->created_at)).'</td>
            <td><a href="" data-toggle="modal" data-target="#myModal">'.$item2->title.'</a></td>
            <td>'.$item2->author["getauthor"].'</td>
        </tr>';
 

    }

    echo $inner;
        
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $author = Author::pluck('name', 'author_id');        
        $category = ArticleCategory::pluck('name', 'article_category_id');
        $profile_of = array('' => 'Please Select','player' => 'player', 'event' => 'event', 'ground' => 'ground', 'author' => 'author' );
        return view('backEnd.admin.articles.create',compact('author','category','profile_of'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {   

        
        $article = Article::create($request->all());
        if($request->picture!=''){
            $picture = new PictureArticle;
            $picture->picture_id = $request->picture;
            $picture->article_id = $article->article_id;
            $picture->save();
        }
        Session::flash('message', 'Article added!');
        Session::flash('status', 'success');

        return redirect('admin/articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $article = Article::findOrFail($id);

        return view('backEnd.admin.articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $article = Article::findOrFail($id);
        $author = Author::pluck('name', 'author_id');      
        $author->prepend('Select','');  
        $category = ArticleCategory::pluck('name', 'article_category_id');
        // $article = Article::findOrFail($id)->picture()->get();

        return view('backEnd.admin.articles.edit', compact('article','author','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        if($request['owner']==''){$request['owner'] = $request['author'];}
        $article = Article::findOrFail($id);
        $article->update($request->all());
        if($request['picture']!=''){
            DB::table('picture_article')->where('article_id', '=', $id)->delete();

            DB::table('picture_article')->insert(['picture_id' => $request['picture'], 'article_id' => $id]);
        }

        Session::flash('message', 'Article updated!');
        Session::flash('status', 'success');

        return redirect('admin/articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);

        $article->delete();

        DB::table('picture_article')->where('article_id', '=', $id)->delete();

        Session::flash('message', 'Article deleted!');
        Session::flash('status', 'success');

        return redirect('admin/articles');
    }

}
