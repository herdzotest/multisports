<?php

namespace App\Http\Controllers\wr;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\wr\PlayerTeam as PlayerTeam;
use App\Model\wr\PictureTeam;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;
use App\Game;
use App\Sport;
use App\Model\wr\EventSquad;
class PlayerTeamController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    protected $sport_id,$table_prefix;
    public function __construct()
    {   
         $sport_id="";   
         // $this->table_prefix= \Session::get('table_prefix');
        $this->middleware(function ($request, $next){
        //$this->table_prefix = session('table_prefix');
        session()->put('table_prefix', 'wr');
        $this->table_prefix =   session()->get('table_prefix');
        $sport_id_arr=Sport::where('identifier',$this->table_prefix)->first();
        $this->sport_id=$sport_id_arr->sport_id;
        return $next($request);
        });
         
    }


     public function index()
    {
        $playerteam = PlayerTeam::leftjoin('player as p','wr_playerteam.player_id','=','p.player_id')->get();

        return view('backEnd.admin.playerteam.index', compact('playerteam'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.admin.playerteam.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'player_id' => 'required',
            'team_id' => 'required',
            'start_season' => 'required'
        ]);
        // return "hereh";
        $playerteam = PlayerTeam::create($request->all());

       

        Session::flash('message', 'Player Team added!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/playerteam');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $playerteam = PlayerTeam::findOrFail($id);

        return view('backEnd.admin.playerteam.show', compact('playerteam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $playerteam = PlayerTeam::findOrFail($id);
        return view('backEnd.admin.playerteam.edit', compact('playerteam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $playerteam = PlayerTeam::findOrFail($id);
        $playerteam->update($request->all());


        Session::flash('message', 'Player Team updated!');
        Session::flash('status', 'success');

         return redirect('admin/'.$this->table_prefix.'/playerteam');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $playerteam = PlayerTeam::findOrFail($id);

        $playerteam->delete();

        Session::flash('message', 'Player Team deleted!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/playerteam');
    }
   
}
