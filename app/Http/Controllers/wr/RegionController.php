<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SubRegion;
use App\Region;
use App\Country;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class RegionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $region = Region::orderby('country_id')->orderby('name')->get();

        return view('backEnd.admin.region.index', compact('region'));
    }

    public function getregion(Request $request)
    {

        $i='<select class="form-control" id="region" name="region">';
        $region = Region::where('country_id','=',$request->country_id)->get(array('region_id','name'));
        $i.= '<option value="">Select Region</option>';
        foreach($region as $Obj){
            $i.= '<option value="'.$Obj->region_id.'">'.$Obj->name.'</option>';
        }
        echo $i.'</select>';
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $country = Country::orderby('name','asc')->pluck('name', 'country_id');
        $country =  $country->prepend('Select Country', ''); 
        return view('backEnd.admin.region.create',compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique: regions',
            'country_id' => 'required'
        ],
        [
          'name.required' => 'The region name is required.',
          'country_id.required' => 'The country name is required.'
        ]);
        Region::create($request->all());

        Session::flash('message', 'Region added!');
        Session::flash('status', 'success');

        return redirect('admin/region');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $region = Region::findOrFail($id);

        return view('backEnd.admin.region.show', compact('region'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $region = Region::findOrFail($id);
        $country = Country::orderby('name','asc')->pluck('name', 'country_id');

        return view('backEnd.admin.region.edit', compact('region','country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'country_id' => 'required'
        ]);
        $region = Region::findOrFail($id);
        $region->update($request->all());

        Session::flash('message', 'Region updated!');
        Session::flash('status', 'success');

        return redirect('admin/region');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $subregionDetails = SubRegion::where('region_id','=',$id)->get();
        if(count($subregionDetails) == 0)
        {
            $region = Region::findOrFail($id);

            $region->delete();

            Session::flash('message', 'Region deleted!');
            Session::flash('status', 'success');

            
        }
        else
        {
            Session::flash('message', 'cannot delete because this region id is used to subregion table!');
            Session::flash('status', 'success');
        }
        return redirect('admin/region');
    }

}
