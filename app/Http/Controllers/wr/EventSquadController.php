<?php

namespace App\Http\Controllers\wr;

use App\Http\Requests;
use App\Http\Controllers\wr\Controller;

use App\Event;
use App\EventSquad;
use App\WrestlingStyle;
use App\WeightCategory;
use App\PlayerTeam;
use App\GamePlayer;
use DB;
use App\Country;
use App\Model\Team;
use App\Sport;
use App\Region;
use App\SubRegion;
use App\PictureEvent;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Player;

class EventSquadController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
   protected $sport_id,$table_prefix;
   public function __construct()
   {
     $sport_id = "";
     // $this->table_prefix= \Session::get('table_prefix');
     $this->middleware(function ($request, $next) {
       //$this->table_prefix = session('table_prefix');
       session()->put('table_prefix', 'wr');
       $this->table_prefix =   session()->get('table_prefix');
       $sport_id_arr = Sport::where('identifier', $this->table_prefix)->first();
       $this->sport_id = $sport_id_arr->sport_id;
       return $next($request);
     });
     // session()->put('table_prefix', 'wr');
     // $this->table_prefix = session()->get('table_prefix');
     // $sport_id_arr = Sport::where('identifier', $this->table_prefix)->first();
     // $this->sport_id = $sport_id_arr->sport_id;
     // return $next($request);
   }
    public function index()
    {
        $event_squad = EventSquad::orderBy('event_squad_id', 'desc')->get();

        return view('backEnd.admin.event.index', compact('event'));
    }

    public function frontEndviewsquads($id)
    {
        $event_id = $id;
        $event_name = Event::where('event_id','=',$id)->first()->name;
        $eventsquads = EventSquad::where('event_id','=',$id)->get();

        return view('frontEnd.wr.eventsquads', compact('eventsquads','event_name','event_id'));
    }

        /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
        $event=Event::orderBy('name','asc')->pluck('name','event_id');
        $event->prepend('Select Event','');
        $players=Player::where('sport_id',$this->sport_id)->orderBy('full_name','asc')->pluck('full_name','player_id');
        $players->prepend('Select Player','');
        $wr_style=WrestlingStyle::orderBy('style_name','asc')->pluck('style_name','style_id');
        $wr_style->prepend('Select Wrestling style','');
        $team=array('Select Team','');
        $position = array(''=>'Select Position','First' => 'First','Second' => 'Second','Third' => 'Third');
        // print_r($eventsquad);
        return view('backEnd.admin.eventsquad.create',compact('event','players','wr_style','team','position'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {  
        if (EventSquad::where('player_id', '=', $request->player_id)->where('event_id', '=', $request->event_id)->exists()) {
            return "<div class='alert alert-danger'>Data already exists.</div>";
        }         
        if (EventSquad::where('weight_cat', '=', $request->weight_cat)->where('event_id', '=', $request->event_id)->where('player_number', '=', $request->player_number)->exists()) {
            return "<div class='alert alert-danger'>There is another player with the same player number in the squad.</div>";
        } 

        $player = Player::where('player_id', '=', $request->player_id)->where('sport_id', '=', $this->sport_id)->first();
        if ($player === null) {
           return "<div class='alert alert-danger'>Entered player id not found in database. Please recheck.</div>";
        }        
        $team = Team::where('team_id', '=', $request->team_id)->first();
        if ($team === null) {
           return "<div class='alert alert-danger'>Entered team id not found in database. Please recheck.</div>";
        }

        $weight_cat = WeightCategory::where('category_desc', '=', $request->weight_cat)->first();
        if($weight_cat === null) {
           $weight_category = new WeightCategory;
           $weight_category->category_desc = $request->weight_cat;
           $weight_category->save();
           $weight_category_id = $weight_category->weight_category_id;
        }else{
            $weight_category_id = $weight_cat->weight_category_id;
        }


        $event_squad="";
        $eventsquad=new EventSquad;
        $eventsquad->event_id=$request->event_id;
        $eventsquad->player_id=$request->player_id;
        $eventsquad->team_id=$request->team_id;
        $eventsquad->player_number=$request->player_number;
        $eventsquad->wr_style=$request->wr_style;
        $eventsquad->weight_cat=$weight_category_id;
        if($eventsquad->weight != null)
        {

            $eventsquad->weight=$request->weight;
        }
        if($request->position != '')
        {

            $eventsquad->position=$request->position;
        }
        $eventsquad->save();
        
        $event_id = $request->event_id;
        $team_id = $request->team_id;
        $player_id = $request->player_id;
        $events = Event::where('event_id','=',$event_id)->get()->first();
        $season = $events->season;

        $playerteam = PlayerTeam::leftJoin('wr_event_team_squad as es1','wr_playerteam.player_id','=','es1.player_id')->leftJoin('wr_event_team_squad as es2','wr_playerteam.team_id','=','es2.team_id')->leftJoin('wr_event as e','wr_playerteam.start_season','=','e.season')->where(array('wr_playerteam.player_id' => $player_id,'wr_playerteam.team_id' => $team_id,'wr_playerteam.start_season' => $season))->get()->first();        
        $val = $playerteam;
       

        if($val == null)
        {


            
         
                $season1 = explode('/', $events->season);
                $previousseason1  = ($season1[0]-1).'/'.($season1[1]-1);
                $checkValue1 = PlayerTeam::where(array('player_id' => $player_id,'team_id' => $team_id,'start_season' => $previousseason1) )->get()->first();
                if($checkValue1 == null)
                {

                    $playerteamValue = new PlayerTeam;
                            $playerteamValue->player_id=$request->player_id;
                            $playerteamValue->team_id=$request->team_id;
                            $playerteamValue->start_season=$season;
                            $playerteamValue->save();

                    $season2 = explode('/', $previousseason1);
                    $previousseason2  = ($season2[0]-1).'/'.($season2[1]-1);
                        

                    $checkValue2 = PlayerTeam::where(array('player_id' => $player_id,'team_id' => $team_id,'start_season' => $previousseason2) )->get()->first();
                    if($checkValue2 != null)
                    {

                    
                            if($checkValue2->end_season == NULL)
                            {
                              $endSeason = $checkValue2->end_season;
                              $startSeason = $checkValue2->start_season;
                                $playerteam_id = $checkValue2->playerteam_id;
                                $request['end_season'] = $season;  
                                DB::table('wr_playerteam')->where('playerteam_id',$playerteam_id)->update(['end_season'=> $startSeason]) ; 
                            }
                    }

                }
               

                

            
        }
        Session::flash('message', 'Event Squad added!');
        Session::flash('status', 'success');
        $evsq=EventSquad::where('event_id',$request->event_id)->orderBy('event_squad_id', 'desc')->get();
        $i=0;
        foreach ($evsq as $key ) {
            $firstone = ($i==0)?'bg-success':'';
          $event_squad.="<li class='".$firstone."'>".$key->player['player_id']." ".$key->player['full_name']." ( ".$key->player_number.")  "."<button data-id='$key->player_id' type='button' class='jsgrid-button jsgrid-edit-button delplayer'><span class='fa fa-times' aria-hidden='true'></span></button></li>";
          $i++;
            // $event_squad="<tr>".$key->player['full_name']."</tr>"."<tr>"."Delete"."</tr>";
         }
    return $event_squad;
        // return redirect('admin/'.$this->table_prefix.'/event');
    }
    public function getevents(Request $request)
    {   
        $event_squad="";
    $event_id=$_GET['event_id'];
    $evsq=EventSquad::where('event_id',$_GET['event_id'])->orderBy('event_squad_id', 'desc')->get();
        foreach ($evsq as $key ) {
           $event_squad.="<li  class='list-group-item'>".$key->player['player_id']." ".$key->player['full_name']." ( ".$key->player_number.")  "."<button data-id='$key->player_id' type='button' class=' jsgrid-button jsgrid-edit-button delplayer'><span class='fa fa-times' aria-hidden='true'></span></button></li>";
            // $event_squad="<tr>".$key->player['full_name']."</tr>"."<tr>"."Delete"."</tr>";
         }
    $teams = DB::Select("select distinct value,value1 from
                    (select distinct name as value,fbg.team1_id as value1 from wr_game as fbg left join wr_event_game as feg on feg.game_id = fbg.game_id 
                        left join wr_team as fbt on fbt.team_id = fbg.team1_id
                           where feg.event_id = ".$event_id." union
                        select distinct name as value,fbg.team2_id as value1 from wr_game as fbg left join wr_event_game as feg on feg.game_id = fbg.game_id
                        left join wr_team as fbt on fbt.team_id = fbg.team2_id                         
                           where feg.event_id = ".$event_id." ) as teams order by value");
          $team = "";
          foreach ($teams as $key ) {
              $team .= "<option value='".$key->value1."'>".$key->value."</option>";
          }
    return ['event_squad' => $event_squad,'team' => $team];
     
     }
     
public function delplayer ()
    {  
    $event_squad="";
    $event_id=$_GET['player_id'];
    /*delete playerteam values*/
    $EventSquadDetails  = EventSquad::where('event_id',$_GET['event_id'])->where('player_id',$_GET['player_id'])->get()->first();
    $gamePlayerDetails = GamePlayer::leftJoin('wr_game as g','wr_game_player.game_id','=','g.game_id')
                                         ->leftJoin('wr_event_game as eg','g.game_id','=','eg.game_id')
                                         ->where(array('eg.event_id' => $_GET['event_id'],'wr_game_player.player_id' => $_GET['player_id']))
                                         ->get();
        $count = count($gamePlayerDetails);
        if($count == 0)
        {
            $teamId = $EventSquadDetails->team_id;
            $PlayerTeam=PlayerTeam::where('team_id',$teamId)->where('player_id',$_GET['player_id'])->delete();
            /*delete playerteam values*/
            $evsq=EventSquad::where('event_id',$_GET['event_id'])->where('player_id',$_GET['player_id'])->delete();
               
            $evsq=EventSquad::where('event_id',$_GET['event_id'])->orderBy('event_team_squad_id','desc')->get();
                foreach ($evsq as $key ) {
                   $event_squad.="<li  class='list-group-item'>".$key->player['player_id']." ".$key->player['full_name']."( ".$key->player_number.")  "."<button data-id='$key->player_id' type='button' class=' jsgrid-button jsgrid-edit-button delplayer'><span class='fa fa-times' aria-hidden='true'></span></button></li>";
                    // $event_squad="<tr>".$key->player['full_name']."</tr>"."<tr>"."Delete"."</tr>";
                 }
     }
     else
     {
        $event_squad = "";
     }
    return $event_squad;
     
     }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);

        return view('backEnd.admin.event.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        $country = Country::orderBy('name','asc')->pluck('name', 'country_id');
        $region=Region::orderBy('name','asc')->pluck('name','region_id');
        $region->prepend('Select Region',' ');
        $subregion=SubRegion::orderBy('name','asc')->pluck('name','sub_region_id');
        $subregion->prepend('Select Sub Region',' ');
        $event_picture=PictureEvent::where('event_id',$id)->first();
       
        return view('backEnd.admin.event.edit', compact('event','country','region','subregion','event_picture'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        
        $request['start_date'] = date("Y-m-d", strtotime($request['start_date']));    
        $event = Event::findOrFail($id);
        $event->update($request->all());
        if($request['picture_id']!=''){
        DB::table('wr_picture_event')->where('event_id', '=', $id)->delete();

        DB::table('wr_picture_event')->insert(['picture_id' => $request['picture_id'], 'event_id' => $id]);
        }
        Session::flash('message', 'Event updated!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);

        $event->delete();

        DB::table('wr_picture_event')->where('event_id', '=', $id)->delete();

        Session::flash('message', 'Event deleted!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/event');
    }

    
}
