<?php

namespace App\Http\Controllers\wr;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PlayerMetaData;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;
use App\Sport;
use App\Country;
use App\Model\wr\EventOfficial as EventOfficial;
use App\Model\wr\GamePlayer as GamePlayer;
use App\Model\wr\PicturePlayer as PicturePlayer;
use App\Model\wr\Team as Team;
use App\Model\wr\PlayerTeam as PlayerTeam;
use App\SubRegion;

class EventOfficialController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
     protected $sport_id,$table_prefix;
     public function __construct()
    {   
         $sport_id="";   
         // $this->table_prefix= \Session::get('table_prefix');
        $this->middleware(function ($request, $next){
        //$this->table_prefix = session('table_prefix');
        session()->put('table_prefix', 'wr');
        $this->table_prefix =   session()->get('table_prefix');
        $sport_id_arr=Sport::where('identifier',$this->table_prefix)->first();
        $this->sport_id=$sport_id_arr->sport_id;
        return $next($request);
        });
         
    }

    
    public function index()
    {  
        $eventofficial = EventOfficial::where('sport_id',$this->sport_id)->groupby('official_id')->get();
        return view('backEnd.admin.eventofficial.index', compact('eventofficial'));
    }
    public function create()
    {
        
        $team = Team::orderby('name','asc')->pluck('name', 'team_id');
        $team->prepend('Select', '');
        $post = array( 'coach' => 'coach','assistant coach' => 'assistant coach','manager' => 'manager','assistant manager' => 'assistant manager','manager & coach' => 'manager & coach' );
        return view('backEnd.admin.eventofficial.create',compact('team','post'));
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'official_id' => 'required|unique:wr_event_official,official_id',
            'post' => 'required|array|max:2',
            'team_id' => 'required'          
        ]);
        $request['sport_id']=$this->sport_id;
        $post = $request['post'];
        if(isset($post))
        {
            for ($i=0; $i < sizeof($post) ; $i++) { 
                /*$team_id = $request->team_id;
                $official_id = $request->official_id;
                $start_season = $request->start_season;
                $end_season = $request->end_season;
                $postValues = $post[$i];
                $sport_id = $request['sport_id'];
                $values = array('sport_id' => $sport_id,'team_id' => $team_id,'official_id' => $official_id, 'post' => $postValues,'start_season' => $start_season,'end_season' => $end_season );
                DB::table('event_official')->insert($values);*/
               
                            $eventofficialValue = new EventOfficial;
                            $eventofficialValue->sport_id=$request['sport_id'];
                            $eventofficialValue->official_id=$request['official_id'];
                            $eventofficialValue->post=$post[$i];
                            $eventofficialValue->team_id=$request['team_id'];
                            $eventofficialValue->save();
            }
        }
        //$player_id = EventOfficial::create($request->all());
 
        Session::flash('message', 'Event Official added!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/eventofficial');
    }

     public function edit($id)
    {  
     // return 'dumbo';
        $eventofficial = EventOfficial::findOrFail($id);
        $official_id = $eventofficial->official_id;
        $eventpost = EventOfficial::where('official_id','=',$official_id)->select('post')->get();
        $eventpostValue = array();
        foreach ($eventpost as $key => $value) {
            $eventpostValue[] = $value->post;
        }
        $team = Team::orderby('name','asc')->pluck('name', 'team_id');
        $team->prepend('Select', '');
        $post = array( 'coach' => 'coach','assistant coach' => 'assistant coach','manager' => 'manager','assistant manager' => 'assistant manager','manager & coach' => 'manager & coach' );
        return view('backEnd.admin.eventofficial.edit', compact('eventofficial','team','post','eventpostValue'));
    }
     public function update($id, Request $request)
    {
        $this->validate($request, [
            'official_id' => 'required',
            'post' => 'required|array|max:2',
            'team_id' => 'required'          
        ]);
        $eventofficial = EventOfficial::findOrFail($id);
        

        $official_id = $eventofficial->official_id;
        EventOfficial::where('official_id','=',$official_id)->delete();
        $request['sport_id']=$this->sport_id;
        $post = $request['post'];
        if(isset($post))
        {
            for ($i=0; $i < sizeof($post) ; $i++) { 
               
                            $eventofficialValue = new EventOfficial;
                            $eventofficialValue->sport_id=$request['sport_id'];
                            $eventofficialValue->official_id=$request['official_id'];
                            $eventofficialValue->post=$post[$i];
                            $eventofficialValue->team_id=$request['team_id'];
                            $eventofficialValue->save();
            }
        }

        Session::flash('message', 'Event Official updated!');
        Session::flash('status', 'success');

       return redirect('admin/'.$this->table_prefix.'/eventofficial');
    }
     public function destroy($id)
    {
        $eventofficial = EventOfficial::findOrFail($id);
        $official_id = $eventofficial->official_id;
        EventOfficial::where('official_id','=',$official_id)->delete();

        Session::flash('message', 'Event Official deleted!');
        Session::flash('status', 'success');
   

        return redirect('admin/'.$this->table_prefix.'/eventofficial');
    }
    
}
