<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Document;
use App\Sport;
use App\DocumentCategory;
use App\DocumentSubCategory;
use App\GoverningBody;
use App\UserHasAccess;
use Carbon\Carbon;
use Session;
use Auth;
use DB;

class DocumentSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $documentsubcategory=DocumentSubCategory::all();
        return view('backEnd.admin.documentsubcategory.index', compact('documentsubcategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
       $documentcategory=DocumentCategory::orderBy('category_name','asc')->pluck('category_name','category_id');
       $sports = Sport::pluck('sport_name','sport_id');
        $sports->prepend('Select Associated Sport',' ');
       return view('backEnd.admin.documentsubcategory.create',compact('documentcategory','sports'));
    }

   
    public function store(Request $request)
    {
        $this->validate($request, [
            'document_subcategory' => 'required'
        ]);

       $documentsubcategory=new DocumentSubCategory;
       // $documentsubcategory->category_id=$request->document_category;
       $documentsubcategory->subcategory_name=$request->document_subcategory;
       $documentsubcategory->user_id=auth()->user()->id;
       $documentsubcategory->sport_id=$request->sport;
       $documentsubcategory->save();
       Session::flash('message', 'Document SubCategory added!');
       Session::flash('status', 'success');
       $documentsubcategory=DocumentSubCategory::all();
        return view('backEnd.admin.documentsubcategory.index', compact('documentsubcategory'));   
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $documentsubcategory = DocumentSubCategory::findOrFail($id);
        $documentcategory=DocumentCategory::orderBy('category_name','asc')->pluck('category_name','category_id');
        $sports = Sport::pluck('sport_name','sport_id');
        $sports->prepend('Select Associated Sport',' ');
     return view('backEnd.admin.documentsubcategory.edit', compact('documentsubcategory','documentcategory','sports'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'document_subcategory' => 'required'
        ]);
        $documentsubcategory = DocumentSubCategory::findOrFail($id);
        // $documentsubcategory->category_id=$request->document_category;
        $documentsubcategory->subcategory_name=$request->document_subcategory;
        $documentsubcategory->sport_id=$request->sport;
        
        $documentsubcategory->save();
        Session::flash('message', 'Document SubCategory updated!');
        Session::flash('status', 'success');

        return redirect('admin/documentsubcategory');
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory = Document::where('subcategory_id','=',$id)->get();
        $subcategory1 = DB::table('cat_subcat_link')->where('subcategory_id','=',$id)->get();
      
        if((count($subcategory) == 0) && (count($subcategory1) == 0))
        {
            $documentsubcategory = DocumentSubCategory::findOrFail($id);

            $documentsubcategory->delete();

            Session::flash('message', 'Document SubCategory deleted!');
            Session::flash('status', 'success');

            
        }
        else
        {
            Session::flash('message', 'cannot delete because this SubCategory id is used to document table!');
            Session::flash('status', 'success');
        }
        return redirect('admin/documentsubcategory');
    }
}
