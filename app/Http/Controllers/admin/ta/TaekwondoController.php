<?php

namespace App\Http\Controllers\admin\ta;

use App\Http\Controllers\admin\ta\Controller;
use App\Event;
use DB;
use App\Sport;
use Illuminate\Http\Request;
use Session;

class TaekwondoController extends Controller
{

	/**
	* Display a listing of the resource.
	*
	* @return Response
	*/
	protected $sport_id, $table_prefix;
	public function __construct()
	{
		$sport_id = "";
		session()->put('table_prefix', 'ta');
		$this->table_prefix =   session()->get('table_prefix');
		$sport_id_arr = Sport::where('identifier', $this->table_prefix)->first();
		$this->sport_id = $sport_id_arr->sport_id;
	}

	public function index()
	{
		
	}

	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	*
	* @return Response
	*/
	public function scorecard($gameId)
	{
		$player = DB::table('player')->where('sport_id', '=', 25)->get();
		//$event = Event::where('event_id', $id)->first();
		$event = '';
		// game official
		$gameOfficial = DB::table($this->table_prefix.'_game_official')->where('game_id', '=', $gameId)->get();
		$gameArray = array();
		if(count($gameOfficial) > 0) {
			foreach($gameOfficial as $game) {
				$gameArray[$game->official_type]['name'] = $game->official_name;
				$gameArray[$game->official_type]['id'] = $game->game_official_id  ;
			}
		}
		// game team official
		$gameteamOfficial = DB::table($this->table_prefix.'_game_team_official')->where('game_id', '=', $gameId)->get();
		$gameteamArray = array();
		if(count($gameteamOfficial) > 0) {
			foreach($gameteamOfficial as $gameteam) {
				$gameteamArray[$gameteam->official_type]['name'] = $gameteam->official_name;
				$gameteamArray[$gameteam->official_type]['id'] = $gameteam->game_team_official_id;
			}
		}
		// game player data
		$gamePlayer = DB::table($this->table_prefix.'_game_player')->where('game_id', '=', $gameId)
					->join('player', 'player.player_id', '=', $this->table_prefix.'_game_player.player_id')
					->select($this->table_prefix.'_game_player.*', 'player.full_name')
					->orderBy('ta_game_player_id', 'ASC')->get();	
		$gamePlayerArray = array();
		if(count($gamePlayer) > 0) {
			$i=0;
			foreach($gamePlayer as $gamePlayerData) {
				$gamePlayerArray[$i]['full_name'] = $gamePlayerData->full_name;
				$gamePlayerArray[$i]['player'] = $gamePlayerData->player_id;
				$gamePlayerArray[$i]['player'] = $gamePlayerData->player_id;
				$gamePlayerArray[$i]['punch'] = $gamePlayerData->punch;
				$gamePlayerArray[$i]['k1'] = $gamePlayerData->k1;
				$gamePlayerArray[$i]['k2'] = $gamePlayerData->k2;
				$gamePlayerArray[$i]['k3'] = $gamePlayerData->k3;
				$gamePlayerArray[$i]['k4'] = $gamePlayerData->k4;
				$gamePlayerArray[$i]['deduction'] = $gamePlayerData->deduction;
				$gamePlayerArray[$i]['knockout'] = $gamePlayerData->knockout;
				$i++;
			}
		}
		$gamesArray = DB::table($this->table_prefix.'_game')->where('game_id', '=', $gameId)->get();
		
		return view('backEnd.admin.taekwondo.scorecard',compact('event','player','gameArray','gameteamArray','gamePlayerArray','gamesArray','gameId'));
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  int  $id
	*
	* @return Response
	*/
	public function update($gameId, Request $request)
	{
		$player = DB::table('player')->where('sport_id', '=', 25)->get();
		// game_official table entry
		if($request['refreeId'] == '') {
			DB::table($this->table_prefix.'_game_official')->insert([
				'official_name' => $request['refree'],
				'game_id' => $gameId,
				'official_type' => 'referee'
			]);
		} else {
			DB::table($this->table_prefix.'_game_official')->where('game_official_id', $request['refreeId'])
				->update(['official_name' => $request['refree']]);		
		}
		if($request['judge1Id'] == '') {
			DB::table($this->table_prefix.'_game_official')->insert([
				'official_name' => $request['judge1'],
				'game_id' => $gameId,
				'official_type' => 'judge_1'
			]);
		} else {
			DB::table($this->table_prefix.'_game_official')->where('game_official_id', $request['judge1Id'])
				->update(['official_name' => $request['judge1']]);		
		}
		if($request['judge2Id'] == '') {
			DB::table($this->table_prefix.'_game_official')->insert([
				'official_name' => $request['judge2'],
				'game_id' => $gameId,
				'official_type' => 'judge_2'
			]);
		} else {
			DB::table($this->table_prefix.'_game_official')->where('game_official_id', $request['judge2Id'])
				->update(['official_name' => $request['judge2']]);			
		}
		// game team official table entry
		if($request['coachId'] == '') {
			DB::table($this->table_prefix.'_game_team_official')->insert([
				'official_name' => $request['coach'],
				'game_id' => $gameId,
				'player_id' => $player[0]->player_id,
				'official_type' => 'coach'
			]);
		} else {
			DB::table($this->table_prefix.'_game_team_official')->where('game_team_official_id', $request['coachId'])
				->update(['official_name' => $request['coach']]);				
		}
		if($request['coach2Id'] == '') {
			DB::table($this->table_prefix.'_game_team_official')->insert([
				'official_name' => $request['coach2'],
				'game_id' => $gameId,
				'player_id' => $player[1]->player_id,
				'official_type' => 'assistantcoach'
			]);
		} else {
			DB::table($this->table_prefix.'_game_team_official')->where('game_team_official_id', $request['coach2Id'])
				->update(['official_name' => $request['coach2']]);				
		}
		DB::table($this->table_prefix.'_game_player')->where('game_id', '=', $gameId)->delete();
		$playerDatas = json_decode($request['playerDatas'],true);
		DB::table($this->table_prefix.'_game_player')->insert($playerDatas);
		
		DB::table($this->table_prefix.'_game')->where('game_id', $gameId)->update(['score1' => $request['total_score'],'score2' => $request['total_score2']]);
		
		Session::flash('message', 'Taekwondo scoreboard updated!');
		Session::flash('status', 'success');
		
		return redirect('admin/' . $this->table_prefix . '/scorecard/'.$gameId);
	}

}
