<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sport;
use App\UserHasAccess;
use Carbon\Carbon;
use Session;
use Auth;

class SportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $sport=Sport::all();
        return view('backEnd.admin.sport.index', compact('sport'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('backEnd.admin.sport.create');
    }

   
    public function store(Request $request)
    {
       $this->validate($request, [
            'sport_name' => 'required'
        ]);
       $sport=new Sport;
       $sport->sport_name=$request->sport_name;
       $sport->user_id=auth()->user()->id;
       $sport->save();
       Session::flash('message', 'Sport added!');
       Session::flash('status', 'success');
       $sport=Sport::all();
       return view('backEnd.admin.sport.index', compact('sport'));   
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $sport = Sport::findOrFail($id);
     return view('backEnd.admin.sport.edit', compact('sport'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sport = Sport::findOrFail($id);
        $sport->sport_name=$request->sport_name;
        
        $sport->save();
        Session::flash('message', 'Sport updated!');
        Session::flash('status', 'success');

       $sport=Sport::all();
       return view('backEnd.admin.sport.index', compact('sport'));   
     
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sport = Sport::findOrFail($id);

        $sport->delete();

        Session::flash('message', 'Sport deleted!');
        Session::flash('status', 'success');

        $sport=Sport::all();
       return view('backEnd.admin.sport.index', compact('sport'));
    }
}
