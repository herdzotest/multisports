<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GoverningBody;
use App\Sport;
use Carbon\Carbon;
use Session;

class GoverningBodyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $governingbody = GoverningBody::all();//Model Name

        return view('backEnd.admin.governingbody.index', compact('governingbody'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
         $sports = Sport::orderBy('sport_name','asc')->pluck('sport_name','sport_id');
         $sports->prepend('Select Associated Sport',' ');
         return view('backEnd.admin.governingbody.create',compact('sports'));
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'contact' => 'required',
           
        ]);


       // GoverningBody::create($request->all());

        $governingbody=new GoverningBody;
        $governingbody->name=$request->name;
        $governingbody->short_code=$request->short_code;
        $governingbody->address=$request->address;
        $governingbody->contact=$request->contact;
        $governingbody->user_id=auth()->user()->id;
        $governingbody->sport_id=$request->sport;
        $governingbody->save();

       Session::flash('message', 'Governing Body added!');
       Session::flash('status', 'success');
       return redirect('admin/governingbody');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $governingbody = GoverningBody::findOrFail($id);

        return view('backend.governingbody.show', compact('governingbody'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $governingbody = GoverningBody::findOrFail($id);
        $sports = Sport::orderBy('sport_name','asc')->pluck('sport_name','sport_id');
        $sports->prepend('Select Associated Sport',' ');
        return view('backEnd.admin.governingbody.edit', compact('governingbody','sports'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        // echo "here:";
        // return;
        $governingbody = GoverningBody::findOrFail($id);
        $governingbody->name=$request->name;
        $governingbody->short_code=$request->short_code;
        $governingbody->address=$request->address;
        $governingbody->contact=$request->contact;
        $governingbody->sport_id=$request->sport;
        $governingbody->save();

        Session::flash('message', 'Governing Body updated!');
        Session::flash('status', 'success');

        return redirect('admin/governingbody');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $governingbody = GoverningBody::findOrFail($id);

        $governingbody->delete();

        Session::flash('message', 'GoverningBody deleted!');
        Session::flash('status', 'success');

        return redirect('admin/governingbody');
    }
}
