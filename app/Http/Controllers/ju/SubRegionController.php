<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Location;
use App\SubRegion;
use App\Region;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class subRegionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $subregion = SubRegion::orderby('region_id')->orderby('name')->get();

        return view('backEnd.admin.subregion.index', compact('subregion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $region = Region::orderby('name','asc')->pluck('name', 'region_id');
        $region =  $region->prepend('Select Region', ''); 
        return view('backEnd.admin.subregion.create',compact('region'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'region_id' => 'required'
        ],
        [
          'name.required' => 'The subregion name is required.',
          'region_id.required' => 'The region name is required.'
        ]);  
        SubRegion::create($request->all());

        Session::flash('message', 'SubRegion added!');
        Session::flash('status', 'success');

        return redirect('admin/subregion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $subregion = SubRegion::findOrFail($id);

        return view('backEnd.admin.subregion.show', compact('subregion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $subregion = SubRegion::findOrFail($id);
        $region = Region::orderby('name','asc')->pluck('name', 'region_id');

        return view('backEnd.admin.subregion.edit', compact('subregion','region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'region_id' => 'required'
        ]); 
        $subregion = SubRegion::findOrFail($id);
        $subregion->update($request->all());

        Session::flash('message', 'SubRegion updated!');
        Session::flash('status', 'success');

        return redirect('admin/subregion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $locationDetails = SubRegion::where('sub_region_id','=',$id)->get();
        if(count($locationDetails) == 0)
        {
            $subregion = SubRegion::findOrFail($id);

            $subregion->delete();


            Session::flash('message', 'Subregion deleted!');
            Session::flash('status', 'success');

            
        }
        else
        {
            Session::flash('message', 'cannot delete because this subregion id is used to location table!');
            Session::flash('status', 'success');
        }
        return redirect('admin/subregion');
    }

}
