<?php

namespace App\Http\Controllers\wr;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\wr\Game;
use App\Model\wr\GamePlayer;
use App\Model\wr\Team;
use App\Venue;
use App\Sport;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;
use App\Player;

class GameController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
   protected $sport_id,$table_prefix;
   public function __construct()
    {   
         $sport_id="";   
         // $this->table_prefix= \Session::get('table_prefix');
        $this->middleware(function ($request, $next){
        //$this->table_prefix = session('table_prefix');
        session()->put('table_prefix', 'wr');
        $this->table_prefix =   session()->get('table_prefix');
        $sport_id_arr=Sport::where('identifier',$this->table_prefix)->first();
        $this->sport_id=$sport_id_arr->sport_id;
        return $next($request);
        });
         
    }
    public function index()
    {
        $game = Game::all();
        return view('backEnd.admin.game.index', compact('game'));
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {  
        $event=Event::orderBy('name','asc')->pluck('name','event_id');    
        $venuetype = array('homeaway' => 'homeaway', 'neutral' => 'neutral' );
        $player = Player::orderBy('full_name','asc')->pluck('full_name', 'player_id');
        $venue = Venue::orderBy('name','asc')->pluck('name', 'venue_id');
        return view('backEnd.admin.game.create',compact('player','venue','venuetype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $request['game_date'] = date("Y-m-d", strtotime($request['game_date']));
        Game::create($request->all());

        Session::flash('message', 'Game added!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/game');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $game = Game::findOrFail($id);

        return view('backEnd.admin.game.show', compact('game'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $game = Game::select('game_id','status')->where('game_id','=',$id)->first();
        // $game->game_date = date("m/d/Y", strtotime($game->game_date));
        $status = array('full' => 'full', 'fixture' => 'fixture', 'summary' => 'summary' );
        return view('backEnd.admin.game.edit', compact('game','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $game = Game::findOrFail($id);
        $request['game_date'] = date("Y-m-d", strtotime($request['game_date']));
        $game->update($request->all());

        Session::flash('message', 'Game updated!');
        Session::flash('status', 'success');

        return redirect('admin/'.$table_prefix.'/game');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $game = Game::findOrFail($id);

        $game->delete();

        Session::flash('message', 'Game deleted!');
        Session::flash('status', 'success');

        return redirect('admin/'.$table_prefix.'/game');
    }

    public function getUpcoming()
    {   
        $dt = date("Y-m-d");
        $dtplus = date("Y-m-d", strtotime("+ 5 day"));
        $dtminus = date("Y-m-d", strtotime("+ 1 day"));

        $game = Game::orderBy('game_date','asc')->orderBy('local_start_time','asc')->where('game_date','>=',$dtminus)->where('game_date','<=',$dtplus)->get();
        return $game;
    }

    public function getCurrent()
    {
        $dt = date("Y-m-d");
        $game = Game::orderBy('game_date','asc')->orderBy('local_start_time','asc')->where('game_date','=',$dt)->get();
        return $game;
    }  

    public function getResults()
    {

        $game = Game::orderBy('game_date','desc')->where('status','=','full')->orWhere('status','=','summary')->limit(5)->get();
        return $game;
    } 


        public function playergamesbyid($id)
    {
        $title = "Player Games";
        // $country = Country::orderby('name')->pluck('name', 'country_id');
        // $country->prepend('Select Country', '');

            $games = DB::table('game_player as gp')
            ->join('player as p', 'gp.player_id', '=', 'p.player_id')
            ->join('game as g', 'gp.game_id', '=', 'g.game_id')
            ->join('team as t1', 'g.team1_id', '=', 't1.team_id')
            ->join('team as t2', 'g.team2_id', '=', 't2.team_id')
            ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')
            ->select('t1.name as team1','t2.name as team2','g.game_date as date','g.game_id as game_id','v.venue_id as venue_id','v.name as venue','p.game_name as player')
            ->where('gp.player_id','=',$id)
            ->get();

            $breadcrumb = '<ol class="breadcrumb col-lg-11">
  <li class="breadcrumb-item"><a href="'.url('/home').'">Home</a></li>
  <li class="breadcrumb-item"><a href="'.url('/players').'">Players</a></li>
</ol>';
        return view('frontEnd.playergames', compact('games','title','breadcrumb'));
    }

    public function venuegamesbyid($id)
    {
        $title = "Venue Games";
        $games = DB::table('game as g')
        ->join('team as t1', 'g.team1_id', '=', 't1.team_id')
        ->join('team as t2', 'g.team2_id', '=', 't2.team_id')
        ->select('t1.name as team1','t2.name as team2','g.game_date as date','g.game_id as game_id')
        ->where('g.venue_id','=',$id)
        ->get();


        $breadcrumb  =  '<ol class="breadcrumb col-lg-11">
                          <li class="breadcrumb-item"><a href="'.url('/home').'">Home</a></li>
                          <li class="breadcrumb-item"><a href="'.url('/venues').'">Venues</a></li>
                        </ol>';
        return view('frontEnd.venuegames', compact('games','title','breadcrumb'));
    }

        public function teamgamesbyid($id)
    {
        $title = "Venue Games";
        $games = DB::table('game as g')
        ->join('team as t1', 'g.team1_id', '=', 't1.team_id')
        ->join('team as t2', 'g.team2_id', '=', 't2.team_id')
        ->select('t1.name as team1','t2.name as team2','g.game_date as date','g.game_id as game_id')
        ->where('g.team1_id','=',$id)->orWhere('g.team2_id','=',$id)
        ->get();


        $breadcrumb  =  '<ol class="breadcrumb col-lg-11">
                          <li class="breadcrumb-item"><a href="'.url('/home').'">Home</a></li>
                          <li class="breadcrumb-item"><a href="'.url('/teams').'">Team</a></li>
                        </ol>';
        return view('frontEnd.teamgames', compact('games','title','breadcrumb'));
    }



   

}