<?php

namespace App\Http\Controllers\wr;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PlayerMetaData;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;
use App\Sport;
use App\Country;
use App\Model\wr\OfficialTeam as OfficialTeam;
use App\Model\wr\GamePlayer as GamePlayer;
use App\Model\wr\PicturePlayer as PicturePlayer;
use App\Model\wr\Team as Team;
use App\Model\wr\PlayerTeam as PlayerTeam;
use App\SubRegion;

class OfficialTeamController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
     protected $sport_id,$table_prefix;
     public function __construct()
    {   
         $sport_id="";   
        $this->middleware(function ($request, $next){
        session()->put('table_prefix', 'wr');
        $this->table_prefix =   session()->get('table_prefix');
        $sport_id_arr=Sport::where('identifier',$this->table_prefix)->first();
        $this->sport_id=$sport_id_arr->sport_id;
        return $next($request);
        });
         
    }

    
    public function index()
    {  
        $officialteam = DB::table('ju_official_team')->groupby('official_id')->get();
        return view('backEnd.admin.officialteam.index', compact('officialteam'));
    }
    public function create()
    {
        
        $team = Team::orderby('name','asc')->pluck('name', 'team_id');
        $team->prepend('Select', '');
        $post = array( 'coach' => 'coach','assistant coach' => 'assistant coach','manager' => 'manager','assistant manager' => 'assistant manager','manager & coach' => 'manager & coach' );
        return view('backEnd.admin.officialteam.create',compact('team','post'));
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'official_id' => 'required|unique:ju_official_team,official_id',
            'post' => 'required|array|max:2',
            'team_id' => 'required' ,          
            'start_season' => 'required',          
            'end_season' => 'required'           
        ]);
        $post = $request['post'];
        if(isset($post))
        {
            for ($i=0; $i < sizeof($post) ; $i++) { 
               
                            $officialteamValue = new OfficialTeam;
                            $officialteamValue->official_id=$request['official_id'];
                            $officialteamValue->post=$post[$i];
                            $officialteamValue->team_id=$request['team_id'];
                            $officialteamValue->start_season=$request['start_season'];
                            $officialteamValue->end_season=$request['end_season'];
                            $officialteamValue->save();
            }
        }
 
        Session::flash('message', 'Official Team added!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/officialteam');
    }

     public function edit($id)
    {  
     // return 'dumbo';
        $officialteam = OfficialTeam::findOrFail($id);
        $official_id = $officialteam->official_id;
        $officialpost = OfficialTeam::where('official_id','=',$official_id)->select('post')->get();
        $officialpostValue = array();
        foreach ($officialpost as $key => $value) {
            $officialpostValue[] = $value->post;
        }
        $team = Team::orderby('name','asc')->pluck('name', 'team_id');
        $team->prepend('Select', '');
        $post = array( 'coach' => 'coach','assistant coach' => 'assistant coach','manager' => 'manager','assistant manager' => 'assistant manager','manager & coach' => 'manager & coach' );
        return view('backEnd.admin.officialteam.edit', compact('officialteam','team','post','officialpostValue'));
    }
     public function update($id, Request $request)
    {
        
        $this->validate($request, [
            'official_id' => 'required',
            'post' => 'required|array|max:2',
            'team_id' => 'required',          
            'start_season' => 'required',          
            'end_season' => 'required'          
        ]);
        $officialteam = OfficialTeam::findOrFail($id);
        

        $official_id = $officialteam->official_id;
        OfficialTeam::where('official_id','=',$official_id)->delete();
        $request['sport_id']=$this->sport_id;
        $post = $request['post'];
        if(isset($post))
        {
            for ($i=0; $i < sizeof($post) ; $i++) { 
               
                            $officialteamValue = new OfficialTeam;
                            $officialteamValue->official_id=$request['official_id'];
                            $officialteamValue->post=$post[$i];
                            $officialteamValue->team_id=$request['team_id'];
                            $officialteamValue->start_season=$request['start_season'];
                            $officialteamValue->end_season=$request['end_season'];
                            $officialteamValue->save();
            }
        }	

        Session::flash('message', 'Official Team updated!');
        Session::flash('status', 'success');

       return redirect('admin/'.$this->table_prefix.'/officialteam');
    }
     public function destroy($id)
    {
        $officialteam = OfficialTeam::findOrFail($id);
        $official_id = $officialteam->official_id;
        OfficialTeam::where('official_id','=',$official_id)->delete();

        Session::flash('message', 'Official Team deleted!');
        Session::flash('status', 'success');
   

        return redirect('admin/'.$this->table_prefix.'/officialteam');
    }
    
}
