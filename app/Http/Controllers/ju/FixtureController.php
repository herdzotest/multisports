<?php

namespace App\Http\Controllers\wr;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\wr\Event;
use App\Model\wr\EventSquad;
use DB;
use App\Country;
use App\Model\wr\Team;
use App\Sport;
use App\Region;
use App\SubRegion;
use App\Model\wr\PictureEvent;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Player;
use App\Imports\wr\FixtureImport;
use Maatwebsite\Excel\Facades\Excel;

class FixtureController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
   protected $sport_id,$table_prefix;
   public function __construct()
    {   
         $sport_id="";   
         // $this->table_prefix= \Session::get('table_prefix');
        $this->middleware(function ($request, $next){
        //$this->table_prefix = session('table_prefix');
        session()->put('table_prefix', 'wr');
        $this->table_prefix =   session()->get('table_prefix');
        $sport_id_arr=Sport::where('identifier',$this->table_prefix)->first();
        $this->sport_id=$sport_id_arr->sport_id;
        return $next($request);
        });
         
    }
   public function upload()
   {
    $event=Event::pluck('name','event_id');
    $event->prepend("Select Event","");
   
    return view('backEnd.admin.game.uploadfixture', compact('event'));
   }
   public function import(Request $request) 
    {   
      $this->validate($request, [
            'event_id' => 'required'
        ]);
        // return "Am here";
        Excel::import(new FixtureImport, request()->file('file'));
        Session::flash('message', 'Fixture added!');
        Session::flash('status', 'success');

        $event=Event::pluck('name','event_id');
        $event->prepend("Select Event","");
        return view('backEnd.admin.game.uploadfixture', compact('event'));
    }
	public function select_game_editfixture()
   {
    $event=Event::pluck('name','event_id');
    $event->prepend("Select Event","");
    return view('backEnd.admin.game.editfixture_selectevent', compact('event'));
   } 

   public function updatefixture(Request $request){

    $game=Game::find($request->game_id);
    $game->team1_id=$request->team1;
    $game->team2_id=$request->team2;
    $game->venue_id=$request->venue;
    $game->game_date=$request->gamedate;
    $game->local_start_time=$request->localstarttime;
    $game->phase=$request->phase;
    $game->save();
    
    return 'success';  
      
  }

  public function massupdatefixture(Request $request){
    for ($i=0; $i < sizeof($request->game_id) ; $i++) { 
          $game=Game::find($request->game_id[$i]);
          // $game->team1_id=$request->team1[$i];
          // $game->team2_id=$request->team2[$i];
          // $game->venue_id=$request->venue[$i];
          $game->game_date=$request->game_date[$i];
          $game->local_start_time=$request->local_start_time[$i];
          // $game->phase=$request->phase[$i];
          $game->save();
         
     }  
      return "success";

  }


   public function editfixture($id)
   {
    // $event=Event::pluck('name','event_id');
    // $event->prepend("Select Event","");
    // $games = DB::table('ju_game')
    //         ->join('ju_event_game', 'ju_event_game.game_id', '=', 'ju_game.game_id')
    //         ->where('ju_event_game.event_id', '=', $id)
    //         ->select('ju_game.*')
    //         ->get();
    // return view('backEnd.admin.game.editfixture', compact('games'));
    $event_id = $id;



            $games =EventGame::select('g.game_id','t1.team_id as team1_id','t2.team_id as team2_id','t1.name as team1_name','t2.name as team2_name','g.game_date','g.local_start_time', 'ju_event_game.game_id','g.phase','v.venue_id as venue_id','v.name as venue_name')
            ->join('ju_game as g', 'ju_event_game.game_id', '=', 'g.game_id') 
            ->join('ju_team as t1', 'g.team1_id', '=', 't1.team_id')
            ->join('ju_team as t2', 'g.team2_id', '=', 't2.team_id')
            ->join('venue as v', 'g.venue_id', '=', 'v.venue_id') 
            ->where('ju_event_game.event_id','=',$id)
            ->where('g.status','!=', 'full') 
            ->where('g.status','!=', 'summary') 
            ->orderBy('g.game_date')->get();
            // print_r($games);
        $team=DB::Select("SELECT t.team_id,t.name from ju_team t
            where t.team_id in 
            (SELECT DISTINCT t1.team_id from ju_event_game eg
            left outer join ju_game g on g.game_id=eg.game_id
            left outer join ju_team t1 on t1.team_id=g.team1_id
            where
            eg.event_id=$id
            )
            OR
            t.team_id in 
            (SELECT DISTINCT t2.team_id from ju_event_game eg
            left outer join ju_game g on g.game_id=eg.game_id
            left outer join ju_team t2 on t2.team_id=g.team2_id
            where
            eg.event_id=$id
            )");
        // $team=json_decode(json_encode($team,True));
     
       
     
        // $team=Team::orderBy('name','asc')->distinct()->pluck('name','team_id'); 
        $venue=Venue::orderBy('name','asc')->distinct()->pluck('name','venue_id');  
        $phase=Game::orderBy('phase','asc')->distinct()->pluck('phase','phase');             
        return view('backEnd.admin.game.editfixture',compact('games','event_id','team','venue','phase'));    
   }
   public function deletefixture(Request $request)
   {

      $id = $request->input('game_id');
      $query = DB::table('ju_event_game')->where('game_id', '=', $id)->delete();
      $query = DB::table('ju_game_interval_score')->where('game_id', '=', $id)->delete();
      $query = DB::table('ju_game_official')->where('game_id', '=', $id)->delete();
      $query = DB::table('ju_game_overtime_score')->where('game_id', '=', $id)->delete();
      $query = DB::table('ju_game_player')->where('game_id', '=', $id)->delete();
      $query = DB::table('ju_game_team_official')->where('game_id', '=', $id)->delete();
      $query = DB::table('ju_game')->where(array('game_id'=> $id,'status' => 'fixture'));
      if ($query->delete()) {
          Session::flash('message', 'Fixture deleted!');
          Session::flash('status', 'success');

      return 'success';
      } else {
           return "<div class='alert alert-danger'>Can't Delete...! Scorecard details has been updated for this game...</div>";
      }
        
      
   }
}
