<?php

namespace App\Http\Controllers\wr;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\wr\Team as Team;
use App\Country;
use App\Region;
use App\SubRegion;
use App\Model\wr\PictureTeam;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;
use App\Game;
use App\Sport;
class TeamController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    protected $sport_id,$table_prefix;
    public function __construct()
    {   
         $sport_id="";   
         // $this->table_prefix= \Session::get('table_prefix');
        $this->middleware(function ($request, $next){
        //$this->table_prefix = session('table_prefix');
        session()->put('table_prefix', 'wr');
        $this->table_prefix =   session()->get('table_prefix');
        $sport_id_arr=Sport::where('identifier',$this->table_prefix)->first();
        $this->sport_id=$sport_id_arr->sport_id;
        return $next($request);
        });
         
    }


     public function index()
    {
        $team = Team::all();

        return view('backEnd.admin.team.index', compact('team'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $country = Country::orderby('name','asc')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');   
        $region = Region::orderby('name','asc')->pluck('name', 'region_id');
        $region->prepend('Select Region', '');
        $subregion = SubRegion::orderby('name','asc')->pluck('name', 'sub_region_id');
        $subregion->prepend('Select SubRegion', ''); 
        return view('backEnd.admin.team.create',compact('country','region','subregion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'name' => 'required',
            'country_id' => 'required',
            'region' => 'required'
        ]);
        // return "hereh";
        //$team = Team::create($request->all());
        $team = new Team;
            $team->name = $request->name;
            $team->country_id = $request->country_id;
            $team->region = $request->region;
            if($request->sub_region_id != null)
            {

            $team->sub_region_id = $request->sub_region_id;
            }
            if($request->related_team != null)
            {
                $team->related_team = $request->related_team;
            }
            $team->save();
        if($request->picture!=''){
            $picture = new PictureTeam;
            $picture->picture_id = $request->picture;
            $picture->team_id = $team->team_id;
            $picture->save();
        }

        Session::flash('message', 'Team added!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/team');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $team = Team::findOrFail($id);

        return view('backEnd.admin.team.show', compact('team'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $team = Team::findOrFail($id);
        $country = Country::orderby('name','asc')->pluck('name', 'country_id');
        $region = Region::where('country_id','=',$team->country_id)->orderby('name','asc')->pluck('name', 'region_id');  
        $subregion = SubRegion::orderby('name','asc')->pluck('name', 'sub_region_id');
        $subregion->prepend('Select', '');
        $team_picture=PictureTeam::where('team_id',$id)->first();
        if(isset($team_picture)){$team_picture_pic=$team_picture->picture_id;}else{$team_picture_pic="";}
        return view('backEnd.admin.team.edit', compact('team','country','region','subregion','team_picture_pic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'country_id' => 'required',
            'region' => 'required'
        ]);
        $team = Team::findOrFail($id);
        $team->update($request->all());

        if($request['picture']!=''){
            DB::table('ju_picture_team')->where('team_id', '=', $id)->delete();

            DB::table('ju_picture_team')->insert(['picture_id' => $request['picture'], 'team_id' => $id]);
        }

        Session::flash('message', 'Team updated!');
        Session::flash('status', 'success');

         return redirect('admin/'.$this->table_prefix.'/team');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $team = Team::findOrFail($id);

        $team->delete();

        Session::flash('message', 'Team deleted!');
        Session::flash('status', 'success');

        return redirect('admin/'.$this->table_prefix.'/team');
    }
    public function teams()
    {
        $title = "Teams";
        $d_alph = 'A';
        $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        $teams = Team::orderBy('name')->where('name','LIKE',$d_alph.'%')->get();
        $team =$teams->first();

        return view('frontEnd.teams', compact('teams','team','country','title','d_alph'));
    }
    public function teamsnew()
    {
        $title = "Teams";
        $d_alph = '';
         // $country=DB::table('event as e')
         //  ->join('country as c', 'c.country_id', '=', 'e.country_id')
         //  ->distinct()
         //  ->pluck('c.name', 'e.country_id');
        $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        $teams = Team::orderBy('name')->where('name','LIKE',$d_alph.'%')->get();
        $team =$teams->first();
        $teams= DB::Select("SELECT t.*,IFNULL(p.picture_id,'')picture_id from ju_team t 
            left outer join ju_picture_team pt on pt.team_id=t.team_id
            left outer join picture p on p.picture_id=pt.picture_id
            where
            t.name like '".$d_alph."%'
            "
            );

        $team =$teams[0];



        $team_id=$team->team_id;
        $c_id=$team->country_id;
        $c_names=Country::where('country_id',$c_id)->first();
        $c_name=$c_names->name;
        $t_srch = Team::orderby('name')->pluck('name', 'team_id');
        $t_srch->prepend('Select Team','');
        $games = DB::table('ju_game as g')
        ->join('player as t1', 'g.player1_id', '=', 't1.player_id')
        ->join('player as t2', 'g.player2_id', '=', 't2.player_id')
        ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')
        ->select('t1.full_name as team1','t2.full_name as team2','g.game_date as date','g.game_id as game_id','g.phase','v.name as venue','v.venue_id as venue_id','g.status','g.local_start_time as time')
        ->where('g.team1_id','=',$team_id)->orWhere('g.team2_id','=',$team_id)
        ->get();
		$winnercount = DB::select( "select count(if(winner='1' and team1_id=".$team_id.", team1_id, NULL))+count(if(winner='2' and team2_id=".$team_id.", team2_id, NULL)) as cntwinner, count(if(winner='1' and team2_id=".$team_id.", team2_id, NULL))+count(if(winner='2' and team1_id=".$team_id.", team1_id, NULL)) as cntlooser
            from ju_game where (team1_id=".$team_id." or team2_id=".$team_id.")");
        return view('frontEnd.teams', compact('teams','team','country','title','d_alph','t_srch','c_name','games','winnercount'));
    }
        public function teamsindex()
    {
        $title = "Teams";
        $d_alph = '';
         // $country=DB::table('event as e')
         //  ->join('country as c', 'c.country_id', '=', 'e.country_id')
         //  ->distinct()
         //  ->pluck('c.name', 'e.country_id');
        $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        // $teams = Team::orderBy('name')->where('name','LIKE',$d_alph.'%')->get();
        // $team =$teams->first();
        $teams= DB::Select("SELECT t.*,IFNULL(p.picture_id,'')picture_id from team t 
            left outer join picture_team pt on pt.team_id=t.team_id
            left outer join picture p on p.picture_id=pt.picture_id
            where
           t.team_id in (
11, 12, 19, 20, 21, 23, 25, 121 ,134 ,137 ,138 ,182 ,183 ,184 ,185 ,186 ,187 ,189 ,191 ,192 ,193 ,194 ,195 ,196 ,197 ,198 ,199 ,200 ,202 ,207 ,208 ,284,317 ,492 ,493 ,494 ,495 ,496 ,497 ,499 ,504 ,505 ,506 ,508 ,510 ,512 ,513 ,514 ,515 ,518 ,519 ,720 ,852 ,916 ,918 ,920 ,921 ,922 ,924 ,925,926,927 ,928 ,930 ,931 ,932 ,934 ,936 ,937 ,938 ,940 ,941 ,942 ,944 ,946 ,947 ,948 ,950 ,951 ,952 ,954 ,955 ,956 ,958 ,959 ,960 ,961 ,963 ,964 ,965 ,967 ,968 ,972 ,973 ,974 ,975 ,977 ,978 ,980 ,981 ,982 ,983 ,984 ,985 ,986 ,987 ,988 ,989 ,991 ,992 ,993 ,994 ,996 ,997 ,999 ,1000,   1001,1003,1004,1006,1007,1008,1009,1011,1012,   1013,   1014,   1017,   1018,   1019,   1020,   1027,   1028,   1029,   1030,1031,   1032,   1033,   1034,   1036,   1037,   1038,   1039,   1040,   1042,   1043,   1044,   1046,   1050,   1052,   1053,   1054,   1055,   1056,   1057,   1059,   188 ,190 ,203 ,204 ,205 ,206 ,319 ,498 ,500 ,501 ,502 ,503 ,509 ,511 ,516 ,715 ,917 ,919 ,923 ,929 ,933 ,935 ,939 ,943 ,945 ,949 ,953 ,957 ,962 ,966 ,969 ,970 ,971 ,976 ,979 ,990 ,995 ,998 ,1002,   1005,   1010,   1015,   1016,1035,1041,   1045,   1047,   1048,   1049,   1051,   1058)
            order by t.name"
        );

        $team =$teams[0];



        $team_id=$team->team_id;
        $c_id=$team->country_id;
        $c_names=Country::where('country_id',$c_id)->first();
        $c_name=$c_names->name;
        $t_srch = Team::orderby('name')->pluck('name', 'team_id');
        $t_srch->prepend('Select Team','');
        $games = DB::table('game as g')
        ->join('team as t1', 'g.team1_id', '=', 't1.team_id')
        ->join('team as t2', 'g.team2_id', '=', 't2.team_id')
        ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')
        ->select('t1.name as team1','t2.name as team2','g.game_date as date','g.game_id as game_id','g.phase','v.name as venue','v.venue_id as venue_id','g.status','g.local_start_time as time')
        ->where('g.team1_id','=',$team_id)->orWhere('g.team2_id','=',$team_id)
        ->get();
        return view('frontEnd.teamsindex', compact('teams','team','country','title','d_alph','t_srch','c_name','games'));
    }
     public function searchteamnew(Request $request)
    {   
     
        $query_str = $request->query_str;
        $d_alph = strtoupper(substr($query_str, 0, 1));
        $vsql = Team::where('name','like','%'.$query_str.'%')->where('region','=',1)->where('team_id','<>',1)->get();
        $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        $t_srch = Team::orderby('name')->pluck('name', 'team_id');
        $t_srch->prepend('Select Team','');
        return view('frontEnd.teamlist',compact('vsql','country','t_srch','d_alph','query_str')); 

  
    }
    public function getteam(Request $request)
    {
        $team_id = $request->team_id;

        $team = Team::where('team_id','=',$team_id)->first();

                            $image = $team->picture['picture_id'];

                    if($image){
                    $img = url('/uploads').'/'.$image.'.jpg';
                    }else{
                    $img = url('/images')."/team-profile-img02.png";
                    }
        $game_url = url("/team")."/".$team->team_id."/games";
        $inner = '<div class="team-img col-lg-4">
        <img src="'.$img.'" width="200">
        </div>
      
        <div class="team-info col-lg-8">

            <h3>'.$team->name.'</h3>
            <h2><small>'.$team->country["name"].'</small></h2>
            <a href="'.$game_url.'"><button class="btn btn-default src_btn" type="button">View Games Played</button></a>
        </div>';

    echo $inner;
    }
    public function teambyid($id)
    {
        $title = "Teams";
        $d_alph = '';
        $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        $teams = Team::where('team_id','=',$id)->get();
        $team =$teams->first();

        return view('frontEnd.teams', compact('teams','team','country','title','d_alph'));
    }
    public function teambyidnew($id)
    {
        $title = "Teams";
        $d_alph = '';
        $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
       

            $teams= DB::Select("SELECT t.*,IFNULL(p.picture_id,'')picture_id from ju_team t 
            left outer join ju_picture_team pt on pt.team_id=t.team_id
            left outer join picture p on p.picture_id=pt.picture_id
            where t.team_id=$id
             "
            );

        $team =$teams[0];



        $c_id=$team->country_id;
        $c_names=Country::where('country_id',$c_id)->first();
        $c_name=$c_names->name;
        $team_id = $id;

        $team_has_squads = 'n';
        /*if (EventSquad::where('team_id', '=', $team_id)->exists()) {
            $team_has_squads = 'y';
        }*/
        $team_name = Team::where('team_id','=',$id)->get()->first();
        $t_srch = Team::orderby('name')->pluck('name', 'team_id');
        $t_srch->prepend('Select Team', '');
         $games = DB::table('ju_game as g')
        ->join('ju_team as t1', 'g.team1_id', '=', 't1.team_id')
        ->join('ju_team as t2', 'g.team2_id', '=', 't2.team_id')
        ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')
        ->join('ju_event_game as eg', 'g.game_id', '=', 'eg.game_id')
        ->join('ju_event as e', 'eg.event_id', '=', 'e.event_id')
        ->select('t1.name as team1','t2.name as team2','e.name as event_name','e.event_id','g.game_date as date','g.game_id as game_id','v.venue_id','v.name as venue','g.phase','g.status','g.local_start_time as time')
        ->where('g.team1_id','=',$id)->orWhere('g.team2_id','=',$id)
        ->get();
        $winnercount = DB::select( "select count(if(winner='1' and team1_id=".$id.", team1_id, NULL))+count(if(winner='2' and team2_id=".$id.", team2_id, NULL)) as cntwinner, count(if(winner='1' and team2_id=".$id.", team2_id, NULL))+count(if(winner='2' and team1_id=".$id.", team1_id, NULL)) as cntlooser
            from ju_game where (team1_id=".$id." or team2_id=".$id.")");
        return view('frontEnd.teamsteam', compact('c_name','games','t_srch','teams','team','country','title','d_alph','team_id','team_has_squads','winnercount','team_name'));
    }
    public function teamsbyalph($alph)
    {
        $title = "Teams";
        $d_alph = ucwords($alph);
        $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        $teams = Team::orderBy('name', 'asc')->where('name','LIKE',$alph.'%')->get();
        $team =$teams->first();

        return view('frontEnd.teams', compact('teams','team','country','title','d_alph'));
    }
      public function teamsnewbyalph($alph)
    {
        $d_alph = ucwords($alph);
        $query_str = "";
        $vsql = Team::where('name','like',$d_alph.'%')->where('region','=',1)->where('team_id','<>',1)->get();
        $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        $t_srch = Team::orderby('name')->pluck('name', 'team_id');
        $t_srch->prepend('Select Team','');
        return view('frontEnd.teamlist',compact('vsql','country','t_srch','d_alph','query_str'));
    }
    public function teamsbycountry($country_id)
    {
        $title = "Teams";
        $d_alph = '';
        $country_name = Country::where('country_id','=',$country_id)->first()->name;
        $nav_title = 'Showing teams in <b>'.$country_name.'</b>';
        $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        $teams = Team::orderBy('name', 'asc')->where('country_id','=',$country_id)->get();
        $team =$teams->first();

        return view('frontEnd.teams', compact('teams','team','country','title','d_alph','nav_title'));
    }
     public function teamsbycountrynew($country_id)
    {
        $title = "Teams";
        $d_alph = '';
        $country_name = Country::where('country_id','=',$country_id)->first()->name;
        $nav_title = 'Showing teams in <b>'.$country_name.'</b>';
         $country = Country::orderby('name')->pluck('name', 'country_id');
        $country->prepend('Select Country', '');
        $countryteams = Team::orderBy('name', 'asc')->distinct()->where('country_id','=',$country_id)->get();
        // $team =$teams->first();
        $t_srch = Team::orderby('name')->where('country_id','=',$country_id)->pluck('name', 'team_id');
        $t_srch->prepend('Select Team', '');
        return view('frontEnd.teamscountry', compact('countryteams','t_srch','country','title','d_alph','nav_title'));
    }
    public function getteamsbycountry(Request $request){

     $teams = Team::orderBy('name')->where('country_id','=',$request->country_id)->pluck('name','team_id');
        
        $teams->prepend('Select Team', 0);
        return json_encode($teams);
    }
    public function searchteam(Request $request)
    {   
        $query_str = $request->query_str;
        $teams = Team::orderBy('name', 'asc')->where('name','LIKE','%'.$query_str.'%')->get();
        $team = $teams->first();
        
        $i=0;
        $list = "";


        foreach($teams as $item1){
          if($i==0){$active = "active";}else{$active = "";} $i++;

            $list.='<li class="menuli '.$active.'"><a class="team_id" id="'.$item1->team_id.'">'.$item1->name.'</a></li>';
        }

        $game_url = url("/team")."/".$team->team_id."/games";
        $detail = '<div class="team-img col-lg-4">
        <img src="images/team-profile-img02.png" width="200">
        </div>
      
        <div class="team-info col-lg-8">

            <h3>'.$team->fullname.'</h3>
            <h2><small>Team</small></h2>
            <a href="'.$game_url.'"><button class="btn btn-default src_btn" type="button">View Games Played</button></a>
        </div>';


        if($i==0){
           $arr = array('list' => '', 'detail' => '', 'error' => 1);
                echo json_encode($arr);
        }else{
                $arr = array('list' => $list, 'detail' => $detail, 'error' => 0);
                echo json_encode($arr);
        }



    }


   

}
