<?php

namespace App\Http\Controllers\admin\ju;

use App\Http\Requests;
use App\Http\Controllers\admin\ju\Controller;

use App\Event;
use DB;
use App\Country;
use App\Model\ju\Team;
use App\Player;
use App\Sport;
use App\Region;
use App\SubRegion;
use App\Location;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class JudoController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  protected $sport_id, $table_prefix;
  public function __construct()
  {
    $sport_id = "";
    session()->put('table_prefix', 'ju');
    $this->table_prefix =   session()->get('table_prefix');
    $sport_id_arr = Sport::where('identifier', $this->table_prefix)->first();
    $this->sport_id = $sport_id_arr->sport_id;
  }

  public function index()
  {
    $event = Event::orderBy('event_id', 'desc')->get();

    //echo '<pre>'; print_r($event); exit;

    return view('backEnd.admin.judo.index', compact('event'));
  }
  public function getregion(Request $request)
  {
    $region = Region::where('country_id', '=', $request->country_id)->orderby('name', 'asc')->pluck('name', 'region_id');
    $region->prepend('Select Region', 0);
    return json_encode($region);
  }

  public function getsubregion(Request $request)
  {
    $subregion = SubRegion::where('region_id', '=', $request->region_id)->orderby('name')->pluck('name', 'sub_region_id');
    $subregion->prepend('Select Sub Region', 0);
    return json_encode($subregion);
  }
  public function getlocation(Request $request)
  {
    $location = Location::where('sub_region_id', '=', $request->sub_region_id)->orderby('name')->pluck('name', 'location_id');
    $location->prepend('Select Location', 0);
    return json_encode($location);
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $country = Country::orderBy('name', 'asc')->pluck('name', 'country_id');
    $region = Region::orderBy('name', 'asc')->pluck('name', 'region_id');
    $region->prepend('Select Region', ' ');
    $subregion = SubRegion::orderBy('name', 'asc')->pluck('name', 'sub_region_id');
    $subregion->prepend('Select Sub Region', ' ');
    $location = Location::orderBy('name', 'asc')->pluck('name', 'location_id');
    $location->prepend('Select Location', ' ');
    $category = array('International', 'National', 'State');

    return view('backEnd.admin.judo.create', compact('country', 'region', 'subregion', 'category', 'location'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required',
      'season' => 'required',
      'country_id' => 'required',
      'generic_name' => 'required',
      'start_date' => 'required',
      'category' => 'required'
    ]);
    // return 'here';
    if ($request['start_date'] != null) {
      $dateArray = explode('-', $request['start_date']);
      $count = count($dateArray);
      if ($count == 1) {
        $request['start_date'] = $request['start_date'] . '-00-00';
      } else {
        $request['start_date'] = $request['start_date'] . '-00';
      }
    }
    $event = Event::create($request->all());

    /*
    if ($request->picture_id != '') {
      $picture = new PictureEvent;
      $picture->picture_id = $request->picture_id;
      $picture->event_id = $event->event_id;
      $picture->save();
    }
	*/

    Session::flash('message', 'Event added!');
    Session::flash('status', 'success');

    return redirect('admin/' . $this->table_prefix . '/event');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   *
   * @return Response
   */
  public function show($id)
  {
    $event = Event::findOrFail($id);

    return view('backEnd.admin.judo.show', compact('event'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   *
   * @return Response
   */
  public function edit($id, $gameId)
  {
    $event = Event::where('event_id', $id)->first();
    $player = DB::table('player')->where('sport_id', '=', 24)->get();
    // $scoringMethod = ScoreMethod::where('method_status','=',1)->get();
    // echo '<pre>';print_r($scoringMethod);die();
    $gameOfficial = DB::table($this->table_prefix . '_game_official')->where('game_id', '=', $gameId)->get();
    $gameArray = array();
    if (count($gameOfficial) > 0) {
      foreach ($gameOfficial as $game) {
        $gameArray[$game->official_type]['name'] = $game->official_name;
        $gameArray[$game->official_type]['id'] = $game->game_official_id;
      }
    }

    $gameteamOfficial = DB::table($this->table_prefix . '_game_team_official')->where('game_id', '=', $gameId)->get();
    $gameteamArray = array();
    if (count($gameteamOfficial) > 0) {
      foreach ($gameteamOfficial as $gameteam) {
        $gameteamArray[$gameteam->official_type]['name'] = $gameteam->official_name;
        $gameteamArray[$gameteam->official_type]['id'] = $gameteam->game_team_official_id;
      }
    }
    $gamePlayer = DB::table($this->table_prefix . '_game_player')->where('game_id', '=', $gameId)->get();
    $gamePlayerArray = array();
    if (count($gamePlayer) > 0) {
      $i = 0;
      foreach ($gamePlayer as $gamePlayerData) {
        $gamePlayerArray[$i]['player'] = $gamePlayerData->player_id;
        $gamePlayerArray[$i]['scoring_method'] = $gamePlayerData->scoring_method;
        $gamePlayerArray[$i]['technique'] = $gamePlayerData->technique;
        $gamePlayerArray[$i]['time'] = $gamePlayerData->time;
        $gamePlayerArray[$i]['score'] = $gamePlayerData->score;
        $i++;
      }
    }

    $games = DB::table($this->table_prefix . '_game')->where('game_id', '=', $gameId)->get();
    $gamesArray = array();
    if (count($games) > 0) {
      $i = 0;
      foreach ($games as $gamesData) {
        $gamesArray[$i]['score1'] = $gamesData->score1;
        $gamesArray[$i]['score2'] = $gamesData->score2;
        $i++;
      }
    }

    return view('backEnd.admin.judo.index', compact('event', 'player', 'gameArray', 'gameteamArray', 'gamePlayerArray', 'gamesArray', 'gameId'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   *
   * @return Response
   */
  public function update($id, Request $request)
  {
    echo $id;die();
    $gameId = $request['gameIdVal'];
    $playerId = 8522;
    // game_official table entry

    if ($request['refreeId'] == '') {
      DB::table($this->table_prefix . '_game_official')->insert([
        'official_name' => $request['refree'],
        'game_id' => $gameId,
        'reference_player_id' => $playerId,
        'official_type' => 'referee'
      ]);
    } else {
      DB::table($this->table_prefix . '_game_official')->where('game_official_id', $request['refreeId'])
        ->update(['official_name' => $request['refree']]);
    }
    if ($request['timekeeperId'] == '') {
      DB::table($this->table_prefix . '_game_official')->insert([
        'official_name' => $request['timekeeper'],
        'game_id' => $gameId,
        'reference_player_id' => $playerId,
        'official_type' => 'timekeeper'
      ]);
    } else {
      DB::table($this->table_prefix . '_game_official')->where('game_official_id', $request['timekeeperId'])
        ->update(['official_name' => $request['timekeeper']]);
    }
    if ($request['contest_recorderId'] == '') {
      DB::table($this->table_prefix . '_game_official')->insert([
        'official_name' => $request['contest_recorder'],
        'game_id' => $gameId,
        'reference_player_id' => $playerId,
        'official_type' => 'contest_recorder'
      ]);
    } else {
      DB::table($this->table_prefix . '_game_official')->where('game_official_id', $request['contest_recorderId'])
        ->update(['official_name' => $request['contest_recorder']]);
    }
    if ($request['senior_recorderId'] == '') {
      DB::table($this->table_prefix . '_game_official')->insert([
        'official_name' => $request['senior_recorder'],
        'game_id' => $gameId,
        'reference_player_id' => $playerId,
        'official_type' => 'senior_recorder'
      ]);
    } else {
      DB::table($this->table_prefix . '_game_official')->where('game_official_id', $request['senior_recorderId'])
        ->update(['official_name' => $request['senior_recorder']]);
    }
    if ($request['competition_recorderId'] == '') {
      DB::table($this->table_prefix . '_game_official')->insert([
        'official_name' => $request['competition_controller'],
        'game_id' => $gameId,
        'reference_player_id' => $playerId,
        'official_type' => 'competition_recorder'
      ]);
    } else {
      DB::table($this->table_prefix . '_game_official')->where('game_official_id', $request['competition_recorderId'])
        ->update(['official_name' => $request['competition_controller']]);
    }
    // game team official table entry
    if ($request['coachId'] == '') {
      DB::table($this->table_prefix . '_game_team_official')->insert([
        'official_name' => $request['coach'],
        'game_id' => $gameId,
        'player_id' => $playerId,
        'official_type' => 'coach'
      ]);
    } else {
      DB::table($this->table_prefix . '_game_team_official')->where('game_team_official_id', $request['coachId'])
        ->update(['official_name' => $request['coach']]);
    }
    if ($request['coach2Id'] == '') {
      DB::table($this->table_prefix . '_game_team_official')->insert([
        'official_name' => $request['coach2'],
        'game_id' => $gameId,
        'player_id' => $playerId,
        'official_type' => 'assistantcoach'
      ]);
    } else {
      DB::table($this->table_prefix . '_game_team_official')->where('game_team_official_id', $request['coach2Id'])
        ->update(['official_name' => $request['coach2']]);
    }

    DB::table($this->table_prefix . '_game_player')->where('game_id', '=', $gameId)->delete();
    $playerDatas = json_decode($request['playerDatas'], true);
    DB::table($this->table_prefix . '_game_player')->insert($playerDatas);
    DB::table($this->table_prefix . '_game')->where('game_id', $gameId)->update(['score1' => $request['total_score'], 'score2' => $request['total_score2']]);

    Session::flash('message', 'Event updated!');
    Session::flash('status', 'success');

    return redirect('admin/' . $this->table_prefix . '/events/' . $id . '/' . $gameId);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    $event = Event::findOrFail($id);

    $event->delete();

    DB::table('wr_picture_event')->where('event_id', '=', $id)->delete();

    Session::flash('message', 'Event deleted!');
    Session::flash('status', 'success');

    return redirect('admin/' . $this->table_prefix . '/event');
  }

  public function events()
  {
    $seasons = Event::groupBy('season')->orderBy('season', 'desc')->select('season')->get();
    $defaultseason = $seasons->first()->season;

    $events = Event::orderBy('start_date', 'desc')->where('season', 'LIKE', $defaultseason . '%')->get();
    if (count($events)) {

      $event_id = $events->first()->event_id;

      $games = DB::table('wr_event_game as p')
        ->join('wr_game as g', 'p.game_id', '=', 'g.game_id')
        ->join('wr_team as t1', 'g.team1_id', '=', 't1.team_id')
        ->join('wr_team as t2', 'g.team2_id', '=', 't2.team_id')
        ->join('venue as v', 'g.venue_id', '=', 'v.venue_id')
        ->select('t1.name as team1', 't2.name as team2', 'g.status as status', 'g.game_date as date', 'g.game_id as game_id', 'v.venue_id as venue_id', 'v.name as venue', 'p.event_id as event_id', 'g.gmt_start_time as time', 'g.phase')
        ->where('p.event_id', '=', $event_id)
        ->get();
    } else {

      $events = array();
      $games = array();
    }
    return view('frontEnd.events', compact('events', 'games', 'seasons', 'defaultseason', 'event_id'));
  }
  public function archiveeventsbyseason($id)
  {
    $defaultseason = str_replace('-', '/', $id);
    $events = Event::orderBy('start_date', 'desc')->where('season', 'LIKE', $defaultseason . '%')->get();
    return view('frontEnd.archive.events.events', compact('events'));
  }
  public function archiveeventseasons()
  {

    $seasons = Event::groupBy('season')->orderBy('season', 'desc')->select('season')->get();
    return view('frontEnd.archive.events.seasons', compact('seasons'));
  }

  public function getcountries(Request $request)

  {


    $countries = DB::table('event as e')
      ->join('country as c', 'c.country_id', '=', 'e.country_id')
      ->distinct()
      ->where('season', 'LIKE', $request->season)
      ->orderBy('c.name', 'asc')
      ->pluck('c.name', 'e.country_id');


    $countries->prepend('Select Country', 0);
    return json_encode($countries);
  }
  public function getevents(Request $request)

  {
    // $events=DB::table('event as e')
    //         ->where('season','=',$request->season)
    //         ->where('country_id','=',$request->country_id)
    //         ->pluck('e.name','e.event_id');

    $events = Event::orderBy('name')->where('season', '=', $request->season)->where('country_id', '=', $request->country_id)->pluck('name', 'event_id');

    $events->prepend('Select Event', 0);
    return json_encode($events);
  }



  /*import event*/
  public function upload()
  {
    return view('backEnd.admin.event.uploadevents');
  }
}
