<?php

namespace App\Http\Controllers\wr;

use App\Http\Requests;
use App\Http\Controllers\wr\Controller;
use App\Model\wr\Event;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;  
use File;  
use App\Game;
use App\EventGame;
use App\Player;
use App\Model\wr\GamePlayer;
use App\Model\wr\GameOfficial;
use App\Model\wr\GameTeamOfficial;
use App\Model\wr\Videos;

class ScorecardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function uploadscorecardsel()
    {
      $event=Event::pluck('name','event_id');
      $event->prepend('Select Event','');
      $game=array('Select Game','');
      return view('backEnd.admin.scorecard.ju_scorecard',compact('event','game'));
    }
      public function uploadscorecardselsubmit(Request $request)
    {
     
    $event_id=$request->event_id;
    $game_id=$request->game_id;
    $event_name=Event::where('event_id',$event_id)->first()->name;
    
     $player1_id=Game::where('game_id',$game_id)->first()->player1_id;
     $player2_id=Game::where('game_id',$game_id)->first()->player2_id;
     $player1_name=Player::where('player_id',$player1_id)->first()->full_name;
     $player2_name=Player::where('player_id',$player2_id)->first()->full_name;
     $game_name=$player1_name." Vs ".$player2_name;
     $player_name_sql=DB::Select("SELECT p1.full_name player_name,p1.player_id FROM 
      ju_game g
      left outer join player p1 on p1.player_id=g.player1_id
      union ALL
      SELECT p2.full_name player_name,p2.player_id FROM 
      ju_game g
      left outer join player p2 on p2.player_id=g.player2_id");
    $player_name_arr=array(); 
    $players = array(0 =>$player1_name,1=>$player2_name );
    $playerids = array(0 =>$player1_id,1=>$player2_id );

 foreach ($player_name_sql as $key) {
  // echo $key->player_name;
  $player_name_arr[$key->player_id]=$key->player_name;
 }

    return view('backEnd.admin.scorecard.ju_fullscorecard',compact('event_id','game_id','game_name','event_name','player_name_arr','players','playerids'));
    }
    public function getgames()
    {
      $event_id=$_GET['event_id'];
      $games=EventGame::
      leftJoin('ju_game as g', function($join) {
      $join->on('g.game_id', '=', 'ju_event_game.game_id');
      })
      -> leftJoin('player as p1', function($join) {
      $join->on('g.player1_id', '=', 'p1.player_id');
      })
      ->leftJoin('player as p2', function($join) {
      $join->on('g.player2_id', '=', 'p2.player_id');
      })
      ->select('p1.full_name as f1','p2.full_name as f2','g.game_id')
      ->where('event_id',$event_id)
      ->where('g.status','!=','full')
      ->get();
      $game="";
      foreach ($games as $key ) {
         // echo $key->player1_id;
          $game.="<option value='$key->game_id'>$key->f1 vs $key->f2</option>";
          // $game.="<option value='".$key->game_id."'>".$key->f1." vs ".$key->f2."</option>";
      }
    return $game;

    }
    public function submitscorecard(Request $request)
    {
      $error_txt="";
    if($request->player_id[0] == $request->player_id[1]){
      $error_txt.="Players cannot be the same .Please check your selection";
    }else{


      for ($i=0; $i < sizeof($request->player_id) ; $i++) { 
        if($request->player_id[$i] != "")
        {
              $game_player=new GamePlayer;
              $game_player->game_id=$request->game_id;
              $game_player->player_id=$request->player_id[$i];
              $game_player->takedown_t2= ($request->takedown_t2[$i]=="")?-1:$request->takedown_t2[$i];
              $game_player->nearfall_n2=($request->nearfall_n2[$i]=="")?-1:$request->nearfall_n2[$i];
              $game_player->nearfall_n3=($request->nearfall_n3[$i]=="")?-1:$request->nearfall_n3[$i];
              $game_player->reversal_r2=($request->reversal_r2[$i]=="")?-1:$request->reversal_r2[$i];
              $game_player->escape_e1=($request->escape_e1[$i]=="")?-1:$request->escape_e1[$i];
              $game_player->penalty_p1=($request->penalty_p1[$i]=="")?-1:$request->penalty_p1[$i];
              $game_player->penalty_p2=($request->penalty_p2[$i]=="")?-1:$request->penalty_p2[$i];
              $game_player->stalling=($request->stalling[$i]=="")?-1:$request->stalling [$i];
              $game_player->stalling_w2=($request->stalling_w2[$i]=="")?-1:$request->stalling_w2[$i];
              $game_player->caution=($request->caution[$i]=="")?-1:$request->caution[$i];
              $game_player->caution_c1=($request->caution_c1[$i]=="")?-1:$request->caution_c1[$i];
              $game_player->gametime_seconds=($request->gametime_seconds[$i]=="")?-1:$request->gametime_seconds[$i];
              $game_player->save();
            }
      }
      if($request->mat_chair!=""){
        $mat_chair=new GameOfficial;
        $mat_chair->game_id=$request->game_id;
        $mat_chair->official_name=$request->mat_chair;
        $mat_chair->official_type='matchairman';
        $mat_chair->save();
      }
        if($request->referee!=""){
        $referee=new GameOfficial;
        $referee->game_id=$request->game_id;
        $referee->official_name=$request->referee;
        $referee->official_type='referee';
         $referee->save();
      }

    if($request->judge!=""){
        $judge=new GameOfficial;
        $judge->game_id=$request->game_id;
        $judge->official_name=$request->judge;
        $judge->official_type='judge';
         $judge->save();
      }
      if($request->coach_1!=""){
        $coach_1=new GameTeamOfficial;
        $coach_1->game_id=$request->game_id;
        $coach_1->player_id=$request->player_id[0];
        $coach_1->official_name=$request->coach_1;
        $coach_1->official_type='coach';
         $coach_1->save();
      }
      if($request->coach_2!=""){
        $coach_2=new GameTeamOfficial;
        $coach_2->game_id=$request->game_id;
        $coach_2->player_id=$request->player_id[1];
        $coach_2->official_name=$request->coach_2;
        $coach_2->official_type='coach';
        $coach_2->save();
      }
      if($request->player1_score > $request->player2_score){
        $game_winner='1'; 
      }elseif($request->player1_score < $request->player2_score){
        $game_winner='2'; 
      }else{
        $game_winner='0';
      }
      $game=Game::where('game_id',$request->game_id)->update(array('score1'=> $request->player1_score,'score2'=>$request->player2_score,'status'=>'full','winner'=>$game_winner));
     }//end of validation else
      $event=Event::pluck('name','event_id');
      $event->prepend('Select Event','');
      $game=array('Select Game','');
      // Session::flash('message', 'Scorecard Uploaded Successfully!');
      // Session::flash('status', 'success');

      if($error_txt==""){
        return 1;
      }else
      {
        return $error_txt;
      }
      return view('backEnd.admin.scorecard.ju_scorecard',compact('event','game'));
          
    }
     public function scorecardnew($game_id)
    {
      
        $game = Game::where('game_id','=',$game_id)->with('venue','player1','player2')->first();
        $scorecard = GamePlayer::leftJoin('ju_team as t','ju_game_player.team_id','=','t.team_id')->leftJoin('regions as r','t.region','=','r.region_id')->where('game_id','=',$game_id)->with('player')->get();
        $gameofficial = GameOfficial::where('game_id','=',$game_id)->get();
        $gameteamofficial = GameTeamOfficial::where('game_id','=',$game_id)->get();
        $title = 'SCORECARD';
       
        $videos=Videos::where ('game_id','=',$game_id)->get();
        $event_data = EventGame::where('ju_event_game.game_id','=',$game_id)->join('ju_event','ju_event_game.event_id','=','ju_event.event_id')->select('ju_event.name','ju_event.event_id')->first();
         // print_r($game);
       // foreach ($scorecard as $key ) {
        // echo $key->player_id;
       // }
        // echo $game_id;
       // return;
        return view('frontEnd.ju_scorecard', compact('title','scorecard','game','gameofficial','gameteamofficial','event_data','videos'));
    }
    public function editscorecardsel()
    {
      $event=Event::pluck('name','event_id');
      $event->prepend('Select Event','');
      $game=array('Select Game','');
      return view('backEnd.admin.scorecard.ju_editscorecard',compact('event','game'));
    }
    public function editgetgames()
    {
      $event_id=$_GET['event_id'];
      $games=EventGame::
      leftJoin('ju_game as g', function($join) {
      $join->on('g.game_id', '=', 'ju_event_game.game_id');
      })
      -> leftJoin('player as p1', function($join) {
      $join->on('g.player1_id', '=', 'p1.player_id');
      })
      ->leftJoin('player as p2', function($join) {
      $join->on('g.player2_id', '=', 'p2.player_id');
      })
      ->select('p1.full_name as f1','p2.full_name as f2','g.game_id')
      ->where('event_id',$event_id)
      ->where('g.status','!=','')
      ->get();
      $game="";
      foreach ($games as $key ) {
         // echo $key->player1_id;
          $game.="<option value='$key->game_id'>$key->f1 vs $key->f2</option>";
          // $game.="<option value='".$key->game_id."'>".$key->f1." vs ".$key->f2."</option>";
      }
    return $game;

    }
      public function editscorecardselsubmit(Request $request)
    {
     
      $event_id=$request->event_id;
      $game_id=$request->game_id;
      $event_name=Event::where('event_id',$event_id)->first()->name;
    
       $player1_id=Game::where('game_id',$game_id)->first()->player1_id;
       $player2_id=Game::where('game_id',$game_id)->first()->player2_id;
       $player1_name=Player::where('player_id',$player1_id)->first()->full_name;
       $player2_name=Player::where('player_id',$player2_id)->first()->full_name;
       $game_name=$player1_name." Vs ".$player2_name;
       $player_name_sql=DB::Select("SELECT p1.full_name player_name,p1.player_id FROM 
        ju_game g
        left outer join player p1 on p1.player_id=g.player1_id
        union ALL
        SELECT p2.full_name player_name,p2.player_id FROM 
        ju_game g
        left outer join player p2 on p2.player_id=g.player2_id");
        $player_name_arr=array(); 

         foreach ($player_name_sql as $key) {
          $player_name_arr[$key->player_id]=$key->player_name;
         }
         $players = array(0 =>$player1_name,1=>$player2_name );
         $playerids = array(0 =>$player1_id,1=>$player2_id );
         $gamedetails = GamePlayer::where('game_id',$game_id)->get()->toArray();
         $player1_score = Game::where('game_id',$game_id)->first()->score1;
         $player2_score = Game::where('game_id',$game_id)->first()->score2;
         $mat_chair = '';
         $referee = '';
         $judge = '';
         $coach_1 = '';
         $coach_2 = '';
         $details = GameOfficial::where(array('game_id'=>$game_id))->get()->toArray();
         $GameTeamOfficial = GameTeamOfficial::where(array('game_id'=>$game_id))->get()->toArray();
         if(!empty($details))
         {
          $matchair = GameOfficial::where(array('game_id'=>$game_id,'official_type'=>'matchairman'))->get()->toArray();
          if(!empty($matchair))
          {
            $mat_chair .= GameOfficial::where(array('game_id'=>$game_id,'official_type'=>'matchairman'))->first()->official_name;
          }
          $refereedata = GameOfficial::where(array('game_id'=>$game_id,'official_type'=>'referee'))->get()->toArray();
          if(!empty($refereedata))
          {
            $referee .= GameOfficial::where(array('game_id'=>$game_id,'official_type'=>'referee'))->first()->official_name;
          }
          $judgedata = GameOfficial::where(array('game_id'=>$game_id,'official_type'=>'judge'))->get()->toArray();
          if(!empty($judgedata))
          {
            $judge .= GameOfficial::where(array('game_id'=>$game_id,'official_type'=>'judge'))->first()->official_name;
          }

         }
         if(!empty($GameTeamOfficial))
         {
          $coach_1data = GameTeamOfficial::where(array('game_id'=>$game_id,'player_id'=>$player1_id,'official_type'=>'coach'))->get()->toArray();
          if(!empty($coach_1data))
          {

            $coach_1 .= GameTeamOfficial::where(array('game_id'=>$game_id,'player_id'=>$player1_id,'official_type'=>'coach'))->first()->official_name;
          }
          $coach_2data = GameTeamOfficial::where(array('game_id'=>$game_id,'player_id'=>$player2_id,'official_type'=>'coach'))->get()->toArray();
          if(!empty($coach_2data))
          {

            $coach_2 .= GameTeamOfficial::where(array('game_id'=>$game_id,'player_id'=>$player2_id,'official_type'=>'coach'))->first()->official_name;
          }
        // $coach_2 .= GameTeamOfficial::where(array('game_id'=>$game_id,'player_id'=>$player2_id,'official_type'=>'coach'))->first()->official_name;

         }
         
       /*  echo "<pre>";
         print_r($game);
         exit();*/

      return view('backEnd.admin.scorecard.ju_editfullscorecard',compact('event_id','game_id','game_name','event_name','player_name_arr','player1_id','player2_id','gamedetails','player1_score','player2_score','mat_chair','referee','judge','coach_1','coach_2','players','playerids'));
    }
    public function editsubmitscorecard(Request $request)
    {
      $error_txt="";
    if($request->player_id[0] == $request->player_id[1]){
      $error_txt.="Players cannot be the same .Please check your selection";
    }else{


      for ($i=0; $i < sizeof($request->player_id) ; $i++) { 
        if($request->player_id[$i] != "")
        {
          $gameplayer = DB::table('ju_game_player')->where('game_id',$request->game_id)->get()->toArray();
          if(empty($gameplayer))
          {
              $game_player=new GamePlayer;
              $game_player->game_id=$request->game_id;
              $game_player->player_id=$request->player_id[$i];
              $game_player->takedown_t2= ($request->takedown_t2[$i]=="")?-1:$request->takedown_t2[$i];
              $game_player->nearfall_n2=($request->nearfall_n2[$i]=="")?-1:$request->nearfall_n2[$i];
              $game_player->nearfall_n3=($request->nearfall_n3[$i]=="")?-1:$request->nearfall_n3[$i];
              $game_player->reversal_r2=($request->reversal_r2[$i]=="")?-1:$request->reversal_r2[$i];
              $game_player->escape_e1=($request->escape_e1[$i]=="")?-1:$request->escape_e1[$i];
              $game_player->penalty_p1=($request->penalty_p1[$i]=="")?-1:$request->penalty_p1[$i];
              $game_player->penalty_p2=($request->penalty_p2[$i]=="")?-1:$request->penalty_p2[$i];
              $game_player->stalling=($request->stalling[$i]=="")?-1:$request->stalling [$i];
              $game_player->stalling_w2=($request->stalling_w2[$i]=="")?-1:$request->stalling_w2[$i];
              $game_player->caution=($request->caution[$i]=="")?-1:$request->caution[$i];
              $game_player->caution_c1=($request->caution_c1[$i]=="")?-1:$request->caution_c1[$i];
              $game_player->gametime_seconds=($request->gametime_seconds[$i]=="")?-1:$request->gametime_seconds[$i];
              $game_player->save();
          }
          else
          {
              $game_id=$request->game_id;
              $player_id=$request->player_id[$i];
              $takedown_t2= ($request->takedown_t2[$i]=="")?-1:$request->takedown_t2[$i];
              $nearfall_n2=($request->nearfall_n2[$i]=="")?-1:$request->nearfall_n2[$i];
              $nearfall_n3=($request->nearfall_n3[$i]=="")?-1:$request->nearfall_n3[$i];
              $reversal_r2=($request->reversal_r2[$i]=="")?-1:$request->reversal_r2[$i];
              $escape_e1=($request->escape_e1[$i]=="")?-1:$request->escape_e1[$i];
              $penalty_p1=($request->penalty_p1[$i]=="")?-1:$request->penalty_p1[$i];
              $penalty_p2=($request->penalty_p2[$i]=="")?-1:$request->penalty_p2[$i];
              $stalling=($request->stalling[$i]=="")?-1:$request->stalling [$i];
              $stalling_w2=($request->stalling_w2[$i]=="")?-1:$request->stalling_w2[$i];
              $caution=($request->caution[$i]=="")?-1:$request->caution[$i];
              $caution_c1=($request->caution_c1[$i]=="")?-1:$request->caution_c1[$i];
              $gametime_seconds=($request->gametime_seconds[$i]=="")?-1:$request->gametime_seconds[$i];
              DB::table('ju_game_player')->where('game_id',$game_id)->update(array('player_id'=>$player_id,'takedown_t2'=>$takedown_t2,'nearfall_n2'=>$nearfall_n2,'nearfall_n3'=>$nearfall_n3,'reversal_r2'=>$reversal_r2,'escape_e1'=>$escape_e1,'penalty_p1'=>$penalty_p1,'penalty_p2'=>$penalty_p2,'stalling'=>$stalling,'stalling_w2'=>$stalling_w2,'caution'=>$caution,'caution_c1'=>$caution_c1,'gametime_seconds'=>$gametime_seconds));
              
          }
        }
      }
      if($request->mat_chair!=""){
        $matchairman = DB::table('ju_game_official')->where(array('game_id' =>$game_id,'official_type'=>'matchairman'))->get()->toArray();
        if(empty($matchairman))
        {
          $mat_chair=new GameOfficial;
          $mat_chair->game_id=$request->game_id;
          $mat_chair->official_name=$request->mat_chair;
          $mat_chair->official_type='matchairman';
          $mat_chair->save();
        }
        else
        {
          DB::table('ju_game_official')->where(array('game_id' =>$game_id,'official_type'=>'matchairman'))->update(array('official_name'=>$request->mat_chair));
        }
      }
      else
      {
        DB::table('ju_game_official')->where(array('game_id' =>$game_id,'official_type'=>'matchairman'))->delete();
      }
        if($request->referee!=""){
        $Gamereferee = DB::table('ju_game_official')->where(array('game_id' =>$game_id,'official_type'=>'referee'))->get()->toArray();
        if(empty($Gamereferee))
        {
          $referee=new GameOfficial;
          $referee->game_id=$request->game_id;
          $referee->official_name=$request->referee;
          $referee->official_type='referee';
          $referee->save();
        }
        else
        {
          DB::table('ju_game_official')->where(array('game_id' =>$game_id,'official_type'=>'referee'))->update(array('official_name'=>$request->referee));
        }
      }

      else
      {
        DB::table('ju_game_official')->where(array('game_id' =>$game_id,'official_type'=>'referee'))->delete();
      }

    if($request->judge!=""){
      $Gamejudge = DB::table('ju_game_official')->where(array('game_id' =>$game_id,'official_type'=>'judge'))->get()->toArray();
        if(empty($Gamejudge))
        {
          $judge=new GameOfficial;
          $judge->game_id=$request->game_id;
          $judge->official_name=$request->judge;
          $judge->official_type='judge';
          $judge->save();
        }
        else
        {
          DB::table('ju_game_official')->where(array('game_id' =>$game_id,'official_type'=>'judge'))->update(array('official_name'=>$request->judge));
        }
      }

      else
      {
        DB::table('ju_game_official')->where(array('game_id' =>$game_id,'official_type'=>'judge'))->delete();
      }
      if($request->coach_1!=""){
        $coach_1 = DB::table('ju_game_team_official')->where(array('game_id' =>$game_id,'player_id'=>$request->player_id[0],'official_type'=>'coach'))->get()->toArray();
        if(empty($coach_1))
        {
          $coach_1=new GameTeamOfficial;
          $coach_1->game_id=$request->game_id;
          $coach_1->player_id=$request->player_id[0];
          $coach_1->official_name=$request->coach_1;
          $coach_1->official_type='coach';
           $coach_1->save();
         }
         else
         {
          DB::table('ju_game_team_official')->where(array('game_id' =>$game_id,'player_id'=>$request->player_id[0],'official_type'=>'coach'))->update(array('official_name'=>$request->coach_1));
         }
      }
      else
      {
        DB::table('ju_game_team_official')->where(array('game_id' =>$game_id,'player_id'=>$request->player_id[0],'official_type'=>'coach'))->delete();
      }
      if($request->coach_2!=""){
         $coach_1 = DB::table('ju_game_team_official')->where(array('game_id' =>$game_id,'player_id'=>$request->player_id[1],'official_type'=>'coach'))->get()->toArray();
        if(empty($coach_1))
        {
          $coach_2=new GameTeamOfficial;
          $coach_2->game_id=$request->game_id;
          $coach_2->player_id=$request->player_id[1];
          $coach_2->official_name=$request->coach_2;
          $coach_2->official_type='coach';
          $coach_2->save();
        }
        else
        {
          DB::table('ju_game_team_official')->where(array('game_id' =>$game_id,'player_id'=>$request->player_id[1],'official_type'=>'coach'))->update(array('official_name'=>$request->coach_2));
        }
      }
      else
      {
        DB::table('ju_game_team_official')->where(array('game_id' =>$game_id,'player_id'=>$request->player_id[1],'official_type'=>'coach'))->delete();
      }
      if($request->player1_score > $request->player2_score){
        $game_winner='1'; 
      }elseif($request->player1_score < $request->player2_score){
        $game_winner='2'; 
      }else{
        $game_winner='0';
      }
      $game=Game::where('game_id',$request->game_id)->update(array('score1'=> $request->player1_score,'score2'=>$request->player2_score,'status'=>'full','winner'=>$game_winner));
     }//end of validation else
      $event=Event::pluck('name','event_id');
      $event->prepend('Select Event','');
      $game=array('Select Game','');
      // Session::flash('message', 'Scorecard Uploaded Successfully!');
      // Session::flash('status', 'success');

      if($error_txt==""){
        return 1;
      }else
      {
        return $error_txt;
      }
      return view('backEnd.admin.scorecard.ju_editscorecard',compact('event','game'));
          
    }
}
