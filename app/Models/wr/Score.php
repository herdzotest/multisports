<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Score extends Model
{
    protected $table = 'skillparameter_type';
    protected $fillable = ['type'];
    protected $primaryKey = 'type_id';
    public $timestamps = false;


    public function player()
    {
        return $this->hasOne('App\Player', 'player_id','player_id');
    }
    }
}
