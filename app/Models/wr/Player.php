<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'player';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['full_name','game_name','sort_name','known_as_name','date_of_birth','place_of_birth','date_of_death','place_of_death','gender','country_id','height','weight','known_as','notes','player_profile','sport_id','father_name','mother_name','address','regno'];


    protected $primaryKey = 'player_id';
    public $timestamps = false;

    public function country()
    {
        return $this->hasOne('App\Country', 'country_id','country_id');
    } 

    // public function picture_id()
    // {
    //     return $this->hasOne('App\PicturePlayer', 'player_id','player_id');
    // } 

    public function gamesplayed()
    {
        $this->table_prefix =   session()->get('table_prefix');

        return $this->hasMany('App\Model\\'.$this->table_prefix.'\GamePlayer', 'player_id','player_id')->select("game_id");
		
		
    }
	 
    public function gameofficial()
    {
        return $this->hasMany('App\Model\bb\GameOfficial', 'official_id','player_id')->select("game_id");
    } 
    public function gameteamofficial()
    {

        return $this->hasMany('App\Model\bb\GameTeamOfficial', 'official_id','player_id')->select("game_id");
    }  
    public function player_meta_data()
    {

        return $this->hasMany('App\PlayerMetaData', 'player_id','player_id');
    }         
}
