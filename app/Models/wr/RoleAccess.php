<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleAccess extends Model
{
    protected $table = 'role_has_access';
	public $timestamps = false;
    protected $fillable = [
        'role_id','sport_id'];
    protected $primaryKey = 'role_access_id';	
}
