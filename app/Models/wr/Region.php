<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'regions';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['country_id','name'];

    protected $primaryKey = 'region_id';

    public function country()
    {
        return $this->hasOne('App\Country', 'country_id','country_id');
    } 

}
