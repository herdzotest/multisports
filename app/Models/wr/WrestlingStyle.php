<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class WrestlingStyle extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wr_styles';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['style_name','style_cd'];
    protected $primaryKey = 'style_id';
    public $timestamps = false;
}
