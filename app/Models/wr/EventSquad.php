<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class EventSquad extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wr_event_squad';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['player_id','event_id'];
    protected $primaryKey = 'event_squad_id';
    public $timestamps = false;

    public function event()
    {
        return $this->hasOne('App\Event', 'event_id', 'event_id');
    }
     public function player()
    {
        return $this->hasOne('App\Player', 'player_id', 'player_id');
    }     
    public function team()
    {
        return $this->hasOne('App\Team', 'team_id', 'team_id');
        // return $this->hasOne('App\Model\wr\Team', 'team_id', 'team_id');
    }    
    public function wrstyle()
    {
        return $this->hasOne('App\WrestlingStyle', 'style_id', 'wr_style');
    }    
    public function wtcat()
    {
        return $this->hasOne('App\WeightCategory', 'weight_category_id', 'weight_cat');
    }



}