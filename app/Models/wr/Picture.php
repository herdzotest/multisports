<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'picture';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['picture_id','picture_owner_id','caption','copyright_year'];
    protected $primaryKey = 'picture_id';

}
