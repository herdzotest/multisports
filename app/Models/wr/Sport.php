<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sport extends Model
{
    protected $table = 'sport';
	public $timestamps = false;
    protected $fillable = [
        'sport_name'];
    protected $primaryKey = 'sport_id';	
}
