<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Official extends Model
{
    protected $table = 'official';
	public $timestamps = false;
    protected $fillable = [
        'sport_id','full_name','game_name','sort_name','date_of_birth','place_of_birth','date_of_death','place_of_death','gender','country_id','profile','notes'];
    protected $primaryKey = 'official_id';	
}
