<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerMetaData extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'player_adtnl_meta_data';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['player_id','meta_key','meta_data'];


    protected $primaryKey = 'player_adtnl_meta_data_id';
    public $timestamps = false;

     
}
