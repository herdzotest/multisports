<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarEvent extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'calendar_events';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title','sport_id','start_date','end_date','venue','description','url','category'];

    protected $primaryKey = 'id';

    

}
