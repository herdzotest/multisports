<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wr_event';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','season','country_id','start_date'];
    protected $primaryKey = 'event_id';
    public $timestamps = false;

    public function country()
    {
        return $this->hasOne('App\Country', 'country_id', 'country_id');
    }

    public function picture()
    {
        return $this->hasOne('App\PictureEvent', 'event_id', 'event_id');
    }

}
