<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Featured extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'featured';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title','title_id'];
    protected $primaryKey = 'ft_id';
    public $timestamps = false;


}
