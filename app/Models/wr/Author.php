<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'author';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','email'];

    protected $primaryKey = 'author_id';

}
