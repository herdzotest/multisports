<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueSport extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'venue_sport';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['venue_id','sport_id'];

    protected $primaryKey = 'venue_sport_id';
    public $timestamps = false;


}
