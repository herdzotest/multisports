<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievements extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'achievements';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['player_id','sport_id','year','award_id'];
    protected $primaryKey = 'ac_id';

    public function event()
    {
        return $this->hasOne('App\Player', 'player_id', 'player_id');
    }
    public function sport()
    {
        return $this->hasOne('App\Sport', 'sport_id', 'sport_id');
    }
    public function award()
    {
        return $this->hasOne('App\Award', 'award_id', 'award_id');
    }
}
