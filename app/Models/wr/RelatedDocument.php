<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelatedDocument extends Model
{
    protected $table = 'related_documents';
	public $timestamps = false;
    protected $fillable = [
       'related_document_id','document_number','related_document_num'];
    protected $primaryKey = 'related_document_id';
   
}
