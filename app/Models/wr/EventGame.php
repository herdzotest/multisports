<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventGame extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wr_event_game';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['event_id','game_id'];
    protected $primaryKey = 'event_game_id';

    public function event()
    {
        return $this->hasOne('App\Event', 'event_id', 'event_id');
    }
}
