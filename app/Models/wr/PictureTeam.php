<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PictureTeam extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'picture_team';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['picture_id','team_id'];
    protected $primaryKey = 'picture_team_id';

}
