<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'documents';
	public $timestamps = false;
    protected $fillable = [
       'category_id','subcategory_id','department','document_number','issued_date','issued_by','subject','abstract','file_number','file_location'];
    protected $primaryKey = 'document_id';
    public function governing_body()
    {
        return $this->belongsTo('App\GoverningBody', 'issued_by', 'governing_body_id');
    }    
    public function issued_to_gb()
    {
        return $this->belongsTo('App\GoverningBody', 'issued_to', 'governing_body_id');
    }
    public function document_category()
    {
    	return $this->belongsTo('App\DocumentCategory', 'category_id', 'category_id');
    }
    public function document_subcategory()
    {
    	return $this->belongsTo('App\DocumentSubCategory', 'subcategory_id', 'subcategory_id');
    }

     public function sport()
    {
        return $this->belongsTo('App\Sport', 'sport_id', 'sport_id');
    }
    public function subcat()
    {
        return $this->belongsTo('App\DocumentSubCategory', 'subcategory_id');
    }
}
