<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Team extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wr_team';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','country_id','region','sub_region_id'];

    protected $primaryKey = 'team_id';
    public $timestamps = false;

    public function country()
    {
        return $this->hasOne('App\Country', 'country_id','country_id');
    }
    public function picture()
    {
        return $this->hasOne('App\PictureTeam', 'team_id','team_id');
    }
    public function regionobj()
    {
        return $this->hasOne('App\Region', 'region_id','region');
    }
    public function subregion()
    {
        return $this->hasOne('App\SubRegion', 'sub_region_id','sub_region_id');
    }    

}
