<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelRoles extends Model
{
    protected $table = 'model_has_roles';
	public $timestamps = false;
    // protected $fillable = [
    //     'role_id','sport_id'];
    protected $primaryKey = 'role_id';	
}
