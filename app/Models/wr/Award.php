<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Award extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'award';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['award_id','award_name'];
    protected $primaryKey = 'award_id';

    

}
