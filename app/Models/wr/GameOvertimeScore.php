<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GameOvertimeScore extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'game_overtime_score';
   public $timestamps = false;
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['game_id','score1','score2'];

    protected $primaryKey = 'game_overtime_score_id';

}
