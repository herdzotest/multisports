<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PictureVenue extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'picture_venue';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['picture_id','venue_id','created_at','updated_at'];
    protected $primaryKey = 'picture_venue_id';

}
