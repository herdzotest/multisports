<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSquad extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ju_event_squad';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['event_id','player_id','weight','position'];
	
    protected $primaryKey = 'event_squad_id';
}
