<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubRegion extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sub_regions';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['region_id','name'];
    protected $primaryKey = 'sub_region_id';

    public function region()
    {
        return $this->hasOne('App\Region', 'region_id','region_id');
    } 
    

}
