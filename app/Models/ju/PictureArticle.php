<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class PictureArticle extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'picture_article';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['picture_id','article_id'];
    protected $primaryKey = 'picture_article_id';

    public function picturedetail()
    {
        return $this->hasOne('App\Gallery', 'picture_id', 'picture_id');
    }

}
