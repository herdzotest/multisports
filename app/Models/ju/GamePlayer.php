<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GamePlayer extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'game_player';
        public $timestamps = false;
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['game_id','team_id','player_id','fg3_made','fg3_attempts','fg_made','fg_attempts','ft_made','ft_attempts','points','rebounds','fouls','gametime_seconds','jersey_no','off_rebounds','def_rebounds','assists','steals','blocks','turnovers','captain','starter'];

    protected $primaryKey = 'game_player_id';


    public function player()
    {
        return $this->hasOne('App\Player', 'player_id', 'player_id');
    }
        public function gameofficial()
    {
        return $this->hasMany('App\GameOfficial', 'game_id', 'game_id');
    }
}
