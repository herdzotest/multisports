<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'article';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title','description','author','owner','category','branding','copyright_year','event','scorecards','players','grounds','links','profile','profile_of','added_by'];

    protected $primaryKey = 'article_id';

    public function country()
    {
        return $this->hasOne('App\Country', 'country_id', 'country_id');
    }

    public function getauthor()
    {
        return $this->hasOne('App\Author', 'author_id', 'author');
    }

    public function picture()
    {
        return $this->hasOne('App\PictureArticle', 'article_id', 'article_id');
    }

}
