<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentCategory extends Model
{
    protected $table = 'document_category';
	public $timestamps = false;
    protected $fillable = [
       'category_name'];
    protected $primaryKey = 'category_id';
    // public function governing_body()
    // {
    //     return $this->belongsTo('App\GoverningBody', 'governing_body_id', 'governing_body_id');
    // }

    public function sport()
    {
        return $this->belongsTo('App\Sport', 'sport_id', 'sport_id');
    }
}
