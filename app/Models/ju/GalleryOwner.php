<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryOwner extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'picture_owner';
    public $timestamps = false;
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','owner_website','copyright_message'];

    protected $primaryKey = 'picture_owner_id';

}
