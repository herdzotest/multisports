<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    protected $table = 'role_has_permissions';
	public $timestamps = false;
    protected $fillable = [
        'role_id','permission_id'];
    // protected $primaryKey = 'role_access_id';	
}
