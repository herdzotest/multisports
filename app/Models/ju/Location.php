<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'location';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['sub_region_id','name'];
    protected $primaryKey = 'location_id';

    public function sub_region()
    {
        return $this->hasOne('App\SubRegion', 'sub_region_id','sub_region_id');
    } 
    

}
