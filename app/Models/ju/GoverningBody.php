<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoverningBody extends Model
{
    protected $table = 'governing_body';
	public $timestamps = false;
    protected $fillable = [
        'name','address','contact'];
    protected $primaryKey = 'governing_body_id';

    
   public function sport()
    {
        return $this->belongsTo('App\Sport', 'sport_id', 'sport_id');
    }
}
