<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleBranding extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'article_branding';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    protected $primaryKey = 'article_branding_id';

}
