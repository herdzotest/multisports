<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PicturePlayer extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'picture_player';
    public $timestamps = false;
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['picture_id','player_id'];

    protected $primaryKey = 'picture_player_id';

}
