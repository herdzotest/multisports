<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentSubCategory extends Model
{
    protected $table = 'document_subcategory';
	public $timestamps = false;
    protected $fillable = [
       'subcategory_name'];
    protected $primaryKey = 'subcategory_id';
    // public function governing_body()
    // {
    //     return $this->belongsTo('App\GoverningBody', 'governing_body_id', 'governing_body_id');
    // }
    public function document_category()
    {
    	return $this->belongsTo('App\DocumentCategory','category_id','category_id');
    }
     public function sport()
    {
        return $this->belongsTo('App\Sport', 'sport_id', 'sport_id');
    }
}
