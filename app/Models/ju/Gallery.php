<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'picture';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['picture_owner_id','caption','copyright_year'];

    protected $primaryKey = 'picture_id';

    public function owner()
    {
        return $this->hasOne('App\GalleryOwner', 'picture_owner_id','picture_owner_id');
    }

}
