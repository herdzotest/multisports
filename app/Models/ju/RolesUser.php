<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolesUser extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles_user';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['roles_id','user_id','created_at','updated_at'];

    protected $primaryKey = 'roles_id';

}
