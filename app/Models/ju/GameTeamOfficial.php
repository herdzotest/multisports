<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GameTeamOfficial extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'game_team_official';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['game_id','team_id','player_id','official_type'];

    protected $primaryKey = 'game_team_official_id';

    public function player()
    {
        return $this->hasOne('App\Player', 'player_id', 'player_id');
    }
}
