<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PictureEvent extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'picture_event';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['picture_id','event_id','created_at','updated_at'];

    protected $primaryKey = 'picture_event_id';

}
