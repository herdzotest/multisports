<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'venue';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','country_id','address','region','sub_region','venue_type','floodlight','surface','capacity','lat','lng','location_url'];

    protected $primaryKey = 'venue_id';
    public $timestamps = false;

    public function country()
    {
        return $this->hasOne('App\Country', 'country_id','country_id');
    }

    public function picture()
    {
        return $this->hasOne('App\PictureVenue', 'venue_id', 'venue_id');
    }
    public function regionobj()
    {
        return $this->hasOne('App\Region', 'region_id','region');
    }
    public function subregion()
    {
        return $this->hasOne('App\SubRegion', 'sub_region_id','sub_region');
    } 
    public function venuesport()
    {
         return $this->hasmany('App\VenueSport', 'venue_id','venue_id');
    }

}
