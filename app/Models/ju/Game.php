<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'game';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['team1_id','team2_id','venue_id','venue_type','game_date','score1','score2','overtime','status','winner','phase','gmt_start_time','local_start_time'];

    protected $primaryKey = 'game_id';
    public $timestamps = false;

    public function team1()
    {
        return $this->hasOne('App\Team', 'team_id','team1_id');
    }

    public function team2()
    {
        return $this->hasOne('App\Team', 'team_id','team2_id');
    }

    public function venue()
    {
        return $this->hasOne('App\Venue', 'venue_id', 'venue_id');
    }
    public function eventid()
    {
        return $this->hasOne('App\EventGame', 'game_id', 'game_id');
    }
}
