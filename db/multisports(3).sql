-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 24, 2021 at 08:59 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `multisports`
--

-- --------------------------------------------------------

--
-- Table structure for table `award`
--

CREATE TABLE `award` (
  `award_id` int(11) NOT NULL,
  `award_name` varchar(50) NOT NULL,
  `award_name_slug` varchar(100) NOT NULL,
  `category` varchar(50) NOT NULL,
  `sub_category` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `timezone` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `name`, `timezone`) VALUES
(1, 'India', ''),
(2, 'Maldives', ''),
(3, 'Nepal', ''),
(4, 'Bangladesh', ''),
(5, 'Iran', ''),
(6, 'China', ''),
(7, 'Chinese Taipei', ''),
(8, 'Indonesia', ''),
(9, 'Iraq', ''),
(10, 'Japan', ''),
(11, 'Kazakhstan', ''),
(13, 'Lebanon', ''),
(14, 'Philippines', ''),
(15, 'Thailand', ''),
(17, 'Taiwan', ''),
(18, 'United Arab Emirates', ''),
(19, 'Qatar', ''),
(20, 'Malaysia', ''),
(21, 'Jordan', ''),
(22, 'Bahrain', ''),
(23, 'Democratic People\'s Republic of Korea', ''),
(24, 'Hong Kong', ''),
(25, 'Kuwait', ''),
(26, 'Singapore', ''),
(27, 'Sri Lanka', ''),
(28, 'United States of America', ''),
(29, 'New Zealand', ''),
(30, 'Lithuania', ''),
(31, 'Canada', ''),
(32, 'Australia', ''),
(33, 'Uzbekistan', ''),
(34, 'Fiji', ''),
(35, 'Pakistan', ''),
(36, 'Syrian Arab Republic', ''),
(37, 'Bhutan', ''),
(39, 'Macau', ''),
(40, 'England', ''),
(41, 'Scotland', ''),
(42, 'Cameroon', ''),
(43, 'Nigeria', ''),
(44, 'Jamaica', ''),
(45, 'Mozambique', ''),
(46, 'Germany', ''),
(47, 'Samoa', ''),
(48, 'Guam', ''),
(49, 'Estonia', ''),
(50, 'France', ''),
(51, 'Chile', ''),
(52, 'Belgium', ''),
(53, 'Turkey', ''),
(54, 'Switzerland', ''),
(55, 'Italy', ''),
(56, 'Poland', ''),
(57, 'Egypt', ''),
(58, 'Peru', ''),
(59, 'Brazil', ''),
(60, 'Uruguay', ''),
(61, 'Mexico', ''),
(62, 'Spain', ''),
(63, 'Hungary', ''),
(64, 'Latvia', ''),
(65, 'Czechoslovakia', ''),
(66, 'Mongolia', ''),
(67, 'Khazakhstan', ''),
(68, 'Korea (United)', ''),
(69, 'Kenya', ''),
(70, 'Angola', ''),
(71, 'Democratic Republic of the Congo', ''),
(72, 'Zimbabwe', ''),
(73, 'Madagascar', ''),
(75, 'Papua New Guinea', ''),
(76, 'Botswana', ''),
(77, 'South Africa', ''),
(78, 'Ecuador', ''),
(79, 'Colombia', ''),
(80, 'Paraguay', ''),
(81, 'Argentina', ''),
(82, 'Bolivia', ''),
(83, 'Puerto Rico', ''),
(84, 'Panama', ''),
(85, 'Venezuela', ''),
(86, 'Yugoslavia', ''),
(87, 'Not Known', NULL),
(88, 'Finland', NULL),
(89, 'Denmark', NULL),
(90, 'Soviet Union', NULL),
(91, 'Bulgaria', NULL),
(92, 'Sweden', NULL),
(93, 'Great Britain', NULL),
(94, 'Afghanistan', NULL),
(95, 'South Korea', NULL),
(96, 'South Vietnam', NULL),
(97, 'North Korea', NULL),
(98, 'Turkmenistan', NULL),
(99, 'Burma', NULL),
(100, 'Oman', NULL),
(101, 'Algeria', NULL),
(102, 'Morocco', NULL),
(103, 'Romania', NULL),
(104, 'East Germany', NULL),
(105, 'Zambia', NULL),
(106, 'Russia', NULL),
(107, 'Ghana', NULL),
(108, 'Cambodia', NULL),
(109, 'Kyrgyzstan', NULL),
(111, 'Tajikistan', NULL),
(112, 'Asia', NULL),
(113, 'Greece', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ju_age_category`
--

CREATE TABLE `ju_age_category` (
  `age_category_id` int(10) NOT NULL,
  `category_name` varchar(50) DEFAULT NULL,
  `category_desc` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ju_event`
--

CREATE TABLE `ju_event` (
  `event_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `generic_name` varchar(250) DEFAULT NULL,
  `season` varchar(10) NOT NULL,
  `country_id` smallint(5) UNSIGNED NOT NULL,
  `start_date` date DEFAULT '0000-00-00',
  `region` int(10) DEFAULT NULL,
  `sub_region_id` int(10) DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `category` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ju_event_game`
--

CREATE TABLE `ju_event_game` (
  `event_game_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `game_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ju_event_official`
--

CREATE TABLE `ju_event_official` (
  `event_official_id` int(11) NOT NULL,
  `sport_id` int(10) UNSIGNED NOT NULL,
  `team_id` int(10) UNSIGNED NOT NULL,
  `official_id` int(10) UNSIGNED NOT NULL,
  `post` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ju_event_squad`
--

CREATE TABLE `ju_event_squad` (
  `event_squad_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `player_id` int(10) UNSIGNED NOT NULL,
  `player_number` varchar(20) DEFAULT NULL,
  `team_id` int(10) UNSIGNED DEFAULT NULL,
  `ju_style` int(10) UNSIGNED DEFAULT NULL,
  `weight_cat` int(10) UNSIGNED DEFAULT NULL,
  `weight` decimal(10,3) UNSIGNED NOT NULL,
  `position` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ju_event_team_squad`
--

CREATE TABLE `ju_event_team_squad` (
  `event_team_squad_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `player_number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ju_game`
--

CREATE TABLE `ju_game` (
  `game_id` int(10) UNSIGNED NOT NULL,
  `player1_id` int(10) UNSIGNED NOT NULL,
  `player2_id` int(10) UNSIGNED NOT NULL,
  `team1_id` int(10) UNSIGNED DEFAULT NULL,
  `team2_id` int(10) UNSIGNED DEFAULT NULL,
  `venue_id` int(10) UNSIGNED NOT NULL,
  `venue_type` enum('homeaway','neutral') NOT NULL DEFAULT 'homeaway',
  `game_date` date NOT NULL,
  `score1` smallint(5) NOT NULL,
  `score2` smallint(5) NOT NULL,
  `overtime` enum('n','y') NOT NULL DEFAULT 'n',
  `status` enum('fixture','summary','full') NOT NULL DEFAULT 'fixture',
  `winner` enum('0','1','2') NOT NULL DEFAULT '0',
  `phase` varchar(255) DEFAULT '',
  `gmt_start_time` time DEFAULT NULL,
  `local_start_time` time DEFAULT NULL,
  `sequence` smallint(1) DEFAULT NULL,
  `season` varchar(10) DEFAULT NULL,
  `country_id` smallint(5) UNSIGNED DEFAULT NULL,
  `points_awarded` enum('y','n','','') DEFAULT NULL,
  `game_style` int(10) DEFAULT NULL,
  `age_category` int(10) DEFAULT NULL,
  `weight_category` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ju_game`
--

INSERT INTO `ju_game` (`game_id`, `player1_id`, `player2_id`, `team1_id`, `team2_id`, `venue_id`, `venue_type`, `game_date`, `score1`, `score2`, `overtime`, `status`, `winner`, `phase`, `gmt_start_time`, `local_start_time`, `sequence`, `season`, `country_id`, `points_awarded`, `game_style`, `age_category`, `weight_category`) VALUES
(880, 8522, 8523, NULL, NULL, 363, 'homeaway', '2021-02-26', 0, 0, 'n', 'fixture', '0', 'lorem ipsum', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ju_game_official`
--

CREATE TABLE `ju_game_official` (
  `game_official_id` int(10) UNSIGNED NOT NULL,
  `official_name` varchar(50) DEFAULT NULL,
  `game_id` int(10) UNSIGNED NOT NULL,
  `reference_player_id` int(10) UNSIGNED DEFAULT NULL,
  `official_type` enum('referee','timekeeper','contest_recorder','senior_recorder','competition_recorder') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ju_game_official`
--

INSERT INTO `ju_game_official` (`game_official_id`, `official_name`, `game_id`, `reference_player_id`, `official_type`) VALUES
(1, 'asdasd', 880, 8522, 'referee'),
(2, 'asdasd', 880, 8522, 'timekeeper'),
(3, 'asdasdasd', 880, 8522, 'contest_recorder'),
(4, 'asdasdasd', 880, 8522, 'senior_recorder'),
(5, 'asdsadasd', 880, 8522, 'competition_recorder');

-- --------------------------------------------------------

--
-- Table structure for table `ju_game_player`
--

CREATE TABLE `ju_game_player` (
  `ju_game_player_id` int(10) UNSIGNED NOT NULL,
  `game_id` int(10) UNSIGNED NOT NULL,
  `team_id` int(10) UNSIGNED DEFAULT NULL,
  `player_id` int(10) UNSIGNED NOT NULL,
  `gametime_seconds` smallint(6) DEFAULT -1,
  `ippon` int(11) NOT NULL,
  `wazari` int(11) NOT NULL,
  `penalty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ju_game_player`
--

INSERT INTO `ju_game_player` (`ju_game_player_id`, `game_id`, `team_id`, `player_id`, `gametime_seconds`, `ippon`, `wazari`, `penalty`) VALUES
(1221, 880, NULL, 8522, -1, 2, 2, 1),
(1222, 880, NULL, 8523, -1, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ju_game_team_official`
--

CREATE TABLE `ju_game_team_official` (
  `game_team_official_id` int(11) NOT NULL,
  `official_name` varchar(50) DEFAULT NULL,
  `game_id` int(10) UNSIGNED NOT NULL,
  `player_id` int(10) UNSIGNED NOT NULL,
  `official_type` enum('coach','assistantcoach') NOT NULL,
  `reference_player_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ju_game_team_official`
--

INSERT INTO `ju_game_team_official` (`game_team_official_id`, `official_name`, `game_id`, `player_id`, `official_type`, `reference_player_id`) VALUES
(1, 'asdasdasd', 880, 8522, 'coach', NULL),
(2, 'sdasda', 880, 8522, 'assistantcoach', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ju_official_team`
--

CREATE TABLE `ju_official_team` (
  `official_team_id` int(11) NOT NULL,
  `official_id` int(11) NOT NULL,
  `post` varchar(30) NOT NULL,
  `team_id` int(11) NOT NULL,
  `start_season` varchar(30) DEFAULT NULL,
  `end_season` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ju_picture_event`
--

CREATE TABLE `ju_picture_event` (
  `ju_picture_event_id` int(10) UNSIGNED NOT NULL,
  `picture_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ju_picture_player`
--

CREATE TABLE `ju_picture_player` (
  `ju_picture_player_id` int(10) UNSIGNED NOT NULL,
  `picture_id` int(10) UNSIGNED NOT NULL,
  `player_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ju_picture_team`
--

CREATE TABLE `ju_picture_team` (
  `ju_picture_team_id` int(10) UNSIGNED NOT NULL,
  `picture_id` int(10) UNSIGNED NOT NULL,
  `team_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ju_picture_venue`
--

CREATE TABLE `ju_picture_venue` (
  `ju_picture_venue_id` int(10) UNSIGNED NOT NULL,
  `picture_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ju_playerteam`
--

CREATE TABLE `ju_playerteam` (
  `playerteam_id` int(11) NOT NULL,
  `player_id` varchar(200) NOT NULL,
  `team_id` varchar(200) NOT NULL,
  `start_season` varchar(200) NOT NULL,
  `end_season` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ju_styles`
--

CREATE TABLE `ju_styles` (
  `style_id` int(10) NOT NULL,
  `style_name` varchar(50) DEFAULT NULL,
  `style_cd` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ju_team`
--

CREATE TABLE `ju_team` (
  `team_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `country_id` smallint(5) UNSIGNED NOT NULL,
  `region` varchar(255) DEFAULT '',
  `sub_region_id` int(11) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `related_team` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ju_videos`
--

CREATE TABLE `ju_videos` (
  `game_player_id` int(225) NOT NULL,
  `video_id_prim` int(10) NOT NULL,
  `video_cat` enum('pts','fg','fg2','ft') DEFAULT NULL,
  `game_id` int(225) NOT NULL,
  `team_id` int(225) NOT NULL,
  `player_id` int(225) NOT NULL,
  `video_path` varchar(500) NOT NULL,
  `player_sequence` int(11) NOT NULL,
  `team_sequence` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ju_weight_category`
--

CREATE TABLE `ju_weight_category` (
  `weight_category_id` int(11) NOT NULL,
  `category_desc` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE `player` (
  `player_id` int(10) UNSIGNED NOT NULL,
  `sport_id` int(10) UNSIGNED NOT NULL,
  `full_name` text NOT NULL,
  `game_name` text NOT NULL,
  `sort_name` text NOT NULL,
  `known_as_name` text DEFAULT NULL,
  `date_of_birth` date DEFAULT '0000-00-00',
  `place_of_birth` text DEFAULT NULL,
  `date_of_death` date DEFAULT '0000-00-00',
  `place_of_death` text DEFAULT NULL,
  `gender` enum('m','f') NOT NULL DEFAULT 'm',
  `country_id` smallint(5) UNSIGNED NOT NULL,
  `height` smallint(5) UNSIGNED DEFAULT 0,
  `weight` smallint(5) DEFAULT NULL,
  `notes` varchar(1500) DEFAULT NULL,
  `player_profile` text DEFAULT NULL,
  `district` int(10) DEFAULT NULL,
  `father_name` varchar(100) DEFAULT NULL,
  `mother_name` varchar(100) DEFAULT NULL,
  `season` varchar(50) DEFAULT NULL,
  `team` int(10) DEFAULT NULL,
  `address` varchar(2000) DEFAULT NULL,
  `regno` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `player`
--

INSERT INTO `player` (`player_id`, `sport_id`, `full_name`, `game_name`, `sort_name`, `known_as_name`, `date_of_birth`, `place_of_birth`, `date_of_death`, `place_of_death`, `gender`, `country_id`, `height`, `weight`, `notes`, `player_profile`, `district`, `father_name`, `mother_name`, `season`, `team`, `address`, `regno`) VALUES
(8522, 24, 'Mohammed Iqbal Khan', 'KHAN', 'KHAN', 'KHAN', '2020-01-01', 'Thiruvananthapuram', '0000-00-00', NULL, 'm', 1, 169, 75, NULL, NULL, 1, 'KHAN', 'Mrs Khan', '2019/20', 1, NULL, NULL),
(8523, 24, 'Mohammed Iqbal Khan II', 'KHAN II', 'KHAN II', 'KHAN II', '2020-01-01', 'Thiruvananthapuram', '0000-00-00', NULL, 'm', 1, 169, 75, NULL, NULL, 1, 'KHAN', 'Mrs Khan', '2019/20', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `region_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`region_id`, `name`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'Kerala', 1, '2019-12-09 09:44:52', '2019-12-09 09:44:52'),
(2, 'Chandigarh', 1, '2017-06-07 17:46:48', '2017-06-07 17:46:48'),
(3, 'Bangkok', 15, '2017-06-08 06:10:48', '2017-06-08 06:10:48'),
(5, 'Tamil Nadu', 1, '2017-06-08 06:12:50', '2017-06-08 06:12:50'),
(6, 'Gujarat', 1, '2017-06-08 06:13:23', '2017-06-08 06:13:23'),
(7, 'Jakarta', 8, '2017-06-08 06:13:42', '2017-06-08 06:13:42'),
(8, 'Karnataka', 1, '2017-06-08 06:13:55', '2017-06-08 06:13:55'),
(9, 'Maharashtra', 1, '2017-06-08 06:14:38', '2017-07-09 23:50:52'),
(10, 'Tehran', 5, '2017-06-08 06:14:57', '2017-06-08 06:14:57'),
(11, 'Telangana', 1, '2017-06-08 06:15:09', '2017-06-08 06:15:09'),
(12, 'Uttar Pradesh', 1, '2017-06-08 06:15:21', '2017-06-08 06:15:21'),
(14, 'Gujarat', 1, '2017-06-13 20:22:12', '2017-06-13 20:22:12'),
(16, 'Hunan', 6, '2017-06-13 20:49:05', '2017-06-13 20:49:05'),
(17, 'Puducherry', 1, '2017-07-09 21:11:55', '2017-07-09 21:11:55'),
(18, 'Haryana', 1, '2017-07-09 22:32:44', '2017-07-09 22:32:44'),
(19, 'West Bengal', 1, '2017-07-09 22:51:19', '2017-07-09 22:51:19'),
(20, 'Chhattisgarh', 1, '2017-07-09 23:04:17', '2017-07-09 23:04:17'),
(21, 'Madhya Pradesh', 1, '2017-07-09 23:10:03', '2017-07-09 23:10:03'),
(22, 'Delhi', 1, '2017-07-09 23:10:14', '2017-07-09 23:10:14'),
(23, 'Jammu and Kashmir', 1, '2017-07-09 23:10:26', '2017-07-09 23:10:26'),
(24, 'Odisha', 1, '2017-07-09 23:10:37', '2017-07-09 23:10:37'),
(25, 'Himachal Pradesh', 1, '2017-07-09 23:10:51', '2017-07-09 23:10:51'),
(26, 'Punjab', 1, '2017-07-09 23:11:04', '2017-07-09 23:11:04'),
(27, 'Goa', 1, '2017-07-09 23:11:16', '2017-07-09 23:11:16'),
(28, 'Rajasthan', 1, '2017-07-09 23:11:28', '2017-07-09 23:11:28'),
(29, 'Andhra Pradesh', 1, '2017-07-09 23:11:40', '2017-07-09 23:11:40'),
(30, 'Bihar', 1, '2017-07-09 23:11:50', '2017-07-09 23:11:50'),
(31, 'Taipei', 7, '2017-07-12 15:21:00', '2017-07-12 15:21:00'),
(32, 'Mount Lebanon Governorate', 13, '2017-08-12 06:25:56', '2017-08-12 06:25:56'),
(33, 'Male', 2, '2017-09-04 23:22:46', '2017-09-04 23:22:46'),
(34, 'Kathmandu', 3, '2017-09-05 19:12:08', '2017-09-05 19:12:08'),
(35, 'Victoria', 32, '2017-11-16 00:10:35', '2017-11-16 00:10:35'),
(36, 'Wellington', 29, '2018-01-12 02:06:41', '2018-01-12 02:06:41'),
(37, 'Doha', 19, '2018-01-12 02:56:41', '2018-01-12 02:56:41'),
(38, 'Tokyo', 10, '2018-01-12 03:12:57', '2018-01-12 03:12:57'),
(39, 'Amman', 21, '2018-01-12 03:34:54', '2018-01-12 03:34:54'),
(40, 'Gyeonggi-do', 12, '2018-01-12 03:39:27', '2018-01-12 03:39:27'),
(41, 'Hong Kong', 24, '2018-01-12 03:58:52', '2018-01-12 03:58:52'),
(42, 'South Australia', 32, '2018-01-12 04:14:30', '2018-01-12 04:14:30'),
(43, 'Manila', 14, '2018-01-12 04:31:29', '2018-01-12 04:31:29'),
(44, 'Akmola', 11, '2018-01-12 04:40:16', '2018-01-12 04:40:16'),
(45, 'Kanagawa Prefecture', 10, '2018-01-12 04:51:31', '2018-01-12 04:51:31'),
(46, 'Guangdong', 6, '2018-01-12 05:08:15', '2018-01-12 05:08:15'),
(47, 'Seoul', 12, '2018-01-12 05:24:10', '2018-01-12 05:24:10'),
(48, 'Jiangsu', 6, '2018-01-12 07:13:38', '2018-01-12 07:13:38'),
(49, 'Queensland', 32, '2018-08-14 05:46:30', '2018-08-14 05:46:30'),
(50, 'Assam', 1, '2018-09-30 02:54:05', '2018-09-30 02:54:05'),
(51, 'Tripura', 1, '2018-09-30 02:55:34', '2018-09-30 02:55:34'),
(52, 'Meghalaya', 1, '2018-09-30 02:56:11', '2018-09-30 02:56:11'),
(55, 'Uttarakhand', 1, '2018-09-30 03:03:07', '2018-09-30 03:03:07'),
(56, 'Maputo', 45, '2018-11-21 19:32:16', '2018-11-21 19:32:16'),
(57, 'North Island', 29, '2018-12-01 05:45:29', '2018-12-01 05:45:29'),
(59, 'Chiba Prefecture', 10, '2018-12-01 06:03:52', '2018-12-01 06:03:52'),
(60, 'Bulacan', 14, '2018-12-01 06:29:59', '2018-12-01 06:29:59'),
(61, 'Almaty', 11, '2018-12-01 06:34:18', '2018-12-01 06:34:18'),
(62, 'Beijing', 6, '2018-12-01 06:43:04', '2018-12-01 06:43:04'),
(63, 'Canterbury', 29, '2018-12-01 06:46:14', '2018-12-01 06:46:14'),
(64, 'Busan', 12, '2018-12-01 06:49:59', '2018-12-01 06:49:59'),
(65, 'Toyama Prefecture', 10, '2018-12-01 06:52:48', '2018-12-01 06:53:17'),
(66, 'Shanghai', 6, '2018-12-01 06:57:43', '2018-12-01 06:57:43'),
(67, 'Port Moresby', 75, '2018-12-04 02:48:07', '2018-12-04 02:48:07'),
(68, 'California', 28, '2018-12-14 01:30:45', '2018-12-14 01:30:45'),
(69, 'Hong Kong Island', 24, '2018-12-14 01:53:03', '2018-12-14 01:53:03'),
(70, 'São Paulo', 59, '2018-12-26 06:22:22', '2018-12-26 06:22:22'),
(71, 'Morona Santiago', 78, '2018-12-26 06:25:07', '2018-12-26 07:35:24'),
(72, 'Valle del Cauca', 79, '2018-12-26 06:28:10', '2018-12-26 06:28:10'),
(73, 'Montevideo', 60, '2018-12-26 06:31:10', '2018-12-26 06:31:10'),
(74, 'Asunción', 80, '2018-12-26 06:32:42', '2018-12-26 06:32:42'),
(75, 'Rio de Janeiro', 59, '2018-12-26 06:34:12', '2018-12-26 06:34:12'),
(76, 'Córdoba', 81, '2018-12-26 07:05:16', '2018-12-26 07:05:16'),
(77, 'Minas Gerais', 59, '2018-12-27 17:09:43', '2018-12-27 17:09:43'),
(78, 'Santa Fe', 81, '2018-12-27 17:19:11', '2018-12-27 17:19:11'),
(79, 'Delhi', 1, '0000-00-00 00:00:00', '2019-10-14 11:33:00'),
(83, 'Mizoram', 1, '2019-01-05 13:33:41', '2019-01-05 13:33:41'),
(84, 'Nagaland', 1, '2019-01-05 13:35:30', '2019-01-05 13:35:30'),
(85, 'Manipur', 1, '2019-01-05 13:45:20', '2019-01-05 13:45:20'),
(86, 'Arunachal Pradesh', 1, '2019-07-29 03:06:14', '2019-07-29 03:06:14'),
(87, 'Sikkim', 1, '2019-07-29 03:39:02', '2019-07-29 03:39:02'),
(88, 'Not Known', 87, '2019-08-05 07:23:49', '2019-08-05 07:23:49'),
(89, 'London', 40, '2019-09-06 12:11:48', '2019-09-06 12:11:48'),
(90, 'Helsinki', 88, '2019-09-06 12:25:12', '2019-09-06 12:25:12'),
(91, 'Abruzzo', 55, '2019-09-06 12:47:17', '2019-09-06 12:47:17'),
(92, 'Budapest', 63, '2019-09-06 15:26:36', '2019-09-06 15:26:36'),
(93, 'Lima', 58, '2019-09-06 15:28:39', '2019-09-06 15:28:39'),
(94, 'Belgrade', 86, '2019-09-06 15:29:16', '2019-09-06 15:29:16'),
(95, 'Copenhagen', 89, '2019-09-06 15:30:44', '2019-09-06 15:30:44'),
(96, 'Moscow', 90, '2019-09-06 15:41:32', '2019-09-06 15:41:32'),
(97, 'Sofia', 91, '2019-09-06 15:42:51', '2019-09-06 15:42:51'),
(98, 'Melbourne', 32, '2019-09-06 15:43:44', '2019-09-06 15:43:44'),
(100, 'Paris', 50, '2019-09-06 15:54:35', '2019-09-06 15:54:35'),
(101, 'Stockholm', 92, '2019-09-06 15:55:37', '2019-09-06 15:55:37'),
(102, 'Kabul', 94, '2019-09-11 03:31:00', '2019-09-11 03:31:00'),
(103, 'Seoul', 95, '2019-09-11 03:34:23', '2019-09-11 03:34:23'),
(104, 'Ho Chi Minh', 96, '2019-09-11 03:44:15', '2019-09-11 03:44:15'),
(105, 'Kuala Lumpur', 20, '2019-09-11 03:46:00', '2019-09-11 03:46:00'),
(106, 'Dhaka', 4, '2019-09-11 03:46:52', '2019-09-11 03:46:52'),
(107, 'Kuwait City', 25, '2019-09-11 03:47:33', '2019-09-11 03:47:33'),
(108, 'Pyongyang', 97, '2019-09-11 03:48:48', '2019-09-11 03:48:48'),
(109, 'Barham Salih', 9, '2019-09-11 03:50:06', '2019-09-11 03:50:06'),
(111, 'Manama', 22, '2019-09-11 03:52:30', '2019-09-11 03:52:30'),
(112, 'Kathmandu', 3, '2019-09-11 03:53:27', '2019-09-11 03:53:27'),
(113, 'Ashgabat', 98, '2019-09-11 03:55:12', '2019-09-11 03:55:12'),
(114, 'Tashkent', 33, '2019-09-11 03:56:29', '2019-09-11 03:56:29'),
(115, 'Naypyitaw', 99, '2019-09-11 03:59:39', '2019-09-11 03:59:39'),
(116, 'Trang', 15, '2019-09-11 04:17:49', '2019-09-11 04:17:49'),
(117, 'Ulsan', 95, '2019-09-11 04:20:52', '2019-09-11 04:20:52'),
(118, 'Busan', 95, '2019-09-11 04:21:05', '2019-09-11 04:21:05'),
(119, 'Yangsan', 95, '2019-09-11 04:22:54', '2019-09-11 04:22:54'),
(120, 'Ar Rayyan', 19, '2019-09-11 04:26:21', '2019-09-11 04:26:21'),
(121, 'Muscat', 100, '2019-10-15 03:23:43', '2019-10-15 03:23:43'),
(122, 'Rome', 55, '2019-10-15 03:30:40', '2019-10-15 03:30:40'),
(123, 'Warsaw', 56, '2019-10-15 03:44:44', '2019-10-15 03:44:44'),
(124, 'Buenos Aires', 81, '2019-10-15 03:46:33', '2019-10-15 03:46:33'),
(125, 'Yaounde', 42, '2019-10-15 04:07:42', '2019-10-15 04:07:42'),
(126, 'Damascus‎', 36, '2019-10-15 04:11:40', '2019-10-15 04:11:40'),
(127, 'Algiers', 101, '2019-10-15 04:33:27', '2019-10-15 04:33:27'),
(128, 'Rabat', 102, '2019-10-15 04:39:07', '2019-10-15 04:39:07'),
(129, 'Bucharest', 103, '2019-10-15 04:51:55', '2019-10-15 04:51:55'),
(130, 'East Berlin', 104, '2019-10-15 04:56:53', '2019-10-15 04:56:53'),
(131, '‎Lusaka', 105, '2019-10-15 05:05:23', '2019-10-15 05:05:23'),
(133, 'Moscow', 106, '2019-10-15 05:15:48', '2019-10-15 05:15:48'),
(134, 'Accra', 107, '2019-10-15 05:18:02', '2019-10-15 05:18:02'),
(135, 'Phnom Penh', 108, '2019-10-15 05:21:02', '2019-10-15 05:21:02'),
(136, 'Bishkek‎', 109, '2019-10-15 05:24:51', '2019-10-15 05:24:51'),
(137, 'Colombo', 27, '2019-10-15 05:27:08', '2019-10-15 05:27:08'),
(138, 'Beirut‎', 13, '2019-10-15 05:29:33', '2019-10-15 05:29:33'),
(139, 'Islamabad', 35, '2019-10-15 08:59:04', '2019-10-15 08:59:04'),
(140, 'New Delhi', 1, '2019-10-15 09:25:44', '2019-10-15 09:25:44'),
(169, 'Grosseto', 55, '2019-10-23 07:45:47', '2019-10-23 07:45:47'),
(170, 'Auckland', 29, '2019-11-13 03:43:12', '2019-11-13 03:43:12'),
(171, 'Dushanbe', 111, '2019-11-15 05:52:38', '2019-11-15 05:52:38'),
(172, 'Seeb', 100, '2019-11-15 06:03:15', '2019-11-15 06:03:15'),
(173, 'Kolkata', 1, '2019-11-21 01:41:16', '2019-11-21 01:41:16'),
(174, 'Jharkhand', 1, '2020-03-16 06:38:05', '2020-03-16 06:38:05'),
(177, 'Hiroshima', 10, '2021-01-09 08:12:21', '2021-01-09 08:12:21'),
(178, 'Fukuoka', 10, '2021-01-11 07:21:24', '2021-01-11 07:21:24'),
(179, 'Canberra', 32, '2021-01-11 07:59:17', '2021-01-11 07:59:17'),
(180, 'Quebec', 31, '2021-01-12 08:58:16', '2021-01-12 08:58:16'),
(181, 'Bavaria', 46, '2021-01-13 03:29:39', '2021-01-13 03:29:39'),
(182, 'Alberta', 31, '2021-01-13 04:42:11', '2021-01-13 04:42:11'),
(183, 'Asia', 112, '2021-01-13 06:08:47', '2021-01-13 06:08:47'),
(184, 'Georgia', 28, '2021-01-14 05:24:57', '2021-01-14 05:24:57'),
(185, 'New South Wales', 32, '2021-01-14 06:46:53', '2021-01-14 06:46:53'),
(186, 'Catalonia', 62, '2021-01-14 08:46:39', '2021-01-14 08:46:39'),
(187, 'Athens', 113, '2021-01-14 09:36:32', '2021-01-14 09:36:32'),
(188, 'Singapore', 26, '2021-01-21 07:28:17', '2021-01-21 07:28:17'),
(189, 'Baghdad', 9, '2021-01-23 08:03:50', '2021-01-23 08:03:50');

-- --------------------------------------------------------

--
-- Table structure for table `sport`
--

CREATE TABLE `sport` (
  `sport_id` int(10) UNSIGNED NOT NULL,
  `sport_name` varchar(50) NOT NULL,
  `user_id` int(10) NOT NULL,
  `identifier` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sport`
--

INSERT INTO `sport` (`sport_id`, `sport_name`, `user_id`, `identifier`) VALUES
(7, 'Wrestling', 4, 'wr'),
(24, 'Judo', 4, 'ju');

-- --------------------------------------------------------

--
-- Table structure for table `sub_regions`
--

CREATE TABLE `sub_regions` (
  `sub_region_id` int(10) UNSIGNED NOT NULL,
  `region_id` smallint(6) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_regions`
--

INSERT INTO `sub_regions` (`sub_region_id`, `region_id`, `name`, `created_at`, `updated_at`) VALUES
(3, 1, 'Thiruvananthapuram', '2017-06-07 18:29:18', '2017-06-07 18:29:18'),
(4, 1, 'Kasaragod', '2017-06-07 18:29:43', '2017-06-07 18:29:43'),
(5, 1, 'Thrissur', '2017-06-07 19:17:37', '2017-06-07 19:17:37'),
(6, 4, 'Hunan', '2017-06-08 06:12:20', '2017-06-08 06:12:20'),
(7, 5, 'Coimbatore', '2017-06-08 06:13:02', '2017-06-08 06:13:02'),
(9, 1, 'Kottayam', '2017-06-08 06:14:24', '2017-06-08 06:14:24'),
(11, 9, 'Pune', '2017-06-13 20:20:12', '2017-06-13 20:20:12'),
(12, 6, 'Ahmedabad', '2017-06-13 20:22:25', '2017-06-13 20:22:25'),
(13, 8, 'Bangalore', '2017-06-13 20:26:52', '2017-06-13 20:26:52'),
(14, 1, 'Ernakulam', '2017-06-13 20:31:15', '2017-06-13 20:31:15'),
(15, 1, 'Alappuzha', '2017-06-13 20:32:31', '2017-06-13 20:32:31'),
(16, 2, 'Chandigarh', '2017-06-13 20:35:44', '2017-06-13 20:35:44'),
(17, 5, 'Chennai', '2017-06-13 20:37:44', '2017-06-13 20:37:44'),
(18, 11, 'Hyderabad', '2017-06-13 20:40:07', '2017-06-13 20:40:07'),
(20, 15, 'Tehran', '2017-06-13 20:45:33', '2017-06-13 20:45:33'),
(21, 16, 'Chenzhou', '2017-06-13 20:50:05', '2017-06-13 20:50:05'),
(22, 7, 'Kelapa Gading', '2017-06-13 20:54:10', '2017-06-13 20:54:10'),
(23, 7, 'Rawamangun', '2017-06-13 20:56:07', '2017-06-13 20:56:07'),
(24, 3, 'Bangkok', '2017-06-13 20:57:31', '2017-06-13 20:57:31'),
(25, 8, 'Hassan', '2017-06-13 20:58:52', '2017-06-13 20:58:52'),
(26, 1, 'Malappuram', '2017-06-13 21:01:26', '2017-06-13 21:01:26'),
(27, 5, 'Theni', '2017-06-13 21:03:16', '2017-06-13 21:03:16'),
(28, 5, 'Karur', '2017-06-13 21:04:08', '2017-06-13 21:04:08'),
(29, 9, 'Mumbai', '2017-06-13 21:05:27', '2017-06-13 21:05:27'),
(30, 12, 'Noida', '2017-06-13 21:07:31', '2017-06-13 21:07:31'),
(31, 17, 'Puducherry', '2017-07-09 21:12:18', '2017-07-09 21:12:18'),
(32, 12, 'Gautam Buddh Nagar', '2017-07-10 02:44:18', '2017-07-10 02:44:18'),
(38, 1, 'Wayanad', '2017-07-10 03:19:05', '2017-07-10 03:19:05'),
(39, 1, 'Idukki', '2017-07-10 03:20:56', '2017-07-10 03:20:56'),
(40, 1, 'Kozhikode', '2017-07-10 03:21:21', '2017-07-10 03:21:21'),
(41, 1, 'Pathanamthitta', '2017-07-10 03:21:45', '2017-07-10 03:21:45'),
(42, 1, 'Kollam', '2017-07-10 03:22:34', '2017-07-10 03:22:34'),
(43, 1, 'Kannur', '2017-07-10 03:23:23', '2017-07-10 03:23:23'),
(44, 1, 'Palakkad', '2017-07-10 03:23:34', '2017-07-10 03:23:34'),
(45, 32, 'Keserwan', '2017-08-12 06:26:30', '2017-08-12 06:26:30'),
(46, 5, 'Tiruchirappalli', '2017-09-01 22:40:37', '2017-09-01 22:40:37'),
(47, 5, 'Kanchipuram', '2017-09-02 01:06:57', '2017-09-02 01:06:57'),
(48, 5, 'Vellore', '2017-09-02 01:14:27', '2017-09-02 01:14:27'),
(49, 33, 'Male', '2017-09-04 23:23:06', '2017-09-04 23:23:06'),
(50, 35, 'Melbourne', '2017-11-16 00:13:44', '2017-11-16 00:13:44'),
(51, 37, 'Doha', '2018-01-12 02:57:19', '2018-01-12 02:57:19'),
(52, 38, 'Setagaya', '2018-01-12 03:15:11', '2018-01-12 03:15:11'),
(53, 40, 'Goyang', '2018-01-12 03:39:47', '2018-01-12 03:39:47'),
(54, 41, 'Wanchai', '2018-01-12 03:59:07', '2018-01-12 03:59:07'),
(55, 42, 'Adelaide', '2018-01-12 04:15:00', '2018-01-12 14:02:41'),
(56, 43, 'Quezon City', '2018-01-12 04:31:46', '2018-01-12 04:31:46'),
(57, 44, 'Astana', '2018-01-12 04:40:37', '2018-01-12 04:40:37'),
(58, 45, 'Yokohama', '2018-01-12 04:51:57', '2018-01-12 04:51:57'),
(59, 46, 'Dongguan', '2018-01-12 05:08:56', '2018-01-12 05:08:56'),
(60, 47, 'Songpa-gu', '2018-01-12 05:24:42', '2018-01-12 05:24:42'),
(61, 43, 'Pasay', '2018-01-12 05:34:43', '2018-01-12 05:34:43'),
(62, 48, 'Nanjing', '2018-01-12 07:14:03', '2018-01-12 07:14:03'),
(63, 46, 'Foshan', '2018-04-03 02:03:40', '2018-04-03 02:03:40'),
(64, 49, 'Townsville', '2018-08-14 05:47:28', '2018-08-14 05:47:28'),
(65, 49, 'Cairns', '2018-08-14 05:56:38', '2018-08-14 05:56:38'),
(66, 49, 'Gold Coast', '2018-08-20 00:48:21', '2018-08-20 00:48:21'),
(67, 29, 'Guntur', '2018-09-16 06:04:19', '2018-09-16 06:04:19'),
(68, 29, 'East Godavari', '2018-09-16 06:06:46', '2018-09-16 06:06:46'),
(69, 5, 'Sivaganga', '2018-09-16 06:08:28', '2018-09-16 06:08:28'),
(70, 29, 'Visakhapatnam', '2018-09-16 06:28:43', '2018-09-16 06:28:43'),
(71, 5, 'Cuddalore', '2018-09-16 06:35:52', '2018-09-16 06:35:52'),
(72, 28, 'Udaipur', '2018-09-30 02:18:57', '2018-09-30 02:18:57'),
(73, 26, 'Kapurthala', '2018-10-07 04:02:37', '2018-10-07 04:02:37'),
(74, 26, 'Chandigarh', '2018-10-28 09:10:24', '2018-10-28 09:10:24'),
(75, 18, 'Rohtak', '2018-10-28 09:12:04', '2018-10-28 09:12:04'),
(76, 9, 'Aurangabad', '2018-10-28 09:15:21', '2018-10-28 09:15:21'),
(77, 24, 'Khordha', '2018-10-28 09:17:46', '2018-10-28 09:17:46'),
(78, 9, 'Kolhapur', '2018-10-28 09:20:58', '2018-10-28 09:20:58'),
(79, 7, 'Gelora', '2018-11-16 06:00:14', '2018-11-16 06:00:14'),
(80, 57, 'Bay of Plenty', '2018-12-01 05:45:56', '2018-12-01 05:45:56'),
(81, 46, 'Shenzhen', '2018-12-01 05:58:31', '2018-12-01 06:00:48'),
(82, 59, 'Chiba', '2018-12-01 06:04:04', '2018-12-01 06:04:04'),
(83, 57, 'Auckland', '2018-12-01 06:16:08', '2018-12-01 06:16:08'),
(84, 60, 'Bocaue', '2018-12-01 06:31:23', '2018-12-01 06:31:23'),
(85, 35, 'Bendigo', '2018-12-01 06:39:33', '2018-12-01 06:39:33'),
(86, 62, 'Haidian District', '2018-12-01 06:43:38', '2018-12-01 06:43:38'),
(87, 63, 'Christchurch', '2018-12-01 06:46:34', '2018-12-01 06:46:34'),
(88, 65, 'Toyama', '2018-12-01 06:53:00', '2018-12-01 06:53:00'),
(89, 66, 'Xuhui', '2018-12-01 06:58:03', '2018-12-01 06:58:03'),
(90, 46, 'Guangzhou', '2018-12-01 07:02:56', '2018-12-01 07:02:56'),
(91, 69, 'Wan Chai', '2018-12-14 01:53:24', '2018-12-14 01:53:24'),
(92, 70, 'Franca', '2018-12-26 06:22:58', '2018-12-26 06:22:58'),
(93, 71, 'Macas', '2018-12-26 06:25:30', '2018-12-26 06:25:30'),
(94, 72, 'Santiago de Cali', '2018-12-26 06:28:35', '2018-12-26 06:28:35'),
(95, 75, 'Rio de Janeiro', '2018-12-26 06:34:22', '2018-12-26 06:34:22'),
(96, 76, 'Córdoba', '2018-12-26 07:05:26', '2018-12-26 07:05:26'),
(97, 77, 'Belo Horizonte', '2018-12-27 17:10:18', '2018-12-27 17:10:18'),
(98, 78, 'Sunchales', '2018-12-27 17:19:28', '2018-12-27 17:19:28'),
(99, 82, 'kowdiar', '2019-01-04 20:37:17', '0000-00-00 00:00:00'),
(100, 82, 'patton', '2019-01-04 20:37:17', '0000-00-00 00:00:00'),
(102, 82, 'Patna', '2019-01-05 06:05:59', '0000-00-00 00:00:00'),
(103, 82, 'Lucknow', '2019-01-05 06:05:59', '0000-00-00 00:00:00'),
(104, 82, 'Bhubaneshwar', '2019-01-05 06:05:59', '0000-00-00 00:00:00'),
(105, 82, 'Indore', '2019-01-05 06:05:59', '0000-00-00 00:00:00'),
(106, 82, 'Dehradun', '2019-01-05 06:05:59', '0000-00-00 00:00:00'),
(107, 82, 'Shimla', '2019-01-05 06:05:59', '0000-00-00 00:00:00'),
(108, 28, 'Jaipur', '2019-01-05 06:05:59', '2019-10-14 11:51:15'),
(109, 82, 'Atal Nagar', '2019-01-05 06:05:59', '0000-00-00 00:00:00'),
(110, 6, 'Bhavnagar', '2019-01-05 10:43:25', '2019-01-05 10:43:25'),
(111, 55, 'Roorkee', '2019-06-22 07:30:18', '2019-06-22 07:30:18'),
(112, 28, 'Jodhpur', '2019-06-22 07:42:12', '2019-06-22 07:42:12'),
(113, 12, 'Lucknow', '2019-06-22 07:44:02', '2019-06-22 07:44:02'),
(114, 19, 'Kolkota', '2019-06-22 07:56:36', '2019-06-22 07:56:36'),
(116, 50, 'Guwahati', '2019-08-27 10:24:52', '2019-08-27 10:24:52'),
(117, 27, 'South Goa', '2019-08-27 10:28:48', '2019-08-27 10:30:34'),
(118, 27, 'Panaji', '2019-08-27 10:30:55', '2019-08-27 10:30:55'),
(119, 24, 'Cuttack', '2019-08-27 10:42:12', '2019-08-27 10:42:12'),
(120, 18, 'Gurgaon', '2019-08-28 04:52:27', '2019-08-28 04:52:27'),
(121, 19, 'Siliguri', '2019-08-29 06:43:05', '2019-08-29 06:43:05'),
(122, 26, 'Jalandhar', '2019-08-29 06:51:23', '2019-08-29 06:51:23'),
(123, 26, 'Ludhiana', '2019-08-29 06:54:19', '2019-08-29 06:54:19'),
(124, 89, 'Illford', '2019-09-06 12:12:26', '2019-09-06 12:12:26'),
(125, 90, 'Toolo', '2019-09-06 12:25:47', '2019-09-06 12:25:47'),
(126, 91, 'L\'Aquila', '2019-09-06 12:48:04', '2019-09-06 12:48:04'),
(127, 30, 'Nawada', '2019-09-26 07:35:19', '2019-09-26 07:35:19'),
(128, 30, 'Tata Nagar', '2019-10-14 11:34:14', '2019-10-14 11:34:14'),
(129, 26, 'Patiala', '2019-10-14 11:35:49', '2019-10-14 11:35:49'),
(130, 12, 'Allahabad', '2019-10-14 11:36:39', '2019-10-14 11:36:39'),
(131, 22, 'Delhi', '2019-10-14 11:37:15', '2019-10-14 11:37:15'),
(132, 21, 'Jabalpur', '2019-10-14 11:38:35', '2019-10-14 11:38:35'),
(133, 21, 'Gwalior', '2019-10-14 11:38:57', '2019-10-14 11:38:57'),
(134, 30, 'Jamshedpur', '2019-10-14 11:42:16', '2019-10-14 11:42:16'),
(135, 6, 'Baroda', '2019-10-14 11:44:32', '2019-10-14 11:44:32'),
(136, 18, 'Faridabad', '2019-10-14 11:45:19', '2019-10-14 11:45:19'),
(137, 21, 'Bhopal', '2019-10-14 11:45:43', '2019-10-14 11:45:43'),
(138, 12, 'Kanpur', '2019-10-14 11:46:10', '2019-10-14 11:46:10'),
(139, 21, 'Bhilai', '2019-10-14 11:46:36', '2019-10-14 11:46:36'),
(140, 18, 'Hissar', '2019-10-14 11:47:00', '2019-10-14 11:47:00'),
(141, 23, 'Jammu', '2019-10-14 11:48:09', '2019-10-14 11:48:09'),
(142, 19, 'Chinsurah', '2019-10-14 11:48:37', '2019-10-14 11:48:37'),
(143, 5, 'Salem', '2019-10-14 11:49:00', '2019-10-14 11:49:00'),
(144, 20, 'Raipur', '2019-10-14 11:49:22', '2019-10-14 11:49:22'),
(145, 18, 'Chautala', '2019-10-14 11:49:44', '2019-10-14 11:49:44'),
(146, 8, 'Davangere', '2019-10-14 11:50:40', '2019-10-14 11:50:40'),
(147, 12, 'Moradabad', '2019-10-14 11:51:42', '2019-10-14 11:51:42'),
(164, 19, 'Kolkata', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(166, 5, 'Tiruppur', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(167, 20, 'Durg', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(168, 5, 'Tirunelveli', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(169, 9, 'Thane', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(170, 5, 'Virudhunagar', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(171, 11, 'Bhdradri Kothagudem', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(172, 8, 'Belagavi', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(173, 12, 'Sonbhadra', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(174, 5, 'Thoothukudi', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(175, 5, 'Krishnagiri', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(176, 12, 'Varanasi', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(177, 29, 'West Godavari', '2019-10-16 07:19:35', '2019-10-16 07:19:35'),
(178, 30, 'Patna', '2019-10-18 02:59:22', '2019-10-18 02:59:22'),
(179, 6, 'Gujarat', '2019-11-14 11:56:49', '2019-11-14 11:56:49'),
(180, 173, 'Nadia', '2019-11-21 01:41:47', '2019-11-21 01:41:47'),
(181, 19, 'Nadiad', '2019-11-21 01:44:58', '2019-11-21 01:44:58'),
(182, 20, 'Bilaspur', '2020-01-16 06:15:08', '2020-01-16 06:15:08'),
(183, 140, 'Delhi', '2020-01-22 03:47:36', '2020-01-22 03:47:36'),
(184, 50, 'Sonapur', '2020-01-28 08:36:41', '2020-01-28 08:36:41'),
(185, 29, 'Anantapur', '2020-03-10 05:49:04', '2020-03-10 05:49:04'),
(186, 86, 'Pasighat', '2020-03-12 08:34:24', '2020-03-12 08:34:24'),
(187, 52, 'Shillong', '2020-03-16 06:08:35', '2020-03-16 06:08:35'),
(188, 133, 'Moscow', '2020-05-19 02:21:16', '2020-05-19 02:21:16'),
(189, 22, 'Delhi', '2020-11-27 06:34:33', '2020-11-27 06:34:33'),
(190, 111, 'Manama', '2020-11-27 07:59:01', '2020-11-27 07:59:01'),
(191, 68, 'Los Angeles', '2020-12-13 04:53:48', '2020-12-13 04:53:48'),
(192, 8, 'Mangaluru', '2021-01-08 02:14:38', '2021-01-08 02:14:38'),
(193, 180, 'Montreal', '2021-01-12 08:58:57', '2021-01-12 08:58:57'),
(194, 181, 'Munich', '2021-01-13 03:30:11', '2021-01-13 03:30:11'),
(195, 182, 'Edmonton', '2021-01-13 04:42:49', '2021-01-13 04:42:49'),
(196, 183, 'Asia', '2021-01-13 06:10:23', '2021-01-13 06:10:23'),
(197, 38, 'Shinjuku', '2021-01-13 06:51:39', '2021-01-13 06:51:39'),
(198, 100, 'Bercy', '2021-01-13 08:26:44', '2021-01-13 08:26:44'),
(199, 184, 'Atlanta', '2021-01-14 05:25:35', '2021-01-14 05:25:35'),
(200, 185, 'Sydney', '2021-01-14 06:47:21', '2021-01-14 06:47:21'),
(201, 186, 'Barcelona', '2021-01-14 08:47:12', '2021-01-14 08:47:12'),
(202, 62, 'Beijing', '2021-01-14 10:38:24', '2021-01-14 10:38:24'),
(203, 89, 'Stratford', '2021-01-15 03:29:52', '2021-01-15 03:29:52'),
(204, 85, 'Imphal', '2021-01-15 08:57:01', '2021-01-15 08:57:01'),
(205, 5, 'Kanyakumari', '2021-01-19 03:41:55', '2021-01-19 03:41:55'),
(206, 174, 'Jamshedpur', '2021-01-20 08:34:21', '2021-01-20 08:34:21'),
(207, 188, 'Choa Chu Kang', '2021-01-21 07:29:07', '2021-01-21 07:29:19'),
(208, 5, 'Tiruvannamalai', '2021-01-28 02:36:02', '2021-01-28 02:36:02');

-- --------------------------------------------------------

--
-- Table structure for table `venue`
--

CREATE TABLE `venue` (
  `venue_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `country_id` smallint(5) UNSIGNED NOT NULL,
  `address` varchar(300) DEFAULT '',
  `region` varchar(255) DEFAULT '',
  `venue_type` enum('indoor','outdoor') NOT NULL DEFAULT 'indoor',
  `sub_region` smallint(5) UNSIGNED DEFAULT NULL,
  `floodlight` enum('y','n','u') DEFAULT NULL,
  `surface` int(11) DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `lat` double(10,6) DEFAULT NULL,
  `lng` double(10,6) DEFAULT NULL,
  `location_url` text DEFAULT NULL,
  `venue_sport_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `venue`
--

INSERT INTO `venue` (`venue_id`, `name`, `country_id`, `address`, `region`, `venue_type`, `sub_region`, `floodlight`, `surface`, `capacity`, `lat`, `lng`, `location_url`, `venue_sport_id`) VALUES
(363, 'Jimmy George Indoor Stadium', 1, 'Sample Address', '1', 'indoor', 1, 'y', 1, 250, NULL, NULL, NULL, NULL),
(364, 'Home', 1, 'lorem Ipsum', 'lorem Ipsum', 'indoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wr_age_category`
--

CREATE TABLE `wr_age_category` (
  `age_category_id` int(10) NOT NULL,
  `category_name` varchar(50) DEFAULT NULL,
  `category_desc` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wr_event`
--

CREATE TABLE `wr_event` (
  `event_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `generic_name` varchar(250) DEFAULT NULL,
  `season` varchar(10) NOT NULL,
  `country_id` smallint(5) UNSIGNED NOT NULL,
  `start_date` date DEFAULT '0000-00-00',
  `region` int(10) DEFAULT NULL,
  `sub_region_id` int(10) DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `category` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wr_event`
--

INSERT INTO `wr_event` (`event_id`, `name`, `generic_name`, `season`, `country_id`, `start_date`, `region`, `sub_region_id`, `location`, `category`) VALUES
(1, 'Sample Event WR', 'Sample generic name with description', '2019/20', 1, '2021-02-12', 1, 3, 1, 'Senior Sample');

-- --------------------------------------------------------

--
-- Table structure for table `wr_event_game`
--

CREATE TABLE `wr_event_game` (
  `event_game_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `game_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wr_event_game`
--

INSERT INTO `wr_event_game` (`event_game_id`, `event_id`, `game_id`) VALUES
(880, 1, 881);

-- --------------------------------------------------------

--
-- Table structure for table `wr_event_official`
--

CREATE TABLE `wr_event_official` (
  `event_official_id` int(11) NOT NULL,
  `sport_id` int(10) UNSIGNED NOT NULL,
  `team_id` int(10) UNSIGNED NOT NULL,
  `official_id` int(10) UNSIGNED NOT NULL,
  `post` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wr_event_official`
--

INSERT INTO `wr_event_official` (`event_official_id`, `sport_id`, `team_id`, `official_id`, `post`) VALUES
(1, 7, 94, 1, 'Physio');

-- --------------------------------------------------------

--
-- Table structure for table `wr_event_squad`
--

CREATE TABLE `wr_event_squad` (
  `event_squad_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `player_id` int(10) UNSIGNED NOT NULL,
  `player_number` varchar(20) DEFAULT NULL,
  `team_id` int(10) UNSIGNED DEFAULT NULL,
  `wr_style` int(10) UNSIGNED DEFAULT NULL,
  `weight_cat` int(10) UNSIGNED DEFAULT NULL,
  `weight` decimal(10,3) UNSIGNED NOT NULL,
  `position` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wr_event_squad`
--

INSERT INTO `wr_event_squad` (`event_squad_id`, `event_id`, `player_id`, `player_number`, `team_id`, `wr_style`, `weight_cat`, `weight`, `position`) VALUES
(1030, 1, 8522, '5', 94, 4, 1, '75.000', 'freestyle'),
(1031, 1, 8523, '5', 94, 4, 1, '75.000', 'freestyle');

-- --------------------------------------------------------

--
-- Table structure for table `wr_event_team_squad`
--

CREATE TABLE `wr_event_team_squad` (
  `event_team_squad_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `player_number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wr_event_team_squad`
--

INSERT INTO `wr_event_team_squad` (`event_team_squad_id`, `event_id`, `team_id`, `player_id`, `player_number`) VALUES
(1, 1, 94, 8522, 5);

-- --------------------------------------------------------

--
-- Table structure for table `wr_game`
--

CREATE TABLE `wr_game` (
  `game_id` int(10) UNSIGNED NOT NULL,
  `player1_id` int(10) UNSIGNED NOT NULL,
  `player2_id` int(10) UNSIGNED NOT NULL,
  `team1_id` int(10) UNSIGNED DEFAULT NULL,
  `team2_id` int(10) UNSIGNED DEFAULT NULL,
  `venue_id` int(10) UNSIGNED NOT NULL,
  `venue_type` enum('homeaway','neutral') NOT NULL DEFAULT 'homeaway',
  `game_date` date NOT NULL,
  `score1` smallint(5) NOT NULL,
  `score2` smallint(5) NOT NULL,
  `overtime` enum('n','y') NOT NULL DEFAULT 'n',
  `status` enum('fixture','summary','full') NOT NULL DEFAULT 'fixture',
  `winner` enum('0','1','2') NOT NULL DEFAULT '0',
  `phase` varchar(255) DEFAULT '',
  `gmt_start_time` time DEFAULT NULL,
  `local_start_time` time DEFAULT NULL,
  `sequence` smallint(1) DEFAULT NULL,
  `season` varchar(10) DEFAULT NULL,
  `country_id` smallint(5) UNSIGNED DEFAULT NULL,
  `points_awarded` enum('y','n','','') DEFAULT NULL,
  `game_style` int(10) DEFAULT NULL,
  `age_category` int(10) DEFAULT NULL,
  `weight_category` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wr_game`
--

INSERT INTO `wr_game` (`game_id`, `player1_id`, `player2_id`, `team1_id`, `team2_id`, `venue_id`, `venue_type`, `game_date`, `score1`, `score2`, `overtime`, `status`, `winner`, `phase`, `gmt_start_time`, `local_start_time`, `sequence`, `season`, `country_id`, `points_awarded`, `game_style`, `age_category`, `weight_category`) VALUES
(881, 8522, 8523, 94, NULL, 363, 'homeaway', '2021-02-13', 8, 9, 'n', 'full', '0', 'test phase', '05:15:00', '05:15:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wr_game_official`
--

CREATE TABLE `wr_game_official` (
  `game_official_id` int(10) UNSIGNED NOT NULL,
  `official_name` varchar(50) DEFAULT NULL,
  `game_id` int(10) UNSIGNED NOT NULL,
  `reference_player_id` int(10) UNSIGNED DEFAULT NULL,
  `official_type` enum('referee','judge','matchairman') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wr_game_official`
--

INSERT INTO `wr_game_official` (`game_official_id`, `official_name`, `game_id`, `reference_player_id`, `official_type`) VALUES
(1, 'Meeran', 881, 8522, 'referee'),
(2, 'Meeran II', 881, 8522, 'judge'),
(3, 'Meeran III', 881, 8522, 'matchairman');

-- --------------------------------------------------------

--
-- Table structure for table `wr_game_player`
--

CREATE TABLE `wr_game_player` (
  `wr_game_player_id` int(10) UNSIGNED NOT NULL,
  `game_id` int(10) UNSIGNED NOT NULL,
  `team_id` int(10) UNSIGNED NOT NULL,
  `player_id` int(10) UNSIGNED NOT NULL,
  `gametime_seconds` smallint(6) DEFAULT -1,
  `takedown_t2` int(10) NOT NULL DEFAULT -1,
  `nearfall_n2` int(10) NOT NULL DEFAULT -1,
  `nearfall_n3` int(10) NOT NULL DEFAULT -1,
  `reversal_r2` int(10) NOT NULL DEFAULT -1,
  `escape_e1` int(10) NOT NULL DEFAULT -1,
  `penalty_p1` int(10) NOT NULL DEFAULT -1,
  `penalty_p2` int(10) NOT NULL DEFAULT -1,
  `stalling` int(10) NOT NULL DEFAULT -1,
  `stalling_w2` int(10) NOT NULL DEFAULT -1,
  `caution` int(10) NOT NULL DEFAULT -1,
  `caution_c1` int(10) NOT NULL DEFAULT -1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wr_game_player`
--

INSERT INTO `wr_game_player` (`wr_game_player_id`, `game_id`, `team_id`, `player_id`, `gametime_seconds`, `takedown_t2`, `nearfall_n2`, `nearfall_n3`, `reversal_r2`, `escape_e1`, `penalty_p1`, `penalty_p2`, `stalling`, `stalling_w2`, `caution`, `caution_c1`) VALUES
(1221, 1, 1, 8522, 10, 12, 0, 1, 10, 12, 0, 0, 10, 9, 1, 10),
(1222, 1, 1, 8523, 10, 12, 0, 1, 10, 12, 0, 0, 10, 9, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `wr_game_team_official`
--

CREATE TABLE `wr_game_team_official` (
  `game_team_official_id` int(11) NOT NULL,
  `official_name` varchar(50) DEFAULT NULL,
  `game_id` int(10) UNSIGNED NOT NULL,
  `player_id` int(10) UNSIGNED NOT NULL,
  `official_type` enum('coach','assistantcoach') NOT NULL,
  `reference_player_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wr_game_team_official`
--

INSERT INTO `wr_game_team_official` (`game_team_official_id`, `official_name`, `game_id`, `player_id`, `official_type`, `reference_player_id`) VALUES
(1, 'Khan', 881, 8522, 'coach', 8522),
(2, 'Khan II', 881, 8523, 'coach', 8523);

-- --------------------------------------------------------

--
-- Table structure for table `wr_official_team`
--

CREATE TABLE `wr_official_team` (
  `official_team_id` int(11) NOT NULL,
  `official_id` int(11) NOT NULL,
  `post` varchar(30) NOT NULL,
  `team_id` int(11) NOT NULL,
  `start_season` varchar(30) DEFAULT NULL,
  `end_season` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wr_picture_event`
--

CREATE TABLE `wr_picture_event` (
  `wr_picture_event_id` int(10) UNSIGNED NOT NULL,
  `picture_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wr_picture_player`
--

CREATE TABLE `wr_picture_player` (
  `wr_picture_player_id` int(10) UNSIGNED NOT NULL,
  `picture_id` int(10) UNSIGNED NOT NULL,
  `player_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wr_picture_team`
--

CREATE TABLE `wr_picture_team` (
  `wr_picture_team_id` int(10) UNSIGNED NOT NULL,
  `picture_id` int(10) UNSIGNED NOT NULL,
  `team_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wr_picture_venue`
--

CREATE TABLE `wr_picture_venue` (
  `wr_picture_venue_id` int(10) UNSIGNED NOT NULL,
  `picture_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wr_picture_venue`
--

INSERT INTO `wr_picture_venue` (`wr_picture_venue_id`, `picture_id`, `venue_id`, `created_at`, `updated_at`) VALUES
(1, 132, 221, NULL, NULL),
(2, 133, 223, '2019-08-05 08:25:34', '2019-08-05 08:25:34'),
(3, 134, 220, NULL, NULL),
(4, 778, 343, '2020-08-08 22:47:20', '2020-08-08 22:47:20'),
(5, 780, 345, '2020-08-08 23:43:39', '2020-08-08 23:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `wr_playerteam`
--

CREATE TABLE `wr_playerteam` (
  `playerteam_id` int(11) NOT NULL,
  `player_id` varchar(200) NOT NULL,
  `team_id` varchar(200) NOT NULL,
  `start_season` varchar(200) NOT NULL,
  `end_season` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wr_styles`
--

CREATE TABLE `wr_styles` (
  `style_id` int(10) NOT NULL,
  `style_name` varchar(50) DEFAULT NULL,
  `style_cd` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wr_styles`
--

INSERT INTO `wr_styles` (`style_id`, `style_name`, `style_cd`) VALUES
(4, 'FreeStyle', 'Fr1');

-- --------------------------------------------------------

--
-- Table structure for table `wr_team`
--

CREATE TABLE `wr_team` (
  `team_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `country_id` smallint(5) UNSIGNED NOT NULL,
  `region` varchar(255) DEFAULT '',
  `sub_region_id` int(11) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `related_team` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wr_team`
--

INSERT INTO `wr_team` (`team_id`, `name`, `country_id`, `region`, `sub_region_id`, `address`, `related_team`) VALUES
(94, 'Senior Wrestinling', 1, '1', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wr_videos`
--

CREATE TABLE `wr_videos` (
  `game_player_id` int(225) NOT NULL,
  `video_id_prim` int(10) NOT NULL,
  `video_cat` enum('pts','fg','fg2','ft') DEFAULT NULL,
  `game_id` int(225) NOT NULL,
  `team_id` int(225) NOT NULL,
  `player_id` int(225) NOT NULL,
  `video_path` varchar(500) NOT NULL,
  `player_sequence` int(11) NOT NULL,
  `team_sequence` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wr_weight_category`
--

CREATE TABLE `wr_weight_category` (
  `weight_category_id` int(11) NOT NULL,
  `category_desc` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wr_weight_category`
--

INSERT INTO `wr_weight_category` (`weight_category_id`, `category_desc`) VALUES
(1, '29-33'),
(2, '36'),
(3, '34-38'),
(4, '39'),
(5, '36-40'),
(6, '41'),
(7, '42'),
(8, '43'),
(9, '44'),
(10, '41-45'),
(11, '46'),
(12, '48'),
(13, '49'),
(14, '50'),
(15, '51'),
(16, '52'),
(17, '53'),
(18, '54'),
(19, '55'),
(20, '57'),
(21, '58'),
(22, '59'),
(23, '60'),
(24, '61'),
(25, '62'),
(26, '63'),
(27, '65'),
(28, '66'),
(29, '67'),
(30, '68'),
(31, '69'),
(32, '70'),
(33, '71'),
(34, '72'),
(35, '73'),
(36, '74'),
(37, '75'),
(38, '76'),
(39, '77'),
(40, '79'),
(41, '80'),
(42, '82'),
(43, '85'),
(44, '86'),
(45, '87'),
(46, '92'),
(47, '97'),
(48, '110'),
(49, '125'),
(50, '130'),
(51, '12'),
(52, '1'),
(55, '38');

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE `zone` (
  `zone_id` int(10) UNSIGNED NOT NULL,
  `zone_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `ju_age_category`
--
ALTER TABLE `ju_age_category`
  ADD PRIMARY KEY (`age_category_id`);

--
-- Indexes for table `ju_event`
--
ALTER TABLE `ju_event`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `ju_event_game`
--
ALTER TABLE `ju_event_game`
  ADD PRIMARY KEY (`event_game_id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `game_id` (`game_id`);

--
-- Indexes for table `ju_event_official`
--
ALTER TABLE `ju_event_official`
  ADD PRIMARY KEY (`event_official_id`);

--
-- Indexes for table `ju_event_squad`
--
ALTER TABLE `ju_event_squad`
  ADD PRIMARY KEY (`event_squad_id`),
  ADD KEY `ju_event_squad_ibfk_1` (`event_id`),
  ADD KEY `ju_event_squad_ibfk_2` (`player_id`);

--
-- Indexes for table `ju_event_team_squad`
--
ALTER TABLE `ju_event_team_squad`
  ADD PRIMARY KEY (`event_team_squad_id`);

--
-- Indexes for table `ju_game`
--
ALTER TABLE `ju_game`
  ADD PRIMARY KEY (`game_id`),
  ADD KEY `venue_id` (`venue_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `player1_id` (`player1_id`),
  ADD KEY `player2_id` (`player2_id`),
  ADD KEY `team1_id` (`team1_id`),
  ADD KEY `team2_id` (`team2_id`);

--
-- Indexes for table `ju_game_official`
--
ALTER TABLE `ju_game_official`
  ADD PRIMARY KEY (`game_official_id`),
  ADD KEY `game_id` (`game_id`),
  ADD KEY `player_id` (`reference_player_id`);

--
-- Indexes for table `ju_game_player`
--
ALTER TABLE `ju_game_player`
  ADD PRIMARY KEY (`ju_game_player_id`),
  ADD KEY `game_id` (`game_id`),
  ADD KEY `team_id` (`team_id`),
  ADD KEY `player_id` (`player_id`);

--
-- Indexes for table `ju_game_team_official`
--
ALTER TABLE `ju_game_team_official`
  ADD PRIMARY KEY (`game_team_official_id`),
  ADD KEY `game_id` (`game_id`),
  ADD KEY `player_id` (`player_id`);

--
-- Indexes for table `ju_official_team`
--
ALTER TABLE `ju_official_team`
  ADD PRIMARY KEY (`official_team_id`);

--
-- Indexes for table `ju_picture_event`
--
ALTER TABLE `ju_picture_event`
  ADD PRIMARY KEY (`ju_picture_event_id`);

--
-- Indexes for table `ju_picture_player`
--
ALTER TABLE `ju_picture_player`
  ADD PRIMARY KEY (`ju_picture_player_id`),
  ADD KEY `picture_id` (`picture_id`),
  ADD KEY `player_id` (`player_id`);

--
-- Indexes for table `ju_picture_team`
--
ALTER TABLE `ju_picture_team`
  ADD PRIMARY KEY (`ju_picture_team_id`),
  ADD KEY `picture_id` (`picture_id`),
  ADD KEY `team_id` (`team_id`);

--
-- Indexes for table `ju_picture_venue`
--
ALTER TABLE `ju_picture_venue`
  ADD PRIMARY KEY (`ju_picture_venue_id`);

--
-- Indexes for table `ju_playerteam`
--
ALTER TABLE `ju_playerteam`
  ADD PRIMARY KEY (`playerteam_id`);

--
-- Indexes for table `ju_styles`
--
ALTER TABLE `ju_styles`
  ADD PRIMARY KEY (`style_id`);

--
-- Indexes for table `ju_team`
--
ALTER TABLE `ju_team`
  ADD PRIMARY KEY (`team_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `ju_videos`
--
ALTER TABLE `ju_videos`
  ADD PRIMARY KEY (`video_id_prim`);

--
-- Indexes for table `ju_weight_category`
--
ALTER TABLE `ju_weight_category`
  ADD PRIMARY KEY (`weight_category_id`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`player_id`),
  ADD UNIQUE KEY `regno` (`regno`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `sport_id` (`sport_id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`region_id`);

--
-- Indexes for table `sport`
--
ALTER TABLE `sport`
  ADD PRIMARY KEY (`sport_id`),
  ADD UNIQUE KEY `identifier` (`identifier`);

--
-- Indexes for table `sub_regions`
--
ALTER TABLE `sub_regions`
  ADD PRIMARY KEY (`sub_region_id`);

--
-- Indexes for table `venue`
--
ALTER TABLE `venue`
  ADD PRIMARY KEY (`venue_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `wr_age_category`
--
ALTER TABLE `wr_age_category`
  ADD PRIMARY KEY (`age_category_id`);

--
-- Indexes for table `wr_event`
--
ALTER TABLE `wr_event`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `wr_event_game`
--
ALTER TABLE `wr_event_game`
  ADD PRIMARY KEY (`event_game_id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `game_id` (`game_id`);

--
-- Indexes for table `wr_event_official`
--
ALTER TABLE `wr_event_official`
  ADD PRIMARY KEY (`event_official_id`);

--
-- Indexes for table `wr_event_squad`
--
ALTER TABLE `wr_event_squad`
  ADD PRIMARY KEY (`event_squad_id`),
  ADD KEY `wr_event_squad_ibfk_1` (`event_id`),
  ADD KEY `wr_event_squad_ibfk_2` (`player_id`);

--
-- Indexes for table `wr_event_team_squad`
--
ALTER TABLE `wr_event_team_squad`
  ADD PRIMARY KEY (`event_team_squad_id`);

--
-- Indexes for table `wr_game`
--
ALTER TABLE `wr_game`
  ADD PRIMARY KEY (`game_id`),
  ADD KEY `venue_id` (`venue_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `player1_id` (`player1_id`),
  ADD KEY `player2_id` (`player2_id`),
  ADD KEY `team1_id` (`team1_id`),
  ADD KEY `team2_id` (`team2_id`);

--
-- Indexes for table `wr_game_official`
--
ALTER TABLE `wr_game_official`
  ADD PRIMARY KEY (`game_official_id`),
  ADD KEY `game_id` (`game_id`),
  ADD KEY `player_id` (`reference_player_id`);

--
-- Indexes for table `wr_game_player`
--
ALTER TABLE `wr_game_player`
  ADD PRIMARY KEY (`wr_game_player_id`),
  ADD KEY `game_id` (`game_id`),
  ADD KEY `team_id` (`team_id`),
  ADD KEY `player_id` (`player_id`);

--
-- Indexes for table `wr_game_team_official`
--
ALTER TABLE `wr_game_team_official`
  ADD PRIMARY KEY (`game_team_official_id`),
  ADD KEY `game_id` (`game_id`),
  ADD KEY `player_id` (`player_id`);

--
-- Indexes for table `wr_official_team`
--
ALTER TABLE `wr_official_team`
  ADD PRIMARY KEY (`official_team_id`);

--
-- Indexes for table `wr_picture_event`
--
ALTER TABLE `wr_picture_event`
  ADD PRIMARY KEY (`wr_picture_event_id`);

--
-- Indexes for table `wr_picture_player`
--
ALTER TABLE `wr_picture_player`
  ADD PRIMARY KEY (`wr_picture_player_id`),
  ADD KEY `picture_id` (`picture_id`),
  ADD KEY `player_id` (`player_id`);

--
-- Indexes for table `wr_picture_team`
--
ALTER TABLE `wr_picture_team`
  ADD PRIMARY KEY (`wr_picture_team_id`),
  ADD KEY `picture_id` (`picture_id`),
  ADD KEY `team_id` (`team_id`);

--
-- Indexes for table `wr_picture_venue`
--
ALTER TABLE `wr_picture_venue`
  ADD PRIMARY KEY (`wr_picture_venue_id`);

--
-- Indexes for table `wr_playerteam`
--
ALTER TABLE `wr_playerteam`
  ADD PRIMARY KEY (`playerteam_id`);

--
-- Indexes for table `wr_styles`
--
ALTER TABLE `wr_styles`
  ADD PRIMARY KEY (`style_id`);

--
-- Indexes for table `wr_team`
--
ALTER TABLE `wr_team`
  ADD PRIMARY KEY (`team_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `wr_videos`
--
ALTER TABLE `wr_videos`
  ADD PRIMARY KEY (`video_id_prim`);

--
-- Indexes for table `wr_weight_category`
--
ALTER TABLE `wr_weight_category`
  ADD PRIMARY KEY (`weight_category_id`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `ju_age_category`
--
ALTER TABLE `ju_age_category`
  MODIFY `age_category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ju_event`
--
ALTER TABLE `ju_event`
  MODIFY `event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `ju_event_game`
--
ALTER TABLE `ju_event_game`
  MODIFY `event_game_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=880;

--
-- AUTO_INCREMENT for table `ju_event_official`
--
ALTER TABLE `ju_event_official`
  MODIFY `event_official_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ju_event_squad`
--
ALTER TABLE `ju_event_squad`
  MODIFY `event_squad_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1030;

--
-- AUTO_INCREMENT for table `ju_event_team_squad`
--
ALTER TABLE `ju_event_team_squad`
  MODIFY `event_team_squad_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ju_game`
--
ALTER TABLE `ju_game`
  MODIFY `game_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=881;

--
-- AUTO_INCREMENT for table `ju_game_official`
--
ALTER TABLE `ju_game_official`
  MODIFY `game_official_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ju_game_player`
--
ALTER TABLE `ju_game_player`
  MODIFY `ju_game_player_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1223;

--
-- AUTO_INCREMENT for table `ju_game_team_official`
--
ALTER TABLE `ju_game_team_official`
  MODIFY `game_team_official_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ju_official_team`
--
ALTER TABLE `ju_official_team`
  MODIFY `official_team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `ju_picture_event`
--
ALTER TABLE `ju_picture_event`
  MODIFY `ju_picture_event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ju_picture_player`
--
ALTER TABLE `ju_picture_player`
  MODIFY `ju_picture_player_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=421;

--
-- AUTO_INCREMENT for table `ju_picture_team`
--
ALTER TABLE `ju_picture_team`
  MODIFY `ju_picture_team_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ju_picture_venue`
--
ALTER TABLE `ju_picture_venue`
  MODIFY `ju_picture_venue_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ju_playerteam`
--
ALTER TABLE `ju_playerteam`
  MODIFY `playerteam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=687;

--
-- AUTO_INCREMENT for table `ju_styles`
--
ALTER TABLE `ju_styles`
  MODIFY `style_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ju_team`
--
ALTER TABLE `ju_team`
  MODIFY `team_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `ju_videos`
--
ALTER TABLE `ju_videos`
  MODIFY `video_id_prim` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ju_weight_category`
--
ALTER TABLE `ju_weight_category`
  MODIFY `weight_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `player`
--
ALTER TABLE `player`
  MODIFY `player_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8524;

--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `region_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;

--
-- AUTO_INCREMENT for table `sport`
--
ALTER TABLE `sport`
  MODIFY `sport_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `sub_regions`
--
ALTER TABLE `sub_regions`
  MODIFY `sub_region_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;

--
-- AUTO_INCREMENT for table `venue`
--
ALTER TABLE `venue`
  MODIFY `venue_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=365;

--
-- AUTO_INCREMENT for table `wr_age_category`
--
ALTER TABLE `wr_age_category`
  MODIFY `age_category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `wr_event`
--
ALTER TABLE `wr_event`
  MODIFY `event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `wr_event_game`
--
ALTER TABLE `wr_event_game`
  MODIFY `event_game_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=881;

--
-- AUTO_INCREMENT for table `wr_event_official`
--
ALTER TABLE `wr_event_official`
  MODIFY `event_official_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wr_event_squad`
--
ALTER TABLE `wr_event_squad`
  MODIFY `event_squad_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1032;

--
-- AUTO_INCREMENT for table `wr_event_team_squad`
--
ALTER TABLE `wr_event_team_squad`
  MODIFY `event_team_squad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wr_game`
--
ALTER TABLE `wr_game`
  MODIFY `game_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=882;

--
-- AUTO_INCREMENT for table `wr_game_official`
--
ALTER TABLE `wr_game_official`
  MODIFY `game_official_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wr_game_player`
--
ALTER TABLE `wr_game_player`
  MODIFY `wr_game_player_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1223;

--
-- AUTO_INCREMENT for table `wr_game_team_official`
--
ALTER TABLE `wr_game_team_official`
  MODIFY `game_team_official_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wr_official_team`
--
ALTER TABLE `wr_official_team`
  MODIFY `official_team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `wr_picture_event`
--
ALTER TABLE `wr_picture_event`
  MODIFY `wr_picture_event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wr_picture_player`
--
ALTER TABLE `wr_picture_player`
  MODIFY `wr_picture_player_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=421;

--
-- AUTO_INCREMENT for table `wr_picture_team`
--
ALTER TABLE `wr_picture_team`
  MODIFY `wr_picture_team_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wr_picture_venue`
--
ALTER TABLE `wr_picture_venue`
  MODIFY `wr_picture_venue_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wr_playerteam`
--
ALTER TABLE `wr_playerteam`
  MODIFY `playerteam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=687;

--
-- AUTO_INCREMENT for table `wr_styles`
--
ALTER TABLE `wr_styles`
  MODIFY `style_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wr_team`
--
ALTER TABLE `wr_team`
  MODIFY `team_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `wr_videos`
--
ALTER TABLE `wr_videos`
  MODIFY `video_id_prim` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wr_weight_category`
--
ALTER TABLE `wr_weight_category`
  MODIFY `weight_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `zone_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ju_event_game`
--
ALTER TABLE `ju_event_game`
  ADD CONSTRAINT `ju_event_game_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `ju_event` (`event_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ju_event_game_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `ju_game` (`game_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `player`
--
ALTER TABLE `player`
  ADD CONSTRAINT `player_ibfk_1` FOREIGN KEY (`sport_id`) REFERENCES `sport` (`sport_id`);

--
-- Constraints for table `venue`
--
ALTER TABLE `venue`
  ADD CONSTRAINT `venue_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`);

--
-- Constraints for table `wr_event`
--
ALTER TABLE `wr_event`
  ADD CONSTRAINT `event_ibfk_4` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`);

--
-- Constraints for table `wr_event_game`
--
ALTER TABLE `wr_event_game`
  ADD CONSTRAINT `wr_event_game_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `wr_event` (`event_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wr_event_game_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `wr_game` (`game_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `wr_event_squad`
--
ALTER TABLE `wr_event_squad`
  ADD CONSTRAINT `wr_event_squad_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `wr_event` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `wr_event_squad_ibfk_2` FOREIGN KEY (`player_id`) REFERENCES `player` (`player_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `wr_game`
--
ALTER TABLE `wr_game`
  ADD CONSTRAINT `wr_game_ibfk_3` FOREIGN KEY (`venue_id`) REFERENCES `venue` (`venue_id`),
  ADD CONSTRAINT `wr_game_ibfk_4` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`),
  ADD CONSTRAINT `wr_game_ibfk_5` FOREIGN KEY (`player1_id`) REFERENCES `player` (`player_id`),
  ADD CONSTRAINT `wr_game_ibfk_7` FOREIGN KEY (`team1_id`) REFERENCES `wr_team` (`team_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
